class DatabaseUpdateService

  include Utils::Logger

  # db_string is a comma separate string
  # ex: foo,bar,baz
  def execute(db_string)
    return if db_string.blank?
    databases = db_string.strip.split(",")
    databases.inject([]) do |updated, database|
      database.strip!
      count = Database.where(["lower(name) = lower(?)", database]).count
      if count == 0
        logger.info("Add new database #{database}")
        Database.create(name: database)
        updated << database
      end
      updated
    end
  end
end
