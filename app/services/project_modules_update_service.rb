class ProjectModulesUpdateService
  include Utils::Logger

  def execute(project, ref)
    begin
      logger.info("[#{project.name_with_namespace}] Start to scan project.")
      if project.forked?
        logger.info("[#{project.name_with_namespace}] This project is forked project, skip to scan modules.")
        return
      end
      tree = project.tree(ref)
      if tree.present? && tree.pom.present?
        tree.pom.each do |pom_module|
          if ( (['war', 'jar', 'nodejs', 'php', 'zipfile'].include? pom_module[:type].downcase) && pom_module[:module_name].present? )
            project_module = project.project_modules.find_or_initialize_by_module_name(pom_module[:module_name])
            if project_module.new_record?
              project_module.save
              logger.info("[#{project.name_with_namespace}] New module found, #{project_module.module_name}.")
            end
          end
        end
      end
    rescue => e
      logger.error("[#{project.name_with_namespace}] Fail to update modules, cause by: #{e}.")
    end
    nil
  end
end
