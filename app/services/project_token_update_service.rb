class ProjectTokenUpdateService
  include Utils::Logger

  def execute(project)
    project.generate_token rescue nil
    logger.info("[#{project.name_with_namespace}] add private_token #{project.private_token}.")
  end
end
