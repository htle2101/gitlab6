class JarDeployWorker
  include Sidekiq::Worker

  sidekiq_options queue: :jar_deploy

  def perform(ci_branch_id, options = {})
    ci_branch = CiBranch.find(ci_branch_id)    
    result =  RemoteCi::JarDeployer.new( ci_branch,options).deploy    
    stdout = ""    
    Notify.delay.jar_deployed(ci_branch.id, options["email"], options["jenkins_job_name"], options["module_name"], result, stdout)     
    return result
  end

end
