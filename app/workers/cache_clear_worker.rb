class CacheClearWorker
  include Sidekiq::Worker

  sidekiq_options queue: :cache_clear

  def perform(jenkins_job_name)
	::RolloutBranches::CacheClearContext.new(jenkins_job_name).execute
  end

end