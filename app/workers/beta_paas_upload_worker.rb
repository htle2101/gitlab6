class BetaPaasUploadWorker
  include Sidekiq::Worker

  sidekiq_options queue: :beta_paas_upload, retry: true

  def perform(id)
    ::BuildBranches::BetaPaasUploadContext.new(id).execute
  end
end
