class PackageUploadWorker
  include Sidekiq::Worker

  sidekiq_options queue: :package_upload, retry: false

  def perform(packing_id, build_number)
    packing = RolloutPacking.find(packing_id)
    RemoteCi::PackageUploader.new(packing, build_number).upload
  end

end
