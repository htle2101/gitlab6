class TomcatRestartWorker
  include Sidekiq::Worker

  sidekiq_options queue: :tomcat_restart, retry: false

  def perform(machine_ip)
    machine = VirtualMachine.find_by_ip(machine_ip)
    conn = machine.conn
    conn.sudo_exec("/sbin/service jboss restart", :timeout => 50)
    machine.update_attributes(status: 7)
  end
end
