class PaasRolloutWorker
  include Sidekiq::Worker

  sidekiq_options queue: :paas_rollout, retry: false

  def perform(paas_rollout_id)
    paas_rollout = PaasRollout.find(paas_rollout_id)
    paas_rollout.deploy
  end

end
