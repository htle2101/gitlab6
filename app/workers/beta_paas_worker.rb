class BetaPaasWorker
  include Sidekiq::Worker

  sidekiq_options queue: :beta_paas, retry: true

  def perform(params)
    ::BuildBranches::BetaPaasContext.new(params).execute
  end
end
