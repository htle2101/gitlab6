class FayePublishWorker
  include Sidekiq::Worker

  sidekiq_options queue: :faye_publish

  def perform(channel, data)
    FayeUtils.publish(channel, data)
  end

end
