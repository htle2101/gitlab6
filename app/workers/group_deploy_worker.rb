class GroupDeployWorker
  include Sidekiq::Worker

  sidekiq_options queue: :group_deploy

  def perform(params)
    RemoteCi::GroupDeployer.new(params).deploy
  end

end
