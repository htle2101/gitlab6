class SonarStatisticsWorker < BaseWorker
  include Sidekiq::Worker

  sidekiq_options queue: :sonar_statistics

  def perform
    begin
      all_groups = Group.find(:all)
      Timeout::timeout(60) do
        all_groups.each do |group|
          group_statistics = group.sonar
          group_statistics.expire_cache if Rails.cache.fetch(group_statistics.cache_key(:average_coverage)).blank?
          group_statistics.average_coverage
          unless group_statistics.average_coverage.blank?
            group_statistics.new_line_code_average_coverage
          end
        end
      end
      true
    rescue => e
      false
    end
  end

end