class BetaPaasRolloutWorker
  include Sidekiq::Worker

  sidekiq_options queue: :beta_paas_rollout, retry: true

  def perform(id)
    ::BuildBranches::BetaPaasRolloutContext.new(id).execute
  end
end
