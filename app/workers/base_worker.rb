class BaseWorker
  def self.not_work? 
    Sidekiq::Workers.new.find {|name, work| work['queue'] == queue_name }.blank? && (Sidekiq::Queue.new(queue_name).size < 2)
  end 

  def self.queue_name
    get_sidekiq_options['queue'].to_s
  end  
end
