class ApplyDockerMachineWorker
  include Sidekiq::Worker

  sidekiq_options queue: :apply_docker_machine, retry: true

  def perform(id)
    ::PerformanceTests::ApplyDockerMachineContext.new().execute
  end
end