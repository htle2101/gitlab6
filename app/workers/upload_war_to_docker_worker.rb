class UploadWarToDockerWorker
  include Sidekiq::Worker

  sidekiq_options queue: :upload_war_to_docker, retry: true

  def perform(id)
    ::PerformanceTests::UploadWarToDockerContext.new().execute
  end
end