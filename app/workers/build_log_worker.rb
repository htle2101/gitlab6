class BuildLogWorker
  include Sidekiq::Worker

  sidekiq_options queue: :build_log, retry: false

  def perform(ci_branch_id, build_number)
    build_branch = BuildBranch.find(ci_branch_id)
    RemoteCi::BuildLog.new(build_branch, build_number).execute
  end
end
