class GroupRollbackWorker
  include Sidekiq::Worker

  sidekiq_options queue: :group_rollback

  def perform(params)
    GroupingStrategies::RollbackContext.new(params).execute
  end

end
