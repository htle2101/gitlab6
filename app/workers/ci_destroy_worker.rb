class CiDestroyWorker
  include Sidekiq::Worker

  sidekiq_options queue: :ci_destroy

  def perform(ci_branch_id)
    if BuildBranch.find(ci_branch_id).type == "CiBranch"
      ::CiBranches::DestroyContext.new(ci_branch_id).execute
    else
      ::BuildBranches::DestroyContext.new(ci_branch_id).execute
    end
    #TODO email
  end
end