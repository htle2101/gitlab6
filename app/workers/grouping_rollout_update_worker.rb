class GroupingRolloutUpdateWorker < BaseWorker
  include Sidekiq::Worker

  sidekiq_options queue: :grouping_rollout_update

  def perform
    ::GroupingStrategies::UpdateRolloutStatusContext.new.execute
  end
end