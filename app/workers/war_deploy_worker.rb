class WarDeployWorker
  include Sidekiq::Worker

  sidekiq_options queue: :war_deploy, retry: false

  def perform(ci_branch_id, options = {} )
    build_branch = BuildBranch.find(ci_branch_id)
    deployed_wars = RemoteCi::WarDeployer.new(build_branch, options).deploy
    # Notify.delay.war_deployed(build_branch.id, deployed_wars) if !build_branch.ci_env.is_cmdb && !deployed_wars.blank?
  end
end
