class MachineUnbindWorker
  include Sidekiq::Worker

  sidekiq_options queue: :machine_unbind, retry: 3

  def perform(build_branch_id, machine_ip, release = false)
  	::VirtualMachines::UnbindContext.new(build_branch_id, machine_ip).execute
    machine = VirtualMachine.find_by_ip(machine_ip)
    machine.release if machine && release
    Logbin.logger("release_machine").info("#{machine.ip} released.", machine.log_params)
  end
end