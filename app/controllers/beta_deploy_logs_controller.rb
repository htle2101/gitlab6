class BetaDeployLogsController < ApplicationController
  before_filter :verify_log_date, only: [:vote, :unvote]

  def index
    @logs = BetaDeployLog.
      where(id: BetaDeployLog.
                  select('max(id)').
                  where(date: Date.today).
                  group('module_name')).order('id desc')
  end

  def vote
    if BetaDeployLogs::VoteContext.new(log, current_user, params).execute
      head :no_content
    else
      head :bad_request
    end
  end

  def unvote
    vote = BetaDeployVote.find_by_user_id_and_beta_deploy_log_id(current_user.id, log.id)

    if vote
      vote.destroy

      if vote.up?
        BetaDeployLog.decrement_counter(:voteup_count, log.id)
      else
        BetaDeployLog.decrement_counter(:votedown_count, log.id)
      end

      head :no_content
    else
      head :not_found
    end
  end

  private

  def verify_log_date
    head :unauthorized if log.date != Date.today
  end

  def log
    @log ||= BetaDeployLog.find(params[:id])
  end
end
