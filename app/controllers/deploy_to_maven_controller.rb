class DeployToMavenController < Projects::ApplicationController  
  skip_before_filter :project, :repository
  include Utils::Time
  def deploy
    ci_branch = CiBranch.find(params[:build_branch_id])

    WarDeployWorker.perform_async(ci_branch.id, 'module_name' => params[:module_name], 'build_number' => ci_branch.current_build_number)
    render :json => { result: ci_branch.jar(params[:module_name]).machine.status_message }
  end

  def pre_deploy
    ci_branch = CiBranch.find(params[:build_branch_id])    
    render :json => { result: ci_branch.jar(params[:module_name]).check }
  end
  
  def deploy_status     
    render :json => { result: VirtualMachine.find_by_build_branch_id_and_module_name(params[:build_branch_id],params[:module_name]).status_message}
  end

end

  