class Admin::CiBuildsController < Admin::ApplicationController
  def show
    begin
      #index_name = Logbin.logger("cibuild").storage_name.index_name
      index_name = "logbin_cibuild_log"
      service = Service::Search::CiBuildAnalysis.new(index_name)
      results = service.execute(params.slice("env", "gt", "lt"))
      @data = results.sort_by {|job_name, stats| (stats['FAILURE'].to_i) }.reverse
    rescue => e
      @error = "Some error occur!"
      Gitlab::GitLogger.error(e)
    end
  end
end
