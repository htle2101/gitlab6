class BuildController < ApplicationController
  def show
    @build = BuildLoadContext.new(nil, current_user, params).execute
  end
end
