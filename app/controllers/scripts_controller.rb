class ScriptsController < ApplicationController

  def show
		script = Script.find(params[:id])
		redirect_to project_script_path(script.project, script)
  end
end
