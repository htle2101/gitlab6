class GroupsController < ApplicationController
  respond_to :html
  before_filter :group, except: [:new, :create]

  # Authorize
  before_filter :authorize_read_group!, except: [:new, :create,:package_logs]
  before_filter :authorize_admin_group!, only: [:edit, :update]
  before_filter :authorize_create_group!, only: [:new, :create]
  before_filter :authorzie_view_databaseinfo_code!, only: [:databaseinfo]

  # Load group projects
  before_filter :projects, except: [:new, :create,:package_logs]
  before_filter :can_viewed_projects, only: [:show]

  layout :determine_layout

  before_filter :set_title, only: [:new, :create]

  def new
    @group = Group.new
  end

  def create
    @group = Group.new(params[:group])
    @group.path = @group.name.dup.parameterize if @group.name
    @group.owner = current_user

    if @group.save
      redirect_to @group, notice: 'Group was successfully created.'
    else
      render action: "new"
    end
  end

  def show
    @events = Event.in_projects(project_ids).limit(20).offset(params[:offset] || 0)
    @last_push = current_user.recent_push

    respond_to do |format|
      format.html
      format.js
      format.atom { render layout: false }
    end
  end

  # Get authored or assigned open merge requests
  def merge_requests
    @merge_requests = current_user.cared_merge_requests.of_group(@group)
    @merge_requests = FilterContext.new(@merge_requests, params).execute
    @merge_requests = @merge_requests.recent.page(params[:page]).per(20)
  end

  # Get only assigned issues
  def issues
    @issues = current_user.assigned_issues.of_group(@group)
    @issues = FilterContext.new(@issues, params).execute
    @issues = @issues.recent.page(params[:page]).per(20)
    @issues = @issues.includes(:author, :project)

    respond_to do |format|
      format.html
      format.atom { render layout: false }
    end
  end

  def members
    @project = group.projects.find(params[:project_id]) if params[:project_id]
    @members = group.users_groups.order('group_access DESC')
    @users_group = UsersGroup.new
  end

  def tickets
    @tickets = Tickets::GroupSearchContext.new(@group, params).execute
  end

  def package_logs
    @per_page = 10
    @logs = Logbin.search("rollout").search(params[:message]).per_page(@per_page).offset(params[:offset] || 0).order(log_at: "desc").results
    respond_to do |format|
      format.html
      format.js
    end
  end

  def rollout_logs
    @logs = nil
    respond_to do |format|
      format.html
    end
  end

  def logs
    # @logs = Logbin.search("rollout").per_page(30).where(:project_id => params[:project_id]).where(:cmdb_group_id =>params[:gs_group_id]).offset(0).order(log_at: "desc").results
    # @logs = Logbin.search("rollout").per_page(30).where(:project_id => 536).where(:cmdb_group_id =>708).offset(0).order(log_at: "desc").order(action_type: "asc").results.group_by(&:log_at)
    @logs = nil if params[:gs_group_id].blank? || params[:project_id].blank?
    @logs = Logbin.search("rollout").per_page(30).where(:project_id => params[:project_id]).where(:cmdb_group_id => params[:gs_group_id]).offset(0).order(log_at: "desc").order(action_type:"asc").results.group_by(&:log_at) rescue nil if !params[:gs_group_id].blank? && !params[:project_id].blank?
    render partial: "groups/logs", locals: {logs: @logs}
  end

  def static_logs
    @logs = nil if params[:project_id].blank?
    @logs = Logbin.search("rolloutstatic").per_page(20).where(:project_id => params[:project_id]).offset(0).order(log_at: "desc").results.group_by(&:log_at) rescue nil if !params[:project_id].blank?
    render partial: "groups/static_logs", locals: {logs: @logs}
  end

  def packages
    pms = ProjectModule.where(:project_id => params[:project_id])
    data = pms.map{|pm|{value:"#{pm.module_name}", id:"#{pm.project_id.to_s+"/"+pm.module_name}"}}
    render :json => data.uniq.to_json
  end

  def groups_for_package
    gps = GroupingStrategy.where(:appname =>params[:project_id].split("/")[1],:project_id => params[:project_id].split("/")[0])
    data = gps.map{|gp|{value:"#{gp.name}", id:"#{gp.group_id}"}}
    render :json => data.uniq.to_json
  end

  def edit
  end

  def update
    group_params = params[:group].dup
    owner_id = group_params.delete(:owner_id)

    if owner_id
      @group.owner = User.find(owner_id)
      @group.save
    end

    if @group.update_attributes(group_params)
      redirect_to @group, notice: 'Group was successfully updated.'
    else
      render action: "edit"
    end
  end

  def destroy
    @group.destroy

    redirect_to root_path, notice: 'Group was removed.'
  end

  def databaseinfo
    @blobs = Service::Search::RepoSearcher.new('', project_ids: @group.projects.pluck("id"), page: params[:page]).databaseinfo_search
  end

  protected

  def group
    if !params[:group_id].nil?
      @group ||= Group.find(params[:group_id])
    else
      @group ||= Group.find_by_path(params[:id])
    end
  end

  def projects
    @projects ||= current_user.authorized_projects.where(namespace_id: group.id).sorted_by_activity
  end

  def can_viewed_projects
    @can_viewed_projects ||= current_user.can_viewed_projects.where(namespace_id: group.id).sorted_by_activity
  end

  def project_ids
    projects.map(&:id)
  end

  # Dont allow unauthorized access to group
  def authorize_read_group!
    unless projects.present? or can?(current_user, :manage_group, @group)
      return render_404
    end
  end

  def authorize_create_group!
    unless can?(current_user, :create_group, nil)
      return render_404
    end
  end

  def authorzie_view_databaseinfo_code!
    unless can?(current_user, :view_databaseinfo_code, group)
      return render_404
    end
  end  

  def authorize_admin_group!
    unless can?(current_user, :manage_group, group)
      return render_404
    end
  end

  def set_title
    @title = 'New Group'
  end

  def determine_layout
    if [:new, :create].include?(action_name.to_sym)
      'navless'
    else
      'group'
    end
  end
end
