class Projects::TaskConfigsController < Projects::ApplicationController

  before_filter :task, only: [:new, :destroy, :edit, :update]

  def new
    @config = task.configs.build || TaskConfig.new
    render "projects/tasks/configs/new"
  end

  def create
    result = TaskConfigs::LionContext.new(project, current_user, params).set_config
    if result !~ /Lion set ok/
      render json: {status: false, flash: "Task config failed to save!Details:#{result}"}.to_json and return
    end

    @config = task.configs.build(params[:task_config])
    if @config.save
      render json: {status: true, flash: result}.to_json
    else
      render json: {status: false, flash: "Task config failed to save!"}.to_json
    end
    
  end

  def task
    @task ||= @project.tasks.find(params[:task_id])
  end

  def edit
    @config =TaskConfig.find(params[:id])
    render "projects/tasks/configs/edit"
  end

  def index
    task
    @configs=TaskConfig.find_all_by_task_id(params[:task_id])
    render "projects/tasks/configs/index"
  end  

  def update
    @config = TaskConfig.find(params[:id])
    result = TaskConfigs::LionContext.new(project, current_user, params).set_config 

    respond_to do |format|
      if result =~ /Lion set ok/ && @config.update_attributes(params[:task_config])
        format.html { redirect_to( project_task_path(@project, @task), notice: result) }
      else
        format.html { 
          flash[:notice] = ( result =~ /Lion set ok/ ) ? "Database error!" : result
          render "projects/tasks/configs/edit"
        }
      end
    end
  end

  def destroy
    config = TaskConfig.find(params[:id])    
    config.destroy
    redirect_to :back, notice: 'Config was successfully deleted.'
  end

end