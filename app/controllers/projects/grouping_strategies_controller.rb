class Projects::GroupingStrategiesController <  Projects::ApplicationController
  before_filter :ticket, :package
  before_filter :charge_group_status, only: :deploy_by_group
  before_filter :charge_ever_deploy, only: :deploy_by_group

  def reset
    group_strategy = package.grouping_strategies.find(params[:id])

    if group_strategy.can_reset? && group_strategy.reset
      Logbin.
        logger('rollout').
        info("Reset #{group_strategy} status to not deploy successfully",
             group_strategy.log_params.merge(action_type: 'reset'))

      render json: { status: true, notice: 'Reset successfully.' }
    else
      render json: { status: false, notice: 'Reset failed.' }
    end
  end

  def deploy_by_group

    gs = GroupingStrategy.find(params[:id])


    if gs.in_progress?
      render json: { status: false, notice: 'Same gs is just in_progress yet,then cant do other operations!' } and return
    end

    @action = (gs.group_id == -255 ? 'prerelease' : 'deploy')

    tip = (gs.name =='static' ? 'Deploying static package now...' : 'Deploying War now...')

    result = GroupingStrategies::GroupDeployContext.new(
      'package_id'           => params[:package_id],
      'grouping_strategy_id' => params[:id],
      'gate'                 => gs.group_id,
      'action'               => @action,
      'package_url'          => params[:package_url]
    ).execute

    notice = result[:status] ? tip : result[:notice]

    render json: { status: result[:status], notice: notice }
  end

  def charge_ever_deploy
    begin
      gs = GroupingStrategy.find(params[:id])
      gss = GroupingStrategy.where(:tag => gs.tag,:project_id => gs.project_id, 
                                   :group_id => gs.group_id,:appname => gs.appname,
                                   :hostname => gs.hostname,:name =>gs.name)
      if gss.collect{|gs|gs.not_deploy?}.include?(false)
        grs = GsRedeployStatistic.new(:tag => gs.tag, :project_id => gs.project_id, 
                                      :appname => gs.appname, :ticket_id => gs.ticket_id, 
                                      :group => gs.name, :hostname => gs.hostname,:group_id => gs.group_id,:gs_id => gs.id)
        grs.save
      end
    rescue => e
    end
  end

  def pre_rollback
    @gss, @gss_rollbacking = nil

    if params[:id].present?
      gp = GroupingStrategy.find(params[:id])
      @gss = GroupingStrategy.where(status: 11, project_id: @project.id)
      @gss.delete_if { |gs| gs.group_id == gp.group_id && gs.name == gp.name }
      @gss << gp if gp.rollbacking_or_over?
    end

    @gss_rollback_today = GroupingStrategy.where(status: 8, project_id: @project.id).where(:updated_at =>DateTime.now-1..DateTime.now)
    @gss_rollback_over  = GroupingStrategy.where(status: [1, 9], project_id: @project.id).where(:updated_at =>DateTime.now-1..DateTime.now)

    @gss_rollback_over.each do |gs|
      @gss_rollback_today << gs
    end
  end

  def terminate
    if GroupingStrategies::TerminateContext.new(params[:id], params[:package_path]).execute
      render json: { status: true, notice: 'Grouping rollout is going to be terminated. Wait please.' }
    else
      render json: { status: true, notice: "Lzm connection error, so can't kill the rollout process" }
    end
  end

  def info_to_verify
    @gs = GroupingStrategy.find(params[:id])

    @branch_error = GroupingStrategies::BranchCheckContext.new(@project, @gs).execute

    render partial: 'projects/grouping_strategies/info_to_verify'
  end

  private

  def package
    @package ||= ticket.packages.find(params[:package_id])
  end

  def ticket
    @ticket ||= project.tickets.find(params[:ticket_id])
  end

  def charge_group_status
    gs = GroupingStrategy.find(params[:id])
    gss = GroupingStrategy.where(package_id: gs.package_id, ticket_id: gs.ticket_id, project_id: gs.project_id)

    if gss.collect(&:in_progress?).include?(true)
      render json: { notice: "Can't deploy because someone is doing this!", status: false }
    end
  end
end
