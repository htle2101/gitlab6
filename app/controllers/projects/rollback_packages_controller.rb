#coding:utf-8
class Projects::RollbackPackagesController <  Projects::ApplicationController
  before_filter :charge_group_status,only: :rollback
  #before_filter :charge_exist_rollback_pacakge, only: :add_rollback_package

  def index
  	@gss,@gss_rollback_today = [],[]
     @gss_rollback_today = GroupingStrategy.where(:project_id=>project.id,:status=>8)
     @gss = GroupingStrategy.where(:status =>11,:project_id =>project.id)
     @gss_rollback_today.each do |gs_rollbacking|
        @gss.delete_if{|gs|gs.group_id == gs_rollbacking.group_id and gs.name ==gs_rollbacking.name}
     end
     @gss_rollback_over = GroupingStrategy.where("status in(1,9) and project_id = '#{project.id}'").where(:updated_at =>DateTime.now-1..DateTime.now)
     if !@gss_rollback_over.blank?
       @gss_rollback_over.each do |gs|
         @gss_rollback_today << gs
       end
     end
     @gss_rollback_today = @gss_rollback_today.delete_if{|gs|gs.blank?}
     render 'projects/grouping_strategies/pre_rollback',locals: {gss:@gss,gss_rollback_today:@gss_rollback_today}
  end

  def charge_exist_rollback_pacakge
    return if params[:swimlane] =~ /\w/
    gp_new = params[:added_id].split("|")
    gp_success = GroupingStrategy.where(:name=>gp_new[0],:appname=>gp_new[1],:status=>7,:project_id=>project.id).order("released_on desc").first
    gp_exist = GroupingStrategy.where(:name=>gp_new[0],:appname=>gp_new[1],:hostname=>gp_new[2].to_s,:swimlane =>gp_new[3]||""||nil,:group_id=>gp_new[4].to_i,:status =>11,:package_id => gp_success.package.id,:project_id =>@project.id,:tag=>gp_success.tag).first
    if !gp_exist.blank?
      ids =!params[:exists_ids].blank? ?  gp_exist.id.to_s+","+ params[:exists_ids].join(",") : gp_exist.id.to_s
      @gss = GroupingStrategy.where("id in (#{ids})")
      respond_to do |format|
      format.html{ render :partial => "projects/grouping_strategies/to_be_rollback" ,locals:{gss:@gss}}
      format.json {notice:"Can't add the same package to rollback!",status: false}
      end
    end
  end

  def groups_for_package
    exists_group_ids = []
    ids = params[:exists_ids]
    ids.delete_if{|id|GroupingStrategy.find(id).rollback_over?} if !ids.blank?
    if !ids.nil?
      !ids.each do |eid|
         exists_group_ids <<  GroupingStrategy.find(eid).group_id
      end
    end
    @gs_new = Service::Op::Cmdb.new.grouping_strategy(params[:action_id])
    @gs = @gs_new["groups"].map{|v|v if v["name"]!="全局"}.delete_if{|v|v.blank?}
    @add_ids = @gs.map{|gs|gs["id"]} - exists_group_ids
    @add_ids.delete_if do |id|
      gss = GroupingStrategy.where(:group_id=>id,:project_id=>project.id)
      @add_ids.delete(id) if !gss.collect{|gs|gs.status}.include?(7)
    end
    data  = @gs.map{|gs|{value:"#{gs['name']}",id:"#{gs['name']}|#{@gs_new['appname']}|#{gs['hostname']}|#{gs['swimlane']}|#{gs['id']}"} if @add_ids.include?(gs['id'])}.delete_if{|v|v.blank?}
    render :json => data.uniq.to_json
  end

  def swimlanes_for_package
    data = Package.where(:name=>params[:action_id],:project_id=>project.id).first.all_swimlanes.map{|s|{value:s,id:s}}
    render :json => data.uniq.to_json
  end

  def add_whole_group_package
    module_name = params[:module_name]
    gs_success = GroupingStrategy.where(:appname=>module_name,:project_id=>project.id).where("status in (1,7)").order("released_on desc").first
    @gss_exist = GroupingStrategy.where(:project_id => @project.id,:appname => params[:module_name],:group_id =>0,:status => 11,:package_id =>gs_success.package_id,:tag=>gs_success.tag,:name => "全部回滚")
    @gss = [] and params[:arr_total].split(",").map{|id|@gss << GroupingStrategy.find(id)}
    gs = GroupingStrategy.new(:project_id => @project.id,:appname => params[:module_name],:group_id =>0,:status => 11,:package_id =>gs_success.package_id,:tag=>gs_success.tag,:name => "全部回滚") and gs.save and  @gss << gs if @gss_exist.blank?
    respond_to do |format|
      format.html{ render :partial => "projects/grouping_strategies/to_be_rollback" ,locals:{gss:@gss}}
      format.json {}
    end
end

  def add_rollback_package
    ids = params[:exists_ids].blank? ? "" : params[:exists_ids].join(",")
    if params[:swimlane] =~ /\w/
      groups = Package.where(project_id:project.id,name:params[:module_name]).first.groups_of_swimlane(params[:swimlane])
      groups.each do |group|
        gp_success = GroupingStrategy.where(:name=>group['name'],:appname=>params[:module_name],:group_id =>group['id'].to_i,:project_id=>project.id).where("status in (1,7)").order("released_on desc").first
        next if gp_success.blank?
        gp = GroupingStrategy.new(:name=>group['name'],:appname=>params[:module_name],:hostname=>group['hostname'],:swimlane=>params[:swimlane],:group_id=>group['id'].to_i,:status =>11,:package_id=>gp_success.package.id,:project_id =>project.id,:tag=>gp_success.tag) and gp.save
        ids = ids.blank? ? gp.id.to_s : ids + "," + gp.id.to_s
      end
    else
      gp_new = params[:added_id].split("|")
      gp_success = GroupingStrategy.where(:name=>gp_new[0],:appname=>gp_new[1],:group_id =>gp_new[4].to_i,:project_id=>project.id).where("status in (1,7)").order("released_on desc").first
      status = GroupingStrategy.where(:name=>gp_new[0],:appname =>gp_new[1],:group_id=>gp_new[4].to_i)
      if status.collect{|s|s.status}.include?(7) || status.collect{|s|s.status}.include?(1)
        gp = GroupingStrategy.new(:name=>gp_new[0],:appname=>gp_new[1],:hostname=>gp_new[2].to_s,:swimlane =>gp_new[3]||nil,:group_id=>gp_new[4].to_i,:status =>11,:package_id => gp_success.package.id,:project_id =>@project.id,:tag=>gp_success.tag) and gp.save
        ids =!params[:exists_ids].blank? ?  gp.id.to_s+","+ params[:exists_ids].join(",") : gp.id.to_s
      else
        ids = !params[:exists_ids].blank? ? params[:exists_ids].join(",") : -255
      end
    end
    @gss = GroupingStrategy.where("id in (#{ids})")
    respond_to do |format|
      format.html{ render :partial => "projects/grouping_strategies/to_be_rollback" ,locals:{gss:@gss}}
      format.json {}
    end
  end

  def rollback_confirm
    @gs = GroupingStrategy.find(params[:id])
    rpm = RolloutPackingModule.find_by_url(params[:package_url])
    @diffs = TaskConfigs::LionConfigDiffContext.new(@project, current_user, params)
    @status = @diffs.execute
    render partial: "projects/grouping_strategies/rollback_confirm"
  end

  def delete_rollback_package
      gs = GroupingStrategy.find(params[:id])
      if gs.delete
        render json: {notice:"The rollback package has been deleted successfully!",status: true}
      end
  end

  def charge_group_status
    gs = GroupingStrategy.find(params[:id])
     if gs.in_progress?
       render json: {notice:"Can't rollback because someone is doing this!",status: false}
      end
  end

  def rollback
    gs = GroupingStrategy.find(params[:id])
    package = Package.find(params[:package_id])
    rpm = RolloutPackingModule.find_by_url(params[:package_url])
    rollback_lion = params[:rollback_lion]
    tag = rpm.packing.tag
    package.update_attributes(:rollback_to_url =>params[:package_url])
    gs_rollbackto = GroupingStrategy.where(:tag =>tag,:name=>gs.name,:group_id=>gs.group_id).where("status in (1,7)").order("released_on").last and gate = gs.group_id if gs.group_id != 0
    gs_rollbackto = GroupingStrategy.where(:tag =>tag, :appname =>gs.appname, :project_id => gs.project_id).where("status in (1,7)").order("released_on").last and gate = 0 if gs.group_id == 0
    target_params ={"package_id" =>gs.id, "app" =>package.warname || package.name, "package" =>params[:package_url],"gate"=>gate,"rollback_to"=>gs_rollbackto.id,"rollback_lion"=>rollback_lion}
    # target_params = target_params.merge("concurrent"=>gs.concurrent) if !gs.concurrent.nil? if gs.group_id !=0
    target_params = target_params.merge("immediately" => 1) if gs.group_id == 0
    tip = "Rollbacking war now..."
    result = GroupingStrategies::RollbackContext.new(target_params).execute
    notice = result[:status]? tip : result[:notice]
    render json:{status: result[:status], notice: notice}
  end

end