class Projects::CheckersController < Projects::ApplicationController
  before_filter :task, only: [:index]

  # GET /checkers
  # GET /checkers.json
  def index
    task
    @checkers = Checker.find_all_by_task_id(params[:task_id])
    render partial: "projects/tasks/checkers/index"
  end

  # GET /checkers/1
  # GET /checkers/1.json
  def show
    @checker = Checker.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @checker }
    end
  end

  # GET /checkers/new
  # GET /checkers/new.json
  def new
    @checker = Checker.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @checker }
    end
  end

  # POST /checkers
  # POST /checkers.json
  def create
    result = Checkers::CreateContext.new(project, current_user, params).execute
 
    if result
      render json: {status: true, flash: "New check item was successfully created."}.to_json
    else
      render json: {status: false, flash: "New check item failed to save!"}.to_json
    end
  end

  # PUT /checkers/1
  # PUT /checkers/1.json
  def update
    @checker = Checker.find(params[:id])

    if @checker.update_attributes(params[:checker])
      render json: {status: true, flash: "Checker was successfully updated."}.to_json
    else
      render json: {status: false, flash: "Checker failed to be updated."}.to_json
    end
  end

  # DELETE /checkers/1
  # DELETE /checkers/1.json
  def destroy
    @checker = Checker.find(params[:id])
    @checker.destroy

    respond_to do |format|
      format.html { redirect_to( project_task_path(@project, @checker.task), notice: 'Checker was successfully deleted.') }
      format.json { head :no_content }
    end
  end

  def task
    @task ||= @project.tasks.find(params[:task_id])
  end 

  def check_result
    @checker = Checker.find(params[:id])
    @checker.checked = params[:check_result]

    if @checker.save
      render json: {status: true, flash: "Check result was changed successfully."}.to_json
    else
      render json: {status: false, flash: "Check result was not changed."}.to_json
    end
    
  end
end
