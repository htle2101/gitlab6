class Projects::RolloutBranchesController < Projects::ApplicationController
  before_filter :rollout
  before_filter :packing,      except: [:index, :new, :create, :static_build, :change_branch, :destroy, :clear_cache, :clear_ip_cache, :add_package_to_clear_cache, :static_rollback, :static_shrinkwrap_build, :change_jdk_version]
  before_filter :job_exists?,  except: [:index, :item, :log, :note, :static_rollback]
  before_filter :job_running?, except: [:index, :show, :create, :item, :log, :note, :static_rollback, :change_jdk_version]

  def index
    @rollout = create_ci
    @packings = @rollout.packings.order('created_at desc').page(params[:page]).per(10)
    job_status
    
    if @rollout.present? && @rollout.static?
      begin
        Timeout::timeout(8) { @rollout.imf.jenkins_datasource.get_build_revisions }
        @appname = @rollout.appname
        @revisions = true
      rescue => e
        Gitlab::GitLogger.error(e)
        @revisions = false
      end
    end  

    respond_to do |format|
      format.html
      format.js { render partial: branch_partial }
    end
  end

  def new
    @packing = @rollout.packings.build
  end

  def show
    job_status
  end

  def item
    item = render_to_string(partial: 'item', layout: false, locals: { rollout: @rollout, packing: @packing })

    if params[:build_status]
      job_status
      build_status = render_to_string(partial: 'build_status', layout: false)
      processing = @packing.processing?
    end

    render json: { item: item, build_status: build_status, processing: processing }
  end

  def create
    success, @rollout, @packing = ::RolloutBranches::CreatePackingContext.new(@project, current_user, params).execute

    if success
      redirect_to project_rollout_branch_path(@project, @packing)
    else
      render :new
    end
  end

  def destroy
    return(render_404) if @rollout.blank? || !@rollout.can_delete?

    begin
      @rollout.ci_env.server.job.delete(@rollout.jenkins_job_name) && @rollout.destroy
    rescue => e
      Gitlab::GitLogger.error(e)
    end

    redirect_to project_rollout_branches_path(@project)
  end

  def clear_cache
    Logbin.logger('rolloutstatic').info("#{params[:app]} start clear static cache", {
      project: @project.id,
      rollout: rollout.id,
      commit: @project.repository.commit(@rollout.branch_name).id[0..9],
      user_id: current_user.id,
      app: params[:app],
      type: params[:type] })

    @status = @rollout.clear_cache(params[:apps], params[:type])

    Logbin.logger('rolloutstatic').info("#{params[:app]} finish clear static cache", {
      project: @project.id,
      rollout: rollout.id,
      commit: @project.repository.commit(@rollout.branch_name).id[0..9],
      user_id: current_user.id,
      app: params[:app],
      type: params[:type] }.merge(@status))

    render json: @status
  end

  def static_rollback
    render json: @rollout.static_rollout(params[:from], params[:to])
  end  

  def log
    data = Gitlab::Logger.read_latest_for(packing.logger_file).join("\n") rescue nil

    data = 'Log not found.' if data.blank?

    render text: data
  end

  def change_jdk_version
    if !params[:jdk_version].blank? && @rollout.change_jdk_version(params[:jdk_version])
      render json: { success: true, message: 'change version success.' }
    else
      render json: { success: false, message: 'change version fail.' }
    end
  end

  def note
    render text: packing.note.to_s
  end  

  def rebuild
    if packing.can_rebuild?
      rollout.change_jenkins_and_build(packing)
      packing.update_status(:deploying)
      packing.logger.info('Call jenkins to build, Waiting jenkins callback.')

      render json: { success: true, msg: 'The job has been added to the queue.' }
    else
      render json: { success: false, msg: 'You can not do this action.' }
    end
  end

  def add_package_to_clear_cache
    @rollout = RolloutBranch.find(params[:id])
    @module_names = params[:module_names]
    @type = 'prelease'

    clear_cache(@module_names, 'prelease')

    render json: { result: 'success' }
  end

  def change_branch
    if params[:branch].present?
      commit = @project.repository(params[:branch]).commit
      short_id = commit.short_id(12)
      link = project_commit_path(@project, commit)
      modules = @rollout.modules_without_type(params[:branch], ['pom']).collect{|m| { text: m.name_with_version, value: m.module_name} }

      render json: { success: true, commit: { short_id: short_id, link: link }, modules: modules }
    else
      render json: { success: false }
    end
  end

  def ticket
    @ticket = project.tickets.find_or_initialize_by_released_at(Date.today.to_s)
    @ticket.save if @ticket.new_record?
    @ticket.add_wars(packing.module_names_strings)

    redirect_to project_ticket_path(@project, @ticket)
  end

  def sign_off
    packing.update_attributes(:sign_off => current_user.name)
    redirect_to project_rollout_branches_path(@project)
  end

  def static_shrinkwrap_build
    unless rollout.job_running?

      rollout.change_static_jenkins_and_nopublish_build

      render json: { success: true, msg: 'clear shrinkwrap has been added to the queue.' }
    else
      render json: { success: false, msg: 'You can not do this action.' }
    end
  end

  private

  def create_ci
    ::RolloutBranches::CreateContext.new(@project, current_user).execute
  end

  def branch_partial
    'projects/rollout_branches/static_index'
  end

  def rollout
    @rollout ||= @project.rollout_branch
  end

  def packing
    @packing ||= @rollout.packings.find(params[:id])
  end

  def job_exists?
    unless rollout.job_exists?
      flash[:alert] = 'We can not find related jenkins job.'

      redirect_to project_rollout_branches_path(@project)
    end
  end

  def job_running?
    if rollout.job_running?
      flash[:alert] = 'You can not do this action because the jenkins job is running.'

      redirect_to project_rollout_branches_path(@project)
    end
  end

  def job_status
    begin
      Timeout::timeout(2) { @job_status = rollout.imf.jenkins_datasource.build_result['status'] if @rollout.present? }
    rescue => e
      Gitlab::GitLogger.error(e)
    end
  end
end
