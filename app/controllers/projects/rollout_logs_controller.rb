class Projects::RolloutLogsController < Projects::ApplicationController
  def index
    @per_page = 10 
    @page = params[:page] ? params[:page].to_i : 1

    @logs =  Logbin.
      search('rollout').
      where(project_id: @project.id).
      order(log_at: 'desc').
      per_page(@per_page).
      page(@page).
      results
  end
end
