#coding:utf-8

class Projects::PerformanceTestModulesController <  Projects::ApplicationController
  
  def index
    @performance_test_modules = PerformanceTestModule.where(:project_id => @project.id)
  end

  def show
    
  end

  def item

  end

  def add_test_sample
    @performance_test_module = params[:performance_test_module_id] == "" ? 
      PerformanceTestModule.new : PerformanceTestModule.find(params[:performance_test_module_id])
    @performance_test_module.update_attributes(:appname =>params[:appname],:package_url => params[:tag],:package_type => params[:package_type],
      :concurrent => params[:concurrent], :duration => params[:duration], :domain_name => params[:domain_name],:cpu => params[:cpu],:memory => params[:memory])
    rpm = RolloutPackingModule.find_by_url(params[:tag])
    #还要加一个给几个sample 标示为同一次test的标识（test_queue）
    @performance_test_module.update_attributes(:tag =>rpm.packing.tag,:uid => SecureRandom.uuid,:project_id =>@project.id)
    if @performance_test_module.save
      render partial: "detail_config", locals: {performance_test_module: @performance_test_module,disable: true}
    else
      render :new
    end

  end

  def edit_test_sample
    
  end

  def detail_config
    @performance_test_module = params[:performance_test_module_id].blank? ?  PerformanceTestModule.new : PerformanceTestModule.find(params[:performance_test_module_id])
    if @performance_test_module.save!
      render partial: "detail_config",locals:{ performance_test_module: @performance_test_module}
    end
  end

  def new
  	
  end

  def add_dependency_performance_test_module
    pt_main_id = params[:main_performance_test_module_id].to_i
    pt_dep = PerformanceTestModule.new
    pt_dep.save!
    @performance_test_modules = PerformanceTestModule.where("id in (#{pt_main_id},#{pt_dep.id})")
    respond_to do |format| 
      format.html{ render :partial => "form" ,locals: {performance_test_modules: @performance_test_modules}} 
      format.json {} 
    end
  end

  def tags_for_module_name
    module_options = []
  	rollout_packings = @project.rollout_branch.packings.where(:status => 7).where("module_names like '%#{params[:appname]}%'")
   	rollout_packings.each do |rp|
      rpk = rp.modules.first
    module_options << {value: "#{rp.tag}", id: "#{rpk.url}"}
    end
    render :json => module_options.uniq.to_json
  end

  def create
    

  end

end
