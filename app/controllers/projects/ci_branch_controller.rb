#coding:utf-8
class Projects::CiBranchController < Projects::ApplicationController
  include ExtractsPath

  # Authorize
  before_filter :authorize_read_project!
  before_filter :authorize_code_access!
  before_filter :require_non_empty_project

  def show
    @ci_branch = CiBranch.find_by_branch_name_and_project_id(params[:id], @project.id)
    create_or_reload_beta_paas if !@ci_branch.nil?  &&  @ci_branch.ci_env.name == "beta"

    respond_to do |format|
      format.html do
        if @ci_branch
          begin
            Timeout::timeout(5) do
              @ci_branch.imf.check
              @ci_branch.shadow_branch.imf.check unless @ci_branch.shadow_branch.blank?
              @status = true
            end
          rescue => e
            Gitlab::GitLogger.error(e)
            @status = false
          end
          render :show
        else
          new
        end
      end
    end
  end

  def create_or_reload_beta_paas
    appnames = @ci_branch.modules_with_type(@ci_branch.branch_name,'war').map{|p|p.module_name}
    commit = Gitlab::Git::Commit.last_for_path(@ci_branch.project.repository, @ci_branch.branch_name, @path).sha
    appnames.each do |appname|
      pm = ProjectModule.find_by_project_id_and_module_name(@ci_branch.project_id,appname)
      if !pm.blank? && pm.has_beta_paas == 1
        beta_paas_rollout = BetaPaasRollout.find_or_create_by_appname_and_build_branch_id_and_tag(appname,@ci_branch.id,commit)
        beta_paas_rollout.reload_imf if !beta_paas_rollout.blank?
      end
    end
  end

  def new
    @ci_branch = CiBranch.new

    render :new
  end

  def create
    @ci_branch = ::CiBranches::CreateContext.new(@project, current_user, params).execute
    @shadow_branch = (@ci_branch && @ci_branch.need_shadow? ) ? ::ShadowBranches::CreateContext.new(@project, current_user, params).execute : 'no need to create'
    @sonar_branch = (@ci_branch && @ci_branch.need_sonar? ) ? ::SonarBranches::CreateContext.new(@project, current_user, params).execute : 'no need to create'
    if @ci_branch && @shadow_branch && @sonar_branch
      redirect_to project_ci_branch_path(@project, @ci_branch.branch_name)
    else
      flash[:notice] = "Can't Create Ci Without Pom When Code Type Is Java!"
      render :new
    end
  end

  def update
    ci_branch = CiBranch.find_by_branch_name_and_project_id(params[:id], @project.id)
    ci_branch.update_attributes(auto_deploy: params[:auto_deploy])
    redirect_to project_ci_branch_path(@project,  params[:id])
  end

  def run_test
    ci_branch = CiBranch.find_by_branch_name_and_project_id(params[:id], @project.id)
    ci_branch.change_run_test(params[:run_test])
    render json: {status: true, message: "ok." }
  rescue
    render json: {status: false, message: "Some error occured."}
  end

  def destroy
    ci_branch = CiBranch.find_by_branch_name_and_project_id(params[:id], @project.id)
    render_404 and return unless ci_branch
    CiDestroyWorker.perform_async(ci_branch.id)
    $redis.sadd("ci_branch:destroy:queue", "#{ci_branch.project_id}-#{ci_branch.id}")
    redirect_to project_ci_branch_path(@project,  params[:id])
  end

  def log
    ci_branch = CiBranch.find_by_branch_name_and_project_id(params[:id], @project.id)
    render partial: 'logs', locals: { logs: ci_branch.module_last_logs(params[:module_name]) }
  end

  def beta_logs
    # @ci_branch = CiBranch.find_by_branch_name_and_project_id(params[:id], @project.id)
    # @logs = ci_branch.log_modules
    # @logs = RemoteCi::LoggerBinder.new(@ci_branch).last_log
  end

  def beta_logs_for_module
    module_name = params[:module_name]
    from_date = params[:from_date]
    end_date = params[:end_date]
    @ci_branch = CiBranch.find_by_branch_name_and_project_id(params[:id], @project.id)
    @logs = RemoteCi::LoggerBinder.new(@ci_branch).last_log
    respond_to do |format|
      format.html{logs:@logs}      
    end
  end

  def shadowlog
    ci_branch = ShadowBranch.find_by_branch_name_and_project_id(params[:id], @project.id)
    render partial: 'logs', locals: { logs: ci_branch.module_last_logs(params[:module_name]) }
  end    

  def war
    @ci_branch = @project.ci_branchs.find_by_branch_name(params[:id])
    @ci_branch.update_attributes(deployers: 'War')
    redirect_to project_ci_branch_path(@project, params[:id])
  end

  def change_branch_for_ci
    result = ::CiBranches::ChangeBranchContext.new(@project, current_user, params).execute
    if result
      redirect_to project_ci_branch_path(@project, params[:ci_branch][:branch_name])
    else
      flash[:notice] = "Can't edit Ci !"
      redirect_to edit_project_ci_branch_path(@project, params[:id])
    end

  end

  def module_deploy_config_for_phoenix
    command = params[:command]
    module_name = params[:module_name]
    ci_branch = @project.ci_branchs.find_by_branch_name(params[:id])
    modules_to_deploy = Rails.cache.read(ci_branch.id.to_s + '_self_modules_to_deploy_for_phoenix')
    if modules_to_deploy.blank? && command == "增加部署"
      Rails.cache.write(ci_branch.id.to_s + '_self_modules_to_deploy_for_phoenix', ',' + module_name + ',')
    end

    if !modules_to_deploy.blank? && command == "移除部署" && (modules_to_deploy.include? (',' + module_name + ','))
      Rails.cache.delete(ci_branch.id.to_s + '_self_modules_to_deploy_for_phoenix')
      modules_to_deploy.sub! ',' + module_name + ',' , ','
      Rails.cache.write(ci_branch.id.to_s + '_self_modules_to_deploy_for_phoenix', modules_to_deploy)
    end

    if !modules_to_deploy.blank? && command == "增加部署" && (!modules_to_deploy.include? (',' + module_name + ','))
      Rails.cache.delete(ci_branch.id.to_s + '_self_modules_to_deploy_for_phoenix')
      modules_to_deploy = modules_to_deploy + ',' + module_name + ','
      Rails.cache.write(ci_branch.id.to_s + '_self_modules_to_deploy_for_phoenix', modules_to_deploy)
    end

    redirect_to project_ci_branch_phoenix_deps_path(@project, ci_branch.branch_name)
  end

  def set_app_default_domain
    ci_branch = CiBranch.find_by_branch_name_and_project_id(params[:id], @project.id)
    Rails.cache.delete(ci_branch.id.to_s + '_app_default_domain')
    Rails.cache.write(ci_branch.id.to_s + '_app_default_domain', params[:app_default_domain])
    render json: {status: true, flash: "Default app domain has been changed."}.to_json and return
  end

  # require_dependency 'remote_ci/type_parser'

end



