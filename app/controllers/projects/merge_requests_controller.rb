#coding:utf-8
require 'gitlab/satellite/satellite'

class Projects::MergeRequestsController < Projects::ApplicationController
  before_filter :module_enabled
  before_filter :merge_request, only: [:edit, :update, :show, :commits, :diffs, :automerge, :automerge_check, :ci_status, :merge_inspect]
  before_filter :validates_merge_request, only: [:show, :diffs]
  before_filter :define_show_vars, only: [:show, :diffs]

  # Allow read any merge_request
  before_filter :authorize_read_merge_request!

  # Allow write(create) merge_request
  before_filter :authorize_write_merge_request!, only: [:new, :create]

  # Allow modify merge_request
  before_filter :authorize_modify_merge_request!, only: [:close, :reject, :edit, :update, :sort]

  def index
    @merge_requests = MergeRequestsLoadContext.new(project, current_user, params).execute
    assignee_id, milestone_id = params[:assignee_id], params[:milestone_id]
    @assignee = @project.team.find(assignee_id) if assignee_id.present? && !assignee_id.to_i.zero?
    @milestone = @project.milestones.find(milestone_id) if milestone_id.present? && !milestone_id.to_i.zero?
  end

  def show
    respond_to do |format|
      format.html
      format.js

      format.diff { render text: @merge_request.to_diff(current_user) }
      format.patch { render text: @merge_request.to_patch(current_user) }
    end
  end

  def diffs
    @commit = @merge_request.last_commit

    @comments_allowed = @reply_allowed = true
    @comments_target = {noteable_type: 'MergeRequest',
                        noteable_id: @merge_request.id}
    @line_notes = @merge_request.notes.where("line_code is not null")
  end

  def new
    @merge_request = MergeRequest.new(params[:merge_request])
    @merge_request.source_project = @project unless @merge_request.source_project
    @merge_request.target_project = @project unless @merge_request.target_project
    @target_branches = @merge_request.target_project.nil? ? [] : @merge_request.target_project.repository.branch_names
    @source_project = @merge_request.source_project
    @merge_request
  end

  def edit
    @source_project = @merge_request.source_project
    @target_project = @merge_request.target_project
    @target_branches = @merge_request.target_project.repository.branch_names
  end

  def create
    @merge_request = MergeRequest.new(params[:merge_request])
    @merge_request.author = current_user
    @target_branches ||= []
    if @merge_request.save
      # invoke webhook
      hook_data = merge_request.to_hook_data(current_user)
      merge_request.project.execute_hooks(hook_data, :merge_request_hooks)

      @merge_request.reload_code

      redirect_to [@merge_request.target_project, @merge_request], notice: 'Merge request was successfully created.'
    else
      @source_project = @merge_request.source_project
      @target_project = @merge_request.target_project
      render action: "new"
    end
  end

  def update
    if @merge_request.update_attributes(params[:merge_request].merge(author_id_of_changes: current_user.id))
      delete_merge_request_check_job(params[:merge_request]) if @merge_request.project.hooks.merge_request_hooks.exists?
      @merge_request.reload_code
      @merge_request.mark_as_unchecked
      redirect_to [@merge_request.target_project, @merge_request], notice: 'Merge request was successfully updated.'
    else
      render "edit"
    end
  end

  def delete_merge_request_check_job(info)
    if ["close","reject","accept"].include?(info[:state_event])
      job_name = @merge_request.project.namespace.name + "-" + @merge_request.project.name + "-" + @merge_request.id.to_s
      jenkins_url = @merge_request.project.hooks.merge_request_hooks.first.url.split("check").first
      url = "#{jenkins_url}delete_job/#{job_name}"
      begin
        RestClient.get(url)
      rescue RestClient::InternalServerError
      end
    end
  end

  def automerge_check
    if @merge_request.unchecked?
      @merge_request.check_if_can_be_merged
    end
    render json: {merge_status: @merge_request.merge_status_name}
  rescue Gitlab::SatelliteNotExistError
    render json: {merge_status: :no_satellite}
  rescue Grit::Git::CommandFailed
    @merge_request.target_project.recreate_satellite
    @merge_request.check_if_can_be_merged
    render json: {merge_status: @merge_request.merge_status_name}
  end

  def automerge
    return access_denied! unless allowed_to_merge?

    if @merge_request.merge_request_inspection.present?
      result = @merge_request.merge_request_inspection.status == 7 ? "success" : "error"
      if result == "error"
        flash[:alert] = "检测失败或者检测重新被打开并未结束，不允许合并!"
        return @status = false
      else
        delete_merge_request_check_job({state_event:"accept"})
      end
    end

    if @merge_request.opened? && @merge_request.can_be_merged?
      @merge_request.should_remove_source_branch = params[:should_remove_source_branch]
      @merge_request.automerge!(current_user)
      @status = true
    else
      @status = false
    end
  end

  def branch_from
    #This is always source
    @source_project = @merge_request.nil? ? @project : @merge_request.source_project
    @commit = @repository.commit(params[:ref]) if params[:ref].present?
  end

  def branch_to
    @target_project = selected_target_project
    @commit = @target_project.repository.commit(params[:ref]) if params[:ref].present?
  end

  def update_branches
    @target_project = selected_target_project
    @target_branches = @target_project.repository.branch_names
    @target_branches
  end

  def ci_status
    status = project.gitlab_ci_service.commit_status(merge_request.last_commit.sha)
    response = {status: status}

    render json: response
  end

  def info_to_verify
    @tasks = Task.where(project_id: params[:merge_request][:source_project_id],
                        branch_name: params[:merge_request][:source_branch]
                       )
    ci_branch = CiBranch.where(project_id: params[:merge_request][:target_project_id],
                               branch_name: params[:merge_request][:target_branch]
                              ).first
    need_verify = (ci_branch && ci_branch.beta?) && !@tasks.blank?
    if need_verify
      info_to_verify = render_to_string(partial: "projects/merge_requests/info_to_verify")
    end
    render json: {need_verify: need_verify, info_to_verify: info_to_verify}.to_json
  end

  def merge_inspect
    render partial: "projects/merge_requests/show/merge_request_inspection",locals: {merge_request: @merge_request}
  end

  protected

  def selected_target_project
    ((@project.id.to_s == params[:target_project_id]) || @project.forked_project_link.nil?) ? @project : @project.forked_project_link.forked_from_project
  end

  def merge_request
    @merge_request ||= @project.merge_requests.find(params[:id])
  end

  def authorize_modify_merge_request!
    return render_404 unless can?(current_user, :modify_merge_request, @merge_request)
  end

  def authorize_admin_merge_request!
    return render_404 unless can?(current_user, :admin_merge_request, @merge_request)
  end

  def module_enabled
    return render_404 unless @project.merge_requests_enabled
  end

  def validates_merge_request
    # Show git not found page if target branch doesn't exist
    return invalid_mr unless @merge_request.target_project.repository.branch_names.include?(@merge_request.target_branch)

    # Show git not found page if source branch doesn't exist
    # and there is no saved commits between source & target branch
    return invalid_mr if @merge_request.source_project.blank?
    return invalid_mr if !@merge_request.source_project.repository.branch_names.include?(@merge_request.source_branch) && @merge_request.commits.blank?
  end

  def define_show_vars
    # Build a note object for comment form
    @note = @project.notes.new(noteable: @merge_request)

    # Get commits from repository
    # or from cache if already merged
    @commits = @merge_request.commits

    @allowed_to_merge = allowed_to_merge?
    @show_merge_controls = @merge_request.opened? && @commits.any? && @allowed_to_merge

    @target_type = :merge_request
    @target_id = @merge_request.id
  end

  def allowed_to_merge?
    action = if project.protected_branch?(@merge_request.target_branch)
               :accept_merge_request_to_protected_branches
               #:push_code_to_protected_branches
             else
               :push_code
             end

    can?(current_user, action, @project) || ( @project.assignee_accept_mr && @merge_request.assignee_id == current_user.id)
  end

  def invalid_mr
    # Render special view for MR with removed source or target branch
    render 'invalid'
  end

end
