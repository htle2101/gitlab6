#coding:utf-8
class Projects::TicketsController <  Projects::ApplicationController

  before_filter :authorize_rollout_app!, only: [:show]

  def index
    @tickets = Tickets::SearchContext.new(project, current_user, params).execute
  end

  def current
    @ticket = project.tickets.find_or_initialize_by_released_at(Date.today.to_s)
    @ticket.save if @ticket.new_record?
    redirect_to project_ticket_path(@project, @ticket)
  end

  def show
    @ticket = @project.tickets.find(params[:id])
    @packages = @ticket.packages.type_is_not('static').order("created_at desc")
  end

  def wars
  	@ticket = @project.tickets.find(params[:id])
  end

  def add_wars
    @ticket = @project.tickets.find(params[:id])
    result = @ticket.add_wars(params[:modules])

    flash[:notice] = @ticket.errors.full_messages.join(' ') unless result
    redirect_to :back
  end

  def grouping_strategy
    @ticket = @project.tickets.find(params[:id])
    @package = @ticket.packages.find(params[:package_id])
    @tag = params[:after_tag] || params[:tag]
    GroupingRolloutUpdateWorker.perform_async if GroupingRolloutUpdateWorker.not_work?
    name = module_name_or_war_name(@package.id)
    @package.update_attributes(:warname => name) if name != @package.name
    @gss_arr = ::GroupingStrategies::DbOperateContext.new(name, params[:package_id], @ticket.id,@tag).execute
    @gss = @gss_arr.first
    @cmdb_info_exist = @gss_arr.last
    params_options = params
    params_options = params_options.merge(appname:name || @package.name)
    params_options = params_options.merge(tag:@tag)
    # binding.pry
    ::PaasRollouts::GetContext.new(@project,current_user,params).execute if @package.has_paas?
    @prs = ::PaasRollouts::SearchContext.new(@project,current_user,params_options).execute if @package.has_paas?    
    render partial: "grouping_strategy",locals:{gss: @gss, appname: @package.name,ticket: @ticket, package: @package , prs: @prs,cmdb_info_exist:@cmdb_info_exist}
  rescue Service::Op::CmdbInterfaceAccessError
    render partial: "grouping_strategy",locals: {gss:nil, error:"Cmdb没有机器配置信息，请先联系OPS添加...",  package: @package}
  rescue Service::Op::CmdbServerConnectError
    render partial: "grouping_strategy",locals: {gss:nil, error:"Cann't connect cmdb !",  package: @package}
  end

  def module_name_or_war_name(id)
    # binding.pry
    @package = Package.find(id)
    @gs_new = Service::Op::Cmdb.new.grouping_strategy(@package.name) rescue nil
    return @package.name if !@gs_new.blank? && @gs_new.code == 200  
    if !@package.warname.blank?
      warName = @package.warname
    else
      pom = @package.project.tree(@package.project.default_branch).pom.detect{|p|p if p[:module_name] == @package.name} if @package.packing.nil?
      pom = @package.project.tree(@package.packing.branch_name).pom.detect{|p|p if p[:module_name] == @package.name}  if !@package.packing.nil?
      warName = !pom.blank? ? pom[:warName] : nil
    end
    name = !Service::Op::Cmdb.new.grouping_strategy(warName).blank? ? warName : @package.name
  end

  def destroy
     #wait to do
     ticket = Ticket.find(params[:id])
     redirect_to :back
  end

  def logs
    logs = Logbin.search("rollout").per_page(20).offset(params[:offset] || 0).where(ticket_id: params[:id]).order(log_at: "asc").results
    render json: {count: logs.count, data: logs.map {|log| {message: log.message, log_at: log.log_at.to_time.to_s(:db)} } }
  end

  protected

  def authorize_rollout_app!
    return render_404 unless can?(current_user, :rollout_app, @project)
  end

end
