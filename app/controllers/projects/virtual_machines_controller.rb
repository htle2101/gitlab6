#coding:utf-8
# Controller for a specific Commit
#
# Not to be confused with CommitsController, plural.
class Projects::VirtualMachinesController < Projects::ApplicationController

  #skip_before_filter :project, :repository, :only => [:apply, :create, :unbind]

  def apply
    machine = VirtualMachines::ApplyContext.new(project, current_user, params).execute
    if machine
      render :json => { ip: machine.ip, status: true }
    else machine.nil?
      render :json => { ip: 'not assigned', status: false, flash: "Sorry,there are no extra machines to apply!" }
    # else
    #   render :json => { ip: 'not assigned', status: false, flash: "Apply machine failed." }
    end
  end

  def create
    machine = VirtualMachines::BindContext.new(project, current_user, params).execute

    if machine.errors.full_messages.blank?
      render :json => { ip: machine.ip, domain_name: machine.domain_name, href: domain_name_href(machine.domain_name), status: true }
    else
      render :json => { status: false, flash: machine.errors.full_messages.first }
    end
  end

  def unbind
    MachineUnbindWorker.perform_async(params[:build_branch_id], params[:machine_ip])
    machine = VirtualMachine.find_by_ip(params[:machine_ip])
    machine.update_attributes( status: 8 )
    render(:json => { status: true, flash: "Machine are going to be unbinded.Wait please." })
  end

  def index
    @machines = MachinesLoadContext.new(project, current_user, params).execute
  end

  def info
    machine = VirtualMachine.find_by_ip(params[:machine_ip]) if params[:machine_ip].present?

    if machine.blank?
      render_404
    else
      render(json: {
        status: machine.status,
        poolname: machine.pool_name,
        nodename: machine.node_name,
        ip: machine.ip,
        swimlane: machine.swimlane
      })
    end
  end

  def addslb
    machine = VirtualMachine.find_by_ip(params[:machine_ip])
    machine.slb_delete_relative_node
    machine.slb_add_node
    render(:json => { status: true, flash: "Add node to slb pool successfully." })
  rescue Service::Slb::SlbConnectError, Service::Slb::SlbAccessError => e
    render(:json => { status: false, flash: "Some error occured when add node to slb pool." })
  end

  def saveswimlane
    machine = VirtualMachine.find_by_ip(params[:machine_ip])
    instances = VirtualMachine.select{|x| x.swimlane.blank? && x.module_name == machine.module_name && !x.ip.blank? && x.deployer == 'War'}
    if instances.size > 1 || !machine.swimlane.blank?
      ::RemoteCi::Command::Swimlane.new(machine.ip, port: machine.port).set_swimlane(params[:swimlane])
      machine.update_attributes(swimlane:  params[:swimlane])
      render(:json => { status: true, flash: "设置泳道成功" })
    else 
      render(:json => { status: false, flash: "设置泳道失败: alpha中只存在一个应用实例时，不允许设置泳道！" })  
    end  
    TomcatRestartWorker.perform_async(machine.ip)
  rescue => e
    render(:json => { status: false, flash: "设置泳道失败: #{e}" })
  end

  def set_dn_for_ip
    # delete the records related to the ip in op env
    name = domain_name(params[:module_name], params[:build_branch_id])
    if ::RemoteOp::DpDns.new.get_ip(name).body =~ /\[.*\]/
      JSON.parse(::RemoteOp::DpDns.new.get_ip(name).body).each do |ip|
        ::RemoteOp::DpDns.new.delete_domain_name(name, ip)
      end
    end
    # set in op env
    ::RemoteOp::DpDns.new.set_domain_name(name, params[:machine_ip])
    # record in this system
    machine = VirtualMachine.find_by_ip(params[:machine_ip])

    VirtualMachine.transaction do
      machine.update_attributes(domain_name: name)
      # machine.slb_add_node
    end

    render(:json => { domain_name: name,
                      status: true,
                      flash: "Domain name has been set." }
          )
  rescue Service::Slb::SlbConnectError, Service::Slb::SlbAccessError => e
    render(:json => { domain_name: name,
                      status: false,
                      flash: "slb pool set error #{e.to_s}" }
          )
  end

  def edit_dn_for_ip
    # check wether the specified domain name has been used in op env
    # not set again and just return info if used
    # set the new pair if not used
    if params[:machine_ip].blank? || params[:domain_name].blank?
      render :json => { status: true, flash: "Check the ip and domain name pls."}
      return
    end

    name = params[:domain_name]
    if ::RemoteOp::DpDns.new.get_ip(name).body =~ /\[.*\]/ && ::RemoteOp::DpDns.new.get_ip(name).body !~ /#{params[:machine_ip]}/
      render  :json => { ip: JSON.parse(::RemoteOp::DpDns.new.get_ip(name).body),
                         status: false,
                         flash: "Domain name once be used with other ip addresses.Pls check it!"
      }
      return
    end

    # set in op env
    ::RemoteOp::DpDns.new.set_domain_name(name, params[:machine_ip]) if ::RemoteOp::DpDns.new.get_ip(name).body !~ /\[.*\]/
    # record in this system
    machine = VirtualMachine.find_by_ip(params[:machine_ip])

    VirtualMachine.transaction do
      # machine.slb_delete_node
      machine.update_attributes(domain_name: name)
      # machine.slb_add_node
    end

    render  :json => { domain_name: name,
                       href: domain_name_href(name),
                       status: true,
                       flash: "Domain name was set successfully."
    }
  rescue Service::Slb::SlbConnectError, Service::Slb::SlbAccessError => e
    render(:json => { domain_name: name,
                      status: false,
                      flash: "slb pool set error #{e.to_s}" }
          )
  end

  def delete_dn_for_ip
    if params[:machine_ip].blank? || params[:domain_name].blank?
      render :json => { status: false, flash: "Check the ip and domain name pls."}
      return
    end
    # delete in op env
    ::RemoteOp::DpDns.new.delete_domain_name(params[:domain_name], params[:machine_ip])
    # delete in this system
    machine = VirtualMachine.find_by_ip(params[:machine_ip])
    VirtualMachine.transaction do
      # machine.slb_delete_node
      machine.update_attributes(domain_name: nil)
    end
    render :json => { status: true, flash: "Domain name was delete successfully." }
  rescue Service::Slb::SlbConnectError, Service::Slb::SlbAccessError => e
    render(:json => {
                      status: false,
                      flash: "slb pool set error #{e.to_s}" }
          )
  end

  def domain_name(module_name, build_branch_id)
    module_name + build_branch_id.to_s + '.' + CiBranch.find(build_branch_id).jenkins_job_name.split(/-/).first + '.dp'
  end

  def domain_name_href(domain_name)
    "http://#{domain_name}:#{Settings.dp['dev_web_service_port']}"
  end

  def borrow_long_term
    machine = VirtualMachine.find_by_ip_and_project_id_and_action_type(params[:machine_ip], @project.id, 'apply')
    if machine
      machine.update_attributes(tenancy: 9)
      render :json => { status: true,
                        flash: "The machine can be used only by your project for a long time."
      }
    else
      render :json => { status: false,
                        flash: "There is no need to set tenancy for the specified machine or sth. wrong."
      }
    end
  end

  def borrow_temporary
    machine = VirtualMachine.find_by_ip_and_project_id_and_action_type(params[:machine_ip], @project.id, 'apply')
    if machine
      machine.update_attributes(tenancy: nil)
      render :json => { status: true,
                        flash: "The machine can be used by other project if it is idle for a long time."
      }
    else
      render :json => { status: false,
                        flash: "There is no need to set tenancy for the specified machine or sth. wrong."
      }
    end
  end

  def machine_for_ip
    vm = @project.machines.available.where(:action_type => ['bind', 'apply']).find_by_ip(params[:ip])
    if vm.blank?
      render json: {status: false, flash: "Machine can not be found with this ip."}
    else
      render json: {status: true, machine: {port: vm.port, username: vm.username, password: vm.password} }
    end
  end

  def release
    machine = VirtualMachine.where(:ip => params[:machine_ip], :project_id => @project.id).first
    if machine
      Logbin.logger("release_machine").info("#{machine.ip} will be released.", machine.log_params)
      machine.update_attributes( status: 8 )
      MachineUnbindWorker.perform_async(machine.build_branch_id, params[:machine_ip], true)
      render(:json => { status: true, flash: "Machine are going to be released.Wait please." })
    else
      render json: {status: false, flash: "Machine can not be found!"}
    end
  end

  def local_nginx
    @machine = VirtualMachine.find(params[:id])
    @config_file_content = ::RemoteCi::Command::NginxConf.new(VirtualMachine.find(params[:id]).ip,
                           port: VirtualMachine.find(params[:id]).port).get_server_conf
    render partial: "local_nginx"
  end

  def update_local_nginx
    result = ::RemoteCi::Command::NginxConf.new(VirtualMachine.find(params[:id]).ip,
                           port: VirtualMachine.find(params[:id]).port).update_server_conf(params[:content])
    if result
      notice = "The nginx.conf updated successfully and service restarted."
      redirect_to project_virtual_machines_path, notice: notice
    else
      error = "Some errors in nginx.conf file on #{VirtualMachine.find(params[:id]).ip} and service stopped. Pls check it."
      redirect_to project_virtual_machines_path, alert: error
    end

  end

  def jboss_log
    @partial_log = ::RemoteCi::Command::JbossLog.new(VirtualMachine.find(params[:id]).ip,
                           port: VirtualMachine.find(params[:id]).port).get_log
  rescue ::RemoteCi::Command::JbossLogError => e
    @partial_log = "读Catalina日志时出现问题，请检查该文件是否存在。 "
  rescue => e
    @partial_log = e.to_s
  ensure
    render partial: "jboss_log"
  end

  def apply_from_paas
    if VirtualMachines::ApplyFromPaasContext.
      new(@project, current_user, params).
      execute
      head :no_content
    else
      head :bad_request
    end
  end

  def get_ips_from_paas
    url = Settings.op["beta_paas_url"]

    if url
      json = DianpingPaas::Client.new(url: url).instances(params[:module_name]).body

      ips = begin
              JSON.parse(json)['data'].map { |data| data['instanceIp'] }.compact
            rescue JSON::ParserError
              []
            end
    end

    if ips.present?
      render text: ips.join(', ')
    else
      head :not_found
    end
  end

end
