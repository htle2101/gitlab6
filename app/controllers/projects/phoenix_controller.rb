class Projects::PhoenixController < Projects::ApplicationController

  def create
    ci_branch.update_attributes(deployers: 'Phoenix')
    Rails.cache.fetch(ci_branch.id.to_s + '_self_modules_to_deploy_for_phoenix') do
      self_modules = ','
      ci_branch.modules_with_type("war").each{|pom_module| self_modules = self_modules + pom_module.module_name + ','}
      self_modules
    end
    redirect_to project_ci_branch_path(@project, params[:ci_branch_id])
  end

  def ci_branches
    project = Project.find(params[:id])
    data = project.ci_branchs.map {|ci| {value: ci.jenkins_job_name, id: ci.id} }
    render :json => data.to_json
  end

  def modules
    ci_branch = CiBranch.find(params[:id])
    data = ci_branch.modules_with_type("war").map { |m| { value: m[:module_name], id: m[:module_name] } }
    render :json => data.to_json
  end

  def all_deps
    need_ref_dep = has_no_dep? && has_ref_deps?
    if need_ref_dep
      @ci_branch = ci_branch
      @all_deps = Dependency.find(filter_deps)
      all_deps_web = render_to_string(partial: "projects/ci_branch/java/deployers/phoenix_ref_deps")
    end

    render json: {need_ref_dep: need_ref_dep , all_deps: all_deps_web}.to_json
  end

  private
  def ci_branch
    @ci_branch ||= @project.ci_branchs.find_by_branch_name(params[:ci_branch_id])
  end

  def has_no_dep?
    if Dependency.where(build_branch_id: ci_branch.id).blank?
      true
    else
      false
    end
  end

  def has_ref_deps?
    if Dependency.where(build_branch_id: @project.ci_branchs.pluck('id')).size > 0
      if BuildBranch.where(id: Dependency.where(build_branch_id: @project.ci_branchs.pluck('id')).pluck('target_build_branch_id')).size > 0
        true
      else
        false
      end
    else
      false
    end
  end

  def filter_deps
    @selected_dep_ids = []
    Dependency.where(build_branch_id: @project.ci_branchs.pluck('id')).select('target_build_branch_id, module_name').uniq.each do |r|
      @selected_dep_ids.push( Dependency.where(target_build_branch_id: r.target_build_branch_id, module_name: r.module_name).first.id )
    end
    @selected_dep_ids
  end

end
