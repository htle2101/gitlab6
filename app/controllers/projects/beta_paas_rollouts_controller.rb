class Projects::BetaPaasRolloutsController <  Projects::ApplicationController
  def deploy
    beta_paas_rollout = BetaPaasRollout.find(params[:id])
    status = beta_paas_rollout.deploy(beta_paas_rollout.paas_version) ? true : false
    render :json =>{status:status,notice:"the war has been sent to beta_paas to deploy..."} 
  end

  def log
    @beta_paas_rollout = BetaPaasRollout.find(params[:beta_paas_id])
    render partial: "projects/ci_branch/java/beta_paas_rollouts",locals:{paas_rollout: @beta_paas_rollout, logs: @beta_paas_rollout.build_branch.module_beta_last_logs(@beta_paas_rollout.appname) }
  end

  def rollback
    beta_paas_rollout = BetaPaasRollout.find(params[:beta_paas_rollout_id])
    result = beta_paas_rollout.deploy(params[:paas_version])
    render json: {result:result}
  end

  def tags
    beta_paas = BetaPaasRollout.find(params[:id])
    beta_logs = BetaPaasLog.where(:module_name =>beta_paas.appname,:build_branch_id => beta_paas.build_branch_id,:status =>200).uniq_by(&:tag)
    data = beta_logs.map{|log|{value:"#{log.tag}".split(//).first(20).join+"("+"#{log.created_at.strftime("%Y-%m-%d %H:%M:%S")}"+")",id:"#{log.tag}"}}
    render :json => data.uniq.to_json
  end

  def get_status_from_paas
    beta_paas_rollout = BetaPaasRollout.find(params[:beta_paas_rollout_id])
    imf = DianpingPaas::Client.new(url: Settings.op['beta_paas_url']).rollout_status(beta_paas_rollout.appname, beta_paas_rollout.operation_id)
    if imf['data']['operationStatus'].to_i != beta_paas_rollout.status
      beta_paas_rollout.mock_paas_callback(imf)
      if imf['data']["operationStatus"].to_i != 400  && imf['data']["operationStatus"].to_i != 200
        beta_paas_rollout.status = imf['data']["operationStatus"].to_i
      elsif imf['data']["operationStatus"].to_i == 200 && beta_paas_rollout.status != 200
        beta_paas_rollout.succeed
      end
      beta_paas_rollout.save
      beta_paas_rollout.reload_log_imf(imf)
    end
    render text: beta_paas_rollout.try(:status_message)
  end
    
end
