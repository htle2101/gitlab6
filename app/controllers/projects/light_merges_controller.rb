class Projects::LightMergesController < Projects::ApplicationController
  before_filter :light_merge, only: [:edit, :update, :automerge_check, :automerge]

  def new
    @light_merge = @project.light_merges.new(params[:light_merge])
  end

  def create
    @light_merge = @project.light_merges.new(params[:light_merge])
    @light_merge.update_status('unchecked')
    # @target_branch = 'paypom'
    if @light_merge.save
      redirect_to project_ci_branch_path(@project,  @light_merge.build_branch.branch_name)
    else
      render action: "new"
    end  
  end 

  def edit
  end
  
  def update
    if @light_merge.update_attributes(params[:light_merge])
      @light_merge.update_status('unchecked') 
      @light_merge.save 
      redirect_to project_ci_branch_path(@project,  @light_merge.build_branch.branch_name)
    else
      render action: "edit"
    end
  end  

  def automerge_check
    @light_merge.check_if_can_be_merged(current_user)
    render partial: 'show', locals: { light_merge: @light_merge }
  rescue Gitlab::SatelliteNotExistError
    render_404
  end

  def automerge
    @light_merge.automerge!(current_user)
    render partial: 'show', locals: { light_merge: @light_merge }
  rescue Gitlab::SatelliteNotExistError
    render_404
  end

  protected

  def light_merge
    @light_merge ||= @project.light_merges.find(params[:id])
  end
 
end
