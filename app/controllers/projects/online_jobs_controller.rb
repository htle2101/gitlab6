#coding:utf-8
class Projects::OnlineJobsController < Projects::ApplicationController

  before_filter :online_job, only: :show

  def index
    #@online_jobs = OnlineJob.where(:project_id => @project.id,:build_branch_id => @project.rollout_branch.id).order("created_at desc")
    @online_jobs = online_jobs_filtered.page(params[:page]).per(10)
  end

  def new
    @online_job = OnlineJob.new
  end

	def tags_for_jar
		jar_name = params[:jar_name]
		data = []
    # rps = @project.rollout.packings.where("module_names like '%#{jar_name}%'")
		rps = RolloutPacking.where("module_names like '%#{jar_name}%'").order("created_at desc")
		data = rps.map{|rp|{value:"#{rp.tag}",id:"#{rp.tag}|#{rp.war_urls[jar_name]}"} if !rp.war_urls.nil?} if !rps.blank?
	  render :json => data.compact.uniq.to_json
	end

  def item
    item = render_to_string(partial: "item", :layout => false, locals: { online_job: online_job })
    if params[:build_status]
      deploy_status
      processing = @online_job.deploying?
    end
    render json: {item: item, build_status: @online_job.human_status_name, processing: processing}
  end

  def jars
    branch_name = params[:branch_name]
    if project.rollout_branch.deployers != "Zipfile" && project.rollout_branch.deployers != "CmdbZipfile"
      jar_names = project.tree(branch_name).pom.map{|pm|pm[:module_name] if pm[:type] =='jar'}.delete_if{|p|p.blank?}
    else
      jar_names = project.tree(branch_name).pom.map{|pm|pm[:module_name] if pm[:type] =='zipfile'}.delete_if{|p|p.blank?}
    end
    data = jar_names.map{|name|{value:name,id:name}}
    render :json => data.uniq.to_json
  end

	def business_groups
    url = get_taurus_url(params[:env])
    res = uri_callback(url,{appName:"all"})
    business_info = JSON.parse(res.body)
    @business_group = Rails.cache.read("hosts_and_ips") == business_info["hosts"] ? Rails.cache.read("business_group") : []
      if @business_group.blank?
        business_info["hosts"].each do |ip|
        uri = URI(Settings['cmdb_machine'])
        params = {private_ip: ip,level: 3}
        uri.query = URI.encode_www_form(params)
        res = Net::HTTP.get_response(uri)
        info = JSON.parse(res.body)
        unless info['bu'].blank?
          @business_group << ["[#{info["bu"]['bu_name']}|#{info["product"]['product_name']}]","#{info["bu"]['bu_name']}|#{info["product"]['product_name']}"]
  		  end
      end
      Rails.cache.write("hosts_and_ips",business_info["hosts"])
      Rails.cache.write("business_group",@business_group)
    end

		data = @business_group.map{|group|{value:group.first,id:group.last}}
		render :json => data.uniq.to_json
	end

  def get_taurus_url(env)
    case env
    when "product"
      url = Settings.taurus['product_url']
    when  "ppe"
      url = Settings.taurus['ppe_url']
    when "beta"
      url = Settings.taurus['beta_url']
    else
      url = Settings.taurus['alpha_url']
    end
    url

  end

  def uri_callback(url,params)
    uri = URI(url)
    uri.query = URI.encode_www_form(params)
    res = Net::HTTP.get_response(uri)
  end

	def ips_for_business
		business_name = params[:business_name]
    env = params[:env]
    url = get_taurus_url(env)
    res = uri_callback(url,{appName:"all"})
    ip_groups_for_business = []
    business_info = JSON.parse(res.body)
    if ["alpha","beta"].include?(env)
      ip_groups_for_business = business_info["hosts"]
    else
      if ip_groups_for_business.blank?
      business_info["hosts"].each do |ip|
        params = {private_ip:ip,level: 3}
        res = uri_callback(Settings['cmdb_machine'],params)
        product_info = JSON.parse(res.body)
        unless product_info['product'].blank?
          ip_groups_for_business << ip if product_info["product"]['product_name'] == business_name
        end
      end
      Rails.cache.write(business_name,ip_groups_for_business)
    end
    end
    #ip_groups_for_business = Rails.cache.read("hosts_and_ips") == business_info["hosts"] ? Rails.cache.read(business_name) : []
    data = ip_groups_for_business.map{|name|{value:name,id:name}}
    render :json=> data.uniq.to_json
	end

	def jars_test
		branch_name = params[:branch_name]
		@jar_names = project.tree(branch_name).pom.map{|pm|pm[:module_name] if pm[:type] =='jar'}.delete_if{|p|p.blank?}
		module_options = [[]]
		@jar_names.each do |name|
			rpks = RolloutPackingModule.where(:name => name ).where("url is not null")
			rpks.each do |rpk|
				module_options << [rpk.packing.tag,rpk.url]
			end
		end
		module_options
	end

  def create
    params_job = params[:online_job]
    business_info =  params_job[:business_belongs_to].split("|") if params_job[:business_belongs_to]
    tag_and_url_info = params_job[:tag].split("|")
    @online_job = OnlineJob.new(:tag => tag_and_url_info.first,
                                :jar_url => tag_and_url_info.last,
                                :machine => params_job[:machine],
                                :jar_name =>params_job[:jar_name],
                                :business_belongs_to => business_info.try(:first),
                                :business_abbreviation => business_info.try(:last),
                                :project_id => @project.id,
                                :build_branch_id => @project.rollout_branch.id,
                                :status => 1024,
                                :env => params_job[:env])
    if @online_job.save
      env = params_job[:env]
      Service::Taurus::Taurus.new({taurus_env: env}).deploy(:deployId => @online_job.id,
                                                            :ip => @online_job.machine,
                                                            :file => "http://10.1.1.81" + @online_job.jar_url,
                                                            :name => @online_job.jar_name)
      @online_job.update_attributes(status: 6)
      redirect_to project_online_job_path(@project, @online_job), notice: 'Job has been saved successfully!'
    else
      render :new
    end
  end

  def show
    deploy_status
  end

  def online_job
    @online_job ||= OnlineJob.find(params[:id])
  end

  protected

  def online_jobs_filtered
    OnlineJobs::ListContext.new(project, current_user, params).execute
  end

  def deploy_status
    @deploy_status = online_job.human_status_name
  end

  def deploying?
    online_job.human_status_name == "发布中"
  end

end