class Projects::PpeBranchesController < Projects::RolloutBranchesController
  
  def rollout
    @rollout ||= @project.ppe_branch
  end

  def create_ci
    ::PpeBranches::CreateContext.new(@project, current_user).execute
  end

  def branch_partial
    "projects/ppe_branches/static_index"
  end  
end