#coding:utf-8

class Projects::PerformanceTestsController <  Projects::ApplicationController
  
  def index
    @performance_tests = PerformanceTest.where(:project_id => @project.id)
  end

  def show
    
  end

  def new
    
  end

  def item

  end

  def add_test_sample
    rpm = RolloutPackingModule.find_by_url(params[:tag])
    if !params[:performance_test_module_id].blank?
      @performance_test_module = PerformanceTestModule.find(params[:performance_test_module_id])
      @performance_test_module.update_attributes(:appname =>params[:appname],:package_url => params[:tag],:app_type => params[:app_type],:domain_name => params[:domain_name],:cpu => params[:cpu],:memory => params[:memory])
      @performance_test =@performance_test_module.performance_test
      @performance_test.update_attributes(:concurrent => params[:concurrent], :duration => params[:duration],:uid => SecureRandom.uuid,:project_id => @project.id,:title => params[:title],:target => params[:target],:upload_file => params[:files])
    else
      @performance_test_module = PerformanceTestModule.new(:appname =>params[:appname],:package_url => params[:tag],:app_type => params[:app_type],:domain_name => params[:domain_name],:cpu => params[:cpu],:memory => params[:memory])
      @performance_test = PerformanceTest.new(:concurrent => params[:concurrent], :duration => params[:duration],:uid => SecureRandom.uuid,:project_id => @project.id,:title => params[:title],:target => params[:target],:upload_file => params[:files]) 
    end
    if @performance_test_module.save && @performance_test.save
      flash[:notice] = "保存成功!"
      @performance_test_module.update_attributes(:performance_test_id => @performance_test.id,:tag =>rpm.packing.tag)
      render partial: "detail_config", locals: {performance_test_module: @performance_test_module,performance_test: @performance_test}
    else
      render :new
    end

  end

  def edit_test_sample
    
  end

  def organize_network
    #丢到sidekiq里面,render到index页面，状态码要标示出来
    # performance_test = PerformanceTest.find(params[:performance_test_id])
    @performance_tests = PerformanceTest.where(:project_id => @project.id)
    redirect_to project_performance_tests_path(@project),locals:{performance_tests: @performance_tests}
  end

  def detail_config
    if !params[:performance_test_module_id].blank?
      @performance_test_module = PerformanceTestModule.find(params[:performance_test_module_id])
      @performance_test = @performance_test_module.performance_test
    end
    @performance_test_module = @performance_test_module.nil? ? nil : @performance_test_module
    @performance_test = @performance_test.nil? ?  nil : @performance_test
    render partial: "detail_config",locals: { performance_test_module: @performance_test_module,performance_test:@performance_test}
  end

  def add_dependency_performance_test_module
    ptm_exist = PerformanceTestModule.find(params[:performance_test_module_id])
    ptm_new = PerformanceTestModule.new(:performance_test_id => ptm_exist.performance_test_id)
    @performance_test = PerformanceTest.find(ptm_exist.performance_test_id)
    if ptm_new.save!
      @performance_test_modules = PerformanceTestModule.where(:performance_test_id => ptm_exist.performance_test_id)
      render :partial => "form" ,locals: {performance_test_modules: @performance_test_modules,performance_test: @performance_test}
    end
  end

  def tags_for_module_name
    module_options = []
    rollout_packings = @project.rollout_branch.packings.where(:status => 7).where("module_names like '%#{params[:appname]}%'")
    rollout_packings.each do |rp|
      rpk = rp.modules.first
    module_options << {value: "#{rp.tag}", id: "#{rpk.url}"}
    end
    render :json => module_options.uniq.to_json
  end

  def create
    # @performance_test_module = PerformanceTestModule.find(params[:performance_test_module_id])
    # @performance_test_modules = PerformanceTestModule.where(:performance_test_id => @performance_test_module.performance_test_id)
    # machines = @performance_test_modules.map{|ptm|{id:ptm.id,processor_size:ptm.cpu,processor_occupy_mode:"private",memory_size:"4"}}

    # ApplyDockerMachineWorker.perform_async(machines)
    @performance_tests = PerformanceTest.where(:project_id => @project.id)
    redirect_to project_performance_tests_path(@project)
    flash[:notice] = "Apply to test successfully"
  end

end
