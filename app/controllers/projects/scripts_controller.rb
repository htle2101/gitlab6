class Projects::ScriptsController < Projects::ApplicationController

  before_filter :script, :except => [:index, :new, :create, :execute]
  before_filter :can_edit?, :only => [:edit, :update]
  before_filter :can_close?, :only => [:close]

  def index
    @scripts = Scripts::SearchContext.new(project, current_user, params).execute
  end

  def new
    @script = @project.scripts.build
  end

  def create
    @script = Scripts::CreateContext.new(@project, current_user, params).execute
    if @script.errors.any?
      render :new
    else
      redirect_to project_script_path(@project, @script), notice: 'Script upload task has been created successfully!'
    end
  end

  def show
  end

  def edit
  end

  def update
    @script = Scripts::UpdateContext.new(@project, current_user, params).execute
    if @script.errors.any?
      render :edit
    else
      redirect_to project_script_path(@project, @script), notice: 'Script upload task has been updated successfully!'
    end
  end

  def close
    success = Scripts::CloseContext.new(@project, current_user, params).execute
    msg = success ? "Script upload task has been closed successfully!" : "Script upload task can not be closed!"
    if request.xhr?
      render json: {status: success, msg: msg, script_status: render_to_string(partial: "status", locals: {script: script}) }
    else
      redirect_to project_script_path(@project, script), notice: msg
    end
  end

  def execute
    success = Scripts::ExecuteContext.new(@project, current_user, params).execute
    if success.is_a?(String)
      msg = success
    else
      msg = success ? "Execute command has been send to dba Successfully." : "Fail to send execute command to dba."
    end
    if request.xhr?
      render json: {status: success, msg: msg, script_status: render_to_string(partial: "status", locals: {script: script}) }
    else
      redirect_to project_script_path(@project, script), :notice => msg 
    end
  end

  def script
    @script ||= @project.scripts.find(params[:id])
  end

  def can_edit?
    unless script.can_edit?
      redirect_to project_script_path(@project, script), notice: "You can not edit this script upload task now."
    end
  end

  def can_close?
    unless script.can_close?
      redirect_to project_script_path(@project, script), notice: "You can not close this script upload task now."
    end
  end
end
