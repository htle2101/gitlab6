class Projects::DepsController < Projects::ApplicationController
  include ExtractsPath

  before_filter :ci_branch

  def index
    @dep = ci_branch.deps.build
  end

  def create
    @dep = ci_branch.deps.build(params[:dependency])
    if @dep.save
      render json: {success: true, message: render_to_string(partial: 'projects/deps/dep', locals: {project: @project, dep: @dep, ci_branch: ci_branch})}
    else
      render json: {success: false, message: render_to_string(partial: 'projects/deps/error')}
    end
  end

  def destroy
    @dep = ci_branch.deps.find(params[:id])
    if @dep.destroy
      render json: {success: true}
    else
      render json: {success: false}
    end
  end

  def bat_create
    params[:selectDeps].split(/,/).each do |dep_id|
      copy(dep_id.to_i)
    end
    redirect_to project_ci_branch_phoenix_deps_path(@project, @ci_branch.branch_name)
  end

  private
  def ci_branch
    @ci_branch ||= @project.ci_branchs.find_by_branch_name(params[:ci_branch_id])
  end

  def copy(dep_id)
    begin
      dep = Dependency.find(dep_id)
      ci_branch.deps.build({"target_build_branch_id"=> dep.target_build_branch_id, "module_name"=> dep.module_name}).save
      true
    rescue
      false
    end
  end

end
