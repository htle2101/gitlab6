class Projects::PaasRolloutsController <  Projects::ApplicationController
  def deploy
    @paas_rollout = PaasRollout.find(params[:id])
    @packing = @paas_rollout.project.rollout_branch.packings.find_by_tag(@paas_rollout.tag) 
    @package = @paas_rollout.package
    @rpk = @packing.modules.find_by_name(@package.name)
    @package.update_attributes(:tag =>@rpk.packing.tag,:url =>@rpk.url,:packing_module_id =>@rpk.id,:packing_id =>@rpk.packing.id)
    @paas_rollout.waited if @paas_rollout.can_waited?
    PaasRolloutWorker.perform_async(@paas_rollout.id)
    render json: { notice: "go to deploy.", status: true }
  end

  def log
    @paas_rollout = PaasRollout.find(params[:id])
    @paas_rollout.paas_log
    render partial: "log"
  end

  def reset
    # binding.pry
    paas_rollout = PaasRollout.find(params[:id])
    if paas_rollout.reset
      render json: {status: true, notice: "Reset successfully."}
    else
      render json: {status: false, notice: "Reset failed."}
    end
  end
end
  