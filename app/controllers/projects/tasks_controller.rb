class Projects::TasksController < Projects::ApplicationController

  def index
    @tasks = Tasks::SearchContext.new(project, current_user, params).execute
    @assignee = @project.team.find(params[:assignee_id]) if params[:assignee_id].present?
  end

  def show
    @task = @project.tasks.find(params[:id])
  end

  def new
    @task = @project.tasks.build
  end

  def create
    @task = ::Tasks::CreateContext.new(@project, current_user, params).execute
    if @task.errors.any?
      render :new
    else
      redirect_to project_task_path(@project, @task), notice: 'Dev task has been created successfully!'
    end
  end

  def edit
    @task = @project.tasks.find(params[:id])
    @config = @task.configs.build
  end

  def update
    @task = ::Tasks::UpdateContext.new(@project, current_user, params).execute
    if @task.errors.any?
      render :edit
    else
      redirect_to project_task_path(@project, @task), notice: 'Dev task has been updated successfully!'
    end
  end

  def update_status
    @task = @project.tasks.find(params[:id])
    transition = @task.status_transitions.find {|t| t.event == params[:to].to_sym }
    if transition && @task.send(transition.event)
      redirect_to project_task_path(@project, @task), notice: 'Dev task has been updated successfully!'
    else
      redirect_to project_task_path(@project, @task), notice: 'Dev task updated failed!'
    end
  end

  def remove_relation
    id = params[:id]
    @task =Task.find(params[:id])    
    ticket_id  = @project.tickets.where(:released_at =>@task.released_at).first.id
    @modules = @task.packages.map{|p|p.project_id.to_s+"|"+p.name}
    params= {:task =>{:released_at=> nil},:id=>id,:modules =>@modules}
    @task = ::Tasks::UpdateContext.new(@project, current_user, params).execute
    begin
      redirect_to :controller =>"tickets", :action =>"show",:id=>ticket_id if !Ticket.find(ticket_id).nil?
      flash[:notice] = "The relation between task and ticket has been removed successfully!"
    rescue Exception => e
      redirect_to :controller =>"tickets", :action =>"index"
      flash[:notice] = "The relation between task and ticket has been removed successfully! The ticket has also been deleted!"
    end
  end

  def destroy
    @task = ::Tasks::DestroyContext.new(@project, current_user, params).execute
    if @task.destroyed?
      redirect_to project_tasks_path, notice: "The task has been removed!"
    else
      redirect_to :back
    end
  end
end
