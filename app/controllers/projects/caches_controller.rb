class Projects::CachesController < Projects::ApplicationController
  def show
    @rollout = @project.rollout_branch
  end

  def clear
    @rollout = @project.rollout_branch
    @status = @rollout.clear_ip_cache(params[:ip], params[:type])
    render :json => @status
  end  
end