class Projects::StaticTicketsController <  Projects::ApplicationController

  def index
    @tickets = ::StaticTickets::SearchContext.new(project, current_user, params).execute
  end

  def create
    @ticket =  ::StaticTickets::CreateContext.new(@project, current_user, params).execute
    if @ticket.errors.any?
      redirect_to project_static_tickets_path(@project), notice: "There're errors during creating static ticket."
    else
      redirect_to project_static_ticket_path(@project, @ticket), notice: 'Static ticket has been created successfully!'
    end
  end

  def show
    @ticket = @project.tickets.find(params[:id])
    @packages = @ticket.packages.order("created_at desc")
  end

  def packages
    @ticket = @project.tickets.find(params[:id])
  end

  def add_packages
    @ticket = @project.tickets.find(params[:id])
    name = params[:module]
    url = params[:url]
    if !name.nil? && !url.blank?
      Package.transaction do
        package = @ticket.packages.where(:name=>name,:project_id=>@project.id).first
        if !package.nil?
          redirect_to :back, notice: "The selected module has been exist. Cann't be added anymore" and return
        else
          package = @ticket.packages.owner_is(:ticket).new(:name=>name, :project_id=>@project.id, :url=>url, :package_type=>'static')
          package.save
        end
      end   
    else
      redirect_to :back, notice: "You haven't provided package or URL to add!" and return
    end
    redirect_to project_static_ticket_path(@project, @ticket), notice: 'Static package has been created successfully!'
  end

  def grouping_strategy
    @ticket = @project.tickets.find(params[:id])
    @package = @ticket.packages.find(params[:package_id])
    if GroupingStrategy.find_by_package_id(params[:package_id]).blank?
      gs = GroupingStrategy.new(:appname=>@package.name, :name =>'static', :package=>@package)
      gs.save
      @gss = [gs]
    else
      @gss = [GroupingStrategy.find_by_package_id(params[:package_id])]
    end
    render partial: "grouping_strategy",locals:{gss: @gss, appname: @package.name, ticket: @ticket, package: @package}
  end


  def update_static_package
    @ticket = @project.tickets.find(params[:id])
    @package = @ticket.packages.find(params[:package_id])
    if @package.blank?
      render :json => { status: false, flash: "The static package doesn't exist." }
    else
      @package.update_attributes( url: params[:url] )
      render :json => { url: params[:url], status: true, flash: "The static package was updated successfully." }
    end
  end


end
