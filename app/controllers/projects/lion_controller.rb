#coding:utf-8
class Projects::LionController < Projects::ApplicationController
  include ExtractsPath

  before_filter :ci_branch

  def show
    Gitlab::GitLogger.info("#{current_user.name} click lion page.")
    @modules = ci_branch.modules.map { |m| m[:module_name] } rescue []
    @teams = JSON(Service::Lion::Request.new.teams)['result']
  rescue Service::Lion::LionServerConnectError, Service::Lion::LionServerRequestError
    @teams = []
  end

  def products
    @products = JSON(Service::Lion::Request.new.products(params[:id]))['result'].map { |m| { value: m, id: m } }
    render json: @products.to_json
  rescue Service::Lion::LionServerConnectError, Service::Lion::LionServerRequestError => e
    render json: [{ value: 'lion请求错误', id: 1 }].to_json
  end

  def new_project
    Gitlab::GitLogger.info("#{current_user.name} create lion project.")
    Service::Lion::Request.new.create(params[:product], params[:project], project.owner.username)
    render json: { status: true, message: "create lion project success." }
  rescue Service::Lion::LionServerConnectError, Service::Lion::LionServerRequestError => e
    render json: { status: false, message: e.message }
  end  
  
  def ci_branch
    @ci_branch ||= @project.ci_branchs.find_by_branch_name(params[:ci_branch_id])
  end  
end



