class ClassificationsController < ApplicationController
  def index
    @classifications = Classification.all
    
    if params[:classification_id].present?
      @classification = Classification.find(params[:classification_id])
      group_ids = @classification.groups.map(&:id)
    end  
    if params[:group_id].present?
      @group = Group.find(params[:group_id]) 
      group_ids = params[:group_id]
    end
    
    group_ids ||= Group.pluck('id')  
   
    @issues = ::Issues::SearchContext.new(group_ids, params).execute
  end
end