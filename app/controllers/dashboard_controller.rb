#coding:utf-8
#require "sane_timeout"

class DashboardController < ApplicationController
  respond_to :html

  before_filter :load_projects, except: [:projects]
  before_filter :event_filter, only: :show

  def show
    @groups = current_user.authorized_groups.sort_by(&:human_name)
    @has_authorized_projects = @projects.count > 0
    @projects_count = @projects.count
    @projects = @projects.limit(20)

    @events = Event.in_projects(current_user.authorized_projects.pluck(:id))
    @events = @event_filter.apply_filter(@events)
    @events = @events.limit(20).offset(params[:offset] || 0)

    @last_push = current_user.recent_push

    respond_to do |format|
      format.html
      format.js
      format.atom { render layout: false }
    end
  end

  def projects
    @projects = case params[:scope]
                when 'personal' then
                  current_user.namespace.projects
                when 'joined' then
                  current_user.authorized_projects.joined(current_user)
                when 'owned' then
                  current_user.owned_projects
                else
                  current_user.authorized_projects
                end.sorted_by_activity

    @labels = current_user.authorized_projects.tags_on(:labels)

    @projects = @projects.tagged_with(params[:label]) if params[:label].present?
    @projects = @projects.page(params[:page]).per(30)
  end

  # Get authored or assigned open merge requests
  def merge_requests
    @merge_requests = current_user.cared_merge_requests
    @merge_requests = FilterContext.new(@merge_requests, params).execute
    @merge_requests = @merge_requests.recent.page(params[:page]).per(20)
  end

  # Get only assigned issues
  def issues
    @issues = current_user.cared_issues
    @issues = FilterContext.new(@issues, params).execute
    @issues = @issues.recent.page(params[:page]).per(20)
    @issues = @issues.includes(:author, :project)

    respond_to do |format|
      format.html
      format.atom { render layout: false }
    end
  end

  def sonar
    @status = false
    begin
      all_groups = Group.find(:all)
      Timeout::timeout(5) do
        @groups = []
        @ave_ut_coverage = {}
        @new_ut_coverage = {}
        @new_line_count = {}
        @covered_line_count = {}
        all_groups.each do |group|
          group_statistics = group.sonar
          unless group_statistics.average_coverage.blank?
            @ave_ut_coverage[group.id] = group_statistics.average_coverage[:average_coverage] rescue nil
            @new_ut_coverage[group.id] = group_statistics.new_line_code_average_coverage[:new_line_code_average_coverage]  rescue nil
            @new_line_count[group.id] = group_statistics.new_line_code_average_coverage[:new_line_count] rescue nil
            @covered_line_count[group.id] = group_statistics.new_line_code_average_coverage[:covered_line_count] rescue nil
            @groups << group
          end
        end
      end
      @status = true unless @groups.blank?
    rescue Timeout::Error => e
      SonarStatisticsWorker.perform_async if SonarStatisticsWorker.not_work?
    ensure
      SonarStatisticsWorker.perform_async if @groups.blank? && SonarStatisticsWorker.not_work?
    end
  end

  def grouping_strategies
    @group = Group.find(params[:namespace_id]) unless params[:namespace_id].blank?
    @grouping_strategies = ::GroupingStrategies::LoadContext.new(current_user, params).execute
  end

  def checkers
    @checkers = params[:current_user] == 'true' ? current_user.cared_checkers.order("id desc") : Checker.order("id desc")
    @checkers = @checkers.page(params[:page]).per(20)
    @checkers = @checkers.where(checked: (params[:checked] == 'true' ? true : false )) unless params[:checked].blank?
    @tasks = Task.where(" released_at >= date('#{( params[:released_at] || Date.today.to_s)}')" )
    @tasks = @tasks.where(project_id: Group.find(params[:namespace_id]).projects.pluck(:id)) unless params[:namespace_id].blank?
    @checkers = @checkers.where(task_id: @tasks.pluck(:id))
  end

  def artifacts
    @artifacts = Artifact.all
  end

  def swimlane
    @swimlane_apps = VirtualMachine.select{|x| x.swimlane == params[:swimlane] && !x.ip.blank? && x.deployer == 'War'}
  end

  protected

  def load_projects
    @projects = current_user.authorized_projects.sorted_by_activity
  end

  def event_filter
    filters = cookies['event_filter'].split(',') if cookies['event_filter'].present?
    @event_filter ||= EventFilter.new(filters)
  end
end
