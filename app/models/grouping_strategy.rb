#coding:utf-8
class GroupingStrategy < ActiveRecord::Base
  attr_accessible :appname, :hostname, :name, :group_id, :swimlane, :status, :package, :ticket, :released_on,:package_id,:project_id,:tag,:url, :project_id ,:artifact_id,:started_at,:finished_at

  belongs_to :ticket
  belongs_to :package
  belongs_to :project

  scope :releasing, -> { where(:status => [2,5,8,10]) }
  scope :created_in_two_days, -> { where( "created_at > ? ", 2.days.ago ) }
  scope :has_tag, -> { where('tag is not NULL') }
  scope :is_not_paas, -> { where(" group_id != -254 ") }
  scope :is_paas, -> { where(group_id: -254)}
  scope :prerelease, -> { where(group_id: -255)}
  scope :succeed, -> { with_statuses(:rollback_success, :pre_deploy_success, :deploy_success) }
  scope :not_succeed, -> { with_statuses(:not_deploy, :pre_deploying, :pre_deploy_fail, :deploying, :deploy_fail, :rollbacking, :wait_for_rollback, :wait_for_deploy, :pre_deploy_killing)}
  scope :running, -> { with_statuses(:pre_deploying, :deploying, :rollbacking, :killing)}
  scope :tag_used, -> { where('tag is not NULL and tag != ""')}

  state_machine :status, :initial => :not_deploy do
    state :not_deploy,           :value => 1024,  :human_name => "未预发"
    state :rollback_success,     :value => 1,     :human_name => "回滚成功"
    state :pre_deploying,        :value => 2,     :human_name => "预发中"
    state :pre_deploy_fail,      :value => 3,     :human_name => "预发失败"
    state :pre_deploy_success,   :value => 4,     :human_name => "预发成功"
    state :deploying,            :value => 5,     :human_name => "发布中"
    state :deploy_fail,          :value => 6,     :human_name => "发布失败"
    state :deploy_success,       :value => 7,     :human_name => "发布成功"
    state :rollbacking,          :value => 8,     :human_name => "回滚中"
    state :rollback_fail,        :value => 9,     :human_name => "回滚失败"
    state :killing,              :value => 10,    :human_name => "中断发布"
    state :wait_for_rollback,    :value => 11,    :human_name => "待回滚"
    state :wait_for_deploy,      :value => 12,    :human_name => "待发布"
    state :pre_deploy_killing,   :value => 13,    :human_name => "中断预发"

    event :reset do
      transition all - [:not_deploy, :pre_deploying, :deploying, :rollbacking] => :not_deploy
    end

    event :kill do
      transition :deploying => :killing
    end

    event :killed do
      transition :pre_deploy_killing => :not_deploy
      transition :killing => :not_deploy
    end

    event :succeed do
      transition :pre_deploying => :pre_deploy_success
      transition :deploying => :deploy_success
      transition :rollbacking => :rollback_success
      transition :killing => :deploy_success
    end

    event :failed do
      transition :pre_deploying => :pre_deploy_fail
      transition :deploying => :deploy_fail
    end

    event :rollback_succeed do
      transition :rollbacking => :rollback_success
    end

    event :rollback_failed do
      transition :rollbacking => :rollback_fail
    end

    after_transition any => all - [:wait_for_rollback, :not_deploy, :wait_for_deploy] do |gs, transition|
      gs.faye_publish
    end

    before_transition any => [:deploy_success, :pre_deploy_success] do |gs, transition|
      gs.released_on = Time.now
      gs.update_attributes(finished_at:Time.now) if gs.finished_at.nil?

      Service::AutoTest::Request.new.call(gs.project_name_for_auto_test) rescue nil
      # status = gs.group_id != -255 ? "deploy_success" : "pre_deploy_success"
      # update_gs_static_finished_at_time and compute_deploy_interval
    end

    before_transition any => [:rollback_success, :rollback_fail] do |gs, transition|
      gs.rollback_on = Time.now
    end

    before_transition any => any do |gs, transition|
      Logbin.logger("rollout").
        info("Update [#{gs.appname}][#{gs.name}]'status
        from '#{transition.from_name.to_s.humanize}' to '#{transition.to_name.to_s.humanize}'", gs.log_params)
    end
  end

  def project_name_for_auto_test
    self.env_name + '-' + self.project.namespace.path + '-' + self.project.path
  end

  def env_name
    self.group_id == -255 ? 'ppe' : 'product'
  end  

  def update_status_when_callback?(name,status)
    if [:pre_deploy_success,:deploy_success].include?(name)
      self.succeed if self.can_succeed?
    elsif [:rollback_success].include?(name)
      self.rollback_succeed if self.can_rollback_succeed?
    elsif [:rollback_fail].include?(name)
      self.rollback_failed if self.can_rollback_failed?
    elsif [:wait_for_rollback, :not_deploy, :wait_for_deploy].include?(name)
      self.status = status
    else
      self.status = status
      self.update_attributes(finished_at:Time.now)  if status == 6 || status == 3
      # update_gs_static_finished_at_time and compute_deploy_interval if status == 6 || status == 3
      self.faye_publish
    end
  end

  def update_gs_static_finished_at_time_test(status)
    gs_static = GsStatistic.find_by_gs_id(self.id)
    # status = status == "deploy_fail"
    gs_static.update_attributes(finished_at:Time.now,status:status) if gs_static.finished_at.blank?
  end

  def update_gs_static_finished_at_time
    gs_static = GsStatistic.find_by_gs_id(self.id)
    gs_static.update_attributes(finished_at:Time.now,status:self.status) if gs_static.finished_at.blank?
  end

  def compute_deploy_interval
    gs_static = GsStatistic.where(:tag =>self.tag, :ticket_id => self.ticket_id,:appname =>self.appname).order("started_at").first
    self_gs_static = GsStatistic.find_by_gs_id(self.id)
    time_interval = self_gs_static.finished_at.to_i - gs_static.started_at.to_i
    #compute the interval from the first group deploy
    mm, ss = time_interval.divmod(60)
    hh, mm = mm.divmod(60)
    dd, hh = hh.divmod(24)
    self_gs_static.update_attributes(time_interval: dd.to_s+"d-"+hh.to_s+"h-"+mm.to_s+"m-"+ss.to_s+"s")
  end

  def in_progress?
    self.pre_deploying? || self.deploying? || self.rollbacking? || self.killing? || self.pre_deploy_killing?
  end

  def ready_deploy?
    return true unless self.project.workflow_check_enabled
    return true if self.group_id == -255
    begin
      wfs_by_warname = Service::Op::Workflow.new.transition_status(self.package.warname, (Date.today-0).to_s)
      wfs_by_name = Service::Op::Workflow.new.transition_status(self.package.name, (Date.today-0).to_s)
    rescue Service::Op::WorkflowConnectError
      return false
    end

    if wfs_by_warname['status'].blank? && wfs_by_name['status'].blank?
      return false
    end

    if wfs_by_warname['status'] == 0 || wfs_by_name['status'] == 0
      return true
    else
      return false
    end

  end

  def display_confirm_or_not_when_deploy?
    @gss = GroupingStrategy.where(:ticket_id => self.ticket_id, :appname => self.appname, :package_id => self.package_id,:tag=> self.tag)
    @prs = PaasRollout.where(:ticket_id => self.ticket_id,:appname => self.appname,:package_id => self.package_id,:tag => self.tag)
    if self.status == 6
      if @gss.collect{|gs|gs.status == 6}.count(true) > 1 || @prs.collect{|pr|pr.status == 6}.count(true) > 1
        return true
      else
        return false
      end
    else
      if @gss.collect{|gs|gs.status == 6}.count(true) >= 1 || @prs.collect{|pr|pr.status == 6}.count(true) >= 1
        return true
      else
        return false
      end
    end

  end

  def can_deploy?
    return false unless self.project.rollout_enabled
    if self.package.owner == 2
      !self.in_progress? && !self.package.url.nil? && self.status != 7 && self.status != 4  && self.pre_deploy_success? && !self.same_package_diff_group_in_progress?
    else
      !self.in_progress? && !self.package.url.nil? && self.package.tasks_ready?  && self.status != 7 && self.status != 4  && self.pre_deploy_success? && !self.same_package_diff_group_in_progress?
    end
  end

  def same_package_diff_group_in_progress?
    gss = GroupingStrategy.where(:ticket_id=>self.ticket_id,:package_id =>self.package_id,:project_id=>self.project_id)
    paas_rollouts = PaasRollout.where(:ticket_id =>self.ticket_id,:package_id =>self.package_id,:project_id=>self.project_id)
    gss.collect{|gs|gs.in_progress?}.include?(true) || paas_rollouts.collect{|ps|ps.in_progress?}.include?(true) ? true : false
  end

  def can_rollback?
    gss = GroupingStrategy.where(:name=>self.name,:appname=>self.appname,:group_id=>self.group_id,:tag=>self.tag,:ticket_id =>self.ticket_id)
    gs_in_rollbacking = GroupingStrategy.where(:group_id=>self.group_id,:name=>self.name,:project_id=>self.project_id,:status=>8)
    !self.in_progress? && !self.package.url.nil? && gss.collect{|s|s.status}.count(7) >= 1 && [6,7].include?(self.status) && self.group_id != -255 &&self.pre_deploy_success? && gs_in_rollbacking.blank? && !self.same_package_diff_group_in_progress?
  end

  def rollbacking_or_over?
    ![1,8,9].include?(self.status)
  end

  def can_do?
    self.can_deploy?||self.can_rollback?||self.can_reset?||self.can_kill?  && self.pre_deploy_success?
  end

  def pre_deploy_success?
    gs = GroupingStrategy.where(:appname=>self.appname,:package_id=>self.package_id,:ticket_id=>self.ticket_id,:group_id=>-255,:tag=>self.tag).first
    if !gs.nil?  &&  gs.id == self.id || gs.nil?
      return true
    else
      return true if [1236,1245,1507,1505].include? gs.try(:project_id)
      gs.status == 4 ? true : false
    end
  end

  def can_reset?
     [4,7].include?(self.status)  && self.pre_deploy_success? && !self.same_package_diff_group_in_progress?
  end

  def rollback_over?
    self.status == 1 || self.status == 9
  end

  def action
    self.group_id == -255 ? 'prerelease' : 'deploy'
  end

  def log_params
    Utils.wrap_common_params({
      grouping_strategy_id: id,
      package_id: package_id,
      cmdb_group_id: group_id,
      ticket_id: ticket_id,
      project_id: ticket.try(:project).try(:id),
      project_name: ticket.try(:project).try(:name),
      project_path: ticket.try(:project).try(:path_with_namespace),
      group_id: ticket.try(:project).try(:group).try(:id),
      group_name: ticket.try(:project).try(:group).try(:name)
    })
  end

  def faye_publish
    FayePublishWorker.perform_async("/project/#{self.project.id.to_s}",
      {text: "\t应用名: #{self.appname}\n\t灰度: #{self.name} - #{self.human_status_name.to_s}",
      title: "上线通知"}) if !self.in_progress?
  end

  def concurrent
    app_count = Service::Op::Cmdb.new.app_ips(package.module_name_or_war_name, 'deploy').count
    gs_count = YAML.load(self.hostname || '[]').count rescue 0
    min_count = [(app_count.to_f / 3).ceil, gs_count, (app_count.to_f / 2).floor].min
    min_count = 1 if min_count == 0
    return min_count
  rescue Service::Op::CmdbServerConnectError, Service::Op::CmdbInterfaceAccessError
    return nil
  end


end
