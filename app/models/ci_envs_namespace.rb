class CiEnvsNamespace < ActiveRecord::Base
  belongs_to :ci_env
  belongs_to :namespace

  attr_accessible :ci_env, :namespace, :ci_env_id, :namespace_id, :ci_env_name
end
