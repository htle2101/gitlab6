class CiEnv < ActiveRecord::Base
  attr_accessible :is_single, :name, :password, :url, :user, :ssh_user, :ssh_password, :path, :is_cmdb, :default, :port, :version, :property

  serialize :property

  scope :with_out_product, -> { where(["name not in (?)", ['product', 'ppe']]) }
  scope :defaults, -> { where(default: true) }

  def server
    @server ||= RemoteCi::Jenkins.new(
                  server_ip: url,
                  username: user,
                  password: password,
                  server_port: 80)
  end

  class << self
    def default(name)
      defaults.find_by_name(name)
    end

    def sync_jobs(ci_env_from_ip, ci_env_to_ips, job_name)
      ci_env_from = CiEnv.find_by_url(ci_env_from_ip)
      ci_env_tos = CiEnv.where(url: ci_env_to_ips.split(','))
      return { status: false, message: 'cant find jenkins machines' } if ci_env_from.blank? || ci_env_tos.blank?
      ci_env_tos.each do |ci_env_to|
        begin
          ci_env_to.sync_job(ci_env_from, job_name)
        rescue => e
          return { status: false, message: "#{ci_env_to.url} error: #{e}" }
        end  
      end  
      return { status: true, message: 'all jenkins sync success'}
    end  

    def find_by_project_and_name(project, name)
      ci_envs_namespace = CiEnvsNamespace.where(
        namespace_id: project.namespace_id,
        ci_env_name: name).first

      if ci_envs_namespace
        ci_envs_namespace.ci_env
      else
        # If not found, use default ci env
        default(name)
      end
    end

    def connect_servers
      CiEnv.all.inject({}) do |server_list, server|
        server_list[server.name] = RemoteCi::Jenkins.new(
                                     server_ip: server.url,
                                     username: server.user,
                                     password: server.password,
                                     server_port: 80)

        server_list
      end
    end

    def servers
      @servers ||= CiEnv.connect_servers
    end

    def no_filled_envs(project)
      CiEnv.
        where(name: CiTemplate.
                    is_visable.
                    map(&:ci_env_name).
                    uniq).
        uniq_by(&:name).
        reject do |e|
          e.is_single == true && project.ci_branchs.in_env(e.name).exists?
        end
    end

    def cndb_server
      @cndb_server ||= RemoteOp::Cmdb.new(Settings.op['cmdb_url'])
    end

    def lzm_server
      @lzm_server ||= RemoteOp::Lzm.new(Settings.op['lzm_url'])
    end
  end

  def base_path
    "#{path}/jobs"
  end

  def conn
    @conn ||= RemoteCi::Connection.new(url, port: port, user: ssh_user, password: ssh_password, safe: false)
  end

  def label
    return 'qa' if name == 'beta'
    return 'alpha' if name == 'beta_shadow'
    name
  end

  def cmdb_name
    return 'alpha' if name == 'beta_shadow'
    name
  end

  def jenkins_hook_path
    "http://#{url}/gitlab/build_now"
  end

  def full_url
    "http://#{url}"
  end

  def jboss_script_path
    "#{path}/jboss"
  end

  def nginx_conf_path
    "#{path}/nginx.conf"
  end

  def workspace_path(jenkins_job_name)
    "#{path}/#{property["workspace"].gsub('#{jenkins_job_name}', jenkins_job_name)}"
  end  

  def shadow_env
    CiEnv.find_by_name("#{self.name}_shadow")
  end

  def sonar_env
    CiEnv.find(7)
  end

  def reload_host_url
    conn.execute("sed -i -e 's#<jenkinsUrl>.*#<jenkinsUrl>http://#{url}</jenkinsUrl>#' #{path}/jenkins.model.JenkinsLocationConfiguration.xml")
  end

  def sync_job(ci_env_from, job_name)
    if server.job.exists?(job_name)
      server.job.post_config(job_name, ci_env_from.server.job.get_config(job_name))
    else
      server.job.create(job_name, ci_env_from.server.job.get_config(job_name))
    end
  end  
end
