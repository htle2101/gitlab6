#coding:utf-8
class MergeRequestInspection < ActiveRecord::Base
  attr_accessible :jenkins_job_url, :merge_request_id, :stdout, :stderr, :project_id, :status, :source_branch, :target_branch

  belongs_to :merge_request

  state_machine :status, :initial => :not_deploy do
    state :not_deploy,                :value => 1024,  :human_name => "未启动"
    state :in_inspection,             :value => 1,     :human_name => "检查进行中"
    state :inspect_success,           :value => 7,     :human_name => "检查通过"
    state :inspect_fail,              :value => 6,     :human_name => "检查不通过"
   
    event :start_inspect do
      transition all => :in_inspection
    end

    event :inspect_succeed do
      transition all => :inspect_success
    end

    event :inspect_failed do
      transition all => :inspect_fail
    end

  end

  def finished?
    [6,7].include? self.status
  end

end
