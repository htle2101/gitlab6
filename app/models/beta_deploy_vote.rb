class BetaDeployVote < ActiveRecord::Base
  BetaDeployVote.inheritance_column = nil

  attr_accessible :user_id, :beta_deploy_log_id, :type

  belongs_to :user
  belongs_to :beta_deploy_log

  validates :type, inclusion: ['up', 'down']

  def up?
    type == 'up'
  end

  def down?
    type == 'down'
  end
end
