#coding:utf-8

class TaskConfig < ActiveRecord::Base  
  belongs_to :task
  attr_accessible :type, :key, :name, :swimlane, :online_op, :online_value, :offline_dev, :offline_alpha, :offline_beta, :offline_pre_release, :task_id
  self.inheritance_column = "inheritance_type"
  ENV_FIELD_NAMES = {
    :dev => :offline_dev,
    :alpha => :offline_alpha,
    :qa => :offline_beta,
    :product => :online_value,
    :prelease => :offline_pre_release
  }

  def field_for(env)
    self.read_attribute(ENV_FIELD_NAMES[env])
  end

  def status
    if self.task.status_name == :beta
      "beta已生效"
    elsif self.task.status_name == :release_ready
      "允许上线"
    else
      "开发环境已生效"
    end
  end

  def take_effect_for_beta
    begin
      params = {
        "id" => 2,
        "p" => self.name,
        "e" => "qa",
        "k" => self.key,
        "f" => "n/a",
        "pu" => 1
      }
      Service::Lion::Request.new.take_effect(params)
    rescue => e
      Gitlab::GitLogger.error(e)
    end
  end
  
end
