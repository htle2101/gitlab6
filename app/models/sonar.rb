class Sonar

  def initialize(full_name, args = {})
    @full_name = full_name
    @args = args
  end

  def dashboard_path
    "http://#{Settings.sonar['sonar_url']}/dashboard/index/#{@full_name}"
  end

  def method_missing(method, *args, &block)
    begin
      "Service::Sonar::Api::#{method.to_s.classify}".constantize
      self.class.class_eval <<-EOF, __FILE__, __LINE__ + 1
        def #{method}
          api = Service::Sonar::Api::#{method.to_s.classify}.new(@full_name, @args)
          api.execute
        end
      EOF
      send method
    rescue NameError
      super
    end
  end

end  
