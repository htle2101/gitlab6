class LightMerge < ActiveRecord::Base
  belongs_to :project
  belongs_to :build_branch
  attr_accessible :base_branch, :conflict_branches, :source_branches, :status, :build_branch_id

  serialize :conflict_branches
  serialize :source_branches

  validates :base_branch, presence: true
  validates :build_branch_id, presence: true, uniqueness: {message: "has created LightMerge yet."}
  validate :validate_target_branches
  validate :validate_source_branches

  before_save :strip_blank_branches

  STATUS_LIST = {'unchecked' => 0, 'success' => 1, 'failed' => 2}  


  def validate_target_branches
    if (source_branches.include? build_branch.branch_name) or (base_branch == build_branch.branch_name)
      errors.add :target_branch_conflict, "Target_branch can not be equal to source_branches or base_branch."
    end
  end

  def validate_source_branches
    if source_branches.reject{|b| b.blank? }.blank?
      errors.add :branch_conflict, "Source_branches can not be blank."
    end

    delete_branches = source_branches.reject{|b| b.blank? }.reject{|b| project.repository.branch_names.include?(b) }
    unless delete_branches.blank?
      errors.add :branch_conflict, "#{delete_branches.join(" ")} was deleted."
    end
  end  

  def strip_blank_branches
    #source_branches = source_branches.reject{|b| [""].include? b }
    source_branches.reject!(&:blank?)
  end  


  def check_if_can_be_merged(current_user = project.owner)
    self.valid?
    result = merge_action(current_user).can_be_merged?
  end

  def current_conflict_branches
    merge_action.conflict_branches
  end  

  def merge_action(current_user = project.owner)
    @merge_action ||= Gitlab::Satellite::LightMergeAction.new(current_user, self)
  end  

  def automerge!(current_user = project.owner)
    if merge_action(current_user).merge!
      self.conflict_branches = current_conflict_branches
      self.save
      return true
    end
  
    return false 
  end  

  def update_status(status)
    self.status = LightMerge::STATUS_LIST[status] unless LightMerge::STATUS_LIST[status].nil?
  end 
end

# p = Project.find(1)
#  u = User.find(2)
# l = LightMerge.new
# l.project = p
# l.base_branch = 'mr_0'
# l.source_branches = ['mr_10','mr_11']
# l.branch_name = 'mr_t_t'
# l.automerge!(u)    # will let mr_t_t base from mr_0 and merge from mr_10,mr_11