class BetaPaasLog < ActiveRecord::Base
  attr_accessible :beta_paas_rollout_id, :build_branch_id, :log, :module_name, :status, :tag, :commit_message
  serialize :log

  belongs_to :beta_paas_rollout
end
