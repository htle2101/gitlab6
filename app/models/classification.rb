class Classification < ActiveRecord::Base
  attr_accessible :description, :name

  has_and_belongs_to_many :groups, join_table: "classifications_namespaces", association_foreign_key: 'namespace_id'
end
