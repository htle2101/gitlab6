#coding:utf-8
class RolloutPacking < ActiveRecord::Base

  attr_accessible :build_branch_id, :branch_name, :tag, :commit, :module_names, :war_urls, :status, :author_id, :cant_rollback, :note, :sign_off
  validates_presence_of :build_branch_id, :branch_name, :commit, :tag, :module_names, :author_id
  validates_uniqueness_of :tag, scope: :build_branch_id
  validates_format_of :tag, with: /^[\w.-]+$/, message: "should use only letters, numbers, and .-_ please.", :on => :create

  belongs_to :rollout_branch, foreign_key: "build_branch_id"
  belongs_to :author, class_name: "User", foreign_key: "author_id"

  has_many :modules, class_name: "RolloutPackingModule", foreign_key: "packing_id"

  serialize :war_urls

  delegate :jenkins_job_name, to: :rollout_branch

  STATUS_LIST = {
    :uploading => 5,
    :upload_failed => 6,
    :upload_success => 7,
    :build_failed => 8
  }

  include Utils::Logger

  def status_message
    STATUS_LIST.invert[self.status]
  end

  def module_name_list
    @module_name_list ||= modules.map(&:name)
  end

  def update_status(status)
    unless STATUS_LIST[status].nil?
      self.status = STATUS_LIST[status]
      self.save
    end
  end

  def rollback_status
    cant_rollback ? '不可回滚' : '可以回滚'
  end

  def packages
    @packages ||= begin
      rollout_branch.modules.inject([]) do |packages, m|
        if module_name_list.include?(m.module_name)
          # 'nodejs'.classify => Nodej
          type = if m.type == 'nodejs'
                   'Nodejs'
                 elsif m.type == 'rar'
                    'War'
                 else
                   m.type.classify
                 end

          packages << Deployer.new(type, m.raw.merge(ci_branch: self.rollout_branch))
        end
        packages
      end
    end
  end

  def module_names_strings
    modules.blank? ? [] : modules.map{|m| "#{self.rollout_branch.project.id}|#{m.name}"}
  end

  def command
    "upload/#{self.id}/$BUILD_NUMBER"
  end

  def create_tag
    Gitlab::Satellite::ManageTagAction.new(author, self.rollout_branch.project,
                                             {tag_name: self.tag, commit_id: self.commit}).create_lightweight_tag
  end

  def can_rebuild?
    !rollout_branch.job_running? && (self.status == STATUS_LIST[:upload_failed] || self.status == STATUS_LIST[:build_failed])
  end

  def can_deploy?
    !rollout_branch.job_running? && self.status == STATUS_LIST[:upload_success] && !self.sign_off.blank?
  end

  def can_sign?
    !rollout_branch.job_running? && self.status == STATUS_LIST[:upload_success] && self.sign_off.blank?
  end  

  def processing?
    rollout_branch.job_running? || self.status == STATUS_LIST[:uploading]
  end

  def logger_file
    "ftp/#{rollout_branch.jenkins_job_name}/#{self.id}.log"
  end

  def logger
    self.logger_file = logger_file
    super
  end

end
