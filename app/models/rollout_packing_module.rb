class RolloutPackingModule < ActiveRecord::Base
  attr_accessible :name, :packing_id, :url, :status, :paas_version

  belongs_to :packing, class_name: "RolloutPacking", foreign_key: "packing_id"

  validates_presence_of :name

  STATUS_LIST = { :deleted => -1 }
end
