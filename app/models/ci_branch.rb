class CiBranch < BuildBranch
  include Deployers::Helper

  def progress_bar_value(type)
    value = 75
    if type == "Phoenix"
      vm = self.machines.where(:deployer =>"Phoenix").first
      value = 90 if !vm.blank? and [6,7].include?vm.status
    else
      deploying_vm = self.machines.where(:deployer => "War",:status => 5).first
      vms = self.machines.where(:deployer =>"War")
        value = 90 if !deploying_vm.blank? and deploying_vm.blank?
    end
    return value
  end

end
