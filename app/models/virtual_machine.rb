#coding:utf-8
require 'resolv'

class VirtualMachine < ActiveRecord::Base
  belongs_to :ci_branch, foreign_key: "build_branch_id"
  belongs_to :build_branch
  belongs_to :project
  attr_accessible :ip, :status, :module_name, :build_branch_id, :package_type, :action_type, :username, :password, :project_id, :port, :domain_name, :tenancy, :deployer, :active, :success_at, :swimlane, :sort_key
  scope :available, -> { where(module_name: nil) }
  scope :bindable, -> { where(action_type: 'bind') }
  scope :applicable, -> { where(action_type: 'apply') }
  scope :apply_or_bind, -> { where(action_type: ['bind', 'apply'])}
  scope :linkable, -> { where(action_type: 'link') }
  scope :used, -> { where("status > 0") }
  scope :with_deployer, ->(deployer_name) { where(deployer: deployer_name) }
  scope :recoverable, -> { where(tenancy: nil) }
  scope :finished, -> { where "status in (6,7)" }
  scope :master, -> { where(sort_key: 1) }
  scope :slave, -> { where(sort_key: 2) }
  scope :actived, -> { where(active: true) }

  # validates :ip, format: { :with => Resolv::IPv4::Regex, message: "The format of the specified IP address is not valid!" },
  #           if: ->(v) { v.ci_branch.present? && v.ci_branch.deployers == 'War' && v.ci_branch.ci_env.is_cmdb == false },
  #           uniqueness: true
  validates_presence_of :ip, :username, :password, :port, :if => Proc.new{ |machine| machine.action_type == 'bind' && machine.used? }
  validates_presence_of :deployer, :action_type, :if => :status?

  STATUS_LIST = {
    'to deploy' => 0,
    'not deploy' => 1,
    'master error slave success' => 2,
    'pre deploy error' => 3,
    'deploying' => 5,
    'deploy error' => 6,
    'deploy success' => 7,
    'borrow long term' => 9,
    'releasing' => 8
  }

  class << self
    def similar_type(deployer_type)
      return ['War', 'JavaService'] if ['War', 'JavaService'].include? deployer_type
      deployer_type 
    end  

    def available_machine(deployer_type = 'War', id = 0)
      #VirtualMachine.available.applicable.where("id > #{id} and project_id is null").limit(1).first
      VirtualMachine.available.applicable.actived.with_deployer(similar_type(deployer_type)).where("id > #{id} and project_id is null").limit(1).first
    end

    def applicable_machine(deployer_type = 'War')
      virtual_machine = available_machine(deployer_type)
      while virtual_machine.present? do
        break if virtual_machine.applicable?
        virtual_machine = available_machine(deployer_type,virtual_machine.id)
      end
      virtual_machine
    end

    def list_machines(module_name) 
      VirtualMachine.used.apply_or_bind.where(module_name: module_name).where("build_branch_id is NOT NULL").map{|m| m.info}.join("\n")
    end  
  end

  def master_deploy_error?
    ['deploy error', 'master error slave success'].include? status_message
  end  

  def info
    "#{self.build_branch.gitlab_path} #{self.ip}"
  end  

  def applicable?
    begin
      @conn ||= RemoteCi::Connection.new(ip, { user: username, password: password, port: port })
    rescue => e
      errors.add(:ip, "cannot connect to machine #{ip}")
      nil
    end
  end

  def conn
    begin
      @conn ||= RemoteCi::Connection.new(ip, { user: username, password: password, port: port })
    rescue
      @conn_root = RemoteCi::Connection.new(ip, { user: "root", password: "12qwaszx", port: port })
      encrypt_password = @conn_root.execute("echo '#{password}' | openssl passwd -1 -stdin").first.chomp
      @conn_root.sudo_exec("usermod -p '#{encrypt_password}' #{username}")
      @conn ||= RemoteCi::Connection.new(ip, { user: username, password: password, port: port })
    end
  end

  def slave_machine
    ci_branch.machines.slave.find_by_module_name(self.module_name)
  end  


  def update_status(status)
    self.success_at = Time.now if status == 'deploy success'
    self.status = VirtualMachine::STATUS_LIST[status] unless VirtualMachine::STATUS_LIST[status].nil?
    self.save
  end

  def status_message
    STATUS_LIST.invert[self.status]
  end

  def deploy_error?
    'deploy error' == status_message
  end

  def unbind
    self.update_attributes(module_name: nil, domain_name: nil, build_branch_id: nil, package_type: nil, status: 0, swimlane: nil)
  end

  def release
    if self.action_type == "bind"
      self.destroy
    else
      self.update_attributes(module_name: nil, build_branch_id: nil, package_type: nil, username: nil, password: nil,
                             status: 0, project_id: nil, domain_name: nil, tenancy: nil)
    end
  end

  def used?
    self.status > 0
  end

  def not_initialized?
    self.success_at.blank?
  end

  def keycopy(envs = [])
    ::RemoteCi::Command::KeyCopy.new(ip, { user: username, password: password, port: port, envs: envs }).execute
  end

  def deploy_finished?
    'deploy success' == status_message || 'deploy error' == status_message
  end

  def releaser
    "Releaser::#{deployer}".constantize.new(self)
  end

  def deployer_instance(args = {})
    "Deployers::#{deployer}".constantize.new(pom_module.raw.merge(:ci_branch => ci_branch).merge(args))
  end

  def pom_module
    build_branch.modules.find{|pm| pm.module_name == self.module_name }
  end  

  def pool_name
    "pool_#{module_name}.#{project.namespace.path.to_s}"
  end

  def node_name
    "node_#{domain_name || ip}"
  end

  def slb_member
    {"ip" => ip, "port" => "8080", "name" => node_name}
  end

  def slb_key
    "#{self.project.id}-#{module_name}-slb"
  end

  def slb_add_node
    $redis.set(slb_key, self.id)
    Service::Slb::Slb.new.add_node(pool_name, [slb_member])
  end

  def current_sbl_node?
    $redis.get(slb_key) == self.id.to_s
  end

  def slb_message
    current_sbl_node? ? '(已添加)' : ""
  end

  def slb_delete_node
    return if project.blank?
    $redis.del(slb_key) if current_sbl_node?
    Service::Slb::Slb.new.delete_node(pool_name, [[node_name]])
  end

  def slb_delete_relative_node
    $redis.del(slb_key)
    relative_slb_nodes = VirtualMachine.where(module_name: self.module_name, project_id: self.project_id, deployer: self.deployer).map{|m| m.node_name }
    Service::Slb::Slb.new.delete_node(pool_name, relative_slb_nodes )
  end

  def log_params
    Utils.wrap_common_params({
      ip: self.ip,
      module_name: self.module_name,
      build_branch_id: self.build_branch_id,
      project_id: self.project_id,
      status: self.status,
      domain_name: self.domain_name
    })
  end

  def can_deploy?
    'deploy success' == status_message || 'deploy error' == status_message || 'to deploy' == status_message || 'not deploy' == status_message
  end

  # collect deploy info
  LOG_INFO_PATH = '/data/vm'

  def collect_all
    collect_history
    collect_nginx
    collect_tomcat
    main_ci_conn.disconnect
  end  

  def main_ci_conn 
    @ci_conn ||= CiEnv.find_by_name('alltest').conn
  end
    
  def collect_history
    main_ci_conn.sudo_exec("mkdir -p #{LOG_INFO_PATH}", timeout: 5)
    main_ci_conn.sudo_exec("scp root@#{ip}:/root/.bash_history #{LOG_INFO_PATH}/#{ip}_root", timeout: 10)
    main_ci_conn.sudo_exec("scp root@#{ip}:/home/dianping/.bash_history #{LOG_INFO_PATH}/#{ip}_dianping", timeout: 10) rescue nil
  end
  
  def collect_nginx
    main_ci_conn.sudo_exec("scp root@#{ip}:/usr/local/nginx/conf/nginx.conf #{LOG_INFO_PATH}/#{ip}_nginx", timeout: 10) rescue nil 
  end

  def collect_tomcat
    main_ci_conn.sudo_exec("scp root@#{ip}:/etc/init.d/jboss #{LOG_INFO_PATH}/#{ip}_jboss", timeout: 10) rescue nil 
    main_ci_conn.sudo_exec("scp root@#{ip}:/usr/local/tomcat/bin/catalina.sh #{LOG_INFO_PATH}/#{ip}_catalina", timeout: 10) rescue nil
  end

  def sso_key_import
    main_ci_conn.sudo_exec("scp /data/ssoalpha-oauth.cer root@#{ip}:/tmp ", timeout: 10)
    conn.sudo_exec()
  end

  def set_workflow_uuids(uuids)
    $redis.set("vm:#{id}:workflow_uuids", Array.wrap(uuids).join(','))
    $redis.expire("vm:#{id}:workflow_uuids", 30.minutes)
  end

  def get_workflow_uuids
    $redis.get("vm:#{id}:workflow_uuids").try(:split, ',')
  end

  def delete_workflow_uuids
    $redis.del("vm:#{id}:workflow_uuids")
  end
end

