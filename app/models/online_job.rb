#coding:utf-8
class OnlineJob < ActiveRecord::Base
  belongs_to :project

  attr_accessible :build_branch_id, :jar_name, :machine, :online_script, :project_id, :status,
                  :success_at, :tag, :env,:log, :create_url, :update_url, :jar_url,
                  :business_belongs_to, :business_abbreviation
  validates :tag, :jar_name, :machine, :env, presence: true

  state_machine :status, :initial => :not_deploy do
    state :not_deploy,            :value => 1024,  :human_name => "未发布"
    state :deploying,             :value => 6,     :human_name => "发布中"
    state :fail_timeout,          :value => 2,     :human_name => "超时"
    state :fail_nohost,           :value => 3,     :human_name => "没有找到部署的机器"
    state :fail_nosource,         :value => 4,     :human_name => "ftp地址无法打开"
    state :fail_duplicate,        :value => 5,     :human_name => "重复发布"
    state :fail_unknown,          :value => 1,     :human_name => "其他错误"
    state :deploy_success,        :value => 0,     :human_name => "发布成功"

    event :succeed do
      transition :deploying => :deploy_success
    end

    event :timeout do
      transition :deploying => :deploy_fail_timeout
    end

    event :nohost do
      transition :deploying => :deploy_fail_nohost
    end

    event :nosource do
      transition :deploying => :deploy_fail_nosource
    end

    event :duplicate do
      transition :deploying => :deploy_fail
    end

    event :unknown_error do
      transition :deploying => :deploy_fail_timeout
    end

  end
end
