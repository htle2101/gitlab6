class PpeBranch < BuildBranch
  validates :package_type, :presence => { :message => "You need to create your beta ci first." }
  has_many :packings, foreign_key: "build_branch_id", class_name: "RolloutPacking", dependent: :destroy


  include Online::Helper

  def command
  	"clear_cache"
  end

  def appname
    ""
  end
end