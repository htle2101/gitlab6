#coding:utf-8

class PerformanceTest < ActiveRecord::Base
  attr_accessible :concurrent, :duration, :project_id, :target, :test_log_url, :title, :uid, :upload_file, :project

  belongs_to :project

  state_machine :status, :initial => :not_deploy do
  	state :not_deploy,             :value => 1024,  :human_name => "未启动"
	  state :apply_machine,          :value => 1,     :human_name => "机器申请中"
	  state :apply_machine_success,  :value => 2,     :human_name => "机器申请成功"
	  state :apply_machine_fail,     :value => 3,     :human_name => "预发中"
	  state :apply_agent,            :value => 4,     :human_name => "agent申请中"
	  state :apply_agent_success,    :value => 5,     :human_name => "agent申请成功"
	  state :apply_agent_fail,       :value => 6,     :human_name => "agent申请失败"
	  state :in_test,                :value => 7,     :human_name => "性能测试中"
	  state :test_success,           :value => 8,     :human_name => "发布失败"
	  state :test_fail,              :value => 9,     :human_name => "发布成功"
	end
	
end
