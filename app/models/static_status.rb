class StaticStatus < ActiveRecord::Base
  attr_accessible :build_branch_id, :build_number, :build_status, :cache_status
end
