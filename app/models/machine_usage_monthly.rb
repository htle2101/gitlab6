class MachineUsageMonthly < ActiveRecord::Base
  belongs_to :namespace
  belongs_to :project
  belongs_to :virtual_machine
  attr_accessible :duration, :month, :year, :namespace_id, :project_id, :virtual_machine_id

  # just use the month after 
  def self.save_collect_month_uasge(year, month)
    a, b, c = collect_uasge(Date.new(year, month), Date.new(year, month).next_month)
    (a.keys + b.keys + c.keys).uniq.each do |mum|
      MachineUsageMonthly.new(namespace_id: mum[0], project_id: mum[1], virtual_machine_id: mum[2], month: month, year: year, duration: [a[mum].to_i, b[mum].to_i, c[mum].to_i].sum).save
    end 
  end

  def self.collect_uasge(start_date, end_date)
    [collect_start_in_month(start_date, end_date), collect_end_in_month(start_date, end_date), collect_start_end_in_month(start_date, end_date)]
  end

  def self.save_collect_usage(start_date, end_date)
    a, b, c = collect_uasge(start_date, end_date)
    (a.keys + b.keys + c.keys).uniq.each do |mum|
      MachineUsageMonthly.new(namespace_id: mum[0], project_id: mum[1], virtual_machine_id: mum[2], month: nil, year: nil, duration: [a[mum].to_i, b[mum].to_i, c[mum].to_i].sum).save
    end
  end

  def self.collect_start_in_month(start_date, end_date)
    not_end_usages = VirtualMachineUsage.where(using_from: start_date.to_datetime..end_date.to_datetime).where(using_to: nil)
    usages = (not_end_usages + VirtualMachineUsage.where(using_from: start_date.to_datetime..end_date.to_datetime).where("using_to >= ? ", end_date.to_datetime)).group_by{|u| [u.group_id, u.project_id, u.machine_id]}
    usage_sum = {}
    usages.keys.each do |usage_key|
      usage_sum[usage_key] = usages[usage_key].sum{|vu| ( end_date.to_datetime - vu.using_from.to_datetime ) * 1.days } 
    end
    usage_sum
  end

  def self.collect_end_in_month(start_date, end_date)
    not_end_usages = VirtualMachineUsage.where("using_from < ? ", start_date.to_datetime).where(using_to: nil)
    usages = (not_end_usages + VirtualMachineUsage.where("using_from < ? ",  start_date.to_datetime).where(using_to: start_date.to_datetime..end_date.to_datetime)).group_by{|u| [u.group_id, u.project_id, u.machine_id]}
    usage_sum = {}
    usages.keys.each do |usage_key|
      usage_sum[usage_key] = usages[usage_key].sum{|vu| ( (vu.using_to || end_date).to_datetime - start_date.to_datetime ) * 1.days }
    end
    usage_sum
  end

  def self.collect_start_end_in_month(start_date, end_date)
    usages = VirtualMachineUsage.where("using_from >= ? ",  start_date).where("using_to < ? ", end_date.to_datetime).group_by{|u| [u.group_id, u.project_id, u.machine_id]}
    usage_sum = {}
    usages.keys.each do |usage_key|
      usage_sum[usage_key] = usages[usage_key].sum{|vu| ( vu.using_to.to_datetime - vu.using_from.to_datetime ) * 1.days }
    end
    usage_sum
  end

end
