class Task < ActiveRecord::Base

  attr_accessible :project_id, :subject, :description, :assignee_id, :creator_id, :status, :released_at, :branch_name

  belongs_to :project
  belongs_to :creator, class_name: "User", foreign_key: "creator_id"
  belongs_to :assignee, class_name: "User", foreign_key: "assignee_id"

  has_and_belongs_to_many :packages, join_table: "tasks_packages"
  has_many :configs, class_name: "TaskConfig"
  has_many :checkers

  validates_presence_of :subject, :assignee_id, :creator_id

  scope :assigned_to, ->(user) { where(assignee_id: user.id) }
  scope :created_by, ->(user) { where(creator_id: user.id) }
  scope :after_today, -> { where("released_at >= date('#{Date.today.to_s}')" )}

  alias_method :author, :creator

  def package_name_list
    packages.map(&:name)
  end

  def can_edit?(user)
    user == creator || user == assignee || user.admin?
  end

  def can_destroy?(user)
    user == creator || user.admin?
  end

  def configs_take_effect_for_beta
    self.configs.each do |config|
      config.take_effect_for_beta
    end
  end

  def after_alpha?
    (self.status > 3) ? true : false
  end

  state_machine :status, :initial => :new do
    state :new,           :value => 1
    state :developing,    :value => 2
    state :alpha,         :value => 3
    state :beta,          :value => 4
    state :release_ready, :value => 5

    after_transition any => :beta, :do => :configs_take_effect_for_beta

    event :to_new do
      transition :developing => :new
    end

    event :to_developing do
      transition [:new, :alpha] => :developing
    end

    event :to_alpha do
      transition [:developing, :beta] => :alpha
    end

    event :to_beta do
      transition [:alpha, :release_ready] => :beta
    end

    event :to_release_ready do
      transition :beta => :release_ready
    end
  end
end
