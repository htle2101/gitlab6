class BetaDeployLog < ActiveRecord::Base
  attr_accessible :project_id, :date, :voteup_count,
                  :votedown_count, :project_path_with_namespace,
                  :module_name, :date, :branch_name

  belongs_to :project
end
