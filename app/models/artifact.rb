class Artifact < ActiveRecord::Base
  attr_accessible :artifact_id, :group_id, :version

	validates_presence_of :artifact_id, :group_id, :version
	validates_uniqueness_of :version, scope: [:group_id, :artifact_id]

	def to_s
		"#{group_id}:#{artifact_id}:#{version}"
	end
end
