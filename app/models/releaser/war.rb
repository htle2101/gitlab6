module Releaser
  class War
    attr_accessor :machine

    include Utils::Logger
    include Utils::Time

    def initialize(machine)
      @machine = machine
      self.logger_file = logger_file_name
      @errors = ""
    end

    def release
      begin
        ::RemoteOp::DpDns.new.delete_domain_name(machine.domain_name,machine.ip)
        ::RemoteCi::Command::JbossService.new(machine.ip, port: machine.port, user: machine.username, password: machine.password).down
        ::RemoteCi::Command::NginxService.new(machine.ip, port: machine.port, user: machine.username, password: machine.password, env: 'alpha').down
        machine.slb_delete_node
        ::RemoteCi::Command::Swimlane.new(machine.ip, port: machine.port).del_swimlane
        true
      rescue ::RemoteOp::DpDnsError
        logger.error("Machine unbinding releated error:cann't delete dns of #{machine.ip}.")
        @errors = "[Unbinding machine related error:cann't delete dns of #{machine.ip}.]"
        false
      rescue ::RemoteCi::Command::JbossServiceError
        logger.error("Machine unbinding releated error:cann't stop jboss service of #{machine.ip}.")
        @errors = @errors + "[Unbinding machine releated error:cann't stop jboss service of #{machine.ip}.]"
        false
      rescue ::RemoteCi::Command::NginxServiceError
        logger.error("Machine unbinding releated error:cann't stop nginx service of #{machine.ip}.")
        @errors = @errors + "[Unbinding machine releated error:cann't stop nginx service of #{machine.ip}.]"
        false
      rescue Service::Slb::SlbConnectError, Service::Slb::SlbAccessError => e
        logger.error("Machine slb release error.")
        @errors = @errors + "[Unbinding machine releated error:slb release error of #{machine.ip}.]"
        false
      rescue ::RemoteCi::Command::DelSwimlaneError => e
        logger.error("Delete Swimlane error.")
        @errors = @errors + "[Unbinding machine releated error:delete swimlane error of #{machine.ip}.]"
        false  
      ensure
        machine.unbind
      end
    end  

    def check_usage
      time = file_date('/usr/local/tomcat/logs/catalina.out')
      logger.info("#{machine.id} #{machine.ip} #{time.strftime("%FT%T%:z")} #{(time > 5.days.ago) ? "used" : "unused "}")
    rescue => e
      logger.error("#{machine.id} #{machine.ip} #{e}")  
    end

    def logger_file_name
      @logger_file_name = "machine/release/#{self.class.to_s.demodulize}.log"
    end

    def file_date( file = '/usr/local/tomcat/logs/catalina.out' )
      DateTime.iso8601(machine.conn.ls(file,"-l --time-style '+%Y-%m-%dT%H:%M:%S+08:00'").to_s.split(' ')[5])
    end  
  end
end    
