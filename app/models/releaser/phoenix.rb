module Releaser
  class Phoenix < War

    def check_usage
      time = file_date('/usr/local/nginx/logs/APPNAME.access.log')
      logger.info("#{machine.id} #{machine.ip} #{time.strftime("%FT%T%:z")} #{(time > 5.days.ago) ? "used" : "unused "}")
    rescue => e
      logger.error("#{machine.id} #{machine.ip} #{e}")  
    end

  end
end  