class TasksPackage < ActiveRecord::Base
  belongs_to :task
  belongs_to :package

  attr_accessible :task_id, :package_id
end