#coding:utf-8
class BuildBranch < ActiveRecord::Base
  belongs_to :project
  belongs_to :ci_env

  has_one :light_merge, class_name: 'LightMerge', dependent: :destroy
  has_many :deps, class_name: 'Dependency', dependent: :destroy
  has_many :machines, class_name: 'VirtualMachine'


  attr_accessible :branch_name, :gitlab_created, :jenkins_job_name, :package_type, :ci_env_id, :deployers, :auto_deploy, :run_test

  scope :in_env, ->(ci_env) { where(ci_env_id: CiEnv.where(name: ci_env)) }

  validates :branch_name, presence: true
  validates :jenkins_job_name, presence: true

  after_create :expire_cache
  before_destroy :expire_cache

  def java?
    ['Cmdb', 'War', 'CmdbShadow', 'Phoenix', "SonarShadow"].include? self.deployers
  end

  def machine_deployers
    return [['单机部署', 'War'], ['service双机切换部署(测试中慎用)', 'JavaService']] if self.deployers == 'War'
    [self.deployers]
  end

  def static?
    ['Static', 'StaticOld'].include? self.deployers
  end

  def nodejs?
    ['Nodejs'].include? self.deployers
  end

  def php?
    ['Php'].include? self.deployers
  end

  def zipfile?
    ['Zipfile', 'CmdbZipfile'].include? self.deployers
  end

  def cmdb_php?
    ['CmdbPhp'].include? self.deployers
  end

  def beta?
    ci_env.name == 'beta'
  end

  def change_run_test(new_value)
    new_content = "-Denv=#{self.ci_env.label} -U -B clean install #{ new_value == 'true' ? "-Dmaven.test.failure.ignore=true" : "-DskipTests=true" } com.dianping.maven.plugins:dependency-check-plugin:check -Dchecklist=http://code.dianpingoa.com/api/v3/artifacts"
    begin
      Timeout::timeout(3) do
        xml = self.ci_env.server.job.get_config(self.jenkins_job_name)
        doc = Nokogiri::XML(xml)
        doc.search("goals").each do |goal|
          goal.content = new_content
        end
        self.ci_env.server.job.post_config(self.jenkins_job_name,doc.to_s)
      end
      self.update_attributes(run_test: new_value)
      return true
    rescue
      return false
    end
  end

  # VirtualMachine.where(id: 141..200).select{|v| v.build_branch.imf.job_status == 'failure' rescue nil}
  def remove_check_version
    new_content = "-Denv=#{self.ci_env.label} -U -B clean install -DskipTests"
    begin
      Timeout::timeout(3) do
        xml = self.ci_env.server.job.get_config(self.jenkins_job_name)
        doc = Nokogiri::XML(xml)
        doc.search("goals").each do |goal|
          goal.content = new_content
        end
        self.ci_env.server.job.post_config(self.jenkins_job_name,doc.to_s)
      end
      return true
    rescue
      return false
    end
  end

  # JDK1.6
  # JDK1.7.0_72
  def change_jdk_version(version)
    begin
      Timeout::timeout(3) do
        xml = self.ci_env.server.job.get_config(self.jenkins_job_name)
        doc = CiTemplate.change_jdk_version(version, xml)
        self.ci_env.server.job.post_config(self.jenkins_job_name,doc.to_s)
      end
      return true
    rescue
      return false
    end
  end  

  def reload_run_test
    change_run_test(self.run_test)
  end

  def gitlab_path
    "http://#{Settings.gitlab['host']}/#{self.project.path_with_namespace}/ci_branch/#{self.branch_name}"
  end

  def mvn
    return (ci_env.property["maven32"] || ci_env.property["maven3"]) if package_type == 'search-arts'
    package_type.include?('maven3') ? ci_env.property["maven3"] : ci_env.property["maven2"]
  end

  def branch_for_sonar?
    ci_env.try(:name) == 'beta'
  end

  def need_shadow?
    Settings.dp['need_shadow'] && self.ci_env.is_cmdb && self.java? && self.try(:shadow_branch).blank?
  end

  def need_sonar?
    Settings.dp['need_sonar'] && self.ci_env.is_cmdb && self.java? && self.try(:sonar_branch).blank?
  end

  def not_to_deploy?
    !auto_deploy
  end

  def create_check
    return false if self.deployers.blank?
    Deployer.new(self.deployers, :ci_branch => self).create_check
  end

  def jenkins_url
    "#{self.ci_env.url}/job/#{self.jenkins_job_name}"
  end

  def workspace_path
    ci_env.workspace_path(jenkins_job_name)
  end

  def http_workspace_path
    "#{ci_env.full_url}/job/#{jenkins_job_name}/ws"
  end

  def command
    "deploy"
  end

  def ci_template
    @ci_template ||= CiTemplate.find_by_package_type_and_ci_env_name(package_type, ci_env.name)
  end

  def ci_last_build
    RemoteCi::Job.new(self.ci_env.server, self.jenkins_job_name)
  end

  def build
    job.build(self.jenkins_job_name)
  end

  def modules(_branch_name = nil)
    tree(_branch_name).pom
  end

  def imf
    @imf ||= RemoteCi::CiImf.new(self)
  end

  def modules_with_type(_branch_name = nil, type)
    types = Array(type)
    modules(_branch_name).select {|m| types.include?(m[:type]) }
  end

  def modules_without_type(_branch_name = nil, type)
    types = Array(type)
    modules(_branch_name).select {|m| !types.include?(m[:type]) }
  end

  def machine_for_module(module_name)
    machines.master.find { |machine| machine.module_name == module_name }
  end

  def machine_for_phoenix
    machines.find_by_module_name(CiBranch::PHOENIX_MODULE)
  end

  def log_modules
    logs = RemoteCi::LoggerBinder.new(self).last_log
    return [] if logs.blank? || logs["logs"].blank?
    logs["logs"].map{|l| l["module_name"] }.uniq
  end

  def module_last_logs(module_name)
    logs = RemoteCi::LoggerBinder.new(self).last_log
    return [] if logs.blank? || logs["logs"].blank?
    logs["logs"].select{|l| l["module_name"] == module_name }
  end

  def module_logs(module_name, tomcat_log = nil)
    res = module_last_logs(module_name).select{|l| l["location_type"] == 'file' }.map{|l| [l["time"], l["module_name"]] }.uniq
    res.push(['Tomcat Log', tomcat_log]) if tomcat_log
    res
  end

  def module_beta_last_logs(module_name)
    logs = RemoteCi::LoggerBinder.new(self).last_log
    return [] if logs.blank? || logs["logs"].blank?
    logs["logs"].select{|l| l["module_name"] == "paas"+module_name }
  end

  def module_beta_paas_logs(module_name, tomcat_log = nil)
    res = module_beta_last_logs(module_name).last(5).select{|l| l["location_type"] == 'paas' }.map{|l| [l["time"], l["module_name"]] }.uniq
    res.push(['Tomcat Log', tomcat_log]) if tomcat_log
    res
  end

  def tree(_branch_name = nil)
    project.tree(_branch_name || branch_name)
  end

  def pom_exist?
    !tree.try(:pom).blank?
  end

  def expire_cache
    Rails.cache.delete(project.cache_key("simplified-ci-banches")) if project.present?
  end

  def job
    @job ||= self.ci_env.server.job
  end

  def current_build_status
    @current_build_status ||= job.get_current_build_status(self.jenkins_job_name)
  end

  def job_exists?
    begin
      Timeout::timeout(2) { job.exists?(self.jenkins_job_name) }
    rescue
      false
    end
  end

  def job_running?
    begin
      Timeout::timeout(2) { current_build_status == "running" }
    rescue
      false
    end
  end

  def current_build_number
    job.get_current_build_number(self.jenkins_job_name)
  end

  def add_delete_before_build_config
    xml = self.ci_env.server.job.get_config(self.jenkins_job_name)
    doc = Nokogiri::XML(xml)
    Nokogiri::XML::Builder.with(doc.xpath("//buildWrappers")[0]) do |xml|
       xml << '<hudson.plugins.ws__cleanup.PreBuildCleanup plugin="ws-cleanup@0.19"><deleteDirs>false</deleteDirs><cleanupParameter/><externalDelete/></hudson.plugins.ws__cleanup.PreBuildCleanup>'
    end
    self.ci_env.server.job.post_config(self.jenkins_job_name, doc.to_s)
  end

  def post_config(config)
    job.post_config(self.jenkins_job_name, config)
  end

  def current_config
    job.get_config(self.jenkins_job_name)
  end

  def cortex_old_standard?(standard_shell_list = nil)
    doc = Nokogiri::XML(current_config)
    shell = doc.xpath('/project/builders/hudson.tasks.Shell/command')[0]
    standard_shell = (standard_shell_list || ['[[ -e "package.json" ]] && npm install', '[[ -e "Gruntfile.js" ]] && grunt --no-color', 'ctx package --cwd $WORKSPACE --env', 'ctx upload --cwd $WORKSPACE --env'])
    standard_shell.each do |line|
      return false unless shell.content.include?(line)
    end
    return true
  end

  def reload_cortex_old_config
    doc = Nokogiri::XML(current_config)
    shell = doc.xpath('/project/builders/hudson.tasks.Shell/command')[0]

    template_shell = Nokogiri::XML(job.get_config(self.ci_template.template_name)).xpath('/project/builders/hudson.tasks.Shell/command')[0].content
    shell.content = template_shell
    post_config(doc.to_s)
  end

  def all_deploy_finished?
    self.modules_with_type("war").each do |war|
      return false unless machine_for_module(war.module_name).deploy_finished?
    end
    return true
  end

  def project_name_for_auto_test
    self.ci_env.name + '-' + self.project.namespace.path + '-' + self.project.path
  end

  def appname
    @appname ||= self.imf.pom_datasource.values[0].module_name rescue nil
  end

  def download_urls
    modules_with_type(self.packings.last.try(:commit) || self.branch_name , 'war').inject({}) do |h, war|
      h.merge!({war.module_name => "#{http_workspace_path}#{war.path}/target/#{war.warName}-#{ci_env.label}-#{war.version}.war"})
    end
  end

  def all_trans(ip_from, ip_to)
    return true if VirtualMachine.find_by_ip(ip_from).build_branch.blank?
    return false if ['error', false].include? trans_tomcat(ip_from, ip_to)
    trans_dns(ip_from, ip_to)
    trans_slb(ip_from, ip_to)
    # trans_vm_status(ip_from, ip_to)
  end

  def trans_tomcat(ip_from, ip_to)
    # ensure jenkins was build_success
    from_vm = VirtualMachine.find_by_ip(ip_from)
    if ['not_run', 'failure'].include? from_vm.ci_branch.current_build_status
      from_vm.ci_branch.build
      return 'error'
    end
    trans_tomcat_init(ip_from, ip_to)
    check_tomcat(ip_to)
  end

  def trans_tomcat_init(ip_from, ip_to)
    from_vm = VirtualMachine.find_by_ip(ip_from)
    virtual_machine = VirtualMachine.find_by_ip(ip_to)
      sudo_account = ::RemoteCi::Command::SudoAccount.new(virtual_machine.ip, port: virtual_machine.port).up
    ::RemoteCi::Command::KeyCopy.new(virtual_machine.ip, port: virtual_machine.port, envs: ['alpha']).execute

    ::RemoteCi::Command::JbossConf.new(virtual_machine.ip, port: virtual_machine.port, module_name: (from_vm.project.tree.hashed_pom[from_vm.module_name].warName rescue from_vm.module_name)).up
    attrs = {project_id: from_vm.project.id, build_branch_id: from_vm.ci_branch.id, module_name: from_vm.module_name, package_type: 'war' , status: 1, username: sudo_account[:account], password: sudo_account[:password], domain_name: from_vm.domain_name, deployer: 'WarSlave', sort_key: 2}
    virtual_machine.update_attributes(attrs)
    virtual_machine.deployer_instance.deploy
  end

  def copy_trans_tomcat_string(ip_from, ip_to)
    # ls -t|awk 'NR>3{print $0|"xargs rm -rf"}'
    "scp -r -P 58422 webapps root@#{ip_to}:/data"
  end

  def trans_dns(ip_from, ip_to)
    from_vm = VirtualMachine.find_by_ip(ip_from)
    dns_domain_names = JSON(RemoteOp::DpDns.new.get_domain_name(from_vm.ip).body) rescue nil
    return true if dns_domain_names.blank?
    dns_domain_names.each do |dname|
      ::RemoteOp::DpDns.new.delete_domain_name(dname, ip_from)
      ::RemoteOp::DpDns.new.set_domain_name(dname, ip_to)
    end
    return true
  end

  def check_tomcat(ip_to)
    HTTParty.get("http://#{ip_to}:8080").code == 200 || HTTParty.get("http://#{ip_to}:8080").body.index('setInterval(') != nil
  end

  def self.slb_strings_xml
    # git clone git@code.dianpingoa.com:arch/phoenix-slb-model-alpha.git into /tmp
    # git clone git@code.dianpingoa.com:roger.chen/alphatrans.git into /tmp
    @slb_strings_xml ||= Nokogiri::XML(File.read('/tmp/phoenix-slb-model-alpha/slb_base.xml'))
  end

  def self.slb_strings
    @slb_strings ||= File.read('/tmp/phoenix-slb-model-alpha/slb_base.xml')
  end

  def self.lion_strings
    @lion_strings ||= File.read('/tmp/alphatrans/service.csv')
  end

  def self.lion_readline
    @lion_readline ||= File.open('/tmp/alphatrans/service.csv').readlines
  end

  def trans_slb(ip_from, ip_to)
    member_nodes = BuildBranch.slb_strings_xml.xpath("//member[@ip='#{ip_from}']")
    ip_to_domain = VirtualMachine.find_by_ip('ip_from').domain_name
    member_nodes = member_nodes + BuildBranch.slb_strings_xml.xpath("//member[@ip='#{ip_to_domain}']").to_a unless ip_to_domain.blank?
    return true if member_nodes.blank?
    member_nodes.each do |m|
      node_name = m.attributes['name'].value
      pool_name = m.parent.attributes['name'].value
      Service::Slb::Slb.new(timeout: 10).delete_node(pool_name, [[node_name]])
      Service::Slb::Slb.new(timeout: 10).add_node(pool_name, [{"ip" => ip_to, "port" => "8080", "name" => "node_#{ip_to}"}])
      Service::Slb::Slb.new(timeout: 30).deploy_node(pool_name)
    end
  end

  # VirtualMachine.where(" ip like '%10.1.77%' ").each{|m|   trans_lion(m.ip, '')}
  def trans_lion(ip_from, ip_to)
    return true if BuildBranch.lion_strings.index(ip_from).blank?
    result = []
    BuildBranch.lion_readline.each do |l|
      result << [VirtualMachine.find_by_ip(ip_from).module_name, l] if l.index(ip_from).present? && l.split(',')[2] == '2'
    end
    return result
  end

  # 获取服务地址：http://lionapi.dp:8080/service2/get?env=alpha&service=http://service.dianping.com/test/testService_1.0.0
  # 设置服务地址：http://lionapi.dp:8080/service2/set?id=2&env=alpha&service=http://service.dianping.com/test/testService_1.0.0&address=1.1.1.1:1111,2.2.2.2:2222

  def get_lion(service_url)
    Service::Lion::Request.new.get_service(service_url)
  end

  def set_lion(service_url, ips_with_port)
    Service::Lion::Request.new.set_service(service_url, ips_with_port)
  end

  def trans_vm_status(ip_from, ip_to)
    from_vm = VirtualMachine.find_by_ip(ip_from)
    virtual_machine = VirtualMachine.find_by_ip(ip_to)

    from_vm.update_attributes(sort_key: 2, build_branch_id: nil, project_id: nil)
    virtual_machine.update_attributes(sort_key: 1, deployer: 'War')
    from_vm.conn.sudo_exec("service jboss stop", timeout: 60)
  end
end
