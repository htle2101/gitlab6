class ProjectModule < ActiveRecord::Base
  belongs_to :project
  attr_accessible :module_name, :has_paas, :paas_url, :ftp_url

  def self.search_by_artifactid(query)
    Project.where(id: ProjectModule.where(module_name: query).pluck(:project_id))
  end 
end
