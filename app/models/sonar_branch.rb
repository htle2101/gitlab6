class SonarBranch < BuildBranch
  include Deployers::Helper

  alias_method :deployer_for_sonarshadow, :wars
end