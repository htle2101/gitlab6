class Dependency < ActiveRecord::Base
  belongs_to :ci_branch, :foreign_key => :build_branch_id
  belongs_to :target_ci_branch, class_name: "CiBranch", :foreign_key => :target_build_branch_id
  attr_accessible :target_build_branch_id, :build_branch_id, :module_name

  validates_presence_of :target_build_branch_id, :build_branch_id, :module_name
  validates_uniqueness_of :module_name, :scope => [:build_branch_id], message: "has been added."

  def target_project
    if BuildBranch.where( id: self.target_build_branch_id ).blank?
      nil
    else
      BuildBranch.where( id: self.target_build_branch_id ).first.project
    end
  end

  def target_ci_branch_name
    if BuildBranch.where( id: self.target_build_branch_id ).blank?
      nil
    else
      BuildBranch.where( id: self.target_build_branch_id ).first.jenkins_job_name
    end
  end
end
