module Deployers
  class Zipfile < War
    def name
      "#{module_name}.#{ext}"
    end

    def ext
      'zip'
    end
  end
end
