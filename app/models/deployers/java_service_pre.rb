module Deployers
  class JavaServicePre < War

    def deploy
      simple_deploy
    end

    def get_machine
      ci_branch.machines.slave.find_by_module_name(module_name)
    end

  end
end
