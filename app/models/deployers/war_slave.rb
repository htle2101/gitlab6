module Deployers
  class WarSlave < War

    def get_machine
      ci_branch.machines.slave.find_by_module_name(module_name)
    end

  end
end    