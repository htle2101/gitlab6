module Deployers
  class CmdbZipfile < Cmdb


    def name
      "#{module_name}.#{ext}"
    end

    def ext
      'zip'
    end

  end
end
