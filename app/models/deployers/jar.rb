module Deployers
  class Jar < War
    def check
      [HTTParty.get(maven_url(:snapshots)).code, HTTParty.get(maven_url(:releases)).code].include?(200) ? "deploy success" : "deploy error"
    end

    def deploy
      return nil if !$redis.get("#{ci_branch}-#{module_name}").nil?
      $redis.set("#{ci_branch}-#{module_name}",1)
      machine.update_status('deploying')
      logger.info("begin to send jar to maven_storage ...")
      begin
        logger.info("execute #{ci_branch.mvn} deploy")
        pom_choice = type == "pom" ? "-N" : ""
        ci_branch.ci_env.conn.execute("cd #{ci_branch.workspace_path}#{path} && #{ci_branch.mvn} deploy #{pom_choice} -Dmaven.test.skip=true ", :timeout => 120)
        result = "deploy success"
      rescue Rye::Err => e
        stdout = e.stdout.join("\n")
        result = "deploy error"
        logger.error(stdout)
      ensure
        $redis.del("#{ci_branch}-#{module_name}")
        ci_branch.ci_env.conn.disconnect
      end

      stdout = (stdout =~ /((.*\n){3}.*There are test failures.*(.*\n){3})/) ? $1 : ""
      result = version.include?("SNAPSHOT") ? "deploy success" : check  if result != "deploy error"
      logger.info("the assigned jar doesnot exist in maven_storage ") if result == "deploy error"
      logger.info("the deploying process is #{result}!")
      machine.update_status(result)
      # Notify.delay.jar_deployed(ci_branch.id, current_user.email, jenkins_job_name, module_name, result, stdout) if result == 'error'
      result
    end

    def reset
      machine.update_status('to deploy') unless machine.blank?
    end   

    def ext
      "jar"
    end

    def maven_url(type)
      "#{Settings.dp["#{type}_url"]}/#{groupId.gsub('.', '/')}/#{module_name}/#{version}/#{module_name}-#{version}.#{ci_branch.tree.hashed_pom[module_name].type}"
    end

    def get_machine
      VirtualMachines::LinkContext.new({ build_branch_id: ci_branch.id, module_name: module_name, deployer: 'Jar', status: 'deploying' }).execute
    end
  end
end
