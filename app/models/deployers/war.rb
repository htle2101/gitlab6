module Deployers
  class War < BaseDeployer
    attr_accessor :ci_branch, :module_name, :path, :type, :version, :logger_file_name, :build_number, :warName, :full_name, :groupId
    delegate :ci_env, :to => :ci_branch
    delegate :conn, :to => :machine

    BACKUP_PATH = "/data/webapps/backup"
    APP_PATH = "/data/webapps"

    WAR_PATH = "/data/ftpupload/uploadwar"

    LOG_PATH = "/data/applogs"
    DATA_PATH = "/data/appdatas"

    JBOSS_PATH = "/etc/init.d"

    include Utils::Logger
    include Utils::Time

    def initialize(attrs)
      attrs.each { |k, v| send("#{k}=", v) if respond_to?("#{k}=") }
      self.logger_file = attrs[:logger_file] || logger_file_name
    end

    def create_check
      ci_branch.pom_exist?
    end

    def simple_deploy
      logger.info("Begin to deploy package `#{self.name}`")
      logger.error("Machine or Ip not assigned.") and machine.try(:update_status,'deploy error') and return false if machine.blank? || machine.ip.blank?
      logger.error("Package can not be found with path `#{self.full_path}` or the warName format not like ${artifactId}-${env}-${version}") and machine.update_status('deploy error') and return false unless self.exists?
      begin
        machine.update_status('deploying')
        logger.info("Begin to Create path #{release_path}, #{BACKUP_PATH} #{LOG_PATH} #{DATA_PATH}")
        conn.sudo_exec("mkdir -p #{release_path} #{BACKUP_PATH} #{LOG_PATH} #{DATA_PATH}", :timeout => 10)
        logger.info("Copy war from jenkins server to #{machine.ip}:#{BACKUP_PATH} via scp.")
        ci_env.conn.execute("scp -P #{machine.port || Settings.dp['ssh_port']} #{self.full_path} #{ssh_destination}", :timeout => 100)
        logger.info("Unzip war to #{release_path}/#{self.deploy_name}")
        conn.sudo_exec("unzip -q #{BACKUP_PATH}/#{self.name} -d #{release_path}/#{self.deploy_name}", :timeout => 30)
        logger.info("Remove link directory #{current_path}")
        conn.sudo_exec("rm -rf #{current_path}", :timeout => 10)
        logger.info("Link #{release_path} to #{current_path}")
        conn.sudo_exec("ln -s #{release_path} #{current_path}", :timeout => 10)

        logger.info("chown -R nobody:nobody #{LOG_PATH} #{DATA_PATH}")
        conn.sudo_exec("chown -R nobody:nobody #{LOG_PATH} #{DATA_PATH}", :timeout => 15)

        app_username = machine.username || 'root'

        logger.info("mkdir #{meta_path}")
        conn.sudo_exec("mkdir -p #{meta_path}", :timeout => 10)

        logger.info("start chown #{app_username}:#{app_username} #{APP_PATH} ")
        conn.sudo_exec("chown #{app_username}:#{app_username} #{APP_PATH} ", :timeout => 10)
        logger.info("start chown -R #{app_username}:#{app_username} #{release_path} ")
        conn.sudo_exec("chown -R #{app_username}:#{app_username} #{release_path} ", :timeout => 15)

        logger.info("start generate appenv")
        conn.sudo_exec("[ -f /data/webapps/appenv ] || echo 'deployenv=alpha \nzkserver=alpha.lion.dp:2182' >> /data/webapps/appenv", :timeout => 10)


        logger.info("Update app.properties: appname = #{warName}")
        res = conn.sudo_exec("[ -f  #{meta_path}app.properties ] || echo 'not exists'", :timeout => 10)

        if res.blank?
          conn.sudo_exec("sed -i -e  \'s/app.name.*/app.name=#{warName}/\' #{meta_path}/app.properties", :timeout => 10)
        else
          conn.sudo_exec("echo \"app.name=#{warName}\" >> #{meta_path}/app.properties", :timeout => 10)
        end

        copy_jboss_script if machine.not_initialized? and machine.action_type == 'bind'
        logger.info("Restart jboss service now.")
        tomcat_timeout = true
        conn.sudo_exec("/sbin/service jboss restart", :timeout => 200)
        tomcat_timeout = false
        logger.info("Restart jboss service success.")
        logger.info("Deploy Successfully.")
        machine.update_status('deploy success') if machine.used?
      rescue => e
        logger.error("Deploy Failed, error message: #{e}.")
        logger.error("tomcat restart timeout 180s.") if tomcat_timeout
        logger.error("Please check `/etc/sudoers` on #{machine.ip}, comment `Default requiretty` if this option exists.") if e.to_s =~ /tty/
        machine.update_status('deploy error') unless machine.blank?
        return false
      ensure
        ci_env.conn.disconnect
        unless machine.blank?
          logger.info("disconnect machine")
          conn.disconnect 
        end
        begin
          logger.info("call auto test.")
          # Service::AutoTest::Request.new.call(machine.build_branch.project_name_for_auto_test) and logger.info("call auto success.") if machine.build_branch.all_deploy_finished?
          Service::AutoTest::Request.new.call(machine.build_branch.project_name_for_auto_test) and logger.info("call auto success.")
        rescue
        end
      end
      true
    end

    def deploy
      simple_deploy
    end  

    def destroy
      logger.info("Begin to destroy package `#{self.name}`")
      logger.info("Machine or Ip not assigned.") and return true if machine.blank? || machine.ip.blank?
      begin
        Timeout::timeout(5) {
          conn.sudo_exec("rm -rf #{module_path}")
          logger.info("Destroy Finished.")
        }
      rescue => e
        logger.error("Destroy Failed, error message: #{e}.")
        return false
      end
      true
    end

    def ssh_destination
      "#{Settings.dp['ssh_user']}@#{machine.ip}:#{BACKUP_PATH}"
    end

    def full_path
      "#{ci_branch.workspace_path}#{path}/target/#{name}" # `path` always start with a slash
    end

    def url
      "#{ci_branch.http_workspace_path}#{path}/target/#{name}"
    end

    def release_path
      "#{module_path}/releases/#{ts}"
    end

    def current_path
      "#{module_path}/current"
    end

    def meta_path
      "#{APP_PATH}/current/#{deploy_name}/WEB-INF/classes/META-INF/"
    end  

    def module_path
      @module_path ||= APP_PATH
    end

    def name
      "#{warName}-#{ci_env.label}-#{version}.#{ext}".delete(' ')
    end

    def ext
      @ext ||= get_ext
    end

    def get_ext
      # hard code
      return self.type if !self.type.blank? && self.type != 'jar'
      'war'
    end

    def deploy_name
      "#{warName}.#{ext}"
    end

    def machine
      @machine ||= get_machine
    end

    def get_machine
      ci_branch.machines.master.find_by_module_name(module_name)
    end

    def exists?
      begin
        ci_env.conn.test('-f', full_path, :timeout => 10)
        true
      rescue Rye::Err
        false
      end
    end

    def logger_file_name
      @logger_file_name = "deploy/#{ci_branch.jenkins_job_name}/#{module_name}-#{ts}.log" if !ci_branch.jenkins_job_name.blank? && !module_name.blank?
    end

    def copy_jboss_script
      logger.info("Create jboss script to #{JBOSS_PATH}")
      ci_env.conn.execute("scp -P #{machine.port || Settings.dp['ssh_port']} #{ci_env.jboss_script_path} #{Settings.dp['ssh_user']}@#{machine.ip}:#{JBOSS_PATH}", :timeout => 10)
    end

    protected

    def token
      @token ||= RemoteCi::DigitLink.link(build_number, machine.id)
    end
  end
end