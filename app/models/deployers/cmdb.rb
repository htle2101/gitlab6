#coding:utf-8
module Deployers
  class Cmdb < War
    def deploy
      logger.info("Begin to send package `#{self.name}`")
      logger.error("Machine or Ip not assigned.") and return false if machine.blank?
      logger.error("Package can not be found with path `#{self.full_path}`") and return false unless self.exists?
      begin
        machine.update_status(cmdb_send_war)
      rescue => e
        machine.update_status('deploy error')
        logger.error("Deploy Failed when Call lazyman, error message: #{e}.")
        return false
      ensure
        ci_env.conn.disconnect
      end
      BetaDeployLog.create(project_id: ci_branch.project_id,
                           branch_name: ci_branch.branch_name,
                           project_path_with_namespace: ci_branch.project.path_with_namespace,
                           module_name: module_name,
                           date: Date.today)
      
      true
    end

    def get_machine
      VirtualMachines::LinkContext.new({ build_branch_id: ci_branch.id, module_name: module_name, deployer: 'Cmdb' }).execute
    end  

    protected 

    def cmdb_send_war
      logger.error("Cannot find the jenkins build.") and return 'deploy error' if build_number.nil? 
      logger.info("Start to Call lazyman.")
      
      ci_env.conn.execute("scp -P #{Settings.dp['ssh_port']} #{self.full_path} #{Settings.op['ssh_user']}@#{Settings.op['ssh_host']}:#{WAR_PATH}", :timeout => 120)
      uri = URI("http://#{Settings.op['lzm_url']}/deploy/deploy")
      params = { 'issue_id' => token, 'project' => machine.project_id, 'package_id' => token,
                'package' => "/#{name}", 'app' => warName, 'gate' => 0 }
      uri.query = URI.encode_www_form(params)
      res = Net::HTTP.get_response(uri)
      logger.info("call lzm with token #{token}")
      logger.error("Some error occured on lazyman. Http request return #{res.code}") unless res.code == "200"
      logger.error("This environment was not set up by ops. Please contact to ops 电话: 1234. http://workflow.dp/") unless res.code == "200"
      logger.info("Call lazyman Finished. Wait For lazyman callback.") if res.code == "200"
      (res.code == "200") ? 'deploying' : 'deploy error'
    end
    
  end
end
