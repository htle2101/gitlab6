module Deployers
  class BaseDeployer
    def initialize(attrs)
      attrs.each { |k, v| send("#{k}=", v) if respond_to?("#{k}=") }
    end

    def create_check
      true
    end
  end
end    