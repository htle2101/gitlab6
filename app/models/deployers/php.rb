module Deployers
  class Php < War
    def name
      'build.zip'
    end

    def ext
      'zip'
    end

    def deploy_name
      "#{module_name}"
    end

    def deploy
      return unless check_machine

      begin
        logger.info("Begin to deploy package `#{self.name}`")

        deploy!
      rescue => error
        machine.try(:update_status,'deploy error')

        logger.info("Depoy error: #{error}")
      ensure
        logger.info('Disconnect deploy server and jenkins server')

        ci_env.conn.disconnect
        conn.disconnect
      end
    end

    def deploy!
      machine.update_status('deploying')

      create_path

      copy_zip_from_jenkins

      unzip_zip

      relink_current_dir
    end

    def check_machine
      if machine.blank? || machine.ip.blank?
        logger.error("Machine or Ip not assigned.")
        machine.try(:update_status,'deploy error')
        false
      else
        true
      end
    end

    def create_path
      logger.info("Create path #{release_path} #{BACKUP_PATH}")

      conn.sudo_exec("mkdir -p #{release_path} #{BACKUP_PATH}")
    end

    def copy_zip_from_jenkins
      logger.info("Copy build.zip from jenkins server to #{machine.ip}:#{BACKUP_PATH}")

      command = "scp -P #{machine.port || Settings.dp['ssh_port']} #{self.full_path} #{ssh_destination}"

      ci_env.conn.execute(command, timeout: 30)
    end

    def unzip_zip
      logger.info("Unzip war to #{release_path}/#{self.deploy_name}")

      command = "unzip -q #{BACKUP_PATH}/#{self.name} -d #{release_path}/#{self.deploy_name}"

      conn.sudo_exec(command, timeout: 30)
    end

    def relink_current_dir
      logger.info("Remove link directory #{current_path}")

      conn.sudo_exec("rm -rf #{current_path}", :timeout => 10)

      logger.info("Link #{release_path} to #{current_path}")

      conn.sudo_exec("ln -s #{release_path} #{current_path}", :timeout => 10)
    end
  end
end
