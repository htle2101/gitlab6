module Deployers
  class Nodejs < War
    def name
      'build.zip'
    end

    def ext
      'zip'
    end
  end
end
