module Deployers
  class JavaService < War

    SLAVE_NEW_DEPLOY = 0
    SLAVE_ERROR_DEPLOY = 1
    MASTER_DEPLOY = 2 

    def check_deploy_mod
      if machine.master_deploy_error? 
        return MASTER_DEPLOY
      elsif machine.slave_machine.blank?
        return SLAVE_NEW_DEPLOY
      else 
        return SLAVE_ERROR_DEPLOY  
      end  
    end  

    def deploy
      deploy_mod = check_deploy_mod
      case deploy_mod
      when MASTER_DEPLOY
        return simple_deploy
      when SLAVE_NEW_DEPLOY
        return slave_new_machine_deploy
      when SLAVE_ERROR_DEPLOY
        return slave_machine_deploy
      end  
      return false
    end

    def slave_new_machine_deploy
      if apply_slave_machine
        machine.update_status('deploying')
        slave_simple_deploy ? (simple_deploy ? (machine.update_status('deploy success') and machine.slave_machine.releaser.release ) : machine.update_status('master error slave success') ) : machine.update_status('pre deploy error')
      else
        simple_deploy
      end  
    end

    def slave_machine_deploy
      machine.update_status('deploying')
      slave_simple_deploy ? (simple_deploy ? (machine.update_status('deploy success') and machine.slave_machine.releaser.release ) : machine.update_status('master error slave success') ) : machine.update_status('pre deploy error')
    end

    def slave_simple_deploy
      machine.slave_machine.deployer_instance(logger_file: @logger_file_name).deploy
    end  

    def apply_slave_machine
      # begin
        virtual_machine = VirtualMachine.applicable_machine('JavaServicePre')
        sudo_account = ::RemoteCi::Command::SudoAccount.new(virtual_machine.ip, port: virtual_machine.port).up
        logger.info("Create sudo account for `#{virtual_machine.ip}`, #{sudo_account[:account]}, #{sudo_account[:password]}.")
        ::RemoteCi::Command::KeyCopy.new(virtual_machine.ip, port: virtual_machine.port, envs: ['alpha']).execute
        logger.info("Copy jenkins server ssh keys to authorized_keys to `#{virtual_machine.ip}`.")

        ::RemoteCi::Command::JbossConf.new(virtual_machine.ip, port: virtual_machine.port, module_name: warName).up
        logger.info("Configure ROOT.xml to define the war #{warName} location at `#{virtual_machine.ip}`.")
        attrs = {project_id: ci_branch.project.id, build_branch_id: ci_branch.id, module_name: module_name, package_type: machine.package_type, status: 1, username: sudo_account[:account], password: sudo_account[:password]}
        virtual_machine.update_attributes(attrs)
        return true
      # rescue
      #   logger.error("apply slave machine error . skip pre deploy")
      #   return false
      # end    
    end  

  end
end