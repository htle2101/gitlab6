module Deployers
  class Phoenix < War
    def deploy
      logger.info("Loggin to Phoenix system to deploy war!")
      logger.error("Machine or Ip not assigned.") and return false if machine.blank?
      begin
        machine.update_status(phoenix_call)
      rescue => e
        machine.update_status('deploy error')
        logger.error("Deploy Failed when Call phoenix, error message: #{e}.")
        return false
      end
      true
    end

    protected

    def phoenix_call
      machine.update_status("deploying")
      logger.info("Start to Call Phoenix.")
      uri = URI("http://#{machine.ip}:7463/phoenix/agent")
      app_default_domain = Rails.cache.read(ci_branch.id.to_s + '_app_default_domain')
      params ={ param: {"wars" => ci_branch.war_urls_for_phoenix,
                        "defaultVirtualServer" => app_default_domain.blank? ? "main" : app_default_domain,
                        "forceDeploy" => true,
                        "returnUrl" => "http://#{Settings.dp['releases_ip']}/api/v3/ci/#{ci_branch.jenkins_job_name}/phoenix_result",
                        "privateToken" => "N6snsuvps12PHWNqqz1q" }.to_json }
      uri.query = URI.encode_www_form(params)
      res = Net::HTTP.get_response(uri)

      logger.error("Some error occured on Phoenix. Http request return #{res.code}") unless res.code == "200"
      logger.info("Call Phoenix Finished. Wait For Phoenix callback to change deploy status!") if res.code == "200"
      (res.code == "200") ? 'deploying' : 'deploy error'
    end
  end
end
