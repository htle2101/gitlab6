#coding:utf-8
module Deployers
  class SonarShadow < War
    def get_machine
      cmdb_machine = VirtualMachines::LinkContext.new({ build_branch_id: ci_branch.id, module_name: module_name, deployer: 'SonarShadow' }).execute
      cmdb_machine.ip = ip_from_cmdb
      cmdb_machine
    end

    def ip_from_cmdb
      begin
        Timeout::timeout(1) do
          uri = URI(Settings['cmdb_machine'])
          params = { 'q' => "应用名称:#{warName}", 'fq' => "env:#{ci_env.cmdb_name}" }
          uri.query = URI.encode_www_form(params)
          res = Net::HTTP.get_response(uri)
          JSON.parse(res.body)['result'][0]['private_ip'][0]
        end
      rescue
        nil
      end
    end
  end
end