module Deployers
  class Default < War

  	def initialize(type, args = {})
      @type = type
      @args = args
  	end

    def ext
      type.downcase
    end
  end
end
