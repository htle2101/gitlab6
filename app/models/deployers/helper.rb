module Deployers
  module Helper
    PHOENIX_MODULE = "current_branch"

    def shadow_branch
      BuildBranch.where( type: "ShadowBranch", project_id: self.project_id, branch_name: self.branch_name).try(:first)
    end

    def sonar_branch
      BuildBranch.where( type: "SonarBranch", project_id: self.project_id, branch_name: self.branch_name).try(:first)
    end

    def deployer_for(deployer)
      send "deployer_for_#{deployer.downcase}"
    end

    def wars
      @wars ||= begin
        modules.inject([]) do |wars, m|
          if m[:type] == 'war' || m[:type] == 'zipfile' || m[:type] == 'rar'
            if ['Cmdb', 'CmdbShadow','SonarShadow', 'CmdbZipfile'].include? self.deployers
              machine = VirtualMachines::LinkContext.new({ build_branch_id: self.id, module_name: m[:module_name], deployer: self.deployers }).execute
            else
              machine = machine_for_module(m.module_name)
            end
            wars << Deployer.new(machine.deployer, m.raw.merge(:ci_branch => self)) unless machine.blank?
          end
          wars
        end
      end
    end

    alias_method :deployer_for_war, :wars
    alias_method :deployer_for_cmdb, :wars
    alias_method :deployer_for_sonar, :wars

    def deployer_for_php
      deployers = []

      modules.map do |m|
        machine = machine_for_module(m.module_name)

        deployers << Deployer.new(machine.deployer, m.raw.merge(ci_branch: self)) if machine
      end

      deployers
    end

    alias_method :deployer_for_cmdbphp, :deployer_for_php

    def jar(module_name)
      VirtualMachines::LinkContext.new({ build_branch_id: self.id, module_name: module_name, deployer: 'Jar' }).execute
      deployer_for_jar.find{|j| j.module_name == module_name }
    end

    def destory_hook_path
      token = User.find_by_name('Administrator').authentication_token
      "http://#{RemoteCi::CiPostBuild.host_with_port}/api/v3/ci/destroy?private_token=#{token}"
    end

    def war_urls_for_phoenix
      modules_to_deploy = Rails.cache.read(self.id.to_s + '_self_modules_to_deploy_for_phoenix')
      urls =  self.modules_with_type("war").map {|pom_module|
                if modules_to_deploy.include? ',' + pom_module[:module_name] + ','
                  url_for_module(self, pom_module)
                end
              }
      deps_urls = self.deps.map do |dep|
        if dep.target_ci_branch
          pom_module = dep.target_ci_branch.modules_with_type("war").detect{|m| m[:module_name] == dep.module_name }
          url_for_module(dep.target_ci_branch, pom_module)
        end
      end
      urls.concat(deps_urls)
      urls.reject { |url| url.blank? }
    end

    def deployer_for_phoenix
      [Deployer.new(self.deployers, { ci_branch: self, module_name: PHOENIX_MODULE })]
    end

    def deployer_for_jar
      modules.inject([]) do |jars, m|
        # if ['jar', 'pom'].include?(m[:type])
        #   debugger
        jars << Deployer.new('Jar', m.raw.merge(:ci_branch => self)) if ['jar', 'pom', 'zip'].include?(m[:type])
        # end
        jars
      end
    end

    def deployer_for_javaservice
      self.machines.where(deployer: 'JavaService').map{|m| m.deployer_instance }
    end

    def url_for_module(ci_branch, pom_module)
      "http://#{ci_branch.ci_env.url}/job/" + ci_branch.jenkins_job_name + "/ws" + pom_module[:path] + "/target/" + pom_module[:warName] + "-#{ci_branch.ci_env.label}-" + pom_module[:version] + ".war"
    end
  end
end
