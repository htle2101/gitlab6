class CiTemplate < ActiveRecord::Base
  belongs_to :namespace
  belongs_to :ci_env
  attr_accessible :comment, :package_type, :template_name, :visable, :deployers, :ci_env_name

  scope :is_visable, -> { where(visable: true) }

  def template(ci_branch, xml)
    doc = Nokogiri::XML(xml)
    # doc = Nokogiri::XML(RolloutBranch.last.ci_env.server.job.get_config('product-cytest-xxx'))
    case deployers
    when "Static"
      git_url = doc.xpath("/project/scm/userRemoteConfigs/hudson.plugins.git.UserRemoteConfig/url")[0]
      git_branch = doc.xpath("/project/scm/branches/hudson.plugins.git.BranchSpec/name")[0]
      git_deloy = doc.xpath("/project/builders/hudson.tasks.Shell/command")[0]
    when 'Nodejs', 'Zipfile', 'Php', 'CmdbPhp', 'CmdbZipfile'
      git_url = doc.xpath("/project/scm/userRemoteConfigs/hudson.plugins.git.UserRemoteConfig/url")[0]
      git_branch = doc.xpath("/project/scm/branches/hudson.plugins.git.BranchSpec/name")[0]
      git_deloy = doc.xpath("/project/builders/hudson.tasks.Shell/command")[1]

      git_deloy.content = RemoteCi::CiPostBuild.command(ci_branch)
    else
      git_url = doc.xpath("/maven2-moduleset/scm/userRemoteConfigs/hudson.plugins.git.UserRemoteConfig/url")[0]
      git_branch = doc.xpath("/maven2-moduleset/scm/branches/hudson.plugins.git.BranchSpec/name")[0]
      git_deloy = doc.xpath("/maven2-moduleset/postbuilders/hudson.tasks.Shell/command")[0]

      git_deloy.content = RemoteCi::CiPostBuild.command(ci_branch)
    end

    git_url.content = ci_branch.project.full_ssh_url_to_repo
    git_branch.content = "origin/#{ci_branch.branch_name}"


    doc.to_s
  end

  def switch_ref(packing, xml)
    doc = Nokogiri::XML(xml)

    if packing.rollout_branch.java?
      git_branch = doc.xpath("/maven2-moduleset/scm/branches/hudson.plugins.git.BranchSpec/name")[0]
      git_deloy = doc.xpath("/maven2-moduleset/postbuilders/hudson.tasks.Shell/command")[0]
    elsif packing.rollout_branch.nodejs? || packing.rollout_branch.zipfile? || packing.rollout_branch.php? || packing.rollout_branch.cmdb_php?
      git_branch = doc.xpath("/project/scm/branches/hudson.plugins.git.BranchSpec/name")[0]
      git_deloy = doc.xpath("/project/builders/hudson.tasks.Shell/command")[1]
    end

    git_branch.content = "origin/#{packing.branch_name}"
    git_deloy.content = RemoteCi::CiPostBuild.command(packing)

    doc.to_s
  end

  def self.change_ref(ref, xml)
    doc = Nokogiri::XML(xml)
    git_branch = doc.xpath("/maven2-moduleset/scm/branches/hudson.plugins.git.BranchSpec/name")[0]
    git_branch.content = "origin/#{ref}"
    doc.to_s
  end

  def change_static_ref(ref, xml)
    doc = Nokogiri::XML(xml)
    git_branch = doc.xpath("/project/scm/branches/hudson.plugins.git.BranchSpec/name")[0]
    git_branch.content = "origin/#{ref}" if !git_branch.nil?
    doc.to_s
  end

  def self.change_post_step(branch, xml)
    doc = Nokogiri::XML(xml)

    if branch.java?
      git_deloy = doc.xpath("/maven2-moduleset/postbuilders/hudson.tasks.Shell/command")[0]
      git_deloy.content = RemoteCi::CiPostBuild.command(branch)
    elsif branch.nodejs? || branch.zipfile? || branch.php?
      git_deloy = doc.xpath("/project/scm/branches/hudson.plugins.git.BranchSpec/name")[0]
      git_deloy.content = RemoteCi::CiPostBuild.command(branch)
    end

    doc.to_s
  end

  def self.change_git_url(new_git_url, xml, deployers)
    doc = Nokogiri::XML(xml)
    case deployers
    when  "Static", "Nodejs", "Zipfile", 'Php', 'CmdbPhp', 'CmdbZipfile'
      git_url = doc.xpath("/project/scm/userRemoteConfigs/hudson.plugins.git.UserRemoteConfig/url")[0]
    else
      git_url = doc.xpath("/maven2-moduleset/scm/userRemoteConfigs/hudson.plugins.git.UserRemoteConfig/url")[0]
    end
    git_url.content = new_git_url
    doc.to_s
  end

  def self.change_jdk_version(version, xml)
    doc = Nokogiri::XML(xml)
    doc.search("//jdk").each do |node|
      node.remove
    end  

    # JDK1.6
    # JDK1.7.0_72
    Nokogiri::XML::Builder.with(doc.root) do |xml|
      xml.jdk version
    end

    doc.to_s
  end

end
