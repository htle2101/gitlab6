class Package < ActiveRecord::Base
  attr_accessible :package_type, :name, :packing, :packing_module, :project, :tag, :ticket, :url,:project_id,:packing_id,:packing_module_id,:rollback_to_url,:warname

  belongs_to :packing, class_name: "RolloutPacking", foreign_key: "packing_id"
  belongs_to :packing_module, class_name: "RolloutPackingModule", foreign_key: "packing_module_id"
  belongs_to :project
  belongs_to :ticket

  has_and_belongs_to_many :tasks, join_table: "tasks_packages"
  has_many :grouping_strategies
  has_many :paas_rollouts

  validates_presence_of :project, :name
  #validates_uniqueness_of :name, scope: :ticket_id, message: "can't add same package to one ticket"

  OWNER = { task: 1, ticket: 2 }

  scope :owner_is, ->(owner) { where(owner: OWNER[owner]) }
  scope :type_is, ->(type) { where(package_type: type) }
  scope :type_is_not, ->(type) {where("package_type NOT IN (?) or package_type is NULL", type)}

  def owner_is?(owner)
  	self.owner == OWNER[owner]
  end

  def can_change_tag?
    return true if grouping_strategies.blank?
    grouping_strategies.inject(false) do |result, gs|
      result |= gs.not_deploy?
      result &= !gs.in_progress?
    end
  end

  def tasks_ready?
    return true if self.ticket.package_tasks.blank?
    @tasks_ready ||= (self.ticket.package_tasks.find{|t| !t.release_ready? }.blank? ? true :false)
  end

  def log_params
    Utils.wrap_common_params({
      package_id: id,
      ticket_id: ticket_id,
      project_id: project_id,
      project_name: project.try(:name),
      project_path: project.try(:path_with_namespace),
      group_id: project.try(:group).try(:id),
      group_name: project.try(:group).try(:name)
    })
  end

  def module_name_or_war_name
    return self.name unless (Service::Op::Cmdb.new.grouping_strategy(self.name) rescue nil).blank?
    warName = self.warname || self.project.tree(self.packing.branch_name).pom.detect{|p|p if p[:module_name] == self.name}.try(:[], :warName) || ""
    Service::Op::Cmdb.new.grouping_strategy(warName).blank? ? self.name : warName
  end

  def all_swimlanes
    gs_http_response = Service::Op::Cmdb.new.grouping_strategy(self.module_name_or_war_name)
    all_swimlanes = []
    return nil if gs_http_response.blank?
    gss = gs_http_response["groups"]
    gss.each do |gs|
      if gs["id"] > 0
        all_swimlanes.push(gs["swimlane"])
      end
    end
    #all_swimlanes.uniq.reject{|s| s.blank? || s=~/none/i}
    all_swimlanes.uniq.reject{|s| s.blank?}
  end

  def groups_of_swimlane(swimlane)
    gs_http_response = Service::Op::Cmdb.new.grouping_strategy(self.module_name_or_war_name)
    groups = []
    return nil if gs_http_response.blank?
    gss = gs_http_response["groups"]
    gss.each do |gs|
      if gs["swimlane"] == swimlane
        groups.push(gs)
      end
    end
    groups
  end

  def has_paas?
    (Settings.paas['open'] && self.project.project_modules.find_by_module_name(name).has_paas) rescue false
  end 

  def project_module
    self.project.project_modules.find_by_module_name(name)
  end

  def true_name
    self.warname || self.name
  end

  # def paas_group
  #   DianpingPaas::Client.new(url: project_module.paas_url).groups(true_name) rescue nil
  # end  
end
