class ShadowBranch < BuildBranch
  include Deployers::Helper

  alias_method :deployer_for_cmdbshadow, :wars
end
