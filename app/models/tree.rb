class Tree
  attr_accessor :raw

  def initialize(repository, sha, ref = nil, path = nil)
    @raw = Gitlab::Git::Tree.new(repository, sha, ref, path)
  end

  def pom
    @pom ||= pom_parser.pom_parser('root')
  end

  def pom_parser
    @pom_parser ||= RemoteCi::TypeParser.new(self.raw.raw_tree)
  end

  def error_message
    @error_messgae = pom_parser.error_message
  end

  def hashed_pom
    @hashed_pom ||= pom.inject({}) do |module_list, m|
        module_list[m[:module_name]] = m
        module_list
      end
  end

  def pom_root_type
    root = pom[0]
    return nil if root.nil?
    root['type'] == 'jar' ? 'jar' : 'war'
  end

  def pom_full_name
    pom[0][:full_name]
  end

  def method_missing(m, *args, &block)
    @raw.send(m, *args, &block)
  end

  def respond_to?(method)
    return true if @raw.respond_to?(method)

    super
  end
end
