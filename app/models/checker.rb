class Checker < ActiveRecord::Base
  belongs_to :task
  belongs_to :author, class_name: "User", foreign_key: "author_id"
  belongs_to :assignee, class_name: "User", foreign_key: "assignee_id"
  attr_accessible :checked, :description, :assignee_id, :task_id 

  scope :cared, ->(user) { where('assignee_id = :user OR author_id = :user', user: user.id) }
  scope :of_group, ->(group) { where(task_id: Task.where(project_id: group.project_ids).pluck(:id)) }
  scope :ischecked, -> { where(checked: true) }
  scope :unchecked, -> { where(checked: false) }
  scope :after_today, -> { where(task_id: Task.after_today.pluck(:id)) }
  def can_confirm_by(user)
    self.assignee == user ? true : false
  end  
  
end
