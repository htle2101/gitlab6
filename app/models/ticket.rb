class Ticket < ActiveRecord::Base

  attr_accessible :released_at, :project_id

  belongs_to :project
  has_many :packages
  has_many :grouping_strategies

  validates_uniqueness_of :released_at, scope: :project_id

  validate :released_at_is_a_valid_date?
  validate :released_at_has_been_taken?, :on => :create

  def released_at_is_a_valid_date?
    errors.add(:base, "Release date isn't a valid date.") if released_at.blank?
  end

  def released_at_has_been_taken?
    errors.add(:base, "Release date has been taken.") if self.class.exists?(project_id: project_id, released_at: released_at)
  end

  def can_edit?(user)
    user.admin?
    # true
  end

  def can_destroy?(user)
    true
  end

  def tasks
    @tasks ||= Task.where(released_at: self.released_at)
  end

  def package_tasks
    Task.where(id: TasksPackage.where(package_id: self.packages.pluck(:id)).pluck(:task_id))
  end

  def configs
    @configs ||= TaskConfig.where(task_id: tasks.map(&:id))
  end

  # module_names array like ['project_id|module_name']
  def add_wars(module_names)
    self.errors.add(:add_wars, "You don't select any packages to add!") and return false if module_names.blank?
    Package.transaction do
      module_names.each do |module_name|
        project_id, name = module_name.split("|")
        package = self.packages.where(name: name, project_id: project_id).first
        # self.errors.add(:add_wars, "he selected wars has been exist in relative tasks,can't be added anymore") if package.try(:owner_is?, :task) and self.errors.messages[:add_wars].blank?
        next unless package.blank?
        package = self.packages.owner_is(:ticket).new(name: name, project_id: project_id) and package.save
        Logbin.logger("rollout").info("Add independent war [#{name}] to ticket ##{self.id}.", package.log_params)
      end
    end
    return true
  end

  state_machine :status, :initial => :unreleased do
    state :unreleased, :value => 1
    state :releasing, :value => 2
    state :released,  :value => 3
    state :expired,   :value => 4

    event :to_releasing do
      transition :unreleased => :releasing
    end

    event :to_released do
      transition :releasing => :released
    end

    event :to_expired do
      transition :unreleased => :expired
    end
  end
end
