# == Schema Information
#
# Table name: issues
#
#  id           :integer          not null, primary key
#  title        :string(255)
#  assignee_id  :integer
#  author_id    :integer
#  project_id   :integer
#  created_at   :datetime
#  updated_at   :datetime
#  position     :integer          default(0)
#  branch_name  :string(255)
#  description  :text
#  milestone_id :integer
#  state        :string(255)
#

class Issue < ActiveRecord::Base

  include Issuable

  belongs_to :project
  validates :project, presence: true

  scope :of_group, ->(group) { where(project_id: group.project_ids) }
  scope :of_user_team, ->(team) { where(project_id: team.project_ids, assignee_id: team.member_ids) }
  scope :opened, -> { with_state(:opened) }
  scope :closed, -> { with_state(:closed) }
  scope :unassigned, -> { where('assignee_id is NULL') }
  scope :assigned, -> { where('assignee_id is not NULL') }

  attr_accessible :title, :assignee_id, :position, :description,
                  :milestone_id, :label_list, :author_id_of_changes,
                  :state_event, :project_id, :main_label_list

  acts_as_taggable_on :labels, :main_labels

  scope :cared, ->(user) { where('assignee_id = :user OR author_id = :user', user: user.id) }
  scope :open_for, ->(user) { opened.assigned_to(user) }

  state_machine :state, initial: :opened do
    event :close do
      transition [:reopened, :opened] => :closed
    end

    event :reopen do
      transition closed: :reopened
    end

    state :opened

    state :reopened

    state :closed
    state :rejected
  end

  # Both open and reopened issues should be listed as opened
  scope :opened, -> { with_state(:opened, :reopened) }
  scope :rejected, -> { with_states(:rejected) }
end
