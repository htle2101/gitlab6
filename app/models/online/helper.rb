#coding:utf-8
module Online
  module Helper
    def change_jenkins_and_build(packing)
      self.update_attributes(branch_name: branch_name)
      jenkins = ci_env.server
      jenkins.job.post_config(jenkins_job_name, ci_template.switch_ref(packing, jenkins.job.get_config(jenkins_job_name)))
      jenkins.job.build(jenkins_job_name)
    end

    def change_static_jenkins_and_build(branch_name)
      self.update_attributes(branch_name: branch_name)
      jenkins = ci_env.server
      jenkins.job.post_config(jenkins_job_name, ci_template.change_static_ref(branch_name, jenkins.job.get_config(jenkins_job_name)))
      jenkins.job.build(jenkins_job_name, "NOPUBLISH" => nil) rescue jenkins.job.build(jenkins_job_name)
    end

    def change_static_jenkins_and_nopublish_build
      jenkins = ci_env.server
      jenkins.job.build(jenkins_job_name, "NOPUBLISH" => 1)
    end  

    def branch_name
      if project.repository.commit(self['branch_name'])
        self['branch_name']
      else
        project.default_branch
      end
    end

    def can_delete?
      self.static? && !self.job_running?
    end

    def paas_ips(app, env)
      return [] if env == 'prelease'
      paas_urls = ProjectModule.select("distinct paas_url").map(&:paas_url)
      #another paas console down
      paas_urls = ["http://10.101.0.11:8080/"]
      paas_urls.map{ |pu| DianpingPaas::Client.new(url: pu).groups(app)['groups'].select{|g| g['hostname'] != nil }.map{|g| g['hostname'] } if !DianpingPaas::Client.new(url: pu).groups(app)['groups'].nil?}.flatten.compact!
    end

    def clear_cache(apps, env)
      return { success: false, msg: "please select apps" } if apps.blank?
      cache_server, cmdb_env = Service::Cache::CachePre.new, 'prelease' if env == 'prelease'
      cache_server, cmdb_env = Service::Cache::Cache.new, 'deploy' if env == 'release'

      apps.each do |app|
        begin
          ips = (Service::Op::Cmdb.new.app_ips(app, cmdb_env) + (paas_ips(app, cmdb_env) || [])).uniq.join(',')
          cache_server.static_category.each do |category|
            response = cache_server.clear_cache(category, ips)
          end
        rescue Service::Op::CmdbServerConnectError, Service::Op::CmdbInterfaceAccessError, Service::Op::LzmServerConnectError, Service::Op::LzmInterfaceAccessError, Service::Cache::CacheServerConnectError, Service::Cache::CacheInterfaceAccessError => e
          return { success: false, msg: "#{app} 清楚缓存失败. #{e.class},#{e.message}" }
        end
      end

      return { success: true, msg: "清楚缓存成功. 如果实际清除未成功, 需要自行重启应用服务, 缓存服务相关bug正在改进中." }
    end

    def clear_ip_cache(ips, env)
      return { success: false, msg: "please select ip" } if ips.blank?
      cache_server, cmdb_env = Service::Cache::CachePre.new, 'prelease' if env == 'prelease'
      cache_server, cmdb_env = Service::Cache::Cache.new, 'deploy' if env == 'release'
      begin
        cache_server.static_category.each do |category|
          response = cache_server.clear_cache(category, ips)
        end
      rescue Service::Op::CmdbServerConnectError, Service::Op::CmdbInterfaceAccessError, Service::Op::LzmServerConnectError, Service::Op::LzmInterfaceAccessError, Service::Cache::CacheServerConnectError, Service::Cache::CacheInterfaceAccessError => e
        return { success: false, msg: "#{ips} clear cache fail.#{e.class},#{e.message}" }
      end

      return { success: true, msg: 'clear cache success.' }
    end  
  end
end
