class GsStatistic < ActiveRecord::Base
  attr_accessible  :gs_id, :started_at, :finished_at, :package_id, :project_id, :status, :tag, :ticket_id, :time_interval,:appname,:group_percent,:namespace_id,:time_interval_int

  belongs_to :project, foreign_key: "project_id"
end
