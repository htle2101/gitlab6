  #coding:utf-8
class PaasRollout < ActiveRecord::Base
  belongs_to :project
  belongs_to :package
  attr_accessible :released_on, :status, :tag, :project_id, :operation_id, :appname, :name, :ticket_id,:package_id, :group_id,:package,:hostname

  attr_accessor :logger

  include Utils::Logger

  scope :prerelease, -> { where(group_id: -255)}
  scope :not_succeed, -> { with_statuses(:not_deploy,  :deploying, :deploy_fail)}
  scope :running, -> { with_statuses(:deploying,:deploying_war)}

  state_machine :status, :initial => :not_deploy do
    state :not_deploy,           :value => 0,     :human_name => "未发布"
  
    state :deploying,            :value => 100,     :human_name => "发布中"
    state :deploy_success,       :value => 200,     :human_name => "发布成功"
    state :deploy_fail,          :value => 500,    :human_name => "发布失败"

    state :unknown_deploy,       :value => -2,    :human_name => "未触发的部署"
    state :deploying_war,        :value => 101,   :human_name => "上传war包中"
    state :deploy_war_success,   :value => 102,   :human_name => "上传war包成功"
    state :deploy_war_fail,      :value => 103,   :human_name => "上传war包失败"
    state :wait_for_deploy,      :value => 104,   :human_name => "等待部署中"

    event :succeed do
      transition :deploying => :deploy_success
      transition :deploying_war => :deploy_war_success
    end

    event :waited do
      transition [:not_deploy, :deploy_success] => :wait_for_deploy
    end

    event :to_deploy_war do
      transition all - [:deploying_war, :deploy_war_success, :deploy_success] => :deploying_war
    end

    event :failed do
      transition :deploying_war => :deploy_war_fail
      transition :deploying => :deploy_fail
    end

    event :to_deploy do
      transition :deploy_fail => :deploying
      transition :deploy_war_success => :deploying
    end

    before_transition any => [:deploy_success] do |gs, transition|
      gs.released_on = Time.now
      Service::AutoTest::Request.new.call(gs.project_name_for_auto_test) rescue nil
    end

    event :reset do 
      transition all => :not_deploy
    end
  end

  def project_name_for_auto_test
    'product' + '-' + self.project.namespace.path + '-' + self.project.path
  end

  def wants_paas_info?
    self.deploying?
  end  

  def reload_imf
    return unless wants_paas_info?
    imf = DianpingPaas::Client.new(url:ProjectModule.find_by_module_name_and_project_id(self.appname,self.project.id).paas_url).rollout_status(package.warname || self.package.name, self.operation_id)
    @log = imf['data']['log']
    self.status = imf['data']["operationStatus"].to_i
    logger.info "paas deploy status to #{self.status_name}"
    self.save
  rescue DianpingPaas::PaasServerConnectError => e
    logger.error "paas connect timeout."
    false
  rescue DianpingPaas::PaasRequestError => e
    logger.error e.to_s
    false
  end

  def has_success_prelease?
    @pr_pre = self.package.grouping_strategies.detect{|gs|gs.group_id == -255 and gs.ticket_id.to_s == self.ticket_id.to_s and gs.appname == self.appname and gs.tag == self.tag}
    return true if @pr_pre.blank?
    return @pr_pre.status == 4 ? true : false
  end

  def paas_log
    return nil if self.operation_id.blank?
    imf = DianpingPaas::Client.new(url:ProjectModule.find_by_module_name_and_project_id(self.appname,self.project.id).paas_url).rollout_status(package.warname || package.name, self.operation_id)
    @log = imf['data']['log']
  rescue DianpingPaas::PaasServerConnectError => e
    logger.error "paas connect timeout. when read log"
    false
  rescue DianpingPaas::PaasRequestError => e
    logger.error e.to_s
    false
  end

  def deploy_war
    return false unless can_to_deploy_war?
    self.update_attributes(operation_id: nil)
    self.to_deploy_war
    result = RemoteCi::Ftp.new(self, self.package).upload_to_paas
    result ? self.succeed : self.failed
    return result
  end

  def full_deploy
    call_paas if deploy_war
  end

  def call_paas
    self.to_deploy
    logger.info "call paas to deploy."
    data = DianpingPaas::Client.new(url:ProjectModule.find_by_module_name_and_project_id(self.appname,self.project.id).paas_url).rollout(package.warname || package.name, paas_version,self.group_id)
    if data.try(:[], 'data').try(:[], 'operationId').blank?
      logger.error "paas don't  return the operationId"
      self.failed
      return
    end
    self.update_attributes(operation_id: data['data']['operationId'])
    logger.info "wait paas deploying."
  rescue DianpingPaas::PaasServerConnectError, DianpingPaas::PaasRequestError => e
    self.failed
    logger.info "call paas error. #{e}"
  end

  def paas_version
    self.tag
    # return nil if packing_module.blank?
    # packing_module.paas_version
  end

  def packing_module
    packing = self.project.rollout_branch.packings.find_by_tag(self.tag)
    return nil if packing.blank? 
    packing.modules.find_by_name(package.name)
  end

  def url
    packing_module.try(:url)
  end

  def logger_file
    "paasrollout/#{self.id}.log"
  end

  def has_version?
    !paas_version.blank?
  end

  def in_progress?
    self.deploying? || self.deploying_war?
  end

  def can_deploy?
    [:not_deploy, :deploy_fail, :deploy_war_fail, :unknown_deploy].include?(self.status_name) && has_version? && check_pre_deploy && check_related_running && has_success_prelease?
  end

  def can_reset?
    [:deploy_success].include?(self.status_name) && has_version? && check_pre_deploy && check_related_running && has_success_prelease?
  end

  def can_do?
    can_deploy? || can_reset?
  end

  def check_pre_deploy
    related_gs.prerelease.blank? || related_gs.prerelease.where(tag: self.tag).not_succeed.blank? 
  end

  def display_confirm_or_not_when_deploy?
    @gss = GroupingStrategy.where(:ticket_id => self.ticket_id, :appname => self.appname, :package_id => self.package_id,:tag=> self.tag)
    @prs = PaasRollout.where(:ticket_id => self.ticket_id,:appname => self.appname,:package_id => self.package_id,:tag => self.tag)
    if self.status == 6
      if @gss.collect{|gs|gs.status == 6}.count(true) > 1 || @prs.collect{|pr|pr.status == 6}.count(true) > 1
        return true
      else
        return false
      end
    else
      if @gss.collect{|gs|gs.status == 6}.count(true) >= 1 || @prs.collect{|pr|pr.status == 6}.count(true) >= 1
        return true
      else
        return false
      end
    end

  end

  def check_related_running
    related_gs.running.blank?
  end

  def related_gs
    self.package.paas_rollouts
  end

  def deploy
    if [:not_deploy, :wait_for_deploy, :deploy_success, :deploy_war_fail, :unknown_deploy].include? self.status_name
      full_deploy
    elsif can_deploy?
      call_paas
    end
  end

  def logger
    self.logger_file = logger_file
    super
  end
end
