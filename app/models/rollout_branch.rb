#coding:utf-8
class RolloutBranch < BuildBranch
  attr_accessor :static_rollback_error

  has_many :packings, foreign_key: "build_branch_id", class_name: "RolloutPacking", dependent: :destroy
  validates :package_type, :presence => { :message => "You need to create your beta ci first." }

  include Online::Helper

  def command
    "upload"
  end

  def realname
    self.static? ? 'product' : '打包'
  end

  def commit_equal_and_ppe_result_success?(branch_name)
    #return true if ppe's commit equals with rollout and ppe's ci_build_status is success
    prod_commit = self.project.repository.commit(branch_name).id[0..9]
    return false if self.project.ppe_branch.blank?
    ppe_with_prod_commit = self.project.ppe_branch.imf.jenkins_datasource.get_build_revisions['builds'].select{|v|v["revision"] == prod_commit}
    if !ppe_with_prod_commit.blank?
      last_ppe_with_prod_commit =  ppe_with_prod_commit.first
    else
      return false
    end
    last_ppe_with_prod_commit["result"] == "SUCCESS" ? true : false
  end

  def static_versions
    return [] if !self.static? || self.appname.blank?
    @static_versions ||= Service::Nodejs::Nodejs.new.versions(appname)
  rescue Service::Nodejs::NodejsConnectError
    @static_rollback_error = true
    @static_versions = ["connect to rollback server error"]
  rescue Service::Nodejs::NodejsAccessError 
    @static_rollback_error = true
    @static_versions = ["request rollback server return error"] 
  end

  def static_rollout(from, to)
    return { status: false, message: 'appname is blank' } if self.appname.blank?
    Service::Nodejs::Nodejs.new.rollback(appname, from, to)
    { status: true, message: 'rollback success' }
  rescue Service::Nodejs::NodejsConnectError
    { status: false, message: 'connect to rollbakc server error' }
  rescue Service::Nodejs::NodejsAccessError => e
    { status: false, message: e.message }
  end  

  def can_static_rollback?
    static_versions
    @static_rollback_error != true
  end


  def static_rollout_history
    return [] if !self.static? || self.appname.blank?
    @static_histories ||= Service::Nodejs::Nodejs.new.histories(appname)
  rescue Service::Nodejs::NodejsConnectError
    @static_histories = []
  rescue Service::Nodejs::NodejsAccessError 
    @static_histories = []
  end

 

  # def reload_conf
  #   if self.java? && project.branch_for_sonar.present? && self.package_type != project.branch_for_sonar.try(:package_type)
  #     doc = Nokogiri::XML(current_config)
  #     shell = doc.xpath('/project/builders/hudson.tasks.Shell/command')[0]

  #     template_shell = Nokogiri::XML(job.get_config(self.ci_template.template_name)).xpath('/project/builders/hudson.tasks.Shell/command')[0].content
  #     shell.content = template_shell
  #     post_config(doc.to_s)
  #   end  
  # end
end
