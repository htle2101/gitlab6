# encoding: utf-8
class BetaPaasRollout < ActiveRecord::Base
  belongs_to :project

  include Utils::Logger
  include Utils::Time


  belongs_to :build_branch, foreign_key: "build_branch_id"
  attr_accessible :build_branch_id, :group_id, :hostname, :machine, :operation_id, :released_on, :status, :tag, :appname,:paas_version

  scope :running, -> { with_statuses(:deploying,:deploying_war)}
  scope :operated, -> {where(" operation_id is not null") }

  state_machine :status, :initial => :not_deploy do
    state :not_deploy,           :value => 0,     :human_name => "未发布"  
    state :deploying,            :value => 100,     :human_name => "发布中"
    state :deploy_success,       :value => 200,     :human_name => "发布成功"
    state :deploy_fail,          :value => 500,    :human_name => "发布失败"
    state :uploading_war,        :value => 101,    :human_name => "上传war包中"
    state :upload_war_success,   :value => 102,   :human_name => "上传war包成功"
    state :upload_war_fail,      :value => 103,   :human_name => "上传war包失败"

    event :to_deploy_war do
      transition all - [:deploying_war, :deploy_war_success, :deploy_success] => :deploying_war
    end

    event :succeed do
      transition :deploying => :deploy_success
      transition :uploading_war => :upload_war_success
    end

    event :failed do
      transition :uploading_war => :upload_war_fail
      transition :deploying => :deploy_fail
    end

    before_transition any => [:deploy_success] do |bpr, transition|
      Service::AutoTest::Request.new.call(bpr.project_name_for_auto_test) rescue nil
    end

  end

  STATUS_LIST = {
    'not_deploy' => 0,
    'deploying' => 100,
    'deploy error' => 500,
    'deploy success' => 200,
    'uploading_war' => 101,
    'upload_war_success' => 102,
    'upload_war_fail' => 103,
  }

  def status_message
    STATUS_LIST.invert[self.status]
  end

  def project_name_for_auto_test
    'beta' + '-' + self.build_branch.project.namespace.path + '-' + self.build_branch.project.path
  end

  def can_deploy?
    [:not_deploy,:upload_war_success, :deploy_success, :deploying_war, :deploy_fail, :deploy_war_fail, :unknown_deploy].include?(self.status_name)
  end

  def can_rollback?
    bplog = BetaPaasLog.find_by_module_name_and_build_branch_id(self.appname,self.build_branch_id)
    !bplog.blank? ? true : false
  end

  def in_deploying?
    [:deploying,:uploading_war].include?(self.status_name)
  end

  def get_host
    Settings.op['beta_paas_url']
  end

  def deploy(paas_version)
    #shutdown&& remove instance
    create_instance(paas_version) if get_instance
    full_deploy(paas_version)
    # if [:not_deploy].include?self.status_name
    #   full_deploy(paas_version)
    # elsif can_deploy?
    #   call_paas(paas_version)
    # end
  end

  def logger_binder
    build_number = build_branch.ci_env.server.job.get_current_build_number(build_branch.jenkins_job_name)
    @logger_binder ||= RemoteCi::LoggerBinder.new(build_branch, { build_number: build_number, module_name: self.appname })
    if !$redis.get("logger_file").blank? && $redis.get("logger_file").include?("beta_paasrollout")
      @logger_file_name = logger_file(1,$redis.get("logger_file"))
      $redis.del("logger_file")
    else
      logger_file = self.logger_file
      @logger_binder.bind_beta_paas_log(logger_file, "paas"+self.appname)
      logger_file
    end
  end

  def paas_log
    return nil if self.operation_id.blank?
    imf = DianpingPaas::Client.new(url: get_host).rollout_status(self.appname, self.operation_id)
    @log = imf['data']['log']
    rescue DianpingPaas::PaasServerConnectError => e
      logger.error "paas connect timeout. when read log"
      false
    rescue DianpingPaas::PaasRequestError => e
      logger.error e.to_s
      false
  end

  def get_instance
    json = DianpingPaas::Client.new(url: get_host).instances(self.appname).body
    JSON.parse(json)['data'].blank? ? true : false
  end

  def create_instance(paas_version)
    url = "#{get_host}console/api/app?op=create&appId=#{self.appname}&version=#{paas_version}&number=1"
    begin
      RestClient.get(url)
    rescue RestClient::InternalServerError
    end
  end
  
  def create_beta_logs
    BetaDeployLog.create(project_id: build_branch.project_id,
                         branch_name: build_branch.branch_name,
                         project_path_with_namespace: build_branch.project.path_with_namespace,
                         module_name: self.appname,
                         date: Date.today)
  end

  def full_deploy(version)
    self.update_attributes(status:100)
    if BuildBranches::BetaPaasUploadContext.new(self.id).execute()
      call_paas(version)
    else
      self.update_attributes(status:0)
      logger.info 'package upload fail,finish deploy'
    end
  end

  def reload_log_imf(imf)
    if imf['data']['operationStatus'].to_i == 200 && imf['data']['log'].include?("upgrade")
      bplog = BetaPaasLog.find_or_create_by_module_name_and_tag_and_beta_paas_rollout_id(self.appname,self.tag,self.id)
      if bplog.status.nil?
        commit_message = Gitlab::Git::Commit.last_for_path(build_branch.project.repository, build_branch.branch_name, @path).message
        bplog.update_attributes(status:200,log:imf['data']['log'],build_branch_id:self.build_branch_id,commit_message:commit_message)
        bplog.save
      end
    end
  end

  def reload_imf
    begin
      imf = DianpingPaas::Client.new(url: get_host).rollout_status(self.appname, self.operation_id)
      @log = imf['data']['log']
      if imf['data']['operationStatus'].to_i != self.status
        mock_paas_callback(imf)
        if imf['data']["operationStatus"].to_i != 400 && imf['data']["operationStatus"].to_i != 200
          self.status = imf['data']["operationStatus"].to_i
        elsif imf['data']["operationStatus"].to_i == 200 && self.status != 200
          self.succeed
        end
        self.machine = get_ip_from_paas(self.appname) if !get_ip_from_paas(self.appname).blank? && !get_ip_from_paas(self.appname).include?('paas')
        reload_log_imf(imf)
        self.save
      end
      rescue DianpingPaas::PaasServerConnectError => e
        logger.error "paas connect timeout."
        false
      rescue DianpingPaas::PaasRequestError => e
        false
      end
  end

  def mock_paas_callback(imf)
    if !imf['data']['log'].nil? && self.status != imf['data']["operationStatus"].to_i && self.status != 0 && imf['data']['log'].include?("upgrade")
      log = self.build_branch.module_beta_last_logs(self.appname).last
      @logger_file_name = logger_file(1,log['location'])
      message = imf['data']["operationStatus"].to_i == 200  ? "paas deploy success" : "paas deploy error"
      logger.info "#{message}"
    end
  end

  def get_ip_from_paas(appname)
    url = get_host

    if url
      json = DianpingPaas::Client.new(url: url).instances(appname).body

      ips = begin
              JSON.parse(json)['data'].map { |data| data['instanceIp'] }
            rescue JSON::ParserError
              []
            end
    else
      raise 'No Paas URL found'
    end
    !ips.blank? && ips != [nil] ? ips.join(', ') : []
  end

  def packages
    @packages ||= begin
      build_branch.modules.inject([]) do |packages, m|
        if m.module_name == self.appname
          # 'nodejs'.classify => Nodej
          type = if m.type == 'nodejs'
                   'Nodejs'
                 else
                   m.type.classify
                 end
          packages << Deployer.new(type, m.raw.merge(ci_branch: build_branch))
        end
        packages
      end
    end
  end

  def call_paas(version)
    create_beta_logs
    status = true
    logger_binder
    self.update_attributes(status:100)
    logger.info "call paas to deploy"
    begin
      data = DianpingPaas::Client.new(url: get_host).rollout(self.appname, version,self.beta_app_group_id(self.appname))
      if data.try(:[], 'data').try(:[], 'operationId').blank?
        logger.error "paas don't return the operationId"
        self.failed
        return false
      end
      self.update_attributes(operation_id: data['data']['operationId'])     
      logger.info "paas is deploying"
      logger.info "wait paas callback"
      reload_imf
      true
    rescue DianpingPaas::PaasServerConnectError, DianpingPaas::PaasRequestError => e
      logger.info "call paas error. #{e}"
      self.failed and status = false
    end
  end

  def beta_app_group_id(name)
    groups = DianpingPaas::Client.new(url: get_host).groups(name) rescue nil
    groups['groups'].first['id']
  end

  def logger
    self.logger_file = logger_file
    super
  end

  def logger_file(status = 0, url = "")
    logger_file_name = status == 0 ? "beta_paasrollout/#{build_branch.jenkins_job_name}/#{self.appname}-#{ts}.log" : url
    @logger_file_name ||= logger_file_name
  end

  def paas_version
    Gitlab::Git::Commit.last_for_path(build_branch.project.repository, build_branch.branch_name, @path).sha
  end

  protected

  def logs
    commit = Gitlab::Git::Commit.last_for_path(build_branch.project.repository, build_branch.branch_name, @path).sha
    logs = BetaPaasRollout.where(:appname => self.appname, :tag => commit).operated
    logs = BetaPaasLog.where(:appname => self.appname, :tag => commit,:build_branch_id => self.build_branch.id)
  end

end
