class PerformanceTestModule < ActiveRecord::Base
  attr_accessible :app_type, :appname, :cpu, :domain_name, :memory, :package_url, :performance_test_id, :tag, :performance_test

  belongs_to :performance_test

end
