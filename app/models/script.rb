# encoding: UTF-8
class Script < ActiveRecord::Base
  attr_accessible :db_name, :desc, :path, :name, :project, :todo, :status, :author, :req_url, :ppe_status, :prd_status, :ppe_executed_at, :prd_executed_at

  validates_presence_of :db_name, :todo, :status, :project

  serialize :data

  belongs_to :project
  belongs_to :author, class_name: "User"

  scope :created_by, ->(user) { where(author_id: user.id) }

  TODO = ["上线前", "上线后", "开发通知"]
  ENV_TYPE = { prd: 1, ppe: 0 }

  class << self
    def download_url(path)
      "#{Settings.gitlab['url']}/#{Settings.scripts.download_relative_path}/#{path}"
    end
  end

  def can_edit?
    false
  end

  def can_close?
    self.status == 0
  end

  def can_execute?
    self.status == 2
  end

  def current_execute_type
    if self.can_execute_ppe?
      ENV_TYPE[:ppe]
    elsif self.ppe_execute_successed? && self.can_execute_prd?
      ENV_TYPE[:prd]
    end
  end

  def dba_upload_params
    {
      issue_id: self.id, file_name: self.name, todo: self.todo, db: self.db_name,
      file_link: Script.download_url(self.path), file_id: self.id, project: self.project.try(:name), desc: self.desc, type: 1
    }
  end

  def dba_execute_params
    {
      issue_id: self.id, fileid: self.id, dbname: self.db_name,
      taskid: self.id, type: 1
    }
  end

  def notify_dba_script_upload
    Service::DBA::Request.new.upload(self.dba_upload_params)
  end

  def notify_dba_script_reupload
    Service::DBA::Request.new.upload(self.dba_upload_params.merge(status: 1))
  end

  def notify_dba_script_close
    Service::DBA::Request.new.upload(self.dba_upload_params.merge(status: 6))
  end

  def notify_dba_script_execute(env)
    Service::DBA::Request.new.execute(self.dba_execute_params.merge(env: env))
  end

  state_machine :status, :initial => :not_review do

    event :back_to_not_review do
      transition [:not_review, :dba_in_charge] => :not_review
    end

    event :close do
      transition :not_review => :user_close
    end

    state :not_review,            :value => 0,    :human_name => "未审核"
    state :dba_in_charge,         :value => 1,    :human_name => "DBA负责执行"
    state :dba_review_passed,     :value => 2,    :human_name => "DBA审核通过，允许执行"

    state :user_close,            :value => 100,  :human_name => "用户关闭"
  end

  execute_state_machine = proc do
    event :execute do
      transition :not_execute => :executing
    end

    state :not_execute,           :value => 0,    :human_name => "未执行"
    state :executing,             :value => 3,    :human_name => "执行中"
    state :execute_failed,        :value => 4,    :human_name => "执行失败"
    state :execute_successed,     :value => 5,    :human_name => "执行成功"
  end

  state_machine :ppe_status, :initial => :not_execute, namespace: :ppe, &execute_state_machine

  state_machine :prd_status, :initial => :not_execute, namespace: :prd, &execute_state_machine

  STATUS = Script.state_machine(:status).states.inject({}) {|r, s| r[s.name] = s.value; r}
  PPE_STATUS = Script.state_machine(:ppe_status).states.inject({}) {|r, s| r[s.name] = s.value; r}
  PRD_STATUS = Script.state_machine(:prd_status).states.inject({}) {|r, s| r[s.name] = s.value; r}
end
