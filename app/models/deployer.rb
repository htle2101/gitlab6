class Deployer
  attr_accessor :type, :args, :deployer

  def initialize(type, args = {})
    @type = type
    @args = args
  end

  def deployer
    @deployer ||= "Deployers::#{type}".constantize.new(args) rescue Deployers::Default.new(type, args)
  end 

  def method_missing(name, *args, &block)
    deployer.send(name, *args, &block)  
  end 
end  
