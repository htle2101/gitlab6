#coding:utf-8
module TicketTaskHelper

  def remove_task(id)
    "You are going to remove this task from the ticket.\n Are you sure?"
  end

  def module_rollback_tag_collections(gs)
    gss = GroupingStrategy.where(:name=>gs.name,:appname=>gs.appname,:group_id=>gs.group_id).where("status in (1,7)").order("created_at desc")
    tag_options = [[]]
    pom_module = gs.project.tree.pom.select{|p| p[:warName] == gs.appname and p[:type]=="war"}.first
    module_name = !pom_module.blank? ? pom_module[:module_name] : gs.appname
    rp = RolloutPacking.where(:tag=>gs.tag,:build_branch_id=>gs.project.rollout_branch.id).where("module_names like '%#{module_name}%'").first
    selected = rp.war_urls["#{module_name}"] if !rp.nil?
      gss.each do |gs|
        if !RolloutPacking.where(:tag=>gs.tag,:build_branch_id=>gs.project.rollout_branch.id).where("module_names like '%#{module_name}%'").first.blank?
          url = RolloutPacking.where(:tag=>gs.tag,:build_branch_id=>gs.project.rollout_branch.id).where("module_names like '%#{module_name}%'").first.war_urls["#{module_name}"]
          tag_options << [gs.tag,url] if [1,7].include?(gs.status) && tag_options.flatten.count(gs.tag) == 0
        end
    end
    options_for_select(tag_options.uniq)
  end

def whole_amount_rollback_tag(gs)
  tag_options = [[]]
  module_name = gs.package.name
  rpks = RolloutPackingModule.where(:name => module_name).order("updated_at desc")
  module_name = module_name +"-app" if rpks.blank? && gs.package.name.include?("piccenter")
  module_name = module_name +"-web" if rpks.blank? && !gs.package.name.include?("piccenter")
  rpks = RolloutPackingModule.where(:name => module_name).order("updated_at desc")
  rp = RolloutPacking.where(:tag=>gs.tag,:build_branch_id=>gs.project.rollout_branch.id).where("module_names like '%#{module_name}%'").first
  selected = rp.war_urls["#{module_name}"] if !rp.nil?
  rpks.each do |rpk|
    next if rpk.packing.nil?
    tag = rpk.packing.tag if !rpk.packing.nil?
    check = group_deploy_success_on_tag(tag,gs.project_id,gs.package.warname||gs.package.name) if !tag.nil?
    tag_options << [tag,rpk.packing.war_urls["#{module_name}"]] if !rpk.packing.nil? && !rpk.packing.war_urls.nil? && check
  end
  options_for_select(tag_options.uniq)
end

  def group_deploy_success_on_tag(tag,id,module_name)
    gss = GroupingStrategy.where(:tag =>tag, :project_id => id, :appname =>module_name)
    gss.map{|gs|gs.status}.include?(7) ? true : false
  end

  def get_package_from_log(id)
    package = Package.find(id) rescue nil
  end

  def add_immediately_rollback_package
    href = "createImmediatelyRollbackModal"
    return  { "data-toggle" => "modal", "href" => '#'+href, "class" => "btn-new "+href, "data-keyboard" => "false", "title" => "" }
  end

  def log_info(log)
    log_at =log[0].to_datetime.strftime("%Y-%m-%d %H:%M:%S")
    log_info = log[1]
    if log_info.size > 1
      log_author =log_info.collect{|log|log.author}.uniq.join(",")
      log_message = log_info.collect{|log|log.message}.join(",")
      {log_at:log_at, log_author:log_author,log_message:log_message}
    else
      {log_at:log_at, log_author:log_info.first.author,log_message:log_info.first.message}
    end
  end

  def lastest_module_tag_collections(package,ticket,from)
    rollout_branch = package.project.rollout_branch
    return [] if rollout_branch.blank?
    tag_options = [[]]
      packings = ticket.project.rollout_branch.packings.where("module_names like '%#{package.name}%'").where(:status =>7).order("updated_at desc").limit(50)
      packings.each do |p|
        if !p.war_urls.blank?
          tag_options << [p.tag,p.war_urls[package.name]]
        end
      end
    # tag_options = []
    selected = from == "ticket" ? package.url : package.rollback_to_url
    options_for_select(tag_options,selected)
  end

  def module_last_tag(id)
    package = Package.find(id)
    gss = GroupingStrategy.where(:project_id =>package.project_id,:appname => package.warname || package.name).where("status in (2,3,4,5,6,7)").order("updated_at desc")
    !gss.blank? ? "|最后发布版本:#{gss.first.tag}" : nil
  end

  def add_rollback_package
    href = "createRollbackModal"
    return  { "data-toggle" => "modal", "href" => '#'+href, "class" => "btn-new "+href, "data-keyboard" => "false", "title" => "点击增加回滚包" }
  end

  def check_deploy(id)

    href = "deployCheckModal"
    gs = GroupingStrategy.find(id) rescue nil
    if !gs.blank?
      return  { "data-toggle" => "modal", "href" => '#'+href, "class" => "btn-primary "+href, "data-keyboard" => "false", "title" => "check_deploy" ,"data-link" => info_to_verify_project_ticket_grouping_strategy_path(@project, gs.ticket, gs,{package_id: gs.package.id})}
    else
      pr = PaasRollout.find(id)
      return  { "data-toggle" => "modal", "href" => '#'+href, "class" => "btn-primary "+href, "data-keyboard" => "false", "title" => "check_deploy" ,"data-link" => deploy_project_paas_rollout_path(@project, pr)}
    end
    
  end

  def status(gs)
    return "Wait For Rollback" if gs.status != 11
    gs.status_name.to_s.titleize
  end

  def collect_tips(gs)
    prompt = "操作提示: "
    return prompt + "项目属性设置为不允许发布，请找项目负责人或ops申请上线需求!" unless gs.project.rollout_enabled

    prompt = prompt + "该包未完成预发,不允许发布!" if !gs.pre_deploy_success? && gs.group_id != -255
    prompt = prompt + "有分组正在操作中,不允许进行发布,回滚和重置!" if gs.in_progress?  || gs.same_package_diff_group_in_progress?
    prompt = prompt + "上线发布的开发任务没有全部完成,不允许发布!" if !gs.package.tasks_ready?
    prompt = prompt + "所选tag的包路径为空,不允许发布!" if gs.package.url.nil?
    return prompt if prompt != "操作提示: "

    if gs.project.workflow_check_enabled
      begin
        wfs_by_warname = Service::Op::Workflow.new.transition_status(gs.package.warname, (Date.today-0).to_s)
        wfs_by_name = Service::Op::Workflow.new.transition_status(gs.package.name, (Date.today-0).to_s)
      rescue Service::Op::WorkflowConnectError
        return prompt + "连不上workflow服务器，请找运维解决！"
      end

      if wfs_by_warname['status'].blank? && wfs_by_name['status'].blank?
        return prompt + "workflow的transition接口有问题，请找运维解决！"
      end

      if wfs_by_warname['status'] == 0 || wfs_by_name['status'] == 0
      elsif wfs_by_warname['status'] == 1 || wfs_by_name['status'] == 1
        wfs = wfs_by_warname['status'] == 1 ? wfs_by_warname : wfs_by_name
        return prompt + "该项目共有#{wfs['unapproved_wfs'].size}个流程单没走完," \
               + "urls:#{wfs['unapproved_wfs'].collect{|wf|wf['detail_url']}}" \
               + "ids:#{wfs['unapproved_wfs'].collect{|wf|wf['exec_id']}}"
      else
        return prompt + "该项目无对应的上线工作单，不允许发布。请到 http://workflow.dp/ 上创建!"
      end
    end
    prompt =(prompt == "操作提示: "? nil:prompt)
  end

  def collect_pr_tips(pr)
    prompt = "操作提示: "
    pr_pre = pr.package.grouping_strategies.detect{|gs|gs.group_id == -255 and gs.ticket_id.to_s == pr.ticket_id.to_s and gs.appname == pr.appname and gs.tag == pr.tag} if !pr.package.grouping_strategies.blank?
    prompt = prompt + "该包未完成预发,不允许发布!" if !pr_pre.blank? && pr_pre.status != 4
    prompt = prompt + "有分组正在操作中,不允许进行发布,回滚和重置!" if !pr.package.paas_rollouts.running.blank?    
    prompt =(prompt == "操作提示: "? nil:prompt)
  end

  def own_compose_module_options(project_id)
    module_options = [[]]
    ProjectModule.where(:project_id =>project_id).each do |pm|
      module_options << ["[#{pm.project.name_with_namespace}] [#{pm.module_name}]", "#{pm.module_name}"]
    end
    module_options
  end

  def own_compose_module_options_for_rollback(project_id)
    module_options = [[]]
    Package.where(:project_id =>project_id).uniq_by(&:name).each do |package|
      gss_exist = GroupingStrategy.where(:appname => package.warname || package.name,:project_id =>project_id)
      module_options << ["[#{package.project.name_with_namespace}] [#{package.warname || package.name}]", "#{package.warname || package.name}"] if !gss_exist.blank? && gss_exist.collect{|gs|gs.status}.include?(7)
    end
    module_options.uniq
  end


  def own_compose_module_options_for_whole(project_id)
    module_options = [[]]
    Package.where(:project_id =>project_id).uniq_by(&:name).each do |package|
      gss_exist = GroupingStrategy.where(:appname => package.warname || package.name,:project_id =>project_id)
      gs = GroupingStrategy.where(:appname => package.warname ||package.name,:project_id =>project_id,:group_id =>0).first
      date = DateTime.now
      today_whole_already_exist = false if  gs.blank?
      today_whole_already_exist = true if  !gs.blank? && date.year.to_s+"-"+date.month.to_s+"-"+date.day.to_s ==gs.created_at.year.to_s+"-"+gs.created_at.month.to_s+"-"+gs.created_at.day.to_s
      module_options << ["[#{package.project.name_with_namespace}] [#{package.warname || package.name}]", "#{package.warname || package.name}"] if !gss_exist.blank? && gss_exist.collect{|gs|gs.status}.include?(7) && !today_whole_already_exist
    end
    module_options.uniq
  end

  def can_whole_rollback?(params)
    pm  = params
    gs_success = GroupingStrategy.where(:project_id => pm[:project_id],:appname =>pm[:module_name],:status => 7)
    return false if gs_success.blank?
    project = Project.find(pm[:project_id])
    module_name = module_name_or_warname(pm)
    rpks = RolloutPackingModule.where( :name => module_name )
    tags = rpks.collect{|rpk|rpk.packing.tag if !rpk.packing.nil?}.delete_if{|result|result.nil?}
    tags.each do |tag|
      @result = false
      check = group_deploy_all_success_on_tag(tag,project.id,module_name)
      @result = true and break  if check == true
    end
    return @result
  end

  def group_deploy_all_success_on_tag(tag,id,module_name)
    gs_new = Service::Op::Cmdb.new.grouping_strategy(module_name)
    groups = gs_new["groups"].map{|group|group["name"] if group["name"] !="全局"}.delete_if{|g|g.nil?}
    groups.each do |group|
      @tag_result = true
      gs_exist = GroupingStrategy.where(:tag => tag,:appname =>module_name,:status =>7,:name=>group,:project_id => id)
      @tag_result =  false and break if gs_exist.blank?
    end
    return @tag_result
  end

  def module_name_or_warname(pm)
    module_name = pm[:module_name]
    @gs_new = Service::Op::Cmdb.new.grouping_strategy(module_name) rescue nil
    return module_name if !@gs_new.blank?
    project = Project.find(pm[:project_id])
    pom = project.tree.pom.detect{|p|p if p[:module_name] == module_name}
    warName = !pom.blank? ? pom[:warName] : ""
    name = !Service::Op::Cmdb.new.grouping_strategy(warName).blank? ? warName : module_name
  end

  def package_belongs_to_self?(package,project)
     return package.project == project
  end

  def group_for_ticket_package_in_progress?(package,project,ticket)
    gss = GroupingStrategy.where(:ticket_id=>ticket.id,:package_id=>package.id,:project_id=>project.id,:tag=>package.tag)
    gss.map{|gs|gs.in_progress?}.include?(true) ? true : false
  end

  def paas_for_ticket_package_in_progree?(package,project,ticket)
    paas_rollouts = PaasRollout.where(:ticket_id =>ticket.id,:package_id => package.id, :project_id => project.id,:tag=>package.tag)
    paas_rollouts.map{|ps|ps.status}.include?(101) || paas_rollouts.map{|ps|ps.status}.include?(100) ? true : false
  end

  def add_war_template
    # href = ip.nil? ? "createModal" : "editModal"
    href = "addWarModel"
    { "data-toggle" => "modal", "href" => '#'+href, "class" => href, "data-keyboard" => "false" }
  end

end
