#coding:utf-8
module PerformanceTestsHelper
	
	def new_performance_test_link
    if false
      ["#", { class: "pull-right btn btn-primary disabled has_tooltip", "data-toggle" => "tooltip", "title" => "Jenkins job is running." }]
    else
      [new_project_performance_test_path(@project), { class: "pull-right btn btn-primary", title: "New PerformanceTest" }]
    end
  end

  def memory_for_performance_test(id)
  	pt = PerformanceTestModule.find(id)
		pt.memory == 2048000000 ? "2048000000(2g)" : "4096000000(4g)"
  end

  def selected_value_for_all_select(id)
    if !id.nil?
    	ptm = PerformanceTestModule.find(id)
      pt = ptm.performance_test
    	selected_value  = {appname: ptm.appname,tag: ptm.package_url,app_type: ptm.app_type,
    										 cpu: ptm.cpu, memory: ptm.memory, domain_name: ptm.domain_name,title:pt.title,target:pt.target,
                         concurrent: pt.concurrent,duration: pt.duration}
    else
      selected_value = {}
    end

  end


  #下面两个方法拼装成一个方法，一次sql全部取出来

  def performance_test_apps(id)
    ptms = PerformanceTestModule.where(:performance_test_id => id)
    ptms.map{|ptm|ptm.appname+"("+ptm.tag+")"}.join(",")
  end

  def performance_test_machine(id)
    ptms = PerformanceTestModule.where(:performance_test_id => id)
    ptms.map{|ptm|ptm.machine}.compact.join(",")
  end

  def special_tags_for_appname(project_id,performance_test_module_id)
    tag_options = [[]]
    project = Project.find(project_id)
    ptm = PerformanceTestModule.find(performance_test_module_id)
    rollout_packings = project.rollout_branch.packings.where(:status => 7).where("module_names like '%#{params[:appname]}%'")
    rollout_packings.each do |rp|
      rpk = rp.modules.first
      tag_options << [rp.tag, rpk.url]
    end
    options_for_select(tag_options,ptm.package_url)
  end

  def own_compose_module_options_for_performance_test(project_id)
    module_options = [[]]
    ProjectModule.where(:project_id =>project_id).each do |pm|
      module_options << ["#{pm.module_name}", "#{pm.module_name}"]
    end
    module_options
  end

end
