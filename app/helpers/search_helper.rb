module SearchHelper
  def blob_grep(es_result)
    project = Project.find(es_result.project_id)
    blob_with_filter_query = project.repository.search_files_with_one(params[:search].gsub('code:','').split(' ').reject{|word| word.include? 'language:' }.join('|'), es_result.path + es_result.name)
    blob_with_filter_query || Gitlab::Git::BlobSnippet.new(project.repository.root_ref,[],0,es_result.path + es_result.name)
  end

  def db_info_blob_grep(es_result)
    return nil if es_result.code.gsub('10.1.77', '').index('10.1.').blank?
    project = Project.find(es_result.project_id)
    blob_with_filter_query = project.repository.search_files_with_one("dp!", es_result.path + es_result.name)
    blob_with_filter_query || Gitlab::Git::BlobSnippet.new(project.repository.root_ref,[],0,es_result.path + es_result.name)
  end  
end
