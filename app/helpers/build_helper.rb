module BuildHelper

  ACTIONS = {
    2001 => {label: "success", class: "label label-success"},
    2002 => {label: "failure", class: "label label-important"},
    2003 => {label: "unstable", class: "label label-warning"},
    2004 => {label: "running", class: "label label-info"},
    2005 => {label: "not run", class: "label"},
    2006 => {label: "aborted", class: "label label-inverse"},
    2007 => {label: "invalid", class: "label label-inverse"}
  }

  def display(action)
    ACTIONS[action]
  end
end
