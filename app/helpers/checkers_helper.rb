module CheckersHelper
  def checkers_filter_path(options={})
    exist_opts = {
      checked: params[:checked],
      current_user: params[:current_user],
      released_at: params[:released_at]
    }

    options = exist_opts.merge(options)
    checkers_dashboard_path(options)
  end
end
