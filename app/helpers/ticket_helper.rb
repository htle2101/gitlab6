module TicketHelper

  def tickets_filter_path(options={})
    exist_opts = {
      status: params[:status],
      scope: params[:scope],
      released_at: params[:released_at],
      project_id: params[:project_id]
    }

    exist_opts.delete(:released_at) if options.has_key?(:scope)
    
    options = exist_opts.merge(options)
    list_tickets_path(options)
  end

   def module_compose_ticket_options(ticket)
     module_options = []
       ProjectModule.where(:project_id=>ticket.project_id).find_each do |pm|        
           package = Package.where(:project_id =>pm.project.id, :ticket_id =>ticket.id ,:owner=>1,:name =>pm.module_name)
           module_options << ["[#{pm.project.name_with_namespace}] [#{pm.module_name}]", "#{pm.project_id}|#{pm.module_name}"] if package.blank?          
       end
     module_options
   end

   def compose_project_options(group)
     project_options = [[]]
     group.projects.each do |project|
        project_options << ["[#{project.name_with_namespace}]",project.id]
     end
     project_options  
   end

  def list_tickets_path(options = {})
    if @project
      project_tickets_path(@project, options)
    elsif @group
      tickets_group_path(@group, options)
    end
  end

  def ticket_task_packages(ticket)
        configs = []
        result = false
        if !ticket.package_tasks.blank?
        ticket.package_tasks.each do |task|
            task.configs.each do |config|
               configs.push(config)
            end
       end
       result = true if !configs.blank?
     end
     result
  end

  def  package_owner(package)
      result = "task"
      result = "ticket" if package.owner == 2
      result
  end

  def  war_url(app,tag)
       RolloutPacking.where(:app=>app, :tag =>tag).first.war_urls("#{app}")
  end
  
end
