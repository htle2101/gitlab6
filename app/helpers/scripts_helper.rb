#encoding: UTF-8

module ScriptsHelper

  def scripts_filter_path(options={})
    exist_opts = {
      status: params[:status],
      scope: params[:scope],
      create_at: params[:create_at]
    }

    options = exist_opts.merge(options)
    project_scripts_path(@project, options)
  end

	def script_status_label(status)
		case status
		when 0 then "normal"
    when 2, 5 then "success"
		when 1, 3 then "info"
		when 4, 100 then "error"
		end
	end

  def execute_label(type)
    if type == Script::ENV_TYPE[:ppe]
      "PPE执行"
    elsif type == Script::ENV_TYPE[:prd]
      "线上执行"
    end
  end
end
