#coding:utf-8
module CiBranchHelper
  def ci_env_options(project = nil)
    if project == nil
      envs = CiEnv.all.uniq_by(&:name)
    else
      envs = CiEnv.no_filled_envs(project)
    end

    envs_opts = [ "Envs", envs.map {|e| [e.name, e.name]} ]

    options = [envs_opts]

    selected = envs[0].id

    grouped_options_for_select(options, selected)
  end

  def color_status(status = 'success')
    case status.try(:downcase)
    when 'success', 'upload_war_success','deploy success', 'pre deploy success', 'rollback success', 'true','deploy_success','部署成功'
      "badge badge-success"
    when 'failure', 'deploy error', 'upload_war_fail', 'pre deploy fail', 'deploy fail', 'rollback fail', 'false','deploy_fail', '部署失败'
      "badge badge-important"
    when 'unstable', 'pre deploying', 'rollbacking', 'killing', 'pre deploy killing', '部署中'
      "badge badge-warning"
    else
      "badge"
    end
  end

  def beta_paas_logs(id)
    module_options = []
    bp = BetaPaasRollout.find(id)
    beta_paas_log = BetaPaasLog.find_by_module_name_and_beta_paas_rollout_id_and_tag(bp.appname, id, bp.tag)
    module_options << [beta_paas_log.module_name+"__deploy_log",id] if !beta_paas_log.blank?
    module_options
  end

  def beta_paas_machine(beta_paas)
    begin
      if beta_paas.build_branch.current_build_status == "success" && !["deploying","deploy error"].include?(beta_paas.try(:status_message))
        paas_machine_ip(beta_paas)
      elsif beta_paas.try(:status_message) == "deploying" || beta_paas.try(:status_message) == "deploy error"
        !beta_paas.machine.nil? ? beta_paas.machine+"(paas)" : ""
      else
        ""
      end
    rescue
      paas_machine_ip(beta_paas)
    end
  end

  def package_type_options
    grouped_options_for_select({"CodeType" => CiTemplate.is_visable.map(&:package_type).uniq!}, 'java-maven3')
  end

  # def deploy_ip(pom_module)
  #   pom_module[:machine].try(:ip)
  # end


  def beta_paas_module_status(beta_paas)
    if !beta_paas.build_branch.imf.modules[beta_paas.appname][:jenkins].nil?
      beta_paas.build_branch.imf.modules[beta_paas.appname][:jenkins]['status']
    else
      beta_paas.try(:status_message)
    end

  end

  def modules_type(ip)
    href = ip.nil? ? "createModal" : "editModal"

    if !ip.nil? && ( VirtualMachine.find_by_ip(ip).status == 8 )
      return  { "data-toggle" => "modal", "href" => '#'+href, "class" => href, "data-keyboard" => "false", "disabled" => "disabled", "title" => "during unbinding !" }
    end
    { "data-toggle" => "modal", "href" => '#'+href, "class" => href, "data-keyboard" => "false" }
  end

  def domain_type(domain_name)
    href = domain_name.nil? ? "editDomainName" : "deleteDomainName"
    { "data-toggle" => "modal", "href" => '#'+href, "class" => href, "data-keyboard" => "false" }
  end

  def lzm_restart_tomcat_button(machine, style='')
    if machine.blank?
      disabled = 'false'
    else
      status = machine.try(:status_message)
      status = 'to deploy' unless status
      disabled = (status == 'deploying' ? 'true' : 'false' )
    end
    Haml::Engine.new("%button.btn.lzm-restart-tomcat.has_bottom_tooltip{ disabled: #{disabled}, style: \"#{style}\" }重启").render
  end

  def alpha_restart_tomcat_button(machine, style='')
    if machine.blank?
      disabled = 'false'
    else
      status = machine.try(:status_message)
      status = 'to deploy' unless status
      disabled = (status == 'deploying' ? 'true' : 'false' )
    end
    Haml::Engine.new("%button.btn.alpha-restart-tomcat.has_bottom_tooltip{ disabled: #{disabled}, style: \"#{style}\" }重启").render
  end

  def lzm_deploy_button(machine, style = "")
    # binding.pry
    if machine.blank?
      disabled = 'false'
    else
      status = machine.try(:status_message)
      status = 'to deploy' unless status
      disabled = (status == 'deploying' ? 'true' : 'false' )
    end
    Haml::Engine.new("%button.btn.lzm-deploy.has_bottom_tooltip{ disabled: #{disabled}, style: \"#{style}\" }部署").render
  end

  def lzm_color_status(machine)
    status = 'upload_war_success' == machine.try(:status_message) ? 'to deploy' : machine.try(:status_message)
    color_status(status)
  end

  def lzm_status(machine)
    status = machine.try(:status_message)
    status = 'to deploy' unless status
    status
  end

  def beta_paas_status(beta_paas)
    status = beta_paas.try(:status_message)
    'upload_war_success' == status ? 'to deploy' : status
  end

  def paas_machine_ip(beta_paas)
    machine ||= get_ips_from_paas(beta_paas.appname)+"(paas)" if !get_ips_from_paas(beta_paas.appname).include?('Paas')
    machine ||= get_ips_from_paas(beta_paas.appname)
    if beta_paas.try(:status_message) == "upload_war_success"
      !beta_paas.machine.blank? ? (beta_paas.machine.to_s+"(paas)") : "在申请中,请刷新页面..."
    else
      machine
    end
  end

  def has_beta_paas_or_not(id,app)
    begin
      pm = ProjectModule.find_by_project_id_and_module_name(id,app)
      pm.has_beta_paas == 0 ? false : true
    rescue
      false
    end  
  end

  def beta_paas_has_ip?(beta_paas)
    !beta_paas.machine.blank? || !get_ips_from_paas(beta_paas.appname).include?('paas')
  end

  def get_ips_from_paas(war_name)
    url = Settings.op['beta_paas_url']

    if url
      json = DianpingPaas::Client.new(url: url).instances(war_name).body

      ips = begin
              JSON.parse(json)['data'].map { |data| data['instanceIp'] }
            rescue JSON::ParserError
              []
            end
    else
      raise 'No Paas URL found'
    end

    if ips.present? && ips != [nil]
      ips.join(', ')
    else
      "<button class='btn apply-machine-button' id='apply-machine-button'>申请 Paas 机器</button>".html_safe
    end
  end

  # def get_ips_from_cmdb(war_name, env_name)
  #   uri = URI("http://#{Settings.op['cmdb_url']}/cmdb/device/s")

  #   params = {q: "应用名称:#{war_name}",fq: "env:#{env_name}"}

  #   uri.query = URI.encode_www_form(params)

  #   res = Net::HTTP.get_response(uri)

  #   ips = begin
  #           JSON.parse(res.body)['result'].map { |result| result['private_ip'] }.flatten
  #         rescue JSON::ParserError
  #           []
  #         end
  #   ips.join(', ') if ips.present?      
  # end

  def get_ips_from_cmdb(war_name, env_name)
    begin
      ip = Service::Op::Cmdb.new(:host => Settings.op['lzm_url']).app_ips(war_name, 'deploy')
      ip.blank? ? '请通过workflow申请beta测试机' : ip.join(',')
    rescue => en
      '应用未登记，请联系运维'
    end  
  end

  def module_name(id)
    module_options = [[]]
    ProjectModule.where(:project_id => id).each do |pm|
      module_options << ["[#{pm.project.name_with_namespace}] [#{pm.module_name}]", "[#{pm.project.id}] #{pm.module_name}"]
    end
    module_options
  end

  def get_dn_by_ip(ip)
    return if ip.blank?
    Project.find(@project.id).machines.find_by_ip(ip).domain_name
  end

  def get_machine_tenancy(ip)
    machine = VirtualMachine.find_by_ip(ip)
    return if machine.blank?
    if machine.action_type == "bind"
      "非借用"
    elsif machine.tenancy == 9
      "长期"
    else
      "临时"
    end
  end

  def switch_deployer
    if @ci_branch.deployers == 'War'
      link_to project_ci_branch_phoenix_path(@project, @ci_branch.branch_name), method: :post, class: "btn has_bottom_tooltip", "data-original-title" => '切换到phoenix部署' do
        "#{content_tag(:i, '', class: 'icon-plane')} Phoenix".html_safe
      end
    elsif @ci_branch.deployers == 'Phoenix'
      link_to war_project_ci_branch_path(@project, @ci_branch.branch_name), method: :post, class: "btn has_bottom_tooltip", "data-original-title" => '切换到默认部署' do
        "#{content_tag(:i, '', class: 'icon-film')} Default".html_safe
      end
    end
  end

  def auto_deploy_button
    command  = @ci_branch.not_to_deploy? ? '恢复' : '禁止'
    icon = @ci_branch.not_to_deploy? ? 'icon-share-alt' : 'icon-ban-circle'
    link_to project_ci_branch_path(@project, @ci_branch.branch_name, auto_deploy: !@ci_branch.auto_deploy), method: :put, class: "btn btn-primary ci-branch-destroy" do
      "#{content_tag(:i, '', class: icon)} #{command}机器部署".html_safe
    end
  end

  def phoenix_wars
    @ci_branch.modules_with_type("war").map { |m| { value: m[:module_name], id: m[:module_name] , path: m[:path],  warName: m[:warName]} }
  end

  def ip_autocomplete_source
    if @ci_branch.deployers == 'Phoenix'
      @project.machines.available.where(:action_type => ['apply'],:deployer => @ci_branch.deployers).map { |m| m.ip }.to_json
    else
      @project.machines.available.where(:action_type => ['bind', 'apply'],:deployer => @ci_branch.deployers).map { |m| m.ip }.to_json
    end
  end

  def beta_paas_info(ci_branch,module_name)
    tag = Gitlab::Git::Commit.last_for_path(ci_branch.project.repository, ci_branch.branch_name, @path).sha
    beta_paas = BetaPaasRollout.where(:build_branch_id =>@ci_branch.id,:tag => tag,:appname => module_name)
  end

  def branch_table_class(ci_branch)
    "js-toggle-visibility-container hide"  if 'ShadowBranch' == ci_branch.class.to_s
  end

  def module_deploy?(ci_branch_id, module_name)
    return false unless Rails.cache.read(ci_branch_id.to_s + '_self_modules_to_deploy_for_phoenix')
    if Rails.cache.read(ci_branch_id.to_s + '_self_modules_to_deploy_for_phoenix').include? module_name + ','
      true
    else
      false
    end
  end

  def module_deploy_command(ci_branch_id, module_name)
    if module_deploy?(ci_branch_id, module_name)
      {"command" => '移除部署', "button" => 'btn-remove'}
    else
      {"command" => '增加部署', "button" => 'btn-new'}
    end
  end

  def domain_name_selected?(key)
    if Rails.cache.read(@ci_branch.id.to_s + '_app_default_domain').blank? && key == "main"
      return "selected"
    end

    if Rails.cache.read(@ci_branch.id.to_s + '_app_default_domain') == key
      return "selected"
    end
  end
end
