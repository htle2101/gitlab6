module BetaDeployLogsHelper
  def get_vote(log)
    BetaDeployVote.find_by_user_id_and_beta_deploy_log_id(current_user.id, log.id)
  end

  def voteup_link(log)
    vote = get_vote(log)

    action = 'vote'

    if vote
      if vote.up?
        active = 'active'
        action = 'unvote'
      end
    end

    content_tag :span, class: "#{action} voteup" do
      content_tag(:i, nil, class: "icon-thumbs-up #{active}") <<
      content_tag(:span, ' x ', class: 'x') <<
      content_tag(:span, log.voteup_count, class: 'vote_count')
    end
  end

  def votedown_link(log)
    vote = get_vote(log)

    action = 'vote'

    if vote
      if vote.down?
        active = 'active'
        action = 'unvote'
      end
    end

    content_tag :span, class: "#{action} votedown" do
      content_tag(:i, nil, class: "icon-thumbs-down #{active}") <<
      content_tag(:span, ' x ', class: 'x') <<
      content_tag(:span, log.votedown_count, class: 'vote_count')
    end
  end
end
