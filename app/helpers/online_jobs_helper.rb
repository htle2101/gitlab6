#coding:utf-8
module OnlineJobsHelper
  def jar_names(project)
    @jar_names = project.tree.pom.map{|pm|pm[:module_name] if pm[:type] =='jar'}.delete_if{|p|p.blank?}
    module_options = [[]]
    @jar_names.each do |name|
      module_options << [name,name]
    end
    module_options
  end

  def branch_for_project(project)
    branch_options = [[]]
    project.repository.heads.map(&:name).each do |name|
      branch_options << [name,name]
    end
    options_for_select(branch_options)
  end

  def jar_tags(project)
    @jar_names =project.tree.pom.map{|pm|pm[:module_name] if pm[:type] =='jar'}.delete_if{|p|p.blank?}
    module_options = [[]]
    @jar_names.each do |name|
      rpks = RolloutPackingModule.where(:name => name ).where("url is not null")
      rpks.each do |rpk|
        module_options << [rpk.packing.tag,rpk.url]
      end
    end
    module_options
  end

  def online_jobs(project)
    @online_jobs = OnlineJob.where(:project_id => project.id,:build_branch_id => project.rollout_branch.id)
  end

  def job_running_envs
    ['alpha','beta','product']
  end

  def online_job_label_for_status(online_job)
    case online_job.human_status_name
    when "发布中" then "info"
    when "发布成功" then "success"
    else "important"
    end
  end

end