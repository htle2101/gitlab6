module TasksHelper

  def compose_module_options(task)        
    module_options = []
    ProjectModule.where(:project_id => task.project_id).each do |pm|
      module_options << ["[#{pm.project.name_with_namespace}] [#{pm.module_name}]", "#{pm.project_id}|#{pm.module_name}"]
    end
    module_options
  end

  def exist_packages(task, type = "with_id")
    return [] if task.try(:packages).blank?
    task.packages.map do |package|
    type == "with_id" ? "#{package.project_id}|#{package.name}" : "#{package.name}"
    end
  end

  def project_tasks_filter_path(options={})
    exist_opts = {
      status: params[:status],
      scope: params[:scope],
      released_at: params[:released_at],
    }

    options = exist_opts.merge(options)

    project_tasks_path(@project, options)
  end

  def caption_for_transition(transition)
    return if transition.blank?
    if transition.from > transition.to
      "<i class='icon-circle-arrow-left'></i> Back to #{transition.to_name.to_s.titleize}"
    else
      "Go to #{transition.to_name.to_s.titleize} <i class='icon-circle-arrow-right'></i>"
    end.html_safe
  end
end
