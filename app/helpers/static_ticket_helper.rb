module StaticTicketHelper

  def static_tickets_filter_path(options={})
    exist_opts = {
      status: params[:status],
      scope: params[:scope],
      released_at: params[:released_at],
      project_id: params[:project_id]
    }

    exist_opts.delete(:released_at) if options.has_key?(:scope)
    
    options = exist_opts.merge(options)
    list_static_tickets_path(options)
  end

  def list_static_tickets_path(options = {})
    if @project
      project_static_tickets_path(@project, options)
    elsif @group
      tickets_group_path(@group, options)
    end
  end

  def all_static_modules
    [ 'dpfoot-static', 'account-static', 'exchange-static', 'message-static', 'piccenter-static',\
      'user-static', 'exchange-admin-static', 'bc-tuangou-static', 'group-static','activity-static',\
      'tuangou-static', 'open-portal-static', 'shopsearch-static', 'dpmootools-static', 'bc-shopbiz-static',\
      'shop-static', 'dp-common-static', 'review-admin-static', 'product-bc-membercard-static', 'dpindex-static',\
      'useractivity-static', 'usercard-static', 'bc-admin-common-static', 'neuron-static,promo-static',\
      'm-tuangou-static', 'booking-static', 'bc-booking-static', 'm-dianping-static', 'review-report-static',\
      'ba-static', 'ba-finance-shopbill-static', 'mylist-static', 'activity-shopadmin-static', 'bc-shopclaim-static',\
      'tuangou-event-static', 'bc-coupon-static', 'shop-event-static', 'bc-businesspic-static', 'topic-static',\
      'dpmap-static', 'bc-coupon-static', 'tuangou-movie-static', 'bc-common-static', 'video-static',\
      'group-mobile-static', 'menu-static', 'activityadmin-static', 'bc-shopoffline-static', 'neuron-1.x-static',\
      'freezer-static', 'hippo-static', 'tuangou-hotel-static', 'bc-shopmanagement-static', 'bc-shopaccount-static',\
      'm-booking-static', 'bc-bootstrap', 'takeaway-static'\
    ]
  end

end
