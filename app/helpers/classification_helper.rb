module ClassificationHelper
  def classification_filter_path(options={})
    exist_opts = {
      status: params[:status],
      scope: params[:scope],
      classification_id: params[:classification_id],
    }

    path = request.path
    path << "?#{exist_opts.merge(options).to_param}"
    path
  end

  def issues_per_classification(classification)
    Issue.where(project_id: Project.where(namespace_id: classification.groups.pluck(:id)).pluck(:id)).opened.unassigned.count
    # classification.groups.projects.issues.count
  end
end
