module CiBuildsHelper

  def ci_builds_filter_path(options={})
    exist_opts = {
      gt: params[:gt],
      lt: params[:lt]
    }

    options = exist_opts.merge(options)
    admin_ci_builds_path(options)
  end
end
