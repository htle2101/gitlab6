module GroupingStrategyHelper

  def grouping_strategies_filter_path(options={})
    exist_opts = {
      status: params[:status],
      namespace_id: params[:namespace_id],
      released_at: params[:released_at],
    }

    options = exist_opts.merge(options)
    grouping_strategies_dashboard_path(options)
  end

  #  def ticket_compose_module_options(ticket)
  #    module_options = []
  #      ProjectModule.includes(:project).find_each do |pm|
  #      package = Package.where(:project_id =>pm.project.id, :ticket_id =>ticket.id ,:owner=>1,:name =>pm.module_name)
  #      module_options << ["[#{pm.project.name_with_namespace}] [#{pm.module_name}]", "#{pm.project_id}|#{pm.module_name}"] if package.blank?
  #      end
  #    module_options
  #  end


  # def list_tickets_path(options = {})
  #   if @project
  #     project_tickets_path(@project, options)
  #   elsif @group
  #     tickets_group_path(@group, options)
  #   end
  # end
end  