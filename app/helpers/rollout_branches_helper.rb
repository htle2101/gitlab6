#coding:utf-8
module RolloutBranchesHelper

  def new_packing_link
    if @rollout.job_running?
      ["#", { class: "pull-right btn btn-primary disabled has_tooltip", "data-toggle" => "tooltip", "title" => "Jenkins job is running." }]
    else
      [new_project_rollout_branch_path(@project), { class: "pull-right btn btn-primary", title: "New Packing" }]
    end
  end

  def label_for_status(packing)
    case packing.status
    when RolloutPacking::STATUS_LIST[:uploading] then "info"
    when RolloutPacking::STATUS_LIST[:upload_success] then "success"
    else "important"
    end
  end

  def label_for_rollback_status(packing)
    case packing.rollback_status
    when '不可回滚' then "important"
    else "info"
    end
  end

  # # def add_package_to_clear_cache
  # #   href = "ClearCacheModal"
  # #   return  { "data-toggle" => "modal", "href" => '#'+href, "class" => "btn-new "+href, "data-keyboard" => "false" }
  # # end

  # def add_package_to_clear_ppecache
  #   href = "ClearPpeCacheModal"
  #   return  { "data-toggle" => "modal", "href" => '#'+href, "class" => "btn-new "+href, "data-keyboard" => "false" }
  # end

  def static_status(build_number,id)
    @static_status = StaticStatus.where(:build_number => build_number,:build_branch_id => id).first
    return nil if @static_status.blank?
    return "failure" if @static_status.build_status == "failure"
    return "unstable" if @static_status.build_status == "SUCCESS" && @static_status.cache_status == "failure"
    return "success" if @static_status.build_status == "SUCCESS" && @static_status.cache_status == "SUCCESS"
  end

  def static_status_text(build_number,id)
    @static_status = StaticStatus.where(:build_number => build_number,:build_branch_id => id).first
    return nil if @static_status.blank?
    return "build_failure" if @static_status.build_status == "failure"
    return "cache_clear_fail"  if  @static_status.build_status == "SUCCESS" && @static_status.cache_status == "failure"
    return "success"if  @static_status.build_status == "SUCCESS" && @static_status.cache_status == "SUCCESS"
  end

  def icon_for_status(packing)
    case packing.status
    when RolloutPacking::STATUS_LIST[:uploading] then "icon-refresh cblue"
    when RolloutPacking::STATUS_LIST[:upload_success] then "icon-ok cgreen"
    else "icon-remove cred"
    end
  end

  def packing_include_jar?(packing)
    return false if BuildBranch.find(packing.build_branch_id).project.repository.commit(packing.commit).blank?
    jars = BuildBranch.find(packing.build_branch_id).project.tree(packing.commit).pom.map{|pm|pm[:module_name] if pm[:type] =='jar'}.delete_if{|p|p.blank?}
    !(jars & packing.module_names.split(',')).blank?
  end

  def packing_include_war?(packing)
    return false if BuildBranch.find(packing.build_branch_id).project.repository.commit(packing.commit).blank?
    wars = BuildBranch.find(packing.build_branch_id).project.tree(packing.commit).pom.map{|pm|pm[:module_name] if pm[:type] =='war'}.delete_if{|p|p.blank?}
    !(wars & packing.module_names.split(',')).blank?
  end
end
