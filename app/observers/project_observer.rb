class ProjectObserver < BaseObserver
  def after_create(project)
    project.update_column(:last_activity_at, project.created_at)

    return true if project.forked?

    if project.import?
      RepositoryImportWorker.perform_in(5.seconds, project.id)
    else
      GitlabShellWorker.perform_async(
        :add_repository,
        project.path_with_namespace
      )

      log_info("#{project.owner.name} created a new project \"#{project.name_with_namespace}\"")
    end
    project.repo_indexer.mapping
  end

  def after_update(project)
    project.set_pre_receive_hook if project.blacklist_changed?
    project.send_move_instructions if project.namespace_id_changed?
    if project.path_changed?
      project.rename_repo

      unless project.build_branches.blank?
        begin
          Timeout::timeout(40) do
            project.build_branches.each{|b| ::CiBranches::UpdateContext.new(project, b, project.namespace.path).execute} rescue log_info("BuildBranch remove error #{project.try(:name_with_namespace)} #{b.try(:jenkins_job_name)}")
          end
        rescue => e
          log_info("BuildBranch remove error #{project.try(:name_with_namespace)} timeout.")
        end  
      end  


      #   debugger
      #   project.ci_branchs.each do |ci_branch|
      #     ::CiBranches::UpdateContext.new(project, ci_branch, project.namespace.path).execute
      #   end
      # end
      # if project.shadow_branchs
      #   project.shadow_branchs.each do |shadow_branch|
      #     ::CiBranches::UpdateContext.new(project, shadow_branch, project.namespace.path).execute
      #   end
      # end
      # if project.sonar_branchs
      #   project.sonar_branchs.each do |sonar_branch|
      #     ::CiBranches::UpdateContext.new(project, sonar_branch, project.namespace.path).execute
      #   end
      # end
      # ::CiBranches::UpdateContext.new(project, project.rollout_branch, project.namespace.path).execute if project.rollout_branch
      # rescue
      # end  
    end  


    GitlabShellWorker.perform_async(
      :update_repository_head,
      project.path_with_namespace,
      project.default_branch
    ) if project.default_branch_changed?
  end

  def before_destroy(project)
    project.repository.expire_cache unless project.empty_repo?

    begin
      Timeout::timeout(20) do
        project.repo_indexer.delete!
      end
    rescue => e
      log_info("#{project.try(:name_with_namespace)} try delete search index timeout")
    end
  end

  def after_destroy(project)
    GitlabShellWorker.perform_async(
      :remove_repository,
      project.path_with_namespace
    )

    GitlabShellWorker.perform_async(
      :remove_repository,
      project.path_with_namespace + ".wiki"
    )

    project.satellite.destroy

    log_info("Project \"#{project.name}\" was removed")
  end
end
