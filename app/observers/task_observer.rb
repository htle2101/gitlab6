class TaskObserver < BaseObserver
  observe :task

  def after_to_alpha(task, event)
    notification.change_task_status(task)

    log_info("task \"#{task.id}\" to alpha")
  end

  def after_to_beta(task, event)
    notification.change_task_status(task)

    log_info("task \"#{task.id}\" to beta")
  end
end
