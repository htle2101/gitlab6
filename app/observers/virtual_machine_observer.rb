class VirtualMachineObserver < BaseObserver

  def after_update(virtual_machine)
    # begin
    #   call_autotest(virtual_machine.build_branch) if virtual_machine.build_branch.ci_env.name == "beta" && virtual_machine.deploy_finished?
    # rescue
    # end
    add_or_update_virtual_machine_usage(virtual_machine) if virtual_machine.action_type == "apply"
  end

  protected
  def call_autotest(build_branch)
    Service::AutoTest::Request.new.call(build_branch.project_name_for_auto_test) if build_branch.all_deploy_finished?
  end

  def add_or_update_virtual_machine_usage(virtual_machine)
    return if virtual_machine.project_id_was == virtual_machine.project_id
    project_was = Project.where(id: virtual_machine.project_id_was).first unless virtual_machine.project_id_was.blank?
    project = Project.where(id: virtual_machine.project_id).first unless virtual_machine.project_id.blank?
    return if virtual_machine.project_id && project.blank?

    if virtual_machine.project_id_was && project_was.blank? 
      stop_deleted_project_use_record(virtual_machine, virtual_machine.project_id_was)
    end  

    if virtual_machine.project_id_was.blank? && !virtual_machine.project_id.blank?
      start_use_record(virtual_machine)
    end

    if !virtual_machine.project_id_was.blank? && virtual_machine.project_id.blank?
      stop_use_record(virtual_machine, virtual_machine.project_id_was)
    end

    if !virtual_machine.project_id_was.blank? && !virtual_machine.project_id.blank?
      stop_use_record(virtual_machine, virtual_machine.project_id_was)
      start_use_record(virtual_machine)
    end
  end

  def stop_use_record(virtual_machine,project_id_was)
    usage = VirtualMachineUsage.where(machine_id: virtual_machine.id,
                                      project_id: project_id_was,
                                      using_to: nil).last
    if usage
      usage.using_to = Time.now
      usage.duration = usage.using_to - usage.using_from
      usage.save!
    end
  end

  def stop_deleted_project_use_record(virtual_machine, project_id_was)
    usage = VirtualMachineUsage.where(machine_id: virtual_machine.id,
                                      project_id: project_id_was,
                                      using_to: nil).last
    if usage
      usage.using_to = Time.now
      usage.duration = usage.using_to - usage.using_from
      usage.save!
    end
  end  

  def start_use_record(virtual_machine)
    usage = VirtualMachineUsage.new()
    usage.machine_id = virtual_machine.id
    usage.using_from = Time.now
    usage.group_id = virtual_machine.project.namespace_id
    usage.project_id = virtual_machine.project_id
    usage.save!
  end

end
