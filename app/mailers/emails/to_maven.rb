module Emails
  module ToMaven   
    def jar_deployed(ci_branch_id, email, jenkins_job_name, module_name, status, stdout)    
      @ci_branch = CiBranch.find(ci_branch_id)
      @module_name = module_name
      @status = status  
      @stdout = stdout   
      mail(to: email, subject: subject("jar deployed for ci #{jenkins_job_name}","module_name is #{module_name}"))  	
    end
  end
end
