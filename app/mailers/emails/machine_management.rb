module Emails
  module MachineManagement
    def machine_release_notify_in_advance(machine_id, last_update_time)
   
      @machine = VirtualMachine.find(machine_id)
      @ci_branch = @machine.ci_branch
      @project = Project.find(@machine.project_id)
      
      @last_update_time = last_update_time

      mail(to: @project.users.pluck('email').push('ezc@dianping.com'),
           subject: subject("Inactive machine will be released."))
    end

    def machine_release_notify(machine_id, last_update_time, attrs)
   
      @machine = VirtualMachine.find(machine_id)
      @ci_branch = CiBranch.find_by_id(attrs["build_branch_id"]) 
      @project = Project.find(attrs["project_id"])
      @module_name = attrs["module_name"]
      
      @last_update_time = last_update_time

      mail(to: @project.users.pluck('email').push('ezc@dianping.com'),
           subject: subject("Machine has been released"))
    end

  
  end
end
