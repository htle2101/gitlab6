module Emails
  module Notes
    def note_commit_email(recipient_id, note_id)
      @note = Note.find(note_id)
      @commit = @note.noteable
      @project = @note.project
      subject = subject("note for commit #{@commit.short_id}", @commit.title)
      recipient_id == @note.commit_author.try(:id) ? mark_important(subject) : subject
      mail(to: recipient(recipient_id), subject: subject)
    end

    def note_issue_email(recipient_id, note_id)
      @note = Note.find(note_id)
      @issue = @note.noteable
      @project = @note.project
      mail(to: recipient(recipient_id), subject: subject("note for issue ##{@issue.id}"))
    end

    def note_merge_request_email(recipient_id, note_id)
      @note = Note.find(note_id)
      @merge_request = @note.noteable
      @project = @note.project
      mail(to: recipient(recipient_id), subject: subject("note for merge request !#{@merge_request.id}"))
    end

    def note_wall_email(recipient_id, note_id)
      @note = Note.find(note_id)
      @project = @note.project
      mail(to: recipient(recipient_id), subject: subject("note on wall"))
    end

    def note_commit_author_email(commit_autor_email, note_id)
      @note = Note.find(note_id)
      @commit = @note.noteable
      @commit = CommitDecorator.decorate(@commit)
      @project = @note.project
      mail(to: commit_autor_email, subject: mark_important(subject("note for commit #{@commit.short_id}", @commit.title)))
    end

  end
end
