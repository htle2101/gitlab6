module Emails
  module Ci
    def machine_assign(machine_id, sudo_account, sudo_password)
      @machine = VirtualMachine.find(machine_id)
      @ci_branch = @machine.ci_branch
      @sudo_account = sudo_account
      @sudo_password = sudo_password
      mail(to: @machine.ci_branch.project.users.pluck('email'),
           subject: subject("Machine assigned"))
    end

    def war_deployed(ci_branch_id, deployed_wars)
      @ci_branch = CiBranch.find(ci_branch_id)
      @deployed_wars = deployed_wars
      mail(to: @ci_branch.project.users.pluck('email'),
           subject: subject("War deployed for ci `#{@ci_branch.jenkins_job_name}`"))
    end

    def apply_machine_failed(ci_branch_id)
      @ci_branch = CiBranch.find(ci_branch_id)
      @project = Project.find_by_name("gitlab")
      mail(to: @project.users.pluck("email"),
           subject: subject("There are no more extra machines to apply for ci '#{@ci_branch.jenkins_job_name}'!")) if @project
    end
  end
end
