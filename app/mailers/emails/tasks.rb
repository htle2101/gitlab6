module Emails
  module Tasks   
    def change_task_status_email(recipient_id, task_id)   
      @task = Task.find(task_id)
      mail(to: recipient(recipient_id), subject: subject("task !#{@task.id} swtich to #{@task.human_status_name}", @task.subject))  
    end
  end
end