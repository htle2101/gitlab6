# encoding: utf-8

class ScriptUploader < CarrierWave::Uploader::Base
  storage :file

  def initialize(project)
    @project = project
  end

  def store_dir
    "#{Settings.scripts['db_path']}/#{@project.path_with_namespace.gsub("/", "-")}/#{Time.now.strftime("%Y-%m-%d_%H-%M-%S")}"
  end

end
