#coding:utf-8

module TaskConfigs
  class LionContext < BaseContext

    def initialize(project, user, params = {})
      super(project, user, params)
      @task_config = params[:task_config]
      @task_id = params[:task_id]
      @task_config_id = params[:id]
    end

    def set_config
      result = ""
      TaskConfig::ENV_FIELD_NAMES.keys.each do |env|
        begin
          params = {
            "id" => 2,
            "p" => @task_config["name"],
            "e" => env.to_s,
            "k" => @task_config["key"],
            "v" => lion_v(env),
            "f" => "n/a",
            "ef" => lion_ef(env)
          }
          set_result = Service::Lion::Request.new.set_config(params)
          if set_result !~ /^0/
            result = result + "/#{env}出错:<<" + set_result + ">>"
          end
        rescue Service::Lion::LionSetConfigError => ex
          result = result + "lion 服务连接出错！" unless result =~ /lion 服务连接出错/
        end
      end
      result.blank? ? "Lion set ok." : result
    end

    def lion_ef(env)
      return 1 if [:dev,:alpha].include?(env)
      return 1 if (env == :qa) && (Task.find(@task_id).after_alpha?)
      0
    end

    def lion_v(env)
      if !@task_config[TaskConfig::ENV_FIELD_NAMES[env].to_s].blank?
        v = @task_config[TaskConfig::ENV_FIELD_NAMES[env].to_s]
      else
        if @task_config_id.blank?
          v = @task_config[TaskConfig::ENV_FIELD_NAMES[:product].to_s]
        else
          v = TaskConfig.find(@task_config_id).field_for(env)
          v = @task_config[TaskConfig::ENV_FIELD_NAMES[:product].to_s] if v.blank?
        end
      end
      v
    end

  end
end