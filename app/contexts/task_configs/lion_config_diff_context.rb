#coding:utf-8
module TaskConfigs
  class LionConfigDiffContext < BaseContext

    attr_accessor :result

    def initialize(project, user, params = {})
      super(project, user, params)
      @gs = GroupingStrategy.find(params[:id])
      @result = []
    end

    def execute
      gs_rollbackto = GroupingStrategy.where(:tag =>params[:current_tag], :group_id=>@gs.group_id).where("status in (1,7)").order("released_on").last if @gs.group_id != 0
      gs_rollbackto = GroupingStrategy.where(:tag =>params[:current_tag], :project_id => @gs.project_id).where("status in (1,7)").order("released_on").last if @gs.group_id == 0
      diff_params = {'e' => 'product', 'p' => @gs.appname , 't'=> gs_rollbackto.id}
      response = Service::Lion::Request.new.get_diff(diff_params)
      if Rails.env.development?
        @result = [{ key: 'a1', snapshot: 's1', current: 'c1' }, { key: 'a2', current: 'c2' }, { key: 'a3', snapshot: 's3' }]
        return true
      end

      if response !~ /^1\|/
        @result = JSON.parse(response)
        true
      else
        false
      end

    end

  end
end