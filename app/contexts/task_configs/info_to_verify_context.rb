module TaskConfigs
  class InfoToVerifyContext < BaseContext

    attr_accessor :to_pairs, :now_pairs, :env

    def initialize(project, user, params = {})
      super(project, user, params)
      @gs = GroupingStrategy.find(params[:id])
      @to_pairs = {}
      @now_pairs = {}
    end

    def execute
      return if @gs.ticket.package_tasks.blank?
      action = "deploy"
      if @gs.name != 'static'
        action = "prerelease"  if @gs.group_id == -255
        tcs = TaskConfig.where(name: @gs.package.name, task_id: @gs.ticket.package_tasks.map{|t| t.id }, type: 'Lion', swimlane: ["", @gs.swimlane])
        return if tcs.blank?
        action == "deploy" ? env = "product" : env = "prelease"
        tcs.each do |tc|
          params = { "e" => env, "k" => tc.key }
          result = Service::Lion::Request.new.get_config(params)
          to_value = ( action == "deploy" ?  tc.online_value : ( tc.offline_pre_release ||  tc.online_value ) )
          next if ( result=~/^1\|/ ) || ( result == to_value )
          @to_pairs[tc.key] = to_value
          @now_pairs[tc.key] = result
        end
      end
      @env = env
    end

  end
end