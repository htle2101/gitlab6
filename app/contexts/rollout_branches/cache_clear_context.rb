module RolloutBranches
  class CacheClearContext < BaseContext

  def initialize(jenkins_job_name)
     @rollout = BuildBranch.find_by_jenkins_job_name(jenkins_job_name)
     @type = @rollout.ci_env.name == "product" ? "release" : "prelease"
     @module_key = @rollout.id.to_s + "---"+ @rollout.type+"---"+@rollout.jenkins_job_name
     @build_number = @rollout.ci_env.server.job.get_current_build_number(@rollout.jenkins_job_name)
  end

  def execute
     return if $redis.get(@module_key).blank?
     module_names = $redis.get(@module_key).split(",")
     static_status
     @result = @rollout.clear_cache(module_names, @type) if !module_names.nil?
     @static_status = StaticStatus.where(:build_number =>@build_number,:build_branch_id => @rollout.id).first
     cache_status =  @result[:success] ? "SUCCESS" : "failure"
     @static_status.update_attributes(:cache_status => cache_status)
     $redis.del(@module_key)
      Logbin.logger('rolloutstatic').info("#{module_names.to_s} finish clear static cache", {
      project: @rollout.project.id,
      rollout: @rollout.id,
      commit: @rollout.project.repository.commit(@rollout.branch_name).id[0..9],
      type: @rollout.ci_env.name })
  end

  def static_status
     @rollout.imf.jenkins_datasource.hashable_builds.values.each do |value|
       @static_status = StaticStatus.where(:build_number => value["number"],:build_branch_id => @rollout.id).first
       @static_status = StaticStatus.new(:build_status => value["result"],:build_number => value["number"],:build_branch_id => @rollout.id) and @static_status.save if @static_status.blank?
       @static_status.update_attributes(:build_status => value["result"]) if value["result"] != @static_status.build_status
    end
  end

 end
end
