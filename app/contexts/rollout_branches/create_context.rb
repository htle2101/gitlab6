module RolloutBranches
  class CreateContext < BaseContext
    attr_reader :rollout_branch
    
    def initialize(project, user, params = {})
      super(project, user, params)
      @params = params.blank? ? { rollout_branch: { ci_env_name: (Settings.dp['ci_production'] || 'product') ,  package_type: @project.branch_for_sonar.try(:package_type), deployers: @project.branch_for_sonar.try(:deployers) } } : params.dup
      @rollout_branch = @project.rollout_branch
    end

    def execute
      if @rollout_branch.blank?
        @rollout_branch = single_create
        rollout_branch.project = @project
        rollout_branch.ci_env = CiEnv.find_by_name(params[:rollout_branch][:ci_env_name])
        rollout_branch.jenkins_job_name = "#{rollout_branch.ci_env.name}-#{@project.namespace.path}-#{@project.name}"  
        rollout_branch.package_type = params[:rollout_branch][:package_type]
        rollout_branch.deployers = params[:rollout_branch][:deployers]
        rollout_branch.branch_name = ( @project.default_branch || @project.repository.root_ref)

        rollout_branch.errors.add(:ci_template, 'ci template not found. please connect to ezc@dianping.com.') and return rollout_branch if rollout_branch.package_type.present? and rollout_branch.ci_template.blank? 
        return rollout_branch unless rollout_branch.save
      end
      create_jenkins_job
      rollout_branch.save
      rollout_branch
    end

    def single_create
      RolloutBranch.new
    end  

    def create_jenkins_job
      if rollout_branch.ci_env.server.job.exists?(rollout_branch.jenkins_job_name)
        rollout_branch.gitlab_created = false
      else
        rollout_branch.gitlab_created = true

        ci_template = rollout_branch.ci_template
        ci_template_xml = rollout_branch.ci_env.server.job.get_config(ci_template.template_name)

        ci_template_xml = ci_template.template(rollout_branch, ci_template_xml)
        rollout_branch.ci_env.server.job.create(rollout_branch.jenkins_job_name, ci_template_xml)
      end
    end

  end
end
