module RolloutBranches
  class CreatePackingContext < BaseContext

    def initialize(project, user, params = {})
      super(project, user, params)
    end

    def execute
      join_module_names

      success = true

      params[:packing][:branch_name] = params[:rollout_branch][:branch_name]

      build_packing_modules

      begin
        RolloutBranch.transaction do
          rollout.update_attributes!(params[:rollout_branch])
          packing.attributes = params[:packing].merge(commit: commit, author_id: current_user.id)
          packing.save!
          rollout.change_jenkins_and_build(packing)
          packing.update_status(:uploading)
        end
      rescue => e
        success = false
        rollout.errors.add(:base, "Some error occurred when create packing.")
        Gitlab::GitLogger.error(e)
      end

      packing.logger.info("Call jenkins to build, Waiting jenkins callback.")
      packing.logger.close

      [ success, rollout, packing ]
    end

    def rollout
      @rollout ||= project.rollout_branch
    end

    def packing
      @packing ||= RolloutPacking.new(build_branch_id: rollout.id)
    end

    def build_packing_modules
      params[:packing][:module_names].split(",").each do |module_name|
        packing.modules.build(name: module_name, paas_version: pom[module_name].paas.try(:version))
        packing.logger.info("try get paas version. #{params[:rollout_branch][:branch_name]},#{pom[module_name].paas.try(:version)}")
      end
    end

    def join_module_names
      params[:packing][:module_names] = params[:packing][:module_names].reject! {|m| m.blank?}.join(",")
    end

    def commit
      project.tree(params[:rollout_branch][:branch_name]).sha
    end

    def pom
      @pom ||= project.tree(params[:rollout_branch][:branch_name]).hashed_pom
    end

  end
end
