require 'pathname'

module Scripts
  module Upload
    def process_upload
      uploader = ScriptUploader.new(project)
      uploader.store!(params[:file])

      @script.name = uploader.filename

      pn = Pathname.new(uploader.path)
      
      @script.path = pn.relative_path_from(Pathname.new(Settings.scripts.db_path)).to_s
      @script.data ||= []
      @script.data << { name: @script.name, path: @script.path, uploaded_at: Time.now }
    end

  end
end
