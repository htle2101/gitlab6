require 'pathname'

module Scripts

  class CreateContext < BaseContext

    include Upload

    def execute
      @script = project.scripts.build(params[:script])

      begin
        if params[:file].blank?
          @script.valid?
          @script.errors.add(:base, "You should upload a script file.")
          return @script
        end

        @script.author = current_user
        process_upload

        if @script.save
          Logbin.logger("scripts").info("Notify tools.dba script info created.", @script.dba_upload_params)
          @script.notify_dba_script_upload
        end
      rescue => e
        Gitlab::GitLogger.error(e)
        Logbin.logger("scripts").error("Create script info failed, #{e}.", @script.dba_upload_params)
        @script.errors(:base, "Some error occurred when create script upload task.")
      end

      @script
    end

  end
end
