module Scripts

  class ExecuteContext < BaseContext

    def execute
      begin
        @script = project.scripts.find(params[:id])
        if !@script.can_execute? || execute_status_mismatch?
          return "The status of this task is mismatch, you can not do this job."
        end
        Logbin.logger("scripts").info("Notify tools.dba script to execute.", @script.dba_execute_params)
        env = Script::ENV_TYPE.invert[params[:env].to_i]
        success = env ? @script.send("execute_#{env}") : false
        @script.notify_dba_script_execute(params[:env].to_i) if success
        success
      rescue => e
        Logbin.logger("scripts").error("Fail to send execute script command, #{e}.", @script.dba_execute_params)
        Gitlab::GitLogger.error(e)
        false
      end
    end

    def execute_status_mismatch?
      @script.current_execute_type.blank? || params[:env].blank? || @script.current_execute_type != params[:env].to_i
    end

  end

end
