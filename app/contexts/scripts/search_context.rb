require 'pathname'

module Scripts
  class SearchContext < BaseContext

    def execute
      scripts = @project.scripts.order("created_at desc").page(params[:page]).per(10)
      scripts = scripts.where(["name like ?", "%#{params[:name]}%"]) if params[:name].present?
      scripts = scripts.where(["db_name like ?", "%#{params[:db_name]}"]) if params[:db_name].present?
      scripts = scripts.where(status: params[:status]) if params[:status].present?
      scripts = scripts.where(todo: params[:todo]) if params[:todo].present?
      scripts = scripts.where(["DATE(created_at) = ?", params[:created_at]]) if params[:created_at].present?

      scripts = case params[:scope]
              when 'created-by-me' then scripts.created_by(current_user)
              else scripts
              end
      scripts
    end

  end
end
