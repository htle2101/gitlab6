require 'pathname'

module Scripts

  class UpdateContext < BaseContext

    include Upload

    def execute
      begin
        @script = project.scripts.find(params[:id])
        return @script unless @script.valid?
        process_upload if params[:file].present?
        unless @script.back_to_not_review
          @script.errors.add(:base, "Update failed, status has been changed.")
          return @script
        end
        @script.attributes = params[:script]
        if @script.save
          @script.notify_dba_script_reupload
          Logbin.logger("scripts").info("Notify tools.dba script info changed.", @script.dba_upload_params)
        end
      rescue => e
        Gitlab::GitLogger.error(e)
        Logbin.logger("scripts").error("Update script info failed, #{e}.", @script.dba_upload_params)
        @script.errors.add(:base, "Some error occurred when update script upload task.") if @script
      end
      @script
    end

  end

end
