module Scripts

  class CloseContext < BaseContext

    def execute
      begin
        @script = project.scripts.find(params[:id])
        Logbin.logger("scripts").info("Notify tools.dba script to close.", @script.dba_execute_params)
        if @script.can_close?
          success = @script.close
          @script.notify_dba_script_close if success
          success
        else
          false
        end
      rescue => e
        Logbin.logger("scripts").error("Fail to send close script command, #{e}.", @script.dba_execute_params)
        Gitlab::GitLogger.error(e)
        false
      end
    end

  end

end
