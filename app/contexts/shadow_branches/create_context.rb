module ShadowBranches
  class CreateContext < CiBranches::CreateContext

    def initialize(project, user, params)
      @project, @current_user, @params = project, user, params.dup
    end

    protected

    def jenkins_job_name(env)
      "#{env}-#{@project.namespace.path}-#{@project.name}"
    end

    def repo_url
      "#{Settings.dp.ssh_path_prefix}#{@project.path_with_namespace}.git"
    end

    def find_env
      env_name = params[:ci_branch][:ci_env]

      CiEnv.find_by_project_and_name(@project, env_name).shadow_env
    end

    def find_build_branch
      project.shadow_branchs.new()
    end
  end
end
