class SearchContext
  attr_accessor :project_ids, :params

  def initialize(project_ids, params)
    @project_ids, @params = project_ids, params.dup
  end

  def execute
    query = params[:search]
    username = params[:username]
    projectname = params[:projectname]
    createdat = params[:createdat]

    return result unless query.present? || username.present? || projectname.present? || createdat.present?

    if project_ids.length == 1
      projects = Project.where("id in (?) ", project_ids)
    else
      projects = Project.where("id in (?) or public = ?", project_ids, 1)
    end

    result[:projects] = projects.search(query).limit(10) + ProjectModule.search_by_artifactid(query)

    # Search inside singe project
    project = projects.first if projects.length == 1

    if params[:search_code].present?
      project_id = project.id if projects.length == 1
      searcher = Service::Search::RepoSearcher.new(query, page: params[:page], project_id: project_id)
      result[:blobs] = searcher.search
      result[:keyword] = searcher.keyword
    else
      result[:merge_requests] = MergeRequest.in_projects(project_ids).search(query).order('updated_at DESC').limit(20)
      result[:issues] = Issue.where(project_id: project_ids).search(query).order('updated_at DESC').limit(20)
      result[:wiki_pages] = []
      result[:comments] = Note.where(project_id: project_ids).search(username, projectname, createdat)
    end
    result
  end

  def result
    @result ||= {
      projects: [],
      merge_requests: [],
      issues: [],
      wiki_pages: [],
      comments: [],
      blobs: []
    }
  end

end
