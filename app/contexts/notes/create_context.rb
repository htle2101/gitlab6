module Notes
  class CreateContext < BaseContext
    def execute
      note = project.notes.new(params[:note])
      note.author = current_user

      note.notify = true if params[:notify] == '1'

      note.notify_above = true if params[:notify_above] == '1'
      note.notify_author = true if params[:notify_author] == '1'

      note.save

      update_merge_request(note)

      note
    end

    def update_merge_request(note)
      return if !note.for_merge_request? || note.noteable_id.blank?
      mr = MergeRequest.find(note.noteable_id)
      mr.reviewed = true and mr.save if mr.present? && mr.reviewed?
    end
  end
end
