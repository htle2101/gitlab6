class BuildLoadContext < BaseContext

  def execute
    filter = params[:f] || "all"
    events = filter.in?(["all", "wip"]) ? send(filter.to_sym) : []
    {stats_for_all: stats_for_all, stats_for_wip: stats_for_wip, events: events}
  end

  def stats_for_all
    counts = Event.unscoped.ci_build.group("action").count
    total = counts.values.inject(:+) || 0
    target = total - (counts[Event::SUCCESS] || 0)
    { target: target, total: total, precent: total == 0 ? 0 : (target * 1.0 / total * 100).round(2) }
  end

  def stats_for_wip
    counts = ActiveRecord::Base.connection.select_all(arel_for_wip("action, count(action) as count_all").group("action"))

    target, total = 0, 0
    counts.each do |c|
      target += c["count_all"] if c["action"] != Event::SUCCESS
      total += c["count_all"]
    end
    { target: target, total: total, precent: total == 0 ? 0 : (target * 1.0 / total * 100).round(2) }
  end

  def all
    Event.unscoped.ci_build.ci_exists.order('created_at desc').page(params[:page]).per(20)
  end

  def wip
    arel_for_wip("*").ci_exists.order('created_at desc').page(params[:page]).per(20)
  end

  def arel_for_wip(projection)
    subquery = Event.unscoped.ci_build.ci_exists.select("target_id, max(created_at) created_at").group("target_id")
    Event.unscoped.ci_build.ci_exists.select(projection).where(
      "exists (select 1 from (#{subquery.arel.to_sql}) sub where sub.target_id = events.target_id and sub.created_at = events.created_at)")
  end
end
