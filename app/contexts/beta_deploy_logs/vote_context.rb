module BetaDeployLogs
  class VoteContext < BaseContext
    attr_accessor :log, :user, :params

    def initialize(log, user, params)
      @log = log
      @user = user
      @params = params
    end

    def execute
      vote = BetaDeployVote.find_by_user_id_and_beta_deploy_log_id(user.id, log.id)

      if vote && vote.up? && params[:type] == 'down'
        from_down_to_up(vote)
      elsif vote && vote.down? && params[:type] == 'up'
        from_up_to_down(vote)
      elsif params[:type] == 'up' || params[:type] == 'down'
        create_new_vote
      end
    end

    private

    def create_new_vote
      vote = BetaDeployVote.new(
        user_id: user.id,
        beta_deploy_log_id: log.id,
        type: params[:type])

      if vote.save
        if vote.up?
          BetaDeployLog.increment_counter(:voteup_count, log.id)
        else
          BetaDeployLog.increment_counter(:votedown_count, log.id)
        end
      end
    end

    def from_up_to_down(vote)
      vote.update_attributes(type: 'up')

      BetaDeployLog.increment_counter(:voteup_count, log.id)
      BetaDeployLog.decrement_counter(:votedown_count, log.id)
    end

    def from_down_to_up(vote)
      vote.update_attributes(type: 'down')

      BetaDeployLog.increment_counter(:votedown_count, log.id)
      BetaDeployLog.decrement_counter(:voteup_count, log.id)
    end
  end
end
