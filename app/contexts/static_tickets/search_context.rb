module StaticTickets
  class SearchContext < BaseContext

    include Common

    def initialize(project, user, params = {})
      super(project, user, params)
    end

    def execute
      tickets = Ticket.where(id: Package.type_is('static').where('project_id=?', @project.id).pluck(:ticket_id)).order("released_at asc").page(params[:page]).per(10)
      filter(tickets)
    end

  end
end
