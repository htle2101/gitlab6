module StaticTickets
  class CreateContext < BaseContext

    def initialize(project, user, params = {})
      super(project, user, params)
    end

    def execute

      ticket = Ticket.where(:project_id => @project.id, :released_at => Date.today).first

      if ticket.blank?
        ticket = @project.tickets.build()
        ticket.released_at = Date.today
        ticket.project_id = project.id
        begin
          Ticket.transaction do
            ticket.save!
          end
        rescue => e
          ticket.errors.add(:base, "Some error occurred when create ticket.")
          Gitlab::GitLogger.error(e)
        end
      end
      ticket
    end

  end
end