#coding:utf-8
module Checkers
  class CreateContext < BaseContext

    def initialize(project, user, params = {})
      @project, @current_user, @params = project, user, params.dup
    end

    def execute
      @task = Task.find(params[:task_id])
      case params[:create_style]
      when '1' then
        create_one_item
      when '2' then
        create_items_by_template
      end
    end

    protected
    def create_one_item
      begin
        checker = Checker.new(params[:checker])
        checker.author = current_user
        checker.task = @task
        checker.checked = 0
        checker.save
        true
      rescue => e
        false
      end
    end

    def create_items_by_template
      begin
        ['OP上线前检查','DBA上线前检查','QA上线前检查'].each do |name|
          checker = Checker.new()
          checker.author = current_user
          checker.task = @task
          checker.checked = 0
          checker.description = name
          checker.save
        end
        true
      rescue => e
        false
      end
    end

  end
end
