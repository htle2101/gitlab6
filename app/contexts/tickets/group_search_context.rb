module Tickets
  class GroupSearchContext

    attr_accessor :group, :params

    include Common

    def initialize(group, params = {})
      @group = group
      @params = params
    end

    def execute
      project_ids = @group.projects.pluck("id")
      tickets = Ticket.where(project_id: project_ids).order("released_at asc").page(params[:page]).per(10)
      tickets = tickets.where(project_id: params[:project_id]) if params[:project_id].present?
      filter(tickets)
    end

  end
end
