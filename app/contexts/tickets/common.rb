module Tickets
  module Common
    def filter(tickets)
      tickets = tickets.where(status: params[:status]) if params[:status].present?

      if params[:released_at].present?
        tickets = tickets.where(released_at: params[:released_at])
      else
        tickets = case params[:scope]
                when nil, '' then tickets.where(released_at: Time.now.to_date)
                when 'since-today' then tickets.where("released_at >= '#{Time.now.to_date}'")
                when 'tomorrow' then tickets.where(released_at: 1.day.since.to_date)
                else tickets 
                end
      end

      tickets
    end
  end
end
