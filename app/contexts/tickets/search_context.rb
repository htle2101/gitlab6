module Tickets
  class SearchContext < BaseContext

    include Common

    def initialize(project, user, params = {})
      super(project, user, params)
    end

    def execute
      tickets = @project.tickets.order("released_at asc").page(params[:page]).per(10)
      filter(tickets)
    end

  end
end
