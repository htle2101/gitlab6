module Tasks
  class DestroyContext < BaseContext

    def initialize(project, user, params = {})
      super(project, user, params)
      @task = project.tasks.find(params[:id])
    end

    def execute
      begin
        Task.transaction do
          tickets = @task.packages.map(&:ticket).compact
          @task.packages.each do |package|
            package.destroy if package.tasks.count == 1
          end
          tickets.uniq.each do |ticket|
            ticket.destroy if ticket.packages.count == 0
          end
          @task.destroy
        end
      rescue => e
        @task.errors.add(:base, "Some error occurred when save task.")
        Gitlab::GitLogger.error(e)
      end
      @task
    end

  end
end
