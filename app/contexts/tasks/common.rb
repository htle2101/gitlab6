module Tasks
  module Common
    def build_packages_and_tickets
      grouped_modules = group_modules_by_projects
      tickets, packages = [], []
      grouped_modules.each do |project_id, modules|
        project = Project.find(project_id)
        if params[:task][:released_at].present?
          ticket = project.tickets.find_or_initialize_by_released_at(params[:task][:released_at])
          tickets << ticket
        end
        modules.each do |name|
          if ticket.present? && ticket.persisted?
            package = Package.owner_is(:task).where(name: name, project_id: project.id, ticket_id: ticket.id).first
          end
          if package.blank?
            exist_package = @task.packages.detect { |p| p.name == name }
            if exist_package.present? && exist_package.tasks.count == 1
              package = exist_package
              non_dependency_tickets << package.ticket
              package.ticket = ticket
            end
          end
          # if !ticket.packages.where(:name =>name).blank?
          # package ||= Package.owner_is(:task).new(ticket: ticket, project: project, name: name)
          package = ticket.packages.where(:name=>name).first if ticket.present? && ticket.persisted?
          package = Package.owner_is(:task).new(ticket: ticket, project: project, name: name) if package.blank?
          packages << package
        end
      end
      non_dependency_packages.concat(@task.packages - packages)
      non_dependency_tickets.concat(non_dependency_packages.map(&:ticket))
      [tickets, packages]
    end

    def group_modules_by_projects
      modules.inject({}) do |hash, m|
        project_id, module_name = m.split("|")
        hash[project_id] ||= []
        hash[project_id] << module_name
        hash
      end
    end

    def modules_blank?
      return false if modules.present?
      @task.errors.add(:base, "You must to select at least one module.")
      true
    end

    def released_date_valid?
      valid = true
      if params[:task][:released_at].present?
        begin
          Date.parse(params[:task][:released_at])
          valid = true
        rescue
          @task.errors.add(:base, "Release date isn't a valid date.")
          valid = false
        end
      end
      valid
    end

    def modules
      @modules ||= params[:modules].present? ? params[:modules].reject { |m| m.blank? } : []
    end

    def non_dependency_tickets
      @non_dependency_tickets ||= []
    end

    def non_dependency_packages
      @non_dependency_packages ||= []
    end
  end
end
