module Tasks
  class CreateContext < BaseContext

    include Common

    def initialize(project, user, params = {})
      super(project, user, params)
    end

    def execute
      @task = @project.tasks.build(params[:task])
      @task.creator = current_user
      return @task if !@task.valid? | !released_date_valid?
      tickets, packages = build_packages_and_tickets
      begin
        Task.transaction do
          tickets.map(&:save!)
          packages.map(&:save!)
					@task.packages = packages
          @task.save!
        end
      rescue => e
        @task.errors.add(:base, "Some error occurred when save task.")
        Gitlab::GitLogger.error(e)
      end
      @task
    end

  end
end
