module Tasks
  class SearchContext < BaseContext

    def initialize(project, user, params = {})
      super(project, user, params)
    end

    def execute
      tasks = @project.tasks.order("released_at desc").page(params[:page]).per(10)
      tasks = tasks.where(assignee_id: params[:assignee_id]) if params[:assignee_id].present?
      tasks = tasks.where(status: params[:status]) if params[:status].present?
      tasks = tasks.where(released_at: params[:released_at]) if params[:released_at].present?

      tasks = case params[:scope]
              when 'assigned-to-me' then tasks.assigned_to(current_user)
              when 'created-by-me' then tasks.created_by(current_user)
              else tasks
              end
      tasks
    end

  end
end
