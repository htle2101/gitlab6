module Tasks
  class UpdateContext < BaseContext

    include Common

    def initialize(project, user, params = {})
      super(project, user, params)
      @task = Task.find(params[:id])
    end

    def execute
      @task.attributes = params[:task]
      return @task if !@task.valid?
      tickets, packages = build_packages_and_tickets
      begin
        Task.transaction do
          tickets.map(&:save!)
          packages.map(&:save!)
          @task.packages = packages
          @task.save!
          non_dependency_packages.compact.uniq.each do |package|
            package.destroy if package.tasks.count == 0
          end
          non_dependency_tickets.compact.uniq.each do |ticket|
            ticket.destroy if ticket.packages.count == 0
          end
        end
      rescue => e
        @task.errors.add(:base, "Some error occurred when save task.")
        @task.errors.add(:tickets, tickets.map{|t| t.errors.full_messages }.join(' ') )
        @task.errors.add(:packages, packages.map{|p| p.errors.full_messages }.join(' ') )
        Gitlab::GitLogger.error(e)
      end
      @task
    end

  end
end
