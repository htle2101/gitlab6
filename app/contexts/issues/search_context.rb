module Issues
  class SearchContext < BaseContext
    attr_accessor :issues

    def initialize(group_ids, params)
      @group_ids, @params = group_ids, params.dup
    end

    def execute
      @issues = Issue.where(project_id: Project.where(namespace_id: @group_ids).pluck(:id))

      @issues = case params[:status]
                when 'all' then @issues
                when 'closed' then @issues.closed
                else @issues.opened
                end

      @issues = case params[:scope]
                when 'assigned' then @issues.assigned
                when 'all' then @issues
                else @issues.unassigned
                end          

      @issues = @issues.tagged_with(params[:label_name]) if params[:label_name].present?
      @issues = @issues.page(params[:page]).per(20).order('id desc')

      @issues
    end
  end
end