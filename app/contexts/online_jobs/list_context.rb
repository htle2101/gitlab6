module OnlineJobs
  class ListContext < BaseContext
    attr_accessor :online_jobs

    def execute
      @online_jobs = @project.online_jobs.order("created_at desc")

      if params[:env].present?
        @online_jobs = @online_jobs.where(env: params[:env])
      end

      online_jobs
    end

  end
end