module CiBranches
  class UpdateContext < BaseContext

    attr_accessor :ci_branch, :new_namespace

    def initialize(project, ci_branch, new_namespace)
      @project, @ci_branch, @new_namespace = project, ci_branch, new_namespace
    end

    def execute
      change_jenkins_git_url
      change_jenkins_job_name
      change_jenkins_post_step
    end

    protected

    def old_job_name
      ci_branch.jenkins_job_name
    end

    def new_job_name(env)
      name = "#{env}-#{new_namespace}-#{project.name}"
      name = name + "-#{ci_branch.branch_name.gsub('/','_')}" if env == 'alpha'
      name
    end

    def find_env
      CiEnv.find(ci_branch.ci_env_id)
    end

    def jenkins
      find_env.server
    end

    def change_jenkins_git_url
      new_git_url = "#{Settings.dp.ssh_path_prefix}#{new_namespace}/#{project.path}.git"
      jenkins.job.post_config(old_job_name, CiTemplate.change_git_url(new_git_url, jenkins.job.get_config(old_job_name), ci_branch.deployers)) if !jenkins.job.list(old_job_name).blank?
    end

    def change_jenkins_job_name
      if !jenkins.job.list(old_job_name).blank?
        jenkins.job.rename(old_job_name,new_job_name(find_env.name))
      end
      ci_branch.update_attributes(
        jenkins_job_name: new_job_name(find_env.name)
      )
    end

    def change_jenkins_post_step
      jenkins.job.post_config(new_job_name(find_env.name), CiTemplate.change_post_step(ci_branch, jenkins.job.get_config(new_job_name(find_env.name)))) if !jenkins.job.list(old_job_name).blank?
    end


  end
end
