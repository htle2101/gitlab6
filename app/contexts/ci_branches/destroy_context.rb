module CiBranches
  class DestroyContext < BuildBranches::DestroyContext

    include Utils::Logger
    attr_reader :ci_branch,:errors

    def initialize(ci_branch_id)
      @ci_branch = BuildBranch.find(ci_branch_id)
      @errors = ""
    end

    def execute
      Rails.cache.delete("remove-#{ci_branch.project_id}-#{ci_branch.branch_name}-errors")
      Rails.cache.delete(ci_branch.id.to_s + '_self_modules_to_deploy_for_phoenix')
      Rails.cache.delete(ci_branch.id.to_s + '_app_default_domain')
      destory_related_resources(ci_branch.shadow_branch) unless ci_branch.try(:shadow_branch).blank?
      destory_related_resources(ci_branch.sonar_branch) unless ci_branch.try(:sonar_branch).blank?
      destory_related_resources(ci_branch)

      if !@errors.blank?
        Rails.cache.fetch("remove-#{ci_branch.project_id}-#{ci_branch.branch_name}-errors") do
          @errors
        end
      end

      project_id = ci_branch.project_id
      ci_branch_id = ci_branch.id
      remove_build_branch(ci_branch.shadow_branch) unless ci_branch.try(:shadow_branch).blank?
      remove_build_branch(ci_branch.sonar_branch) unless ci_branch.try(:sonar_branch).blank?
      remove_build_branch(ci_branch)
      $redis.srem("ci_branch:destroy:queue", "#{project_id}-#{ci_branch_id}")
    end

  end
end
