module CiBranches
  class ChangeBranchContext < BaseContext

    def initialize(project, user, params)
      @project, @current_user, @params = project, user, params.dup
    end

    def execute
      if !jenkins.job.exists?(find_ci_branch.jenkins_job_name)
        ci_template = find_ci_branch.ci_template
        ci_template_xml = jenkins.job.get_config(ci_template.template_name)
        ci_template_xml = ci_template.template(find_ci_branch, ci_template_xml)
        jenkins.job.create(find_ci_branch.jenkins_job_name, ci_template_xml)
      else
      change_jenkins_job_ref
      change_jenkins_job_name
      change_branch_and_job_in_db
      change_jenkins_post_step
      end
        unbind_difference_module_machines
        clean_branch_cache
    end

    protected
    def find_ci_branch
      @find_ci_branch ||= CiBranch.find_by_branch_name_and_project_id(old_branch_name, project.id)
    end

    def old_branch_name
      params[:id]
    end

    def new_branch_name
      @new_branch_name = params[:ci_branch][:branch_name]
    end

    def new_job_name(env)
      name = "#{env}-#{project.namespace.path}-#{project.name}"
      name = name + "-#{new_branch_name.gsub('/','_')}" unless 'beta' == env
      name
    end 

    def find_env
      CiEnv.find(find_ci_branch.ci_env_id)
    end

    def jenkins
      find_env.server
    end

    def job_name
      find_ci_branch.jenkins_job_name
    end

    def change_jenkins_job_ref
      jenkins.job.post_config(job_name, CiTemplate.change_ref(new_branch_name, jenkins.job.get_config(job_name)))
    end

    def change_jenkins_job_name
      jenkins.job.rename(job_name,new_job_name('alpha'))
    end

    def change_branch_and_job_in_db
      find_ci_branch.update_attributes(
        branch_name: new_branch_name,
        jenkins_job_name: new_job_name('alpha')
      )
    end

    def change_jenkins_post_step
      jenkins.job.post_config(new_job_name('alpha'), CiTemplate.change_post_step(CiBranch.find_by_branch_name_and_project_id(new_branch_name, project.id), jenkins.job.get_config(new_job_name('alpha'))))
    end


    def unbind_difference_module_machines
      old_branch_modules = project.tree(old_branch_name).pom.select {|m| ['war'].include?(m[:type])}.map(&:module_name)
      new_branch_modules = project.tree(new_branch_name).pom.select {|m| ['war'].include?(m[:type])}.map(&:module_name)
      difference_modules = old_branch_modules - new_branch_modules

      difference_modules.each do |m|
        machine = VirtualMachine.where(:project_id => project.id, :build_branch_id => find_ci_branch.id, :module_name => m).first
        if machine
          MachineUnbindWorker.perform_async(machine.build_branch_id, machine.ip)
        end
      end
    end

    def clean_branch_cache
      CiBranch.find_by_branch_name_and_project_id(new_branch_name, project.id).expire_cache
    end
  end
end
