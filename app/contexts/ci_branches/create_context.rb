module CiBranches
  class CreateContext < BaseContext

    def initialize(project, user, params)
      @project, @current_user, @params = project, user, params.dup
    end

    def execute
      env = find_env
      ci_branch = find_build_branch
      ci_branch.branch_name = params[:ci_branch][:branch_name]
      ci_branch.ci_env = env
      @branch_name = ci_branch.branch_name
      @ci_branch_name =  jenkins_job_name(ci_branch.ci_env.name)
      ci_branch.jenkins_job_name = @ci_branch_name
      ci_branch.package_type = params[:ci_branch][:package_type].empty? ? params[:ci_branch][:root_type] : params[:ci_branch][:package_type]

      ci_template = ci_branch.ci_template
      ci_branch.deployers = ci_template.try(:deployers)
      
      return false unless ci_branch.create_check

      if env.server.job.exists?(ci_branch.jenkins_job_name)
        ci_branch.gitlab_created = false
      else
        ci_branch.gitlab_created = true

        ci_template_xml = env.server.job.get_config(ci_template.template_name)
        ci_template_xml = ci_template.template(ci_branch, ci_template_xml)
        env.server.job.create(ci_branch.jenkins_job_name, ci_template_xml)
      end

      ci_branch.auto_deploy = false if ci_branch.beta?

      ci_branch.save
      ci_branch.project.hooks.create(url: env.jenkins_hook_path) unless ci_branch.project.hooks.exists?(url: env.jenkins_hook_path)
      ci_branch.project.hooks.create(url: ci_branch.destory_hook_path) unless ci_branch.project.hooks.exists?(url: ci_branch.destory_hook_path)
      ci_branch
    end

    protected

    def jenkins_job_name(env)
      name = "#{env}-#{@project.namespace.path}-#{@project.name}"
      name = name + "-#{@branch_name.gsub('/','_')}" unless 'beta' == env
      name
    end

    def repo_url
      "#{Settings.dp.ssh_path_prefix}#{@project.path_with_namespace}.git"
    end

    def find_env
      env_name = params[:ci_branch][:ci_env]

      CiEnv.find_by_project_and_name(@project, env_name)
    end

    def find_build_branch
      project.ci_branchs.new()
    end
  end
end
