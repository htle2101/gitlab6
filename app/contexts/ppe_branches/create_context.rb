module PpeBranches
  class CreateContext < RolloutBranches::CreateContext

    def initialize(project, user, params = {})
      super(project, user, params)
      @params = params.blank? ? { rollout_branch: { ci_env_name: (Settings.dp['ci_ppe'] || 'ppe') ,  package_type: @project.branch_for_sonar.try(:package_type), deployers: @project.branch_for_sonar.try(:deployers) } } : params.dup
      @rollout_branch = @project.ppe_branch
    end

    def single_create
      PpeBranch.new
    end

  end
end