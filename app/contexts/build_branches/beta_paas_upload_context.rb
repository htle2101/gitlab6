module BuildBranches
  class BetaPaasUploadContext < BaseContext 
    def initialize(id)
      @bp = BetaPaasRollout.find(id)
      @build_branch = @bp.build_branch
      $redis.del("logger_file")
      logger_file = @bp.logger_binder
      $redis.set("logger_file",logger_file)
  	end

  	def execute
      @bp.update_attributes(operation_id: nil)
      @bp.to_deploy_war
      result = []
      @bp.packages.each do |package|
        result << upload_to_paas(package) if download_file_from_jenkins(package)
        create_deploy_event
      end
      if !result.include?(false)
        @bp.succeed
        true
      else
        @bp.failed
        false
      end
  	end

    def module_name_list
      modules = []
      build_branch.modules.inject([]) do |packages,m|
        modules << m.module_name if m[:type] == 'war' && m.module_name =='tuangou-mt-web'
      end
      modules
    end

    def module_in_paas?(module_name)
      result = false
      groups = DianpingPaas::Client.new(url: @bp.get_host).groups(@bp.appname) rescue nil
      result = true if !groups.blank?
    end

    def temp_path(package)
      extname = File.extname(name(package))
      basename = File.basename(name(package), extname)
      "#{Settings.ftp['tmp_path']}/#{basename}-#{dir_name}#{extname}"
    end

    def download_file_from_jenkins(package)
      system("wget --quiet -O #{temp_path(package)} #{package.url}")
    end

    def name(package)
      if package.ci_branch.nodejs? || package.ci_branch.zipfile?
        "#{package.module_name}-#{package.ci_env.label}-#{package.version}.#{package.ext}"
      elsif package.ci_branch.php?
        "#{package.module_name}-#{package.ci_env.label}-#{package.version}.#{package.ext}"
      else
        "#{package.warName}-#{package.ci_env.label}-#{package.version}.#{package.ext}"
      end
    end

    def dir_name
      "#{ts}_#{@bp.tag}"
    end

    def ts
      @ts ||= Time.now.strftime("%Y-%m-%d_%H-%M-%S")
    end

    def upload_to_paas(package)
      begin
        logger.info("begin upload war to paas.")
        result = DianpingPaas::Client.new(url: @bp.get_host).upload_war(@bp.appname, @bp.paas_version, temp_path(package))
        @bp.update_attributes(status:102) if result
        logger.info("upload war to paas successfully.")
        return true
      rescue => e
        logger.error("upload war to paas error. #{e}")
        return false
      end
    end

    def logger
      @bp.logger
    end

    def beta_app_group_id(name)
      groups = DianpingPaas::Client.new(url: @bp.get_host).groups(name) rescue nil
      groups['groups'].first['id']
    end


    def create_deploy_event
      Event.create(
          project: @build_branch.project,
          target_id: @build_branch.id,
          target_type: 'CiBranch',
          action: 'SUCCESS',
          title: @build_branch.ci_env.name
        )
    end
  end
end
