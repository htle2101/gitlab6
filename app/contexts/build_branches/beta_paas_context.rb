module BuildBranches
  class BetaPaasContext < BaseContext
  	def initialize(params)
  		@ci_branch_id = params['ci_branch_id']
  		@commit = params['commit']
  		@appnames = params['appname']
  	end

  	def execute
      @appnames.each do |appname|
  		  @beta_paas_rollout = BetaPaasRollout.find_by_appname_and_build_branch_id_and_tag(
                            appname,@ci_branch_id,@commit)
  			if @beta_paas_rollout.blank?
  				BetaPaasRollout.new(:appname => appname,:build_branch_id => @ci_branch_id,
  														:tag =>@commit).save
  			else
  				@beta_paas_rollout
  			end
      end

  	end

  end
end