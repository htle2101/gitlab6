module BuildBranches
  class DestroyContext < BaseContext

    include Utils::Logger
    attr_reader :build_branch,:errors

    def initialize(build_branch_id)
      @build_branch = BuildBranch.find(build_branch_id)
      @errors = ""
    end

    def execute
      destory_related_resources(build_branch)
      remove_build_branch(build_branch)
    end

    protected
    def destory_related_resources(build_branch)
      deal_with_machines(build_branch)
      delete_jenkins_job(build_branch)
    end

    def deal_with_machines(build_branch)
      build_branch.machines.each do |machine|
        if machine.ip.blank? || build_branch.ci_env.is_cmdb
          machine.destroy
        else
          unbind = ::VirtualMachines::UnbindContext.new(machine.build_branch_id, machine.ip)
          @errors = @errors + unbind.errors unless unbind.execute
        end
      end
    end

    def delete_jenkins_job(build_branch)
      begin
        build_branch.ci_env.server.job.delete(build_branch.jenkins_job_name)
      rescue => e
        logger.error("Delete jenkins job error, #{e}.")
        @errors = @errors + "[Delete jenkins job error, #{e}.] "
      end
    end

    def remove_build_branch(build_branch)
      build_branch.destroy
    end
  end
end
