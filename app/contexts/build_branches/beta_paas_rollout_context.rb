module BuildBranches
  class BetaPaasRolloutContext < BaseContext
    def initialize(id)
      @build_branch = BuildBranch.find(id)
      @commit = Gitlab::Git::Commit.last_for_path(@build_branch.project.repository, @build_branch.branch_name, @path).sha   		
    end

    def execute
      appnames = @build_branch.modules_with_type(@build_branch.branch_name,'war').map{|p|p.module_name}
      appnames.each do |appname|
        pm = ProjectModule.find_by_project_id_and_module_name(@build_branch.project_id,appname)
        if !pm.blank? && pm.has_beta_paas == 1 && @build_branch.ci_env.name == "beta"
          beta_paas_rollout = BetaPaasRollout.find_or_create_by_appname_and_build_branch_id_and_tag(appname,@build_branch.id,@commit)
          beta_paas_rollout.deploy(@commit)  if !beta_paas_rollout.blank? && !beta_paas_rollout.get_ip_from_paas(appname).nil?
        end
      end
    end

  end
end
