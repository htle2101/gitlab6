#coding:utf-8
module App
  class WorkflowContext < BaseContext

    def initialize(project, user, params = {})
      @project, @current_user, @params = project, user, params.dup
    end

    def execute
      Service::Op::Workflow.new.transition_status(@params['app'], @params['deploy_time'])
    end
  end
end