#coding:utf-8
module PaasRollouts
  class SearchContext < BaseContext

    def initialize(project,current_user,params)
      @params = params
      @project = project
      @package = Package.find(params[:package_id])
      @tag = params[:tag] || ""
    end

    def execute      
      @paas_new_format = []
      @paas_rollouts_new = DianpingPaas::Client.new(url:ProjectModule.find_by_module_name_and_project_id(@package.name,@project.id).paas_url).groups(@package.warname || @package.name) rescue nil
      @paas_rollouts_exists = PaasRollout.where(:appname =>@package.warname || @package.name,:tag =>@tag,:ticket_id =>@package.ticket_id).collect{|pr|{appname:pr.appname,group_id:pr.group_id,name:pr.name,package_id:pr.package_id,ticket_id:@package.ticket.id,tag:@tag}}

      if !@paas_rollouts_new.blank? && !@paas_rollouts_new["groups"].blank?
        @paas_rollouts_new["groups"].each do |pr|
          @paas_new_format.push(:appname=>@package.warname || @package.name,:name => pr["name"],:group_id =>pr["id"],:package_id =>@package.id,:ticket_id =>@package.ticket.id,group_id:pr["id"],tag:@tag)
        end
      end

      @pr_should_be_delete = @paas_rollouts_exists - @paas_new_format
      if !@pr_should_be_delete.blank?
        @pr_should_be_delete.each do |pr_delete|
          PaasRollout.delete_all(:group_id =>pr_delete[:group_id],:appname => params[:appname],:package_id =>@package.id,:ticket_id=>@package.ticket.id)
        end
      end

      @pr_should_be_add = @paas_new_format - @paas_rollouts_exists
      if !@pr_should_be_add.blank?
        @pr_should_be_add.each do |pr_add|
          hostname_origin = @paas_rollouts_new["groups"].detect{|ps|ps["hostname"] if ps["id"] == pr_add[:group_id]}["hostname"]
          hostname = hostname_origin == [] ? nil : hostname_origin.to_s
          pr = PaasRollout.new(name:pr_add[:name],tag:@tag,package_id:@package.id,project_id:@project.id,hostname:hostname,ticket_id:@package.ticket_id,appname:@package.warname || @package.name,group_id:pr_add[:group_id])
        pr.save
        end
      end

      if !@paas_rollouts_new.blank? && !@paas_rollouts_new["groups"].blank?
        @paas_rollouts_new["groups"].each do |ps|
          paas_rollout = PaasRollout.where(:appname => @package.warname || @package.name,:group_id =>ps["id"],:ticket_id =>@package.ticket_id).first          
          paas_rollout.update_attributes(hostname:ps["hostname"].to_s) if paas_rollout.hostname != ps["hostname"].to_s && ps["hostname"].to_s != "[]"
        end
      end

      @prs = PaasRollout.where(:appname => @package.warname || @package.name,:tag =>@tag,:ticket_id =>@package.ticket_id)
      if !@prs.blank?
        @prs.each do |paas_rollout|
          paas_rollout.try(:reload_imf)
        end
      end
      return @prs
    end

  end
end
