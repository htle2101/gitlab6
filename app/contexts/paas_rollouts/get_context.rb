#coding:utf-8
module PaasRollouts
  class GetContext < BaseContext

    def execute
      # binding.pry
      @current_tag = params[:after_tag] || params[:tag]    
      @package = Package.find(params[:package_id])      
      @paas_rollouts = PaasRollout.where(:tag =>@current_tag,:appname => @package.warname || @package.name,:ticket_id => @package.ticket_id)
      if !@paas_rollouts.blank?
        @paas_rollouts.each do |paas_rollout|
          paas_rollout.try(:reload_imf)
        end
      end
            
    end
   end
end
