module GroupingStrategies
  class TerminateContext < BaseContext

    attr_reader :gs

    def initialize(group_strategy_id,package_path)
      @gs = GroupingStrategy.find(group_strategy_id)
      @package_path = package_path
    end

    def execute
      begin
        Logbin.logger("rollout").info("Start to call lzm/cmdb to terminate deploying war [#{gs.appname}][#{gs.name}].", gs.log_params.merge(:action_type=>"terminate"))
        res =::Service::Op::Lzm.new.terminate_grouping_rollout(gs.id, @package_path)
    	  gs.kill if res.code == 200
        true
      rescue => e
        Logbin.logger("rollout").info("Fail to call lzm/cmdb to terminate deploying war [#{gs.appname}][#{gs.name}], #{e}.", gs.log_params.merge(:action_type=>"terminate"))
        false
      end
    end
 
  end
end
