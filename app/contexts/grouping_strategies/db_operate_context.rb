#coding:utf-8
module GroupingStrategies
  class DbOperateContext

    attr_accessor :current_user, :params

    def initialize(name,package_id,ticket_id,tag)
      @package_name = name
      @package = Package.find(package_id)
      @ticket = Ticket.find(ticket_id)
      @tag = tag  || ""
    end

    def execute
      @gs_new = Service::Op::Cmdb.new.grouping_strategy(@package_name)
      @gs_exists= GroupingStrategy.where("appname ='#{@gs_new["appname"]}' and ticket_id =#{@ticket.id} and tag = '#{@tag}' and group_id != -255 ").collect{|gs|{swimlane:gs.swimlane,name:gs.name,group_id:gs.group_id,hostname:gs.hostname,package_id:gs.package_id,ticket_id:@ticket.id}}
      @gs_new_format = Array.new

      if !@gs_new["groups"].blank?
        @gs_new["groups"].each do |gs|
          @gs_new_format.push(:name=>gs["name"],:hostname => gs["hostname"].to_s,:group_id =>gs["id"],:swimlane=>gs["swimlane"],:package_id =>@package.id,:ticket_id =>@ticket.id) if gs["name"] !="全局"
        end
      end

      @gs_should_be_delete = @gs_exists - @gs_new_format
      if !@gs_should_be_delete.nil?
        @gs_should_be_delete.each do |gs_delete|
          GroupingStrategy.delete_all(:group_id =>gs_delete[:group_id],:appname => @gs_new["appname"],:package_id =>gs_delete[:package_id],:ticket_id=>@ticket.id)
        end
      end

      @gs_should_be_add = @gs_new_format - @gs_exists
      if !@gs_should_be_add.blank?
        @gs_should_be_add.each do |gs_add|
          group_in_hostname =  @gs_new["groups"].detect{|each_gs|each_gs["hostname"] if each_gs["name"] == gs_add[:name]}
          gs = GroupingStrategy.new(:hostname =>group_in_hostname["hostname"].to_s,:group_id =>gs_add[:group_id], :swimlane => gs_add[:swimlane],:appname =>@gs_new["appname"],:name =>gs_add[:name],:package =>@package,:ticket =>@ticket,:project_id =>@ticket.project.id,:tag=>@tag) if gs_add[:name]!="全局"
          gs.artifact_id = @package.name and gs.save if !gs.nil?
        end
      end

      @gs_add = @gs_should_be_add.first if !@gs_should_be_add.blank?
      @gs_pre_exist = GroupingStrategy.where(:group_id=> -255, :package_id => @package.id, :tag => @tag,:ticket_id =>@ticket.id)
      GroupingStrategy.new(:group_id =>-255, :swimlane => "",:appname =>@gs_new["appname"],:name =>"预发",:package =>@package,:ticket =>@ticket,:project_id =>@ticket.project.id,:tag=>@tag ||"",:artifact_id => @package.name).save  if Service::Op::Cmdb.new(:timeout=>2).has_prelease(@gs_new["appname"]) && @gs_pre_exist.blank?
      @gss = GroupingStrategy.where(:appname =>@gs_new["appname"],:ticket_id =>@ticket.id,:tag =>@tag||"").order("hostname")

      @cmdb_info_exist = !@gs_new["groups"].blank? && @gs_new["groups"].size == 1 ? true : false
      [@gss,@cmdb_info_exist]
    end
  end
end
