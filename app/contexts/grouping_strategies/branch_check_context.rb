module GroupingStrategies
  class BranchCheckContext < BaseContext

    def initialize(project, grouping_strategy)
      @project = project
      @grouping_strategy = grouping_strategy
    end

    def execute
      last_deployed_branch, 
        last_deployed_commit, 
        last_deployed_tag = get_last_deployed_branch_and_commit_and_tag

      current_branch,
        current_commit,
        current_tag = get_current_branch_and_commit_and_tag

      return unless last_deployed_commit

      if @project.
        repository.
        commits_between(current_commit, last_deployed_commit).
        present?

        Error.new(last_deployed_branch, last_deployed_commit, last_deployed_tag,
                  current_branch, current_commit, current_tag)
      end
    end

    private

    def get_current_branch_and_commit_and_tag
      packing = find_packing_by_tag(@grouping_strategy.tag)

      [packing.branch_name, packing.commit, packing.tag]
    end

    def find_packing_by_tag(tag)
      @project.
        rollout_branch.
        packings.
        find_by_tag(tag)
    end

    def get_last_success_grouping_strategy
      @project.
        grouping_strategies.
        where(appname: @grouping_strategy.appname).
        succeed.
        last
    end

    def get_last_deployed_branch_and_commit_and_tag
      grouping_strategy = get_last_success_grouping_strategy

      return unless grouping_strategy

      packing = find_packing_by_tag(grouping_strategy.tag)

      return unless packing

      [packing.branch_name, packing.commit, packing.tag]
    end

    class Error
      attr_reader :pre_branch, :current_branch
      attr_reader :pre_commit, :current_commit
      attr_reader :pre_tag,    :current_tag

      def initialize(pre_branch, pre_commit, pre_tag,
                     current_branch, current_commit, current_tag)

        @pre_branch     = pre_branch
        @pre_commit     = pre_commit
        @pre_tag        = pre_tag
        @current_branch = current_branch
        @current_commit = current_commit
        @current_tag    = current_tag
      end
    end
  end
end
