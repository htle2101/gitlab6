#coding:utf-8
module GroupingStrategies
  class GroupDeployContext < BaseContext
    def initialize(params)
      @package_id = params["package_id"]
      @grouping_strategy_id = params["grouping_strategy_id"]
      @gate = params["gate"]
      @action = params["action"]
      @package_url = params["package_url"]
    end

    def execute
      package = Package.find(@package_id)
      @gs = GroupingStrategy.find(@grouping_strategy_id)
      if @gs.name != 'static'
        if package.owner_is?(:task)
          lion_key = get_lion_configs( package )
        end
        params = {"package_id"=>@grouping_strategy_id, "app"=>package.warname || package.name, "package"=>@package_url}
        params = {"package_id"=>@grouping_strategy_id, "app"=>package.warname || package.name, "package"=>@package_url, "gate"=>@gate }  if @action =="deploy"
        params = params.merge("lion_key"=>lion_key, "task"=>"n/a") unless lion_key.blank?
        #params = params.merge("concurrent"=>@gs.concurrent) if !@gs.concurrent.nil?
        Logbin.logger("rollout").info("Start to call lzm to deploy war [#{@gs.appname}][#{@gs.name}]", @gs.log_params.merge(:action_type=>"deploy"))
        if package.project.rollout_branch.nodejs?
          res = Service::Op::Lzm.new.nodejs_grouping_rollout(@action, params)
        else
          res = Service::Op::Lzm.new.grouping_rollout(@action, params)
        end
        lzm_status = true
        if res.code == 200
          status = (@action == "prerelease")? 2 : 5
          @gs.update_attributes(started_at:Time.now) if @gs.started_at.nil?
          # gs_static_exist = GsStatistic.where(:gs_id => gs.id)
          # gs_static = GsStatistic.new(:tag =>gs.tag,:gs_id =>gs.id,:package_url =>gs.package.url,:ticket_id=>gs.ticket_id,:project_id=>gs.project_id,:appname =>gs.appname,:status => gs.status, :started_at => Time.now) and gs_static.save if gs_static_exist.blank?
        end
        notice = "请求lzm错误,请重发或者联系LZM相关人员!"  and  lzm_status = false  if res.code != 200
        status = (@action == "prerelease")? 3 : 6  if res.code != 200
        @gs.update_attributes(status:status)
        Logbin.logger("rollout").info("Finish call lzm/cmdb to deploy war", @gs.log_params.merge(action_type: "wait for call back"))
      else
        Logbin.logger("rollout").info("Start to call lzm to deploy static [#{@gs.appname}]", @gs.log_params.merge(:action_type=>"deploy"))
        res = Service::Op::Lzm.new.static_package_rollout(@gs.appname, @gs.id, @package_url)
        status = 5 if res.code == 200
        lzm_status = true
        status = 6  and notice = "请求lzm错误,请重发或者联系LZM相关人员!" and lzm_status = false if res.code != 200
        @gs.update_attributes(status:status)
        Logbin.logger("rollout").info("Finish call lzm/cmdb to deploy static", @gs.log_params)
      end
      rpm = RolloutPackingModule.find_by_url(@package_url)
      package.update_attributes(:tag=>rpm.packing.tag,:url=>rpm.url,:packing_id=>rpm.packing_id,:packing_module_id=>rpm.id)
      {status: lzm_status,notice: notice}
    end

    protected
    def get_lion_configs(package)
      lion_key = ''
      swimlane = ['']
      swimlane << @gs.swimlane unless @gs.swimlane.blank?
      tcs = TaskConfig.where(name: package.name, task_id: package.ticket.package_tasks.map{|t| t.id }, type: 'Lion')
      lion_key = tcs.where(:swimlane => swimlane).map(&:key).compact.uniq.join('|')
      lion_key
    end

  end
end
