module GroupingStrategies
  class UpdateRolloutStatusContext
    
    def execute
      GroupingStrategy.releasing.created_in_two_days.each do |gs|
        begin
          status = Service::Op::Cmdb.new.latest_rollout_status(gs.id).parsed_response["status"]
          if ['fail', 'killed', 'complete'].include?(status)
            Logbin.logger("rollout").info("Worker 
            is going to update [#{gs.appname}][#{gs.name}] status", gs.log_params)

            if status == 'complete'
              gs.succeed
            elsif status == 'killed'
              gs.killed
            else
              gs.failed
            end
          end
        rescue => e
          Logbin.logger("rollout").info("Grouping Strategy Checking Worker 
          can not update [#{gs.appname}][#{gs.name}] status, #{e}.", gs.log_params)
          false
        end
      end
    end
  end
end
