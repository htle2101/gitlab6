module GroupingStrategies
  class LoadContext

    attr_accessor :current_user, :params

    def initialize(user, params)
      @current_user, @params = user, params.dup
    end

    def execute
      grouping_strategies = GroupingStrategy.has_tag.page(params[:page]).per(40).order("package_id desc")
      grouping_strategies = grouping_strategies.where(status: params[:status]) unless params[:status].blank?

      tickets = Ticket.where(released_at: ( params[:released_at] || Date.today.to_s ))
      tickets = tickets.where(project_id: Group.find(params[:namespace_id]).projects.pluck("id")) unless params[:namespace_id].blank?

      grouping_strategies = grouping_strategies.where(package_id: Package.where(ticket_id: tickets.pluck("id")).pluck("id"))

      grouping_strategies
    end

  end
end
