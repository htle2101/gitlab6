#coding:utf-8
module GroupingStrategies
  class RollbackContext < BaseContext
    def initialize(params)
      @params = params
    end

    def execute
      gs = GroupingStrategy.find(@params["package_id"])
      Logbin.logger("rollout").info("Start to rollback package [#{gs.appname}][#{gs.name}].", gs.log_params.merge(action_type:"rollback"))
      res = Service::Op::Lzm.new.rollback("rollbackto",params)
      status = 8 if res.code == 200
      lzm_status = true
      status = 9  and notice = "请求lzm错误,请重发或者联系LZM相关人员!"  and lzm_status = false  if res.code != 200
      gs.update_attributes(status:status)
      {status: lzm_status, notice: notice}
    end
  end
end
