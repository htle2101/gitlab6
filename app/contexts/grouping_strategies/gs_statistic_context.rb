#coding:utf-8
module GroupingStrategies

  class GsStatisticContext < BaseContext
    def initialize(id,tag)
      @package = Package.find(id)
      @tag  = tag
    end

    def execute
      gss = GroupingStrategy.where(:package_id => @package.id,:tag => @tag).where("status not in (1,11)").where("started_at is not null and finished_at is not null").order("started_at")
      return  if gss.blank?
      first_start = gss.first
      last_finish = gss.last
      return if last_finish.started_at.nil?
      time_interval = last_finish.finished_at.to_i - first_start.started_at.to_i rescue 0
      group_percent,deploy_status = compute_group_percent(@tag)
      gs_statistic = GsStatistic.where(:tag => @tag, :package_id => @package.id, :ticket_id =>first_start.ticket_id).first
      gs_statistic = GsStatistic.new(:gs_id => first_start.id,:started_at => first_start.started_at,:finished_at => last_finish.finished_at, :package_id =>@package.id,:tag => @tag,:ticket_id => first_start.ticket_id,:project_id =>first_start.project_id,:appname =>first_start.appname,:group_percent => group_percent,:status => deploy_status) and gs_statistic.save if gs_statistic.blank?
      set_time_interval(time_interval,gs_statistic.id)
    end

    def compute_group_percent(tag)
      gss = GroupingStrategy.where(:package_id =>@package.id,:tag =>tag)
      group_percent = "#{gss.select{|gs|gs if !gs.log.nil?}.size}"+"/"+ "#{gss.size}"
      statuses = gss.map{|gs|gs.status}
      deploy_status = statuses.count(4)+ statuses.count(7) != statuses.size ? "not finish": "deploy success"
      deploy_status = statuses.include?(3) || statuses.include?(6) ? "deploy error" : deploy_status
      [group_percent,deploy_status]
    end

    def set_time_interval(time_interval,id)
      gs_statistic = GsStatistic.find(id)
      mm, ss = time_interval.divmod(60)
      hh, mm = mm.divmod(60)
      dd, hh = hh.divmod(24)
      gs_statistic.update_attributes(time_interval:dd.to_s+"d-"+hh.to_s+"h-"+mm.to_s+"m-"+ss.to_s+"s")
    end

  end

end
