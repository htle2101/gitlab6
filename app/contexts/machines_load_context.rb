# Build collection of Merge Requests
# based on filtering passed via params for @project
class MachinesLoadContext < BaseContext
  def execute
    type = params[:f]
 
    machines = project.machines

    machines = case type
                when 'alpha_used' then machines.where(:action_type => ['bind', 'apply']).where("status > 0")
                when 'alpha_all'  then machines.where(:action_type => ['bind', 'apply'])
                when 'beta'       then machines.where(:action_type => 'link') 
                else machines.where(:action_type => ['bind', 'apply'])
                end
    machines = machines.order("build_branch_id desc")
  end
end