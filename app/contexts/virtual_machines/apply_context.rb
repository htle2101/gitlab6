module VirtualMachines
  class ApplyContext < BaseContext

    include Utils::Logger
    include Utils::Time

    def initialize(project, user, params)
      super(project, user, params)
      self.logger_file = logger_file_name
    end

    def execute
      begin
        deployer_type = params[:machine][:deployer] || BuildBranch.find(params[:machine][:build_branch_id]).deployers
        virtual_machine = VirtualMachine.applicable_machine(deployer_type)

        # Notify.delay.apply_machine_failed(machine[:build_branch_id]) and return nil if virtual_machine.nil?

        ::RemoteCi::Command::DisableTty.new(virtual_machine.ip, port: virtual_machine.port).execute
        logger.info("Comment `Defaults requiretty` in /etc/sudoers to `#{virtual_machine.ip}`.")
        sudo_account = ::RemoteCi::Command::SudoAccount.new(virtual_machine.ip, port: virtual_machine.port).up
        logger.info("Create sudo account for `#{virtual_machine.ip}`, #{sudo_account[:account]}, #{sudo_account[:password]}.")
        ::RemoteCi::Command::KeyCopy.new(virtual_machine.ip, port: virtual_machine.port, envs: ['alpha']).execute
        logger.info("Copy jenkins server ssh keys to authorized_keys to `#{virtual_machine.ip}`.")

        if virtual_machine.deployer == "War" ||  virtual_machine.deployer == "JavaService"
          war_name = BuildBranch.find(params[:machine][:build_branch_id]).tree.hashed_pom[params[:machine][:module_name]].warName rescue params[:machine][:module_name]
          ::RemoteCi::Command::JbossConf.new(virtual_machine.ip, port: virtual_machine.port, module_name: war_name).up
          logger.info("Configure ROOT.xml to define the war location at `#{virtual_machine.ip}`.")
        end
        ::RemoteCi::Command::CleanEnv.new(virtual_machine.ip, port: virtual_machine.port).clean_webapps
        logger.info("Clean `#{virtual_machine.ip}` /data/webapps.")
        ::RemoteCi::Command::CleanEnv.new(virtual_machine.ip, port: virtual_machine.port).clean_logs
        logger.info("Clean `#{virtual_machine.ip}` /data/applogs,/usr/local/jboss/server/default/log.")

        attrs = machine.merge(status: 1, username: sudo_account[:account], password: sudo_account[:password])
        if virtual_machine.update_attributes(attrs)
          Notify.delay.machine_assign(virtual_machine.id, sudo_account[:account], sudo_account[:password])
          logger.info("Update machine(`#{virtual_machine.ip}`) status to used and send notify email.")
          virtual_machine
        end
      rescue => e
        logger.error("Apply machine failed, #{e}.")
        false
      end
    end

    def ci_branch
      @ci_branch ||= CiBranch.find(machine[:build_branch_id])
    end

    def machine
      params[:machine]
    end

    def logger_file_name
      "machine/apply/#{ci_branch.jenkins_job_name}/#{machine[:module_name]}-#{ts}.log"
    end
  end
end
