module VirtualMachines
  class BindContext < BaseContext

    include Utils::Logger
    include Utils::Time

    def initialize(project, user, params)
      super(project, user, params)
      self.logger_file = logger_file_name
    end

    def execute
      virtual_machine = VirtualMachine.where(:ip => machine[:ip]).first

      if (!virtual_machine || virtual_machine.action_type != 'apply') && ci_branch.deployers == "Phoenix"
        virtual_machine = VirtualMachine.new()
        virtual_machine.errors.add(:ip, "Machines only controlled by gitpub can be binded under phoenix env!")
        return virtual_machine
      end

      # bind new machine
      if !virtual_machine && ci_branch.deployers == "War"
        logger.info("Create machine with ip #{machine[:ip]}")
        virtual_machine = ci_branch.machines.new(machine.merge!(:status => 1, :deployer => ci_branch.deployers))
        if virtual_machine.applicable?
          prepare_result = prepare_env( virtual_machine )
          virtual_machine.save if prepare_result
        end
        logger.error("Create machine failed, #{virtual_machine.errors}") if virtual_machine.errors.present?
        return virtual_machine
      end

      if virtual_machine.deployer != ci_branch.deployers
        virtual_machine.errors.add(:ip, "The machine you provided cann't be used in '#{ci_branch.deployers}' env!")
        return virtual_machine
      end

      # machine has been binded with different module.
      unless virtual_machine.module_name.blank?
        virtual_machine.errors.add(:ip, "The machine you provided has been binded with \
                                            '#{virtual_machine.module_name}' module !")
        return virtual_machine
      end

      virtual_machine.attributes = machine.except!(:action_type).merge!(:status => 1)
      if virtual_machine.applicable?
        prepare_result = prepare_env( virtual_machine )
        virtual_machine.save if prepare_result
      end
      logger.error("Create machine failed, #{virtual_machine.errors}") if virtual_machine.errors.present?
      logger.info("Find exists machine with ip #{machine[:ip]}, update info.") unless virtual_machine.errors.present?
      virtual_machine
    end

    def ci_branch
      @ci_branch ||= CiBranch.find(machine[:build_branch_id])
    end

    def machine
      params[:machine]
    end

    def logger_file_name
      "machine/bind/#{ci_branch.jenkins_job_name}/#{machine[:module_name]}-#{ts}.log"
    end

    def prepare_env( virtual_machine )
      begin
        ::RemoteCi::Command::DisableTty.new(virtual_machine.ip, port: virtual_machine.port).execute
        logger.info("Comment `Defaults requiretty` in /etc/sudoers to `#{virtual_machine.ip}`.")
        ::RemoteCi::Command::KeyCopy.new(virtual_machine.ip, port: virtual_machine.port, envs: ['alpha']).execute
        logger.info("Copy jenkins server ssh keys to authorized_keys to `#{virtual_machine.ip}`.")
        if virtual_machine.deployer == "War"
          war_name = BuildBranch.find(virtual_machine.build_branch_id).tree.hashed_pom[virtual_machine.module_name].warName rescue virtual_machine.module_name
          ::RemoteCi::Command::JbossConf.new(virtual_machine.ip, port: virtual_machine.port, module_name: war_name).up
          logger.info("Configure ROOT.xml to define the war location at `#{virtual_machine.ip}`.")
        end
        if virtual_machine.action_type == 'apply'
          ::RemoteCi::Command::CleanEnv.new(virtual_machine.ip, port: virtual_machine.port).clean_webapps
          logger.info("Clean `#{virtual_machine.ip}` /data/webapps.")
          ::RemoteCi::Command::CleanEnv.new(virtual_machine.ip, port: virtual_machine.port).clean_logs
          logger.info("Clean `#{virtual_machine.ip}` /data/applogs,/usr/local/jboss/server/default/log.")
        end
        true
      rescue => e
        logger.error("Prepare env failed, #{e}.")
        virtual_machine.errors.add(:ip, "Prepare machine env failed, #{e}.")
        false
      end
    end
  end
end
