module VirtualMachines
  class LinkContext < BaseContext

    def initialize(params)
      super(nil, nil, params)
    end

    def execute
      vm = VirtualMachine.find_or_create_by_build_branch_id_and_module_name(
        params[:build_branch_id],
        params[:module_name]
        ).tap do |m|
          m.status = VirtualMachine::STATUS_LIST[params[:status]] unless params[:status].blank?
          m.action_type = 'link'
          m.package_type = 'war'
          m.deployer = params[:deployer]
          m.project_id = BuildBranch.find(params[:build_branch_id]).project_id
        end
      vm.save!
      vm
    end

  end
end
