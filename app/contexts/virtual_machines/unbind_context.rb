module VirtualMachines
  class UnbindContext < BaseContext

    include Utils::Logger
    include Utils::Time
    attr_reader :ci_branch, :machine, :errors

    def initialize(build_branch_id, machine_ip)
      @machine =VirtualMachine.find_by_ip( machine_ip )
      @errors = ""
      self.logger_file = logger_file_name
    end

    def execute
      machine.releaser.release
    end

    def logger_file_name
      "machine/unbind/#{ci_branch.try(:jenkins_job_name)|| "undefined" }/#{machine.ip}-#{ts}.log"
    end
  end
end