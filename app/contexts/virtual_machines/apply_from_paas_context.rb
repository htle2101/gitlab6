# encoding: utf-8
module VirtualMachines
  class ApplyFromPaasContext < BaseContext    

    def execute
      beta_paas_rollout_id = params[:beta_paas_rollout_id]
      @brp = BetaPaasRollout.find(beta_paas_rollout_id)
      host = get_host

      if host
        send_add_request(host) if get_paas_group(host)
        #shutdown && remove instance
        if BuildBranches::BetaPaasUploadContext.new(beta_paas_rollout_id).execute
          send_create_request(host)
          response = send_instance_request(host)
        end

        true
      else
        raise 'No Paas URL found'
      end
    end

    private

    def get_host
      Settings.op['beta_paas_url']
    end

    def send_add_request(host)
      url = "#{host}console/api/app?op=add&app.appId=#{params[:module_name]}&app.owner=scm.dp&app.level=3&app.appPlanName=small&app.type=webservice"

      RestClient.get(url)
    end

    def get_paas_group(host)
      url = "#{host}console/api/app?op=groups&appId=#{params[:module_name]}"
      res = begin
        RestClient.get(url)
      rescue RestClient::InternalServerError
      end
      res == nil || JSON.parse(res.body)['groups'].blank? ? true : false
    end

    def send_create_request(host)
      instances = send_instance_request(host)
      return instances['data'].map{|d|d['instanceIp']} if !instances['data'].blank?
      url = "#{host}console/api/app?op=create&appId=#{params[:module_name]}&version=#{@brp.paas_version}&number=1"
      begin
        RestClient.get(url)
      rescue RestClient::InternalServerError
      end
    end

    def send_instance_request(host)
      DianpingPaas::Client.new(url: host).instances(params[:module_name])
    end
  end
end
