#coding:utf-8
module PerformanceTests
  class ApplyDockerMachineContext < BaseContext

    def initialize(options)
      @machines = options[:machiens]
    end

    def execute
      begin
        machine.update_status('deploying')
        conn.sudo_exec("mkdir -p #{release_path} #{BACKUP_PATH} #{LOG_PATH} #{DATA_PATH}", :timeout => 10)
        ci_env.conn.execute("scp -P #{machine.port || Settings.dp['ssh_port']} #{self.full_path} #{ssh_destination}", :timeout => 30)
        conn.sudo_exec("unzip -q #{BACKUP_PATH}/#{self.name} -d #{release_path}/#{self.deploy_name}", :timeout => 30)
        conn.sudo_exec("rm -rf #{current_path}", :timeout => 10)
        conn.sudo_exec("ln -s #{release_path} #{current_path}", :timeout => 10)

        conn.sudo_exec("chown -R nobody:nobody #{LOG_PATH} #{DATA_PATH}", :timeout => 15)

        app_username = machine.username || 'root'

        conn.sudo_exec("mkdir -p #{meta_path}", :timeout => 10)

        conn.sudo_exec("chown #{app_username}:#{app_username} #{APP_PATH} ", :timeout => 10)
        conn.sudo_exec("chown -R #{app_username}:#{app_username} #{release_path} ", :timeout => 15)

        conn.sudo_exec("[ -f /data/webapps/appenv ] || echo 'deployenv=alpha \nzkserver=alpha.lion.dp:2182' >> /data/webapps/appenv", :timeout => 10)

        res = conn.sudo_exec("[ -f  #{meta_path}app.properties ] || echo 'not exists'", :timeout => 10)

        if res.blank?
          conn.sudo_exec("sed -i -e  \'s/app.name.*/app.name=#{warName}/\' #{meta_path}/app.properties", :timeout => 10)
        else
          conn.sudo_exec("echo \"app.name=#{warName}\" >> #{meta_path}/app.properties", :timeout => 10)
        end

        copy_jboss_script if machine.not_initialized? and machine.action_type == 'bind'
        tomcat_timeout = true
        conn.sudo_exec("/sbin/service jboss restart", :timeout => 200)
        tomcat_timeout = false
        machine.update_status('deploy success') if machine.used?
      rescue => e
        machine.update_status('deploy error') unless machine.blank?
        return false
      ensure
        ci_env.conn.disconnect
        unless machine.blank?
          conn.disconnect 
        end
        begin
          logger.info("call auto test.")
          Service::AutoTest::Request.new.call(machine.build_branch.project_name_for_auto_test) and logger.info("call auto success.") if machine.build_branch.all_deploy_finished?
        rescue
        end
      end
      true

    end

   end
end