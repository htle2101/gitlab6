#coding:utf-8
module PerformanceTests
  class ApplyDockerMachineContext < BaseContext

    def initialize(options)
      @machines = options[:machiens]
    end

    def execute
      Service::Beehome.new.apply_special_containers("performance_test",@machines)
      @performance_test.update_attributes(status:1)
    end

   end
end
