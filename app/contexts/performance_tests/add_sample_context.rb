#coding:utf-8
module PerformanceTests
  class AddSampleContext < BaseContext

    def execute
      begin
        rpm = RolloutPackingModule.find_by_url(params[:tag])
        if !params[:performance_test_module_id].blank?
          @performance_test_module = PerformanceTestModule.find(params[:performance_test_module_id])
          @performance_test_module.update_attributes(:appname =>params[:appname],:package_url => params[:tag],:app_type => params[:app_type],:domain_name => params[:domain_name],:cpu => params[:cpu],:memory => params[:memory])
          @performance_test =@performance_test_module.performance_test
          @performance_test.update_attributes(:concurrent => params[:concurrent], :duration => params[:duration],:uid => SecureRandom.uuid,:project_id => @project.id,:title => params[:title],:target => params[:target],:upload_file => params[:files])
        else
          @performance_test_module = PerformanceTestModule.new(:appname =>params[:appname],:package_url => params[:tag],:app_type => params[:app_type],:domain_name => params[:domain_name],:cpu => params[:cpu],:memory => params[:memory])
          @performance_test = PerformanceTest.new(:concurrent => params[:concurrent], :duration => params[:duration],:uid => SecureRandom.uuid,:project_id => @project.id,:title => params[:title],:target => params[:target],:upload_file => params[:files]) 
        end
          @performance_test_module.update_attributes(:performance_test_id => @performance_test.id,:tag =>rpm.packing.tag)
      rescue Exception => e
        false
      end
      
    end
   end
end
