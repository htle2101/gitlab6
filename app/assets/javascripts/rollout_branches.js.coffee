# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/
replace_static = (callback) ->
  $.get $(".static-index").data("link"), ((data) ->
      $(".static-index").html(data)
    ), "text",$(".span4").chosen()
  callback() if callback?
1
auto_replace_static = () ->
  replace_static(() -> setTimeout(auto_replace_static, 10000))


$ ->
  $("#packing-list tbody tr").click ->
    url = $(this).data("link")
    $.get url, ((data) ->
      $(".packing-item").replaceWith("<div class='span6 packing-item'>#{data.item}</div>")
    ), "json"

  $("#online-job-list tbody tr").click ->
    url = $(this).data("link")
    $.get url, ((data) ->
      $(".packing-item").replaceWith("<div class='span6 packing-item'>#{data.item}</div>")
    ), "json"

  $(".container").on "click", ".packing-log", (event) ->
    url = $(this).data("link")
    $.get url, ((data) ->
      $("#packing-log-modal .log-content").empty()
      $("#packing-log-modal .log-content").html("<pre>#{data}</pre>")
      $("#packing-log-modal").modal("show")
    ), "text"
    event.stopPropagation()

  $(".container").on "click", ".packing-note", (event) ->
    url = $(this).data("link")
    $.get url, ((data) ->
      $("#packing-note-modal .log-content").empty()
      $("#packing-note-modal .log-content").html("<pre>#{data}</pre>")
      $("#packing-note-modal").modal("show")
    ), "text"
    event.stopPropagation()  

  $(".container").on "click", ".packing-rebuild", (event) ->
    if $(this).hasClass("disabled")
      return
    url = $(this).data("link")
    $.ajax(
      type: "PUT"
      url: url
      dataType: "json"
    ).done (data) ->
      if data.success
        $(".packing-rebuild").addClass("disabled")
      flash_notice(data.success, data.msg)
    event.stopPropagation()

  #function for cache clear
  $(".add_package_to_clear_ppecache").click ->
    $("#to_select_the_package_to_clear_cache_chosen")[0].style.width = "80%"
    $("#ClearPpeCacheModal").modal("show")

  $(".add_package_to_clear_cache").click ->
    branch_name = $("#rollout_branch_branch_name_chosen .chosen-single").text()
    id = $(".add_package_to_clear_cache").attr("id")
    $.ajax "/api/v3/ci/#{$("#jenkins_job_name").text()}/can_build",
      type: 'Get'
      data:
        branch_name:branch_name,id:id
      success : (data) ->
        if data["success"] == false
            flash_notice("failure",data["message"])
        else if data["success"] == true
          $("#to_select_the_package_to_clear_cache_chosen")[0].style.width = "80%"
          $("#ClearCacheModal").modal("show")

  $(".container").on "click",".add_ppe_package",(event) ->
    console.log('xxxx')
    id = $(".add_package_to_clear_ppecache").attr("id")
    branch_name = $("#ppe_branch_branch_name_chosen .chosen-single").text()
    if  $(".package_select_for_clear_cache").val() != null
      module_names = $(".package_select_for_clear_cache").val().join(",")
    $.ajax "/api/v3/ci/#{$("#jenkins_job_name").text()}/static_build",
      type: 'GET'
      data:
        module_names: module_names,id:id,branch_name: branch_name,
        private_token: gon.api_token
      success : (data) ->
        $("#ClearPpeCacheModal").modal("hide")
        $("#build_status").removeClass("badge-important badge-warning badge-success")
        $("#build_status").text('running...')
        flash_notice(data["success"],data["message"])
        auto_replace_static()

  $(".container").on "click",".add_prod_package",(event) ->
    id = $(".add_package_to_clear_cache").attr("id")
    branch_name = $("#rollout_branch_branch_name_chosen .chosen-single").text()
    if  $(".package_select_for_clear_cache").val() != null
      module_names = $(".package_select_for_clear_cache").val().join(",")
    $.ajax "/api/v3/ci/#{$("#jenkins_job_name").text()}/static_build",
      type: 'GET'
      data:
        module_names: module_names,id:id,branch_name: branch_name,
        private_token: gon.api_token
      success : (data) ->
        $("#ClearCacheModal").modal("hide")
        $("#build_status").removeClass("badge-important badge-warning badge-success")
        $("#build_status").text('running...')
        flash_notice(data["success"],data["message"])
        auto_replace_static()

  $(".container").on "click", ".static-build" , (event) ->
    if $(this).hasClass("disabled")
      return
    url = $(this).data("link")
    $.ajax(
      type: "POST"
      url: url
      data:
        branch_name: $('.static_branch_name').chosen().val()
      dataType: "json"
    ).done (data) ->
      if data.success
        $(".static-build").addClass("disabled")
        $("#build_status").removeClass("badge-important badge-warning badge-success")
        $("#build_status").text('running...')
      flash_notice(data.success, data.msg)
      auto_replace_static()
    event.stopPropagation()

  $(".container").on "click", ".clear-cache", (event) ->
    if $(this).hasClass("disabled")
      return
    url = $(this).data("link")
    $.ajax(
      type: "POST"
      url: url
      data:
        apps: $('#app').val()
        type: $('#cache_enviroment').chosen().val()
      dataType: "json"
    ).done (data) ->
      flash_notice(data.success, data.msg)
    event.stopPropagation()


  $(".container").on "click", ".clear-ip-cache", (event) ->
    if $(this).hasClass("disabled")
      return
    url = $(this).data("link")
    $.ajax(
      type: "POST"
      url: url
      data:
        ip: $('#cache_ip').val()
        type: $('#cache_enviroment').chosen().val()
      dataType: "json"
    ).done (data) ->
      flash_notice(data.success, data.msg)

  $(".container").on 'change', '#rollout_branch_branch_name', ->
    url = $(this).data('url')
    $.get url, {branch: this.value}, ((data) ->
      if data.success
        $(".commit_short_id a").attr 'href', data.commit.link
        $(".commit_short_id a").text data.commit.short_id
        $("#packing_module_names").empty()
        $(data.modules).each (i, module) ->
          $("#packing_module_names").append("<option value=#{module.value}>#{module.text}</option>")
        $("#packing_module_names").trigger("chosen:updated")
    ), 'json'


  $('.static_rollback').click ->
    $(".static-rollback-info").text ("确认从版本" + $("#rollback_from").val() + "回滚到版本" + $("#rollback_to").val())
    $('#static-rollback-confirm').removeAttr 'disabled'
    $('#staticRollbackModal').modal('show')


  $('#static-rollback-confirm').click ->
    url = $('#static-rollback-confirm').data('link')
    $('#static-rollback-confirm').attr("disabled", "disabled")
    $.post url, {from: $("#rollback_from").val(), to: $("#rollback_to").val()}, ((data) ->
      flash_notice(data.status, data.message)
      ), 'json'


  $('.jdk-change').click ->
    url = $(this).data('link')
    version = $(this).data('version')
    $.ajax(
      type: "POST"
      url: url
      data:
        jdk_version: version
      dataType: "json"
    ).done (data) ->
      flash_notice(data.success, data.message)

class Packing
  @packings = []
  constructor: (@url) ->
    @allow = true
    Packing.packings.push(this)

  stop: () ->
    @allow = false

  refresh: () ->
    $.ajax(
      type: "GET"
      url: @url
      data: {build_status: true}
      dataType: "json"
      context: this
      beforeSend: () ->
        $('.packing-wrap').block(
          message: '<h3><i class="icon-refresh icon-spin"></i>&nbsp;Refreshing</h3>',
        )
      complete: () ->
        $('.packing-wrap').unblock()

    ).done (data) ->
      $("#rollout-form").replaceWith(data.item)
      $(".page_title").replaceWith(data.build_status)
      if data.processing && @allow == true
        setTimeout (()=>
          this.refresh()
        ), 5000

@Packing = Packing

document.addEventListener "page:fetch", ->
  packing.stop() for packing in Packing.packings
