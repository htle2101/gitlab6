class Upload
  constructor: (@context, @options) ->
    @options = @options || {}
    _context = @context
    _btnClass = @options.btnClass || ".js-choose-attachment-button"
    _fileInputClass = @options.fileInputClass || ".js-attachment-input"
    _fileLabelClass = @options.fileLabelClass || ".js-attachment-filename"

    $(document).off "click", _btnClass
    $(document).off "click", _fileInputClass

    $(document).on "click", _btnClass, ->
      $(_context).find(_fileInputClass).click()

    $(document).on "change", _fileInputClass, ->
      filename = $(this).val().replace(/^.*[\\\/]/, '')
      $(_context).find(_fileLabelClass).text(filename)

@Upload = Upload
