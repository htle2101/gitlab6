replace_pt = (callback) ->
  if $(".selectedpt").length > 0 and !$('.loading').is(":visible")
    $(".selectedpt").attr("class","selectedpt highlight")
    performance_test_module_id = $(".selectedpt.highlight").attr("performance_test_module_id")
    if typeof(performance_test_module_id) == "undefined"
      performance_test_module_id = $(".config_params_span").attr("performance_test_module_id")      
    $(".performance-test-item").html ''
    $(".loading").show()
    console.log(performance_test_module_id)
    $.get $(".selectedpt").data("link"),performance_test_module_id: performance_test_module_id, ((data) ->
        $(".performance-test-item").html(data)
        $(".selectedpt.highlight").attr("performance_test_module_id",$(".config_params_span").attr("performance_test_module_id"))
        $(".selectedpt.highlight").attr("performance_test_id",$(".config_params_span").attr("performance_test_id"))
        $(".loading").hide()
      ), "text"
  callback() if callback?

$ ->
  update_selection = (selector, callback) ->
    url = $(selector)[0].getAttribute("url")
    $.get url, {appname: this.value}, ((data) ->
      $(selector).empty()
      $(selector).append("<option></option>")
      callback() if callback
      $.each data, (index, obj) ->
        $(selector).append("<option value='#{obj.id}'>#{obj.value}</option>")
      $(selector).trigger("chosen:updated")
    ), 'json'


  $("body").on 'change', '.performance_test_appname', ->
    $("body #performance-test-module-list").removeAttr("appname")
    $("body #performance-test-module-list").attr("appname",$(this).val())
    update_selection.call this, ".performance_test_tag", ->
      $(".performance_test_tag").append("<option></option>")
      $(".performance_test_tag").trigger("chosen:updated")

  $("body").on 'change', '.performance_test_tag', ->
    $("body #performance-test-module-list").removeAttr("tag")
    $("body #performance-test-module-list").attr("tag",$(this).val())

  $("body").on 'change', '.performance_test_package_type', ->
    $("body #performance-test-module-list").removeAttr("package_type")
    $("body #performance-test-module-list").attr("package_type",$(this).val())

  $("body").on 'click', '.create_performance_test_module', ->
    performance_test_module_id = $(".selectedpt.highlight").attr("performance_test_module_id")
    performance_test_id = $(".selectedpt.highlight").attr("performance_test_id")
    url = $(this).attr("url")
    appname = $("body #performance-test-module-list").attr("appname")
    tag = $("body #performance-test-module-list").attr("tag")
    app_type = $("body .performance_test_package_type").val()  
    concurrent = $("body #concurrent").val()
    duration = $("body #duration").val()
    title = $("body #title").val()
    target = $("body #target").val()
    cpu = $(".performance_test_cpu").val()
    memory = $(".performance_test_memory").val()
    domain_name = $("#domain_name").val()
    files = $("#files").val()
    $.ajax(
      type: "PUT"
      url: url
      data: {appname: appname, tag: tag,app_type: app_type, concurrent: concurrent,duration: duration,cpu: cpu, memory: memory, files: files,performance_test_module_id: performance_test_module_id,domain_name: domain_name,performance_test_id: performance_test_id,title: title,target: target}
      dataType: 'html'
      success: (data) ->
        $(".performance-test-item").html(data)
        $("#files").attr("style","margin-left:10px;")        
        $(".performance_test_domain_name").attr("style","display:block;width:200px;padding-right:135px;")
      complete: ->
      )

  $("body").on 'click', '.edit_performance_test', ->

  $("body").on 'click', '.autocreate_performance_test_domain', ->
    appname = $("body #performance-test-module-list").attr("appname")
    if typeof(appname) != "undefined"
      text = "http://"+appname+"-performance-test"+".dp"
      $(".performance_test_domain_name").val(text)
      $(".performance_test_domain_name").attr("style","display:block;width:200px;padding-right:135px;")
    else
      alert("请先在左侧选择包和branch")

  $("body").on 'click', '.hide_performance_test_config', ->
    $(".config_params_span").attr("style","display:none;")      

  $("body").on 'submit', ".performance-test-form", ->
    console.log($(this))
    console.log($(this).serialize())
    console.log($(this).attr('action'))
    valuesToSubmit = $(this).serialize();
    $.ajax(
      type: "put"
      url: $(this).attr('action')
      data: valuesToSubmit
      dataType: "JSON" 
    success: (data) ->
      return date["result"]
    )

  $("body").on 'click', '.add_dependency_performance_test_module_link', ->
    url = $(this).attr("url")
    if typeof($(".config_params_span").attr("performance_test_module_id")) != "undefined"
      performance_test_module_id = $(".config_params_span").attr("performance_test_module_id")
      $.ajax(
        type: "put"
        url: url
        data: {performance_test_module_id: performance_test_module_id}
        dataType: 'html'
        success: (data) ->
          $("body .performance_test_module_form").html(data)
          $("body .performance_test_appname").chosen()
          $("body .performance_test_tag").chosen()
          $("body .performance_test_package_type").chosen()
          $("body .performance_test_cpu").chosen()
          $("body .performance_test_memory").chosen()
          $("body .performance-test-package-config").attr("style","padding-left:30px;width:650px;")
        complete: (data) ->
        )
    else
      alert("请先添加主测试用例!")
      
  
  $("body").on 'click', '.create_total_performance_tests', ->
    url = $(this).attr("data-link")
    $.ajax(
      type: "put"
      url: url
      data: {main_performance_test_id: main_performance_test_id}
      dataType: 'html'
      success: (data) ->
    )

  $("body").on 'click', '.organize_network_performance_test', ->
    url = $(this).attr("data-link")
    alert(url)
    performance_test_id = $(".config_params_span").attr("performance_test_id")
    alert(performance_test_id)
    $.ajax(
      type: "post"
      url: url
      data: {performance_test_id: performance_test_id}
      dataType: 'html'
      success: (data) ->
        alert("success")
    )
    
  $('body #performance-tests-list').popover
    selector:'.performance_test_machines'
    trigger:'hover'
    "background-color":'black'    
    
  $("body").on 'click', '#performance-test-module-list tbody tr', ->
    $(".selectedpt").attr("class","")
    $(this).addClass('selectedpt')
    replace_pt()

  $("body").on 'click', '#performance-test-module-list tbody tr .chosen-container', (e) ->
    e.stopPropagation()
