# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/
$ ->
  $('.light-merge-show').on "click", "#lmerge-remove", () ->
    $('#lmerge-t').hide()

$ ->
  $('.light-merge-show').on "ajax:beforeSend", ".lmerge-check", (event, data) ->
    $.blockUI()  

$ ->
  $('.light-merge-show').on "ajax:success", ".lmerge-check", (event, data, status) ->
    $(".light-merge-show").html(data)
    $.unblockUI()
    flash_notice(status, 'The light Merge is check.')

$ ->
  $('.light-merge-show').on "ajax:beforeSend", ".lmerge-push", (event, data) ->
    $.blockUI()  

$ ->
  $('.light-merge-show').on "ajax:success", '.lmerge-push', (event, data, status) ->
    $(".light-merge-show").html(data)
    $.unblockUI()
    flash_notice(status, 'The light Merge is pushed!!')

#     $.ajax "#{project_light_merge_automerge_check}",
#       type: 'POST'
#       data:
#         private_token: gon.api_token
#       context: this
#       success : (res, status, xhr) ->
#         $(this).attr("disabled", "disabled")
#         $.unblockUI()    
