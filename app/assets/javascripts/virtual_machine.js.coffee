$ ->
  $("body").on 'click', '.local_nginx', ->
    $('tr').removeClass('editing')
    $(this).closest('tr').addClass('editing')
    url = $(this).data("link")
    $siblings = $(this).siblings(".btn")
    $.ajax(
      type: "GET"
      url: url
      data: {}
      beforeSend: ->
        $siblings.disable()
      success: (data) ->
        $("#editLocalNginxModal").html(data)
        $("#editLocalNginxModal").modal('show')
      complete: ->
        $siblings.enable()
    )

  $("body").on 'click', '.jboss_log', ->
    $('tr').removeClass('editing')
    $(this).closest('tr').addClass('editing')
    url = $(this).data("link")
    $siblings = $(this).siblings(".btn")
    $.ajax(
      type: "GET"
      url: url
      data: {}
      beforeSend: ->
        $siblings.disable()
      success: (data) ->
        $("#jbossLogModal").html(data)
        $("#jbossLogModal").modal('show')
      complete: ->
        $siblings.enable()
    )
