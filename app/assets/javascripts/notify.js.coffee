$ ->

  randomizer = (bottom, top) ->
    Math.floor( Math.random() * ( 1 + top - bottom ) ) + bottom

  class Notify
    @client = new Faye.Client(Gitpub.fayeServer, { timeout: 60 })
    #@client.disable('websocket')

    @publish: (channel, data) ->
      @client.publish(channel, data)

    @subscribe: (channel, callback) ->
      @client.subscribe(channel, callback)

    @disconnect: () ->
      @client.disconnect()

  @Notify = Notify

  class FayeCookie

    @randomId = randomizer(1000000, 10000000).toString()

    @bind: () ->
      fayeId = $.cookie("faye-id", {path: "/"})
      if fayeId? && fayeId != ""
        ids = fayeId.split(".")
        ids.push(FayeCookie.randomId)
        $.cookie("faye-id", ids.join("."), {path: "/"})
      else
        $.cookie("faye-id", FayeCookie.randomId, {path: "/"})

    @unbind: () ->
      fayeId = $.cookie("faye-id", {path: "/"})
      if fayeId? && fayeId != ""
        newIds = []
        ids = fayeId.split(".")
        for id in ids
          newIds.push(id) unless id == FayeCookie.randomId

        $.cookie("faye-id", newIds.join("."), {path: "/"})

    @select: (context, callback) ->
      fayeId = $.cookie("faye-id", {path: "/"})
      if fayeId? && fayeId != ""
        ids = fayeId.split(".")
        if ids[ids.length - 1] == FayeCookie.randomId
          callback.call(context)

  desktopNotify = (message) ->
    FayeCookie.select this, ->
      return unless window.Notification
      options = {}
      if $.isPlainObject(message)
        options.icon = message.icon if message.icon?
        title = message.title || "Gitpub notification"
        options.body = message.text
        link = message.link
        timeout = message.timeout
      else if typeof(message) == "string"
        title = "Gitpub notification"
        options.body = message
      notification = new Notification(title, options)
      if link?
        notification.onclick = () ->
          window.location.href = link
      if timeout?
        setTimeout () ->
          notification.close()
        , parseInt(timeout) || 10000

  return if Gitpub.fayeUserId == null || Gitpub.fayeUserId == ""

  FayeCookie.bind()

  # This listener must be defined before Faye.Client initialize.
  Faye.Event.on Faye.ENV, "beforeunload", ->
    FayeCookie.unbind()
    Notify.disconnect()
  , this

  document.addEventListener "page:fetch", ->
    FayeCookie.unbind()
    Notify.disconnect()

  if window.Notification && Notification.permission != 'grant' && Notification.permission != 'denied'
    $(document).click ->
      Notification.requestPermission (permission) ->
        if !('permission' in Notification)
          Notification.permission = permission

  $(Gitpub.fayeProjects).each (i, id) ->
    Notify.subscribe "/project/#{id}", (message) ->
      desktopNotify.call(this, message)

  Notify.subscribe "/all", (message) ->
    desktopNotify.call(this, message)
