replace_gs = (callback) ->
  if $(".selectedgs").length > 0 and !$('.loading').is(":visible")
    $(".ticket-packing-item").html ''
    $(".loading").show()
    $.get $(".selectedgs").data("link"), ((data) ->
        $(".selectedgs").attr("class","selectedgs highlight")
        $(".ticket-packing-item").html(data)
        $(".loading").hide()
      ), "text"
  callback() if callback?

auto_replace_gs = () ->
  if $('.auto-refresh-box').is(':checked')
    replace_gs(() -> setTimeout(auto_replace_gs, 20000) )

bind_check_rollout = () ->
  $(".package_tag_rollback").chosen().change ->
    gs_id = $(this).parent().parent().attr("data-group-id")
    url = $(this).val()
    i = 1
    while i <this.children.length
      if this.children[i].getAttribute("selected") == "selected"
        value = this.children[i].value
        break
      i++

$ ->
  auto_replace_gs()

  $("table#ticket-packing-list").on 'click', '.editStaticPackage', ->
    $("#static_package_name_edit").attr("value", $(this).parent().attr('name'))
    $("#static_package_name_edit").attr("readonly", true)
    $("#static_package_url_edit").attr("value",$(this).parent().attr('url'))
    $("#request_url").attr("value", $(this).parent().attr('request_url'))
    $("#package_id").attr("value", $(this).parent().attr('package_id'))
    $('.module-machine-list tr').removeClass('editing')
    $(this).closest('tr').addClass('editing')

  $('.auto-refresh').on 'switch-change', (e, data) ->
    auto_replace_gs()

  $(".addWarModel").click ->
    $("#addWarModal").modal('show')
    $("#module_name").attr("value", $(this).closest('td').attr('module-name'))
    $('.module-machine-list tr').removeClass('editing')
    $(this).closest('tr').addClass('editing')

  $("#war_save").click ->
    $.blockUI()

    $.ajax "/#{$("#assign-machine").data('project')}/virtual_machines/apply",
      type: 'GET'
      data:
        "machine[action_type]": 'apply'
        "machine[build_branch_id]": $("#build_branch_id").attr('value')
        "machine[module_name]": $("#module_name").attr('value')
        "machine[package_type]": "war"
        "machine[project_id]": $("#project_id").val()
      success : (res, status, xhr) ->
        show_info_about_apply_or_bind_machine(res, status, xhr)
      error: (jqXHR, textStatus, errorThrown) ->
        $(".module-machine-list tr.editing").removeClass('editing')
        $.unblockUI()

  $("#ticket-packing-list tbody tr").click (e) ->
    $(".selectedgs").removeClass('selectedgs')
    $(this).addClass('selectedgs')
    replace_gs()

  $("#ticket-packing-list tbody tr .chosen-container").click (e) ->
    e.stopPropagation()

  $('body').popover
    selector:'.group-hostname'
    trigger:'hover'
    "background-color":'black'    

  $("body").on 'click', '.grouping-strategy-operation-tips', ->
    $('.auto-refresh-box').prop('checked', false)
    patt=/共有\d个流程单/
    if patt.test(this.children[0].innerText)
      $(".workflow_tr").empty()
      $(".workflow_table").remove()
      index = this.children[0].innerText.indexOf "urls", 0
      temp_string = this.children[0].innerText.substring(index,this.children[0].innerText.length)
      url_string = temp_string.replace /urls:|\[|\]|"|ids:.*/g, ""
      urls = url_string.split ","
      index = this.children[0].innerText.indexOf "ids", 0
      temp_string = this.children[0].innerText.substring(index,this.children[0].innerText.length)
      id_string = temp_string.replace /ids:|\[|\]|"/g, ""
      ids = id_string.split ","
      table = $('<table></table>').addClass('workflow_table')
      row = $('<tr></tr>').addClass('workflow_tr')
      table.append(row)
      $('<td></td>').text("共有#{urls.length}个上线单没有完成：").appendTo(row);
      for i in [0..urls.length-1]
        row = $('<tr></tr>').addClass('workflow_tr')
        td = $('<td></td>').addClass('workflow_td')
        link = document.createElement("a")
        link.href = urls[i]
        link.innerHTML = "上线单号: " + ids[i]
        td.append(link)
        td.appendTo(row)
        table.append(row)
      $("#unfinished_workflow_items").append(table)
      $("td.workflow_td a").attr("target","_blank")
      $("#workflow").modal('show')
    else
      alert(this.children[0].innerText)

  $(".container").on 'hide.bs.modal', '#workflow', ->
    $('.auto-refresh-box').prop('checked', true)

  $("body").on 'click', '.release-reset', ->
    result = confirm("重置会将对应分组的状态置为初始状态,确定吗?")
    if result == true
      url = $(this).data("link")
      package_id = $(this).closest('.grouping-strategies-list').data("package-id")
      $siblings = $(this).siblings(".btn")
      $.ajax(
        type: "PUT"
        url: url
        data: {package_id: package_id}
        dataType: 'json'
        context: this
        beforeSend: ->
          $siblings.disable()
        success: (data) ->
          if data['status']
            $(this).remove()
          flash_notice(data['status'], data['notice'])
        complete: ->
          $siblings.enable()
          replace_gs()
      )


  bind_check_rollout()

  $(".package_tag").chosen().change ->
    package_object = $(this)
    link = $(this).parent().parent().data("link").split("package_id=")
    package_id = parseInt(link[1].split("&")[0])
    i = 1
    while i <this.children.length
      if this.children[i].getAttribute("selected") == "selected"
        value = this.children[i].value
        break
      i++
    tag = $(this).find('option:selected').text()
    choice = confirm("确定要将tag切换到#{tag}进行上线操作？")
    if choice
      url = this.value.toString()
      $(this).val(this.value).trigger("chosen:updated")
      $(this).trigger("liszt:updated")
      j = 1
      while j < this.children.length
        if this.children[j].value == this.value
          this.children[j].setAttribute("selected","selected")
          j++
        else
          $(this.children[j]).removeAttr("selected")
          j++
      $(".project-refs-select ").chosen({max_selected_options: 1})
      $(".selectedgs").attr("class","highlight")
      package_object.parent().parent().attr("class","selectedgs")
      data = $(".selectedgs").data("link")
      if data.indexOf("&after_tag") > 0
        data =  data.split("&after_tag")[0]
      after_tag = $(".selectedgs").find('option:selected').text()
      $(".selectedgs").data("link",data+"&after_tag="+after_tag)
      $.ajax "/api/v3/ci/update_package",
        type:"POST"
        data:{url:url,package_id:package_id,private_token: gon.
        api_token}
        complete: ->
          replace_gs()
    else
       $(this).val(value).trigger("chosen:updated")

  $("#ticket-packing-list tbody tr .chosen-container").click (e) ->
    e.stopPropagation()

  $("body").on 'click', '.release-deploy', ->

    $(this).attr("disabled","disabled")
    url = $(this).data("link")
    package_id = $(this).closest('.grouping-strategies-list').data("package-id")
    package_url =$("#ticket-packing-list").data("package_url")
    $siblings = $(this).siblings(".btn")
    $.ajax(
      type: "PUT"
      url: url
      data: {package_id: package_id,package_url:package_url,private_token: gon.api_token}
      dataType: 'json'
      context: this
      beforeSend: ->
        $siblings.disable()
      success: (data) ->
        flash_notice(data['status'], data['notice'])
        if data['status']
          $(this).remove()
        $($(".selectedgs").children()[1].children[0]).attr('disabled', "disabled").trigger("chosen:updated")
      complete: ->
        $("#confirmModal").modal('hide')
        $siblings.enable()
        replace_gs()
      )

  $("body").on 'click', '.paas-deploy', ->
    url = $(this).data("link")
    if typeof(url) == "undefined"
      url = $("#deployCheckModal").attr("url")
    package_url = $(".selectedgs").children()[1].children[0].value
    $siblings = $(this).siblings(".btn")
    $.ajax(
      type: "PUT"
      url: url
      dataType: 'json'
      data: {package_url:package_url}
      context: this
      beforeSend: ->
        $siblings.disable()
      success: (data) ->
        $("#deployCheckModal").modal('hide')
        flash_notice(data['status'], data['notice'])
        if data['status']
          $(this).remove()
        $($(".selectedgs").children()[1].children[0]).attr('disabled', "disabled").trigger("chosen:updated")
      complete: ->
        $siblings.enable()
        replace_gs()
      )  

  $("body").on 'click', '.rollback_log_info', ->
    url = $(this).data("link")
    $.ajax(
      type: "PUT"
      url: url
     )
  $("body").on 'click', '.delete_rollout_package', ->
    url = $(this).data("link")
    $siblings = $(this).siblings(".btn")
    $.ajax(
      type: "PUT"
      url: url
      dataType: 'json'
      context: this
      beforeSend: ->
        $siblings.disable()
      success: (data) ->
        if data['status']
          $(this).parent().parent().remove()
        flash_notice(data['status'], data['notice'])
      complete: ->
        $siblings.enable()
      )

  $("body").on 'click', '.add_rollback_package', ->
    $.blockUI(baseZ: 9999)
    url = $(this).data("link")
    arr = $(".to-rollback-package-list-info").map ->
      this.getAttribute("data-group-id")
    $.ajax(
      type: "PUT"
      url: url
      data:
        exists_ids:arr.get()
        added_id:$(".group_select_modal").val()
        swimlane:$(".swimlane_select_modal").val()
        module_name:$("#to_select_the_package_to_rollback").val()
      dataType: 'html'
      success: (data) ->
        $(".to-be-rollback-packages-list").html(data)
        bind_check_rollout()
      error: (data)  ->
          if !data['status']
            $(".to-be-rollback-packages-list").html(data)
            bind_check_rollout()
            flash_notice(data['status'], data['notice'])
      complete:(data) ->
        $("#createRollbackModal").modal('hide')
        $.unblockUI()
      )

  $("body").on 'click', '.package_logs', ->
    url = $(this).data("link")
    gs_group_id = $(".logs_group_select").val()
    project_id = $(".project_select").val()
    group_id = url.split("?group_id=")[1]
    $.ajax(
      type: "PUT"
      url: url
      data: {gs_group_id:gs_group_id,project_id:project_id,group_id:group_id}
      dataType: 'html'
      success: (data) ->
        $(".package_logs_body").html(data)
      complete:(data) ->
      )

  $("body").on 'click', '.static_package_logs', ->
    url = $(this).data("link")
    console.log(url)
    project_id = $(".static_project_select").val()
    $.ajax(
      type: "PUT"
      url: url
      data: {project_id:project_id}
      dataType: 'html'
      success: (data) ->
        $(".static_package_logs_body").html(data)
      complete:(data) ->
      )

  $("body").on 'click', '.create_rollback_package', ->
    - if $(".rollback_package_div")[0].style.display == "none"
        $(".rollback_package_div")[0].style.display = "inline-block"
        $("#Select_the_package_to_rollback_chosen")[0].style.width = "45%"
        $("#Select_the_group_to_rollback_chosen")[0].style.width = "45%"
        $(".create_rollback_package")[0].innerText = "Hide"
      else
        $(".rollback_package_div")[0].style.display = "none"
        $(".create_rollback_package")[0].innerText = "Add package to Rollback"

  $("body").on 'click', '.release-rollback', ->
    url = $(this).data("link")
    package_url = $(".to-be-rollback-packages-list").data("currentWarUrl")
    current_tag = $(".to-be-rollback-packages-list").data("currentWarTag")
    package_id = url.split("package_id=")[1]
    rollback_lion = 1
    if !$('.rollback-lion-switch-box').is(':checked')
      rollback_lion = 0
    $.ajax(
      type: "PUT"
      url: url
      data: {package_id:package_id,rollback_lion:rollback_lion,package_url:package_url,private_token: gon.api_token,
      keys: $(".modal-body").find('input:checkbox:checked').map(() -> this.name).get() }
      dataType: 'json'
      context: this
      beforeSend: ->
      success: (data) ->
        flash_notice(data['status'], data['notice'])
        if data['status']
          $(this).remove()
          location.reload("forceGet","true")
     )
     $("#confirmModal").modal('hide')

  $("body").on 'click', '.release-terminate', ->
    result = confirm("确定中止吗?")
    if result == true
      url = $(this).data("link")
      package_id = $(this).closest('.grouping-strategies-list').data("package-id")
      package_path = $(".selectedgs").children()[1].children[0].value
      $siblings = $(this).siblings(".btn")
      $.ajax(
        type: "PUT"
        url: url
        data: {package_id: package_id,package_path:package_path}
        dataType: 'json'
        context: this
        beforeSend: ->
          $siblings.disable()
        success: (data) ->
          if data['status']
            $(this).remove()
          flash_notice(data['status'], data['notice'])
          $($(".selectedgs").children()[1].children[0]).attr('disabled', "disabled").trigger("chosen:updated")
        complete: ->
          $siblings.enable()
          replace_gs()
      )


  $("body").on 'click', '#edit_static_package_name', ->
    url = $("#request_url").attr('value')
    $.ajax
      type: 'PUT'
      url: url
      data: {
        name: $("#static_package_name_edit").attr('value')
        url: $.trim($("#static_package_url_edit").attr('value'))
        package_id: $("#package_id").attr('value')
      }
      success : (res, status, xhr) ->
        if res["status"]
          $("#ticket-packing-list tr.editing .url").text(res["url"])
        flash_notice(res['status'], res['flash'])
        $("#editStaticPackage").modal('hide')

  $("body").on 'click', '.info_to_verify_check_when_fail', ->
    url = $("#deployCheckModal").attr("url")
    package_id = $("#deployCheckModal").attr("package_id")
    $.ajax(
      type: "GET"
      url: url
      data: {package_id: package_id,private_token: gon.api_token}
      success: (data) ->
        $("#deployCheckModal").modal('hide')
        $("#confirmModal").html(data)
        $("#confirmModal").modal('show')
      complete: ->
    )
    
  $("body").on 'click', '.deployCheckModal', ->
    url = $(this).data("link")
    left_id = $(this).closest('.grouping-strategies-list').data("package-id")
    package_id =  $(".selectedgs").data("link").split("package_id=")[1].split("&tag")[0].split("&after_tag")[0]
    $("#deployCheckModal").attr("url",url)
    $("#deployCheckModal").attr("package_id",package_id)
    
    if url.indexOf('info_to_verify') > 0
      $(".deploy_check_btn").attr("class",'deploy_check_btn btn btn-danger info_to_verify_check_when_fail')
    else 
      $(".deploy_check_btn").attr("class",'deploy_check_btn btn btn-danger paas-deploy')

  $("body").on 'click', '.info_to_verify', ->
    $('tr').removeClass('editing')
    $(this).closest('tr').addClass('editing')
    package_url = $(".selectedgs").children()[1].children[0].value
    $("#ticket-packing-list").data("package_url",package_url)
    url = $(this).data("link")
    package_id = $(this).closest('.grouping-strategies-list').data("package-id")
    left_id = $(".selectedgs").data("link").split("package_id=")[1].split("&tag")[0].split("&after_tag")[0]
    if left_id.toString() == package_id.toString()
      $siblings = $(this).siblings(".btn")
      $.ajax(
        type: "GET"
        url: url
        data: {package_id: package_id,private_token: gon.api_token}
        beforeSend: ->
          $siblings.disable()
        success: (data) ->
          $("#confirmModal").html(data)
          $("#confirmModal").modal('show')
        complete: ->
          $siblings.enable()
       )
    else
      alert("系统检测到包和分组策略信息不匹配，请重新选择!")

  $("body").on 'click', '.group-log', ->
    $.ajax(
      type: "GET"
      url: $(this).data("link")
      success: (data) ->
        $("#confirmModal").html("<pre>#{data}</pre>")
        $("#confirmModal").modal('show')
      )


  $("body").on 'click', '.info_to_verify_when_rollback', ->
    package_url =$(this).parent().parent().children()[4].children[0].value
    if package_url == ""
      alert("请您先选择要回滚到的tag信息,再进行操作!")
      return false
    $(".to-be-rollback-packages-list").data("currentWarUrl",package_url)
    current_tag = $($(this).parent().parent().children()[4].children[0]).find("option:selected").text()
    $(".to-be-rollback-packages-list").data("currentWarTag",current_tag)
    $('tr').removeClass('editing')
    $(this).closest('tr').addClass('editing')
    url = $(this).data("link")
    package_id = $(this).closest('.grouping-strategies-list').data("package-id")
    $siblings = $(this).siblings(".btn")
    $.ajax(
      type: "GET"
      url: url
      data: {package_id: package_id,package_url:package_url,private_token: gon.api_token, current_tag: current_tag}
      beforeSend: ->
        $siblings.disable()
      success: (data) ->
        $("#confirmModal").html(data)
        $("#confirmModal").modal('show')
        $(".rollback-lion-switch").bootstrapSwitch()
        $('.rollback-lion-switch').prop('checked', true)
      complete: ->
        $siblings.enable()
     )