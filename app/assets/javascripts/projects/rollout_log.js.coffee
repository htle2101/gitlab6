$ ->
  $('.more #more').click ->
    $('.more #more').addClass('undisplay')
    $('.more #refresh').removeClass('undisplay')

class RolloutLog
  @disableMore: ->
    $('.more #more').removeAttr('href')
    $('.more #more').removeAttr('data-remote')
    $('.more #more').text('No More')
    $('.more #more').css('color', '#ccc')
    $('.more #more').attr('disabled', 'disabled')
    $('.more #more').unbind('click')

  @appendLogs: ->
    $('.timeline').append(html)

  @changeHref: (page) ->
    $('.more #more').attr('href', location.pathname + '?page=' + page)

  @changeStatus: ->
    $('.more #more').removeClass('undisplay')
    $('.more #refresh').addClass('undisplay')

@RolloutLog = RolloutLog
