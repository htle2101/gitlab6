$ ->
  update_selection = (selector, callback) ->
    branch_name = $(".branch_name_select").val()
    jar_name = $(".jar_name_select").val()
    url = $(selector).attr("url")
    env= $(".env_select").val()
    if $(".business_belongs_to_select").val() != null
      business_name = $(".business_belongs_to_select").val().split("|")[1]
    $.get url, {env:env,branch_name: branch_name,business_name:business_name,jar_name:jar_name}, ((data) ->
      $(selector).empty()
      $(selector).append("<option></option>")
      callback() if callback
      $.each data, (index, obj) ->
        $(selector).append("<option value='#{obj.id}'>#{obj.value}</option>")
      $(selector).trigger("chosen:updated")
    ), 'json'

  $("body .branch_name_select ").chosen().change ->
    update_selection.call this, ".jar_name_select", ->
      $(".jar_name_select").empty()
      $(".jar_name_select").append("<option></option>")
      $(".jar_name_select").trigger("chosen:updated")

  $("body .jar_name_select").chosen().change  ->
    update_selection.call this, ".tag_name_select", ->
      $(".tag_name_select").empty()
      $(".tag_name_select").append("<option></option>")
      $(".tag_name_select").trigger("chosen:updated")

  $("body .env_select").chosen().change  ->
    $(".business_belongs_to_detail .chosen-results").height(150)
    if $(".env_select").val() == 'product' || $(".env_select").val() == 'ppe'
      $(".business_belongs_to_group").css("display","block")
      $(".service_ip_select").empty()
      $(".service_ip_select").trigger("chosen:updated")
      update_selection.call this, ".business_belongs_to_select", ->
        $(".business_belongs_to_select").empty()
        $(".business_belongs_to_select").append("<option></option>")
        $(".business_belongs_to_select").trigger("chosen:updated")
    else
      $(".business_belongs_to_group").css("display","none")
      $(".service_ip_select").empty()
      $(".service_ip_select").trigger("chosen:updated")
      update_selection.call this, ".service_ip_select", ->
        $(".service_ip_select").empty()
        $(".service_ip_select").append("<option></option>")
        $(".service_ip_select").trigger("chosen:updated")


  $("body .business_belongs_to_select").chosen().change  ->
    update_selection.call this, ".service_ip_select", ->
    $(".service_ip_select").empty()
    $(".service_ip_select").append("<option></option>")
    $(".service_ip_select").trigger("chosen:updated")