$(function() {
      //set the initial value of the progress bar
  set_initial_bar_value_when_refresh();
  $(".tablesorter").tablesorter({
    theme : 'blue',
    // sort on the first column and third column in ascending order
    sortList: [[0,0],[2,0]]
  });
});

function set_initial_bar_value_when_refresh(){
      var v=0,k=0;
      for(var i=0;i<$(".deploystatus").length;i++){
        if($(".deploystatus")[i].innerText == "deploying"){
           v++;
        }
        if($(".deploystatus")[i].innerText == "deploy error"){
          k++;
        }
      }
if(typeof($("#build_status")[0]) != "undefined" && typeof($(".bar")[0] )!= "undefined"){
 if($("#build_status")[0].innerText != "running" && v == 0){
      set_bar_attribute("100%","");
  if($("#build_status")[0].innerText == "failure" ||  k > 0)
      $(".bar")[0].className = "bar bar-danger";
  else
        $(".bar")[0].className = "bar";
  }else{
  get_percent();
  refresh_deploy_status($("#jenkins_job_name").text());
  }}
}

function set_progress_bar_percent(percent){
  text = $("#jenkins_job_name").text();
   deploy_type = "default";
   if (typeof($("#phoenix")[0]) != "undefined")
      deploy_type = "Phoenix";
  url = "/api/v3/ci/" + text + "/set_percent";
  $.ajax(
  {url: url,
   data:{percent:percent,private_token: gon.api_token,deploy_type:deploy_type},
  success: function(result){
  }}
  )
}

function set_bar_attribute(value,type){
  $(".bar").attr("style","width:" + value);
  if(typeof($(".progress")[0]) !="undefined")
  $(".progress")[0].className = "progress progress-striped" +" " + type;
}

function get_percent(){
  text = $("#jenkins_job_name").text();
  if($("#build_status")[0].innerText == "running"){
   url = "/api/v3/ci/" + text + "/get_percent";
   $.ajax(
    {url: url,
    data:{private_token: gon.api_token},
    success: function(result){
      var len = parseFloat(result.split("%")[0]) *($(".module-name").length)/50 + 1;
      var dis = Math.round(len) - deploy_status_array.length;
      if( dis > 0 ){
        for(var i = 0 ; i < dis; i++){
          deploy_status_array.push("running");
        }
       }
      $(".progress.progress-striped.active .bar").attr("style","width:" + result);
    }})}
   else
   {
    var k = 0,m = 0;
    for(var i=0;i<$(".deploystatus").length;i++){
      if($(".deploystatus")[i].innerText.indexOf("deploying") >= 0 )
            k++;
      if($(".deploystatus")[i].innerText.indexOf("deploy error") >= 0 )
            m++;
       }
      if(k>0){
      $(".bar").attr("style","width:" + "50%");
       }else{
        {
          set_bar_attribute("100%","");
        // $(".bar").attr("style","width:" + "100%");
        // $(".progress")[0].className = "progress progress-striped";
        if(m > 0)
        $(".bar")[0].className = "bar bar-danger";
        else
        $(".bar")[0].className = "bar";
     }}}}

var deploy_status_array = new Array;
function deploy_func(module_path,module_name,version,jenkins_job_name,jar_name,build_branch_id,groupId){
  $.ajax(
    {url: '/deploy_to_maven/pre_deploy/',
      data:{jar_name: jar_name, module_name: module_name, jenkins_job_name: jenkins_job_name, version: version, build_branch_id: build_branch_id,groupId: groupId},
      success: function(result){
        var r = false
        if(result["result"] == "deploy success")
          deploy_to_maven_repos(module_path,module_name,version,jenkins_job_name,jar_name,build_branch_id, groupId);
        else
          r = confirm("确定把jar包上传到maven仓库?");
        if(r == true){
          deploy(module_path,module_name,version,jenkins_job_name,jar_name,build_branch_id,groupId);
        }
      }
  });
}

function deploy_to_maven_repos(module_path,module_name,version,jenkins_job_name,jar_name,build_branch_id,groupId){
  if(version.indexOf("SNAPSHOT")>0){
    var r = confirm("已经存在同名的snapshot,确认覆盖吗?");
    if(r == true){
      deploy(module_path,module_name,version,jenkins_job_name,jar_name,build_branch_id,groupId);
    }
  }
  else{
    alert("已经存在同名的released,不允许覆盖!");
  }
}

function deploy(module_path,module_name,version,jenkins_job_name,jar_name,build_branch_id,groupId){
  $("#"+module_name+"-to-maven").attr("disabled","disabled")
  $.ajax(
    {url: '/deploy_to_maven/deploy',
      data:{jar_name: jar_name, module_path: module_path, module_name: module_name, jenkins_job_name: jenkins_job_name, build_branch_id: build_branch_id, version: version, groupId: groupId},
      success: function(result){
        alert("请求成功，请在本页Log里查看结果!")
          // deploy_status(ci_branch_id,module_name);
   }}
  );
}

function deploy_status(build_branch_id,module_name){
  $.ajax(
 {url: '/deploy_to_maven/deploy_status',
  data: {build_branch_id: build_branch_id ,module_name: module_name},
  success: function(result){
      if(["deploy error", "deploy success"].toString().indexOf(result["result"] ) < 0 ) {
          deploy_status(build_branch_id,module_name);
      }
      else{
      if(result["result"] == "deploy success"){
        alert("Deploy成功!");
      }
      else{
        alert("Deploy失败!");
        }
      }}}
  );
}

function  switch_value(result,type){
  switch(result){
    case "success": case "deploy success" :
    this.value = "badge badge-success" + " " + type;
    break;
    case "failure" : case "deploy error" :
    this.value = "badge badge-important" + " " + type ;
    break;
    case "unstable" :
    this.value = "badge badge-warning" + " " + type ;
    break;
    default :
    this.value = "badge" +" " + type ;
    break;
    }
    return this.value;
}

function refresh_build_status(result){
  if($(".bar").attr("style") == "width:0%"){
        $("#build_status")[0].innerText = "running";
        $("#build_status")[0].className = "badge";
      }else{
        $("#build_status")[0].innerText = result["globel"];
        var  sv_buildstatus = new switch_value($("#build_status")[0].innerText,"");
        $("#build_status")[0].className = sv_buildstatus.value;
      }
}

function refresh_module_status(result){
      var len = $(".module-name").length;
      for(var i=0;i<len;i++){
      if(typeof(result[$(".module-name")[i].innerText]) != "undefined"){
        $(".module_status")[i].innerText = result[$(".module-name")[i].innerText];
        var sv_modulestatus = new switch_value(result[$(".module-name")[i].innerText],"module_status");
        $(".module_status")[i].className = sv_modulestatus.value;
      }
      else{
        $(".module_status")[i].innerText = "not_run";
        $(".module_status")[i].className = "badge module_status";
      }}
}

//change progress bar value
function change_progress_bar_value(){
        if($("#build_status")[0].innerText == "running" && $(".progress.progress-striped.active .bar").width() <= 282){
        percent = (deploy_status_array.length -1)*50/$(".module-name").length + '%';
        if(parseFloat(percent.split("%")[0]) > 50 ){
          percent = "50%";
        }
        set_bar_attribute(percent,"active");
        $(".bar")[0].className = "bar";
        set_progress_bar_percent(percent);
        }
}

//the beginning of refreshing the modules, wars and phoenix status
function refresh_deploy_status(text){
    window.startSpinner();
    deploy_status_array.push($("#build_status")[0].innerText);
    url = "/api/v3/ci/" + text + "/status";
    deploy_type = "default";
     if (typeof($("#phoenix")[0]) != "undefined")
        deploy_type = "Phoenix";
    $.ajax(
    {url: url,
     data:{private_token: gon.api_token,deploy_type:deploy_type},
     success: function(result){
        refresh_build_status(result);
        refresh_module_status(result);
        }});
      change_progress_bar_value();
      charge_refresh_default_or_phoenix();
}

//charge refresh module status or war_status
function charge_refresh_default_or_phoenix(){
      var k = status_not_running_count();
      if(k > 1 && ((deploy_status_array[deploy_status_array.length-1] == "success")||(deploy_status_array[deploy_status_array.length-1] == "unstable")||(deploy_status_array[deploy_status_array.length-1] == "failure") )){
        if (typeof($("#phoenix")[0]) != "undefined")
          {
          set_progress_bar_percent($(".bar").attr("style"));
          refresh_deploy_phoenix_status($("#jenkins_job_name").text());}
        else
          refresh_deploy_default_status($("#jenkins_job_name").text());
        }else{
        window.resetId = 0;
        window.resetId=setTimeout('refresh_deploy_status($("#jenkins_job_name").text());',10000);
      }
}

function refresh_deploy_phoenix_status(text){
    url = "/api/v3/ci/" + text + "/status";
    deploy_type = "default";
    if (typeof($("#phoenix")[0]) != "undefined")
        deploy_type = "Phoenix";
    $.ajax(
    {url: url,
     data:{private_token: gon.api_token,deploy_type:deploy_type},
     success: function(result){
        set_bar_value_when_refresh_phoenix(result);
        refresh_phoenix_status(result);
        }});
}

//end of phoenix deploy
function refresh_phoenix_status(result){
       $(".deploystatus")[0].innerText = result["status_message"]["current_branch"];
       var sv_phoenixstatus = new switch_value($(".deploystatus")[0].innerText,"deploystatus");
       $(".deploystatus")[0].className = sv_phoenixstatus.value;
       if(result["status_message"]["current_branch"] == "deploying"){
       window.resetId = 0;
       window.resetId=setTimeout('refresh_deploy_phoenix_status($("#jenkins_job_name").text());',10000);
       }
       else{
            window.stopSpinner();
            set_bar_attribute("100%","");
            if(result["status_message"]["current_branch"] == "deploy error"){
                  $(".bar")[0].className = "bar bar-danger";
          }
       }
}

//set bar value when phoenix_deploy
function set_bar_value_when_refresh_phoenix(result){
    machine_percent = result["status_message"].machine_percent + '%'
    $(".bar").attr("style","width:" + machine_percent);
}

function  status_not_running_count(){
      var  k=0;
      for(var j = 0; j< deploy_status_array.length;j++){
          if(deploy_status_array[j] != "running" && deploy_status_array[j] != "running..."){
            k=k+1;
          }
      }
    return k;
}

function refresh_deploy_default_status(text){
    if (typeof($("#phoneix")[0]) != "undefined")
        deploy_type = "Phoneix";
     url = "/api/v3/ci/" + text + "/status";
     $.ajax(
      {url: url,
      data:{private_token: gon.api_token,deploy_type:deploy_type},
      success: function(result){
      for(var s = 0; s < $(".war").length; s++){
        //set status for each  war
        if(typeof(result["status_message"][$(".war")[s].innerText]) != "undefined"){
        deploy_status = result["status_message"][$(".war")[s].innerText] ==""?"to deploy" : result["status_message"][$(".war")[s].innerText];
        $(".ip").find(".deploystatus")[s].innerText = deploy_status;
        var sv_deploystatus = new switch_value(deploy_status,"deploystatus");
        $(".ip").find(".deploystatus")[s].className = sv_deploystatus.value;
        machine_percent = result["status_message"].machine_percent + '%';
        $(".bar").attr("style","width:" + machine_percent);
        if(deploy_status.search(/deploy error/i)>=0){
          $(".jboss_log")[s].setAttribute("style","display:yes");
          $(".jboss_log")[s].disabled=false;
          $(".lzm-deploy")[s].setAttribute("style","display:yes");
          $(".lzm-deploy")[s].disabled=false;
        }
        else{
          $(".jboss_log")[s].setAttribute("style","display:none");
          $(".jboss_log")[s].disabled=true;
          $(".lzm-deploy")[s].setAttribute("style","display:none");
          $(".lzm-deploy")[s].disabled=true;
        }
        }
        //all wars has no ip or machine,just set status to "to deploy"
          else{
              if(typeof($(".ip").find(".deploystatus")[s]) != "undefined"){
              $(".ip").find(".deploystatus")[s].innerText = "to deploy";
              $(".ip").find(".deploystatus")[s].className = "badge deploystatus";

              machine_percent = result["status_message"].machine_percent + '%';
             $(".bar").attr("style","width:" + machine_percent);
          }}
        }
        //all war has been deployed,no deploying war
        if(JSON.stringify(result["status_message"]).indexOf("deploying") < 0){
          set_bar_attribute("100%","");
          // $(".bar").attr("style","width:" + "100%");
          // $(".progress")[0].className = "progress progress-striped";
          if(JSON.stringify(result["status_message"]).indexOf("deploy error") >= 0){
              $(".bar")[0].className = "bar bar-danger";
          }
           window.stopSpinner();
        }
      else{
      window.resetId = 0;
      window.startSpinner();
      window.resetId=setTimeout('refresh_deploy_default_status($("#jenkins_job_name").text());',10000);
        }
      }
    });
  }