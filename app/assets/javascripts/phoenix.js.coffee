$ ->

  update_selection = (selector, callback) ->
    url = $(selector).data("url")
    $.get url, {id: this.value}, ((data) ->
      $(selector).empty()
      $(selector).append("<option></option>")
      callback() if callback
      $.each data, (index, obj) ->
        $(selector).append("<option value='#{obj.id}'>#{obj.value}</option>")
      $(selector).trigger("chosen:updated")
    ), 'json'

  $("#m_project_id").chosen().change (event) ->
    update_selection.call this, "#dependency_target_build_branch_id", ->
      $("#dependency_module_name").empty()
      $("#dependency_module_name").append("<option></option>")
      $("#dependency_module_name").trigger("chosen:updated")

  $("#dependency_target_build_branch_id").chosen().change (event) ->
    update_selection.call(this, "#dependency_module_name")
    
  $("#lion_team").chosen().change (event) ->
    update_selection.call(this, "#lion_product")

  $(".add-lion-project").click (event) ->
    url = $(this).data("link")
    $.ajax(
      type: "POST"
      url: url
      data:
        product: $('#lion_product').val()
        project: $('#lion_project').val()
      dataType: "json"
    ).done (data) ->
      flash_notice(data.status, data.message) 

  $(".deps-form").submit ->
    $.ajax
      url: this.action
      type: "POST"
      dataType: "json"
      context: this
      data: $(this).serializeArray()
      success: (data) ->
        $(this).find(".alert-error").remove()
        if data.success
          $("#phoenix-deps-list tbody").append(data.message)
        else
          $(this).find(".span4").prepend(data.message)
    return false


  $("#phoenix-deps-list").on 'click', '.btn-remove', ->
    result = window.confirm("This module will be delete, Are you sure?")
    return false unless result
    $tr = $(this).closest('tr')
    $.ajax
      url: this.href
      type: "DELETE"
      dataType: "json"
      context: this
      success: (data) ->
        if data.success
          $tr.remove()
        else
          alert("This module Can not be deleted.")
    return false


  $('.all-deps').click ->
    $(this).disable()
    $siblings = $(this).siblings(".btn")
    $.ajax $(this).data('link'),
      type: "GET"
      data: {}
      dataType: "json"
      context: this
      beforeSend: ->
        $siblings.disable()
      success: (data) ->
        if data.need_ref_dep
          $("#confirmModal").html(data.all_deps)
          $("#confirmModal").modal('show')
        else
          top.location.href = $('.all-deps').data('has-one-dep-link')
      complete: ->
        $siblings.enable()
        $(this).enable()
    return false

  $(".container").on 'click', '#create-bat-deps', ->
    selectDeps = []
    $('.dep-label').each ->
      if $(this).val() > 0
        selectDeps.push($(this).val())
    input = $("<input>").attr("type", "hidden").attr("name", "selectDeps").val(selectDeps)
    $('#bat-deps-form').append($(input))
    $('#bat-deps-form').submit()

  $("#ci-build-from-deps").click ->
    $.ajax "/api/v3/ci/#{$(this).data('jenkins-job-name')}/build",
      type: 'GET'
      data:
        private_token: gon.api_token
      success : (res, status, xhr) ->
        location.href = $('#ci-build-from-deps').data('link')

