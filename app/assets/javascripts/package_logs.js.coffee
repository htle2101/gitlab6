$ ->
  $(".createRollbackModal").click ->
    $("#to_select_the_package_to_rollback_chosen")[0].style.width = "80%"
    $("#to_select_the_group_to_rollback_chosen")[0].style.width = "80%"
    $(".package_select").val("").trigger("chosen:updated")
    $(".group_select_modal").val("").trigger("chosen:updated")

  update_selection = (selector, callback) ->
    url = $(selector)[0].getAttribute("url")
    arr_to_be_rollback = $(".to-rollback-package-list-info").map ->
      this.getAttribute("data-group-id")
    arr_rollbacking = $(".rollbacking-package-list-info").map ->
      this.getAttribute("data-group-id")
    arr = arr_to_be_rollback.get().concat(arr_rollbacking.get())
    $.get url, {project_id: this.value,exists_ids: arr}, ((data) ->
      $(selector).empty()
      $(selector).append("<option></option>")
      callback() if callback
      $.each data, (index, obj) ->
        $(selector).append("<option value='#{obj.id}'>#{obj.value}</option>")
      $(selector).trigger("chosen:updated")
    ), 'json'   

  $("body .project_select ").chosen().change ->
    update_selection.call this, ".logs_package_select", ->
      $(".logs_package_select").empty()
      $(".logs_package_select").append("<option></option>")
      $(".logs_package_select").trigger("chosen:updated")

  $("body .logs_package_select").chosen().change  ->
    update_selection.call this, ".logs_group_select", ->
      $(".logs_group_select").empty()
      $(".logs_group_select").append("<option></option>")
      $(".logs_group_select").trigger("chosen:updated")

