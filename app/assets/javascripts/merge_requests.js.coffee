#
# * Filter merge requests
#
@merge_requestsPage = ->
  $('#assignee_id').chosen()
  $('#milestone_id').chosen()
  $('#milestone_id, #assignee_id').on 'change', ->
    $(this).closest('form').submit()

class MergeRequest

  constructor: (@opts) ->
    this.$el = $('.merge-request')
    @diffs_loaded = false
    @commits_loaded = false

    this.activateTab(@opts.action)

    this.bindEvents()

    this.initMergeWidget()
    this.$('.show-all-commits').on 'click', =>
      this.showAllCommits()

    modal = $('#modal_merge_info').modal(show: false)

  # Local jQuery finder
  $: (selector) ->
    this.$el.find(selector)

  initMergeWidget: ->
    this.showState( @opts.current_status )

    if this.$('.automerge_widget').length and @opts.check_enable
      $.get @opts.url_to_automerge_check, (data) =>
        this.showState( data.merge_status )
      , 'json'

    if @opts.ci_enable
      $.get @opts.url_to_ci_check, (data) =>
        this.showCiState data.status
      , 'json'

  bindEvents: ->
    this.$('.nav-tabs').on 'click', 'a', (event) =>
      a = $(event.currentTarget)

      href = a.attr('href')
      History.replaceState {path: href}, document.title, href

      event.preventDefault()

    this.$('.nav-tabs').on 'click', 'li', (event) =>
      this.activateTab($(event.currentTarget).data('action'))

    this.$('.accept_merge_request').on 'click', ->
      $('.automerge_widget.can_be_merged').hide()
      $('.merge-in-progress').show()

  activateTab: (action) ->
    this.$('.nav-tabs li').removeClass 'active'
    this.$('.tab-content').hide()
    switch action
      when 'diffs'
        this.$('.nav-tabs .diffs-tab').addClass 'active'
        this.loadDiff() unless @diffs_loaded
        this.$('.diffs').show()
      else
        this.$('.nav-tabs .notes-tab').addClass 'active'
        this.$('.notes').show()

  showState: (state) ->
    $('.automerge_widget').hide()
    $('.automerge_widget.' + state).show()

  showCiState: (state) ->
    $('.ci_widget').hide()
    $('.ci_widget.ci-' + state).show()

  loadDiff: (event) ->
    $.ajax
      type: 'GET'
      url: this.$('.nav-tabs .diffs-tab a').attr('href')
      beforeSend: =>
        this.$('.status').addClass 'loading'

      complete: =>
        @diffs_loaded = true
        this.$('.status').removeClass 'loading'

      dataType: 'script'

  showAllCommits: ->
    this.$('.first-commits').remove()
    this.$('.all-commits').removeClass 'hide'

  alreadyOrCannotBeMerged: ->
    this.$('.automerge_widget').hide()
    this.$('.merge-in-progress').hide()
    if $(".merge_request_inspect_item_box").length > 0
      location.reload()
    this.$('.automerge_widget.already_cannot_be_merged').show()
    
    
this.MergeRequest = MergeRequest

$ ->
  $('.info_to_verify').click ->
    $(this).disable()
    $siblings = $(this).siblings(".btn")
    $.ajax "#{$(".source_project").data('link')}",
      type: "PUT"
      data: $('#merge-request-form').serialize()
      dataType: "json"
      context: this
      beforeSend: ->
        $siblings.disable()
      success: (data) ->
        if data.need_verify
          $("#confirmModal").html(data.info_to_verify)
          $("#confirmModal").modal('show')
        else
          $('#merge-request-form').submit()
      complete: ->
        $siblings.enable()
        $(this).enable()
    return false

$ ->
  $(".container").on 'click', ".create-merge-request-info", ->
    $('#merge-request-form').submit()


class MergeInspectRefresher
  constructor: ()->

  check: ()->
    that = this

    if $(".merge_request_inspect_item_box").length > 0 && $(".merge_request_inspect_item_box").attr("charge") == "true"
    
      url = "/"+ $("body .merge_request_inspect_item_box").data("link") + "/merge_requests/merge_inspect"

      id = $("body .merge_request_inspect_item_box").attr("id")

      $.ajax
        type: 'GET'
        url: url
        dataType: "html"
        data: {id: id}
        success: (data) ->
          $(".merge_request_inspect_item").html(data)
          clearInterval(that.interval)
    else
      clearInterval(that.interval)

  refresh: ->
    if $(".merge_request_inspect_item_box").length >0 && $(".merge_request_inspect_item_box").attr("accept_charge") == "true"
      $(".accept_merge_request").attr("disabled",false)
      $(".accept_merge_request").attr("style","background-color:#2A2;border-color:#2A2;")
    that = this

    @interval = setInterval(
      () ->
        that.check()
      5000)


@MergeInspectRefresher = MergeInspectRefresher
