# usage: 
# var terminal = new Terminal(url, title, {limit: 20, loading: true})
# terminal.minimize();    Minimize the terminal
# terminal.maximize();    Maximize the terminal
# terminal.loading();     Ajax polling the url, refresh the content.
# terminal.stopLoading(); Stop polling.
# terminal.close();       Close the terminal and stop loading.
#
terminal_template = "" +
"<div class='terminal'>
  <div class='terminal-titlebar'>
    <span class='terminal-btn'>
      <a class='terminal-close' href='javascript:void(0);'></a>
      <a class='terminal-minimize' href='javascript:void(0);'></a>
      <a class='terminal-maximize' href='javascript:void(0);'></a>
    </span>
    <div class='terminal-title'></div>
  </div>
  <div class='terminal-body'>
    <div class='terminal-content'></div>
  </div>
</div>"

class Terminal
  @terminals = []
  constructor: (@url, @title, @options) ->

    @options ?= {}
    @limit = @options.limit || 20
    @offset = 0
    @interval = @options.interval || 2000
    width = @options.width || 300
    height = @options.height || 150

    @terminal = $(terminal_template)
    @terminal.data("this", this)
    @terminal.appendTo('body')
    @terminal.width(width).height(height)
    position = @terminal.position()
    @terminal.css('top', position.top).css('left', position.left)
    @terminal.find(".terminal-title").text(@title) if @title
    @terminal.find(".terminal-body").height(height - @terminal.find(".terminal-titlebar").outerHeight())
    @terminal.draggable({handle: ".terminal-titlebar"})
    @terminal.resizable({alsoResize: ".terminal-body", minWidth: 60, minHeight: 80, handles: "n, e, s, w, se, sw"})

    @terminal.find(".terminal-close").click ->
      $terminal = $(this).closest('.terminal')
      _this = $terminal.data("this")
      _this.close()

    @terminal.find(".terminal-maximize").click ->
      $terminal = $(this).closest('.terminal')
      _this = $terminal.data("this")
      _this.maximize()


    @terminal.find(".terminal-minimize").click ->
      $terminal = $(this).closest('.terminal')
      _this = $terminal.data("this")
      _this.minimize()

    this.loading() if @options.loading

    Terminal.terminals.push(this)

  recoveFromMinimize: ->
    if boundary = @terminal.data("minimized")
      @terminal.removeData("minimized")
      position = @terminal.position()
      @terminal.css("position", "absolute").css('top', position.top).css('left', position.left)
      @terminal.resizable("enable")
      @terminal.draggable("enable")
      @terminal.find(".terminal-content").show()
      boundary
    else
      null

  saveBoundary: (type) ->
    _bound = @terminal.position()
    _bound.width = @terminal.width()
    _bound.height = @terminal.height()
    @terminal.data(type, _bound)

  loading: ->
    unless @timer
      terminal = this
      request = ->
        $.ajax
          type: "GET"
          url: terminal.url
          data: "limit=" + terminal.limit + "&offset=" + terminal.offset
          dataType: 'json'
          context: this
          success: this.callback
          complete: ->
      @timer = setInterval (->
        request.call(terminal)
      ), @interval

  stopLoading: ->
    if @timer
      clearInterval(@timer)
      @timer = undefined

  callback: (resp) ->
    @offset += resp.count
    _terminal = @terminal
    $(resp.data).each (i, log) ->
      _terminal.find(".terminal-content").append("<div class='terminal-line'>[#{log.log_at}] #{log.message}</div>")
    $tbody = _terminal.find(".terminal-body")
    $tbody.scrollTop($tbody[0].scrollHeight)

  minimize: ->
    this.show() if this.isHidden()
    if @terminal.data("maximized")
      @terminal.removeData("maximized")

    if boundary = this.recoveFromMinimize()
      minimized = true
    else
      minimized = false
      this.saveBoundary("minimized")
      boundary = { width: 60 , height : 80, top: $(window).height() - 80, left: $(window).width() - 60 }
      @terminal.resizable("disable")
      @terminal.draggable("disable")

    @terminal.animate(boundary, {
      progress: (animation, progress, remaining) ->
        height = $(this).height() - $(this).find(".terminal-titlebar").outerHeight()
        $(this).find(".terminal-body").height(height)
        $(this).find(".terminal-body").width($(this).width())
      done: ->
        unless minimized
          $(this).css("position", "fixed").css("top", "auto").css("left", "auto").css("bottom", 0).css("right", 0)
          $(this).find(".terminal-content").hide()
    })

  maximize: ->
    this.show() if this.isHidden()
    this.recoveFromMinimize()

    boundary = @terminal.data("maximized")
    if boundary?
      @terminal.removeData("maximized")
    else
      this.saveBoundary("maximized")
      boundary = { width: $(window).width(), height : $(window).height(), top: 0, left: 0 }

    @terminal.animate(boundary, {
      progress: (animation, progress, remaining) ->
        height = $(this).height() - $(this).find(".terminal-titlebar").outerHeight()
        $(this).find(".terminal-body").height(height)
        $(this).find(".terminal-body").width($(this).width())
    })

  hide: ->
    @terminal.hide()

  show: ->
    @terminal.show()

  isHidden: ->
    @terminal.is(":hidden")

  close: ->
    this.stopLoading()
    @terminal.remove()

@Terminal = Terminal
document.addEventListener "page:fetch", ->
  terminal.close() for terminal in Terminal.terminals
