$ ->
  $("#scripts-index-form #created_at, #scripts-index-form #db_name, #scripts-index-form #todo").change ->
    this.form.submit()

  $("#scripts-index-form #name").keypress (e)->
    this.form.submit() if e.which == 13

  $(".scripts-list .script-execute, .scripts-list .script-close").click ->
    confirmed = true
    if ($(this).data("confirm"))
      confirmed = confirm($(this).data("confirm"))
    if confirmed
      $.ajax
        url: $(this).attr("href")
        type: 'PUT'
        context: this
        success: (res, status, xhr) ->
          flash_notice(res['status'], res['msg'])
          if res['status']
            $(this).closest("li").find(".status-entry").replaceWith(res['script_status'])
            $(this).closest("li").find(".btn").hide()
    return false
