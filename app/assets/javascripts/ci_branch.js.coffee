# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/
#
show_info_about_apply_or_bind_machine = (res, status, xhr) ->
  $("tr.editing").find('span.ip-addr').text(res["ip"])
  $("tr.editing .dn-button").attr('disabled',false)
  $("tr.editing .createModal").attr('href','#editModal') if res["status"]
  $("tr.editing .createModal").removeClass('createModal').addClass('editModal') if res["status"]
  $("#createModal").modal('hide') if res["status"]
  flash_notice(res['status'], res['flash'])
  $.unblockUI()
  if res["domain_name"]?
    $(".module-machine-list tr.editing .dn-button").attr('style','display:none')
    $(".module-machine-list tr.editing .domain-name").text(res["domain_name"])
    $(".module-machine-list tr.editing .domain-name").wrapInner('<a href="", style="_blank"/>')
    $(".module-machine-list tr.editing .domain-name a").attr('href', res["href"])
    $(".module-machine-list tr.editing .domain-ref").text(res["domain_name"])
    $(".module-machine-list tr.editing .editDomainName").attr('href','#deleteDomainName')
    $(".module-machine-list tr.editing .editDomainName").removeClass('editDomainName').addClass('deleteDomainName')


$ ->
  $('.change-branch-select').on 'change', ->
    $(@).parents('form').submit()

  $(".jar-deploy").popover()

  $(".ci-branch-destroy").popover()

  $(".createModal").click ->
    $("#module_name").attr("value", $(this).closest('td').attr('module-name'))
    $('.module-machine-list tr').removeClass('editing')
    $(this).closest('tr').addClass('editing')

  $(".editModal").click ->
    $('.module-machine-list tr').removeClass('editing')
    $(this).closest('tr').addClass('editing')
    $("#module_name").attr("value", $(this).closest('td').attr('module-name'))  

  $(".swimlaneModal").click ->
    $('.module-machine-list tr').removeClass('editing')
    $(this).closest('tr').addClass('editing')
    $.ajax "/#{$("#assign-machine").data('project')}/virtual_machines/info",
      type: 'GET'
      data: {
        build_branch_id: $("#build_branch_id").val(),
        machine_ip: $.trim($("tr.editing").find('td.ip span.ip-addr').text())
      }
      success : (res, status, xhr) ->
        $("#current_swimlane").attr("value", res['swimlane'])
        $("#swimlaneModal").modal('show')

  $(".slbModal").click ->
    $('.module-machine-list tr').removeClass('editing')
    $(this).closest('tr').addClass('editing')
    $.ajax "/#{$("#assign-machine").data('project')}/virtual_machines/info",
      type: 'GET'
      data: {
        build_branch_id: $("#build_branch_id").val(),
        machine_ip: $.trim($("tr.editing").find('td.ip span.ip-addr').text())
      }
      success : (res, status, xhr) ->
        $(".slb-text").text("alpha路由(" + res['poolname'] + ")将指向机器("+ res['ip'] + ")。")
        $("#slbModal").modal('show')

  $(".borrow-long-term").click ->
    $('.module-machine-list tr').removeClass('editing')
    $(this).closest('tr').addClass('editing')

  $(".borrow-temporary").click ->
    $('.module-machine-list tr').removeClass('editing')
    $(this).closest('tr').addClass('editing')

  $("table.module-machine-list").on 'click', '.editDomainName', ->
    $("#ip_edit").attr("value", $(this).parent().attr('ip'))
    $("#ip_edit").attr("readonly", true)
    $("input#domain_name_edit").attr("value",'.alpha.dp')
    $('.module-machine-list tr').removeClass('editing')
    $(this).closest('tr').addClass('editing')

  $("table.module-machine-list").on 'click', '.deleteDomainName', ->
    $("#domain_name_delete").attr("value", $(this).parent().attr('domain_name'))
    $("#domain_name_delete").attr("readonly", true)
    $("#ip_delete").attr("value", $(this).parent().attr('ip'))
    $("#ip_delete").attr("readonly", true)
    $('.module-machine-list tr').removeClass('editing')
    $(this).closest('tr').addClass('editing')

  $("#editDomainName").on 'shown', ->
    $("input#domain_name_edit").get(0).setSelectionRange("0","0")

  $(".dn-button").click ->
    $(".module-machine-list tr").removeClass('editing')
    $(this).closest('tr').addClass('editing')

  $(".lzm-deploy").click ->
    $.blockUI(baseZ: 9999)

    $.ajax "/api/v3/op/#{$("#jenkins_job_name").text()}/deploy/#{$(this).parent().attr('module-name')}",
      type: 'POST'
      data:
        private_token: gon.api_token
      context: this
      success : (res, status, xhr) ->
        $(this).attr("disabled", "disabled")
        $.unblockUI()

  $(".alpha-restart-tomcat").click ->
    machine_ip = $(this).parent().parent().find('.ip-addr').text().trim()
    project_name = $("#assign-machine").data('project')

    $(this).attr('disabled', 'disabled')
    $(this).text('重启中...')
    $(this).parent().parent().find('.deploystatus').removeClass('badge-success')
    $(this).parent().parent().find('.deploystatus').text('deploying')

    $.ajax
      url: "/api/v3/op/alpha_restart_tomcat"
      type: 'POST'
      data:
        private_token: gon.api_token
        machine_ip: machine_ip
      context: this
      success: (res, status, xhr) ->
        that = this

        interval = setInterval(
          () ->
            $.ajax
              url: "/" + project_name + "/virtual_machines/info"
              type: 'GET'
              data:
                machine_ip: machine_ip
              context: that
              success: (data) ->
                if(data.status == 7)
                  $(this).removeAttr('disabled')
                  $(this).text('重启')
                  $(this).parent().parent().find('.deploystatus').text('deploy success')
                  $(this).parent().parent().find('.deploystatus').addClass('badge-success')
                  clearInterval(interval)
          5000)
      error: (res, status, xhr) ->
        $(this).removeAttr('disabled')
        $(this).text('重启')
        $(this).parent().parent().find('.deploystatus').removeClass('badge-success')
        $(this).parent().parent().find('.deploystatus').text('to deploy')

        flash_notice(false, '重启 Tomcat 失败')

  $(".lzm-restart-tomcat").click ->
    machine_ips = $(this).parent().parent().find('.ip-addr').text().trim().split(', ')
    module_name = $(this).parent().attr('module-name')
    build_branch_id = $("#build_branch_id").attr('value')

    $(this).attr('disabled', 'disabled')
    $(this).text('重启中...')
    $(this).parent().parent().find('.deploystatus').removeClass('badge-success')
    $(this).parent().parent().find('.deploystatus').text('deploying')

    $.ajax
      url: "/api/v3/op/lzm_restart_tomcat"
      type: 'POST'
      data:
        private_token: gon.api_token
        module_name: module_name
        build_branch_id: build_branch_id
        machine_ips: machine_ips
      context: this
      success: (data) ->
        uuids = data.uuids

        that = this

        interval = setInterval(
          () ->
            $.ajax
              url: "/api/v3/op/lzm_restart_tomcat_status"
              type: 'GET'
              data:
                private_token: gon.api_token
                module_name: module_name
                build_branch_id: build_branch_id
                uuids: uuids
              context: that
              success: (data) ->
                if(data.status == 2)
                  $(this).removeAttr('disabled')
                  $(this).text('重启')
                  $(this).parent().parent().find('.deploystatus').text('deploy success')
                  $(this).parent().parent().find('.deploystatus').addClass('badge-success')

                  clearInterval(interval)
                else if(data.status == 0)
                  $(this).removeAttr('disabled')
                  $(this).text('重启')
                  $(this).parent().parent().find('.deploystatus').removeClass('badge-success')
                  $(this).parent().parent().find('.deploystatus').text('to deploy')
                  flash_notice(false, '重启 Tomcat 失败')

                  clearInterval(interval)
          5000)
      error: (res, status, xhr) ->
        $(this).removeAttr('disabled')
        $(this).text('重启')
        $(this).parent().parent().find('.deploystatus').removeClass('badge-success')
        $(this).parent().parent().find('.deploystatus').text('to deploy')
        flash_notice(false, '重启 Tomcat 失败')
        success = false

  $(".beta-paas-restart-tomcat").click ->
    beta_paas_rollout_id = $(this).closest("tr").attr("beta_paas_rollout_id")
    module_name = $(this).parent().attr('module-name')

    $(this).attr('disabled', 'disabled')
    $(this).text('重启中...')
    $(this).parent().parent().find('.deploystatus').removeClass('badge-success')
    $(this).parent().parent().find('.deploystatus').text('deploying')

    $.ajax
      url: "/api/v3/op/beta_paas_restart_tomcat"
      type: 'POST'
      data:
        private_token: gon.api_token
        module_name: module_name
        beta_paas_rollout_id: beta_paas_rollout_id
      context: this
      success: (data) ->
        operation_id = data.operation_id

        that = this

        interval = setInterval(
          () ->
            $.ajax
              url: "/api/v3/op/beta_paas_restart_tomcat_status"
              type: 'GET'
              data:
                private_token: gon.api_token
                beta_paas_rollout_id: beta_paas_rollout_id
                operation_id: operation_id
              context: that
              success: (data) ->
                if(data.status == 2)
                  $(this).removeAttr('disabled')
                  $(this).text('重启')
                  $(this).parent().parent().find('.deploystatus').text('deploy success')
                  $(this).parent().parent().find('.deploystatus').addClass('badge-success')

                  clearInterval(interval)
                else if(data.status == 0)
                  $(this).removeAttr('disabled')
                  $(this).text('重启')
                  $(this).parent().parent().find('.deploystatus').removeClass('badge-success')
                  $(this).parent().parent().find('.deploystatus').text('to deploy')
                  flash_notice(false, '重启 Tomcat 失败')

                  clearInterval(interval)
          5000)
      error: (res, status, xhr) ->
        $(this).removeAttr('disabled')
        $(this).text('重启')
        $(this).parent().parent().find('.deploystatus').removeClass('badge-success')
        $(this).parent().parent().find('.deploystatus').text('to deploy')
        flash_notice(false, '重启 Tomcat 失败')
        success = false

  $(".beta_logs").click ->
    url = $(".beta_logs").data("link")
    module_name = $(".beta_logs_modules_select").val()
    from_date = $("#beta_released_from").val()
    end_date = $("#beta_released_end").val()
    $.ajax
      type: "get"
      url: url
      data: {from_date:from_date,end_date:end_date,module_name:module_name}
      complete: (data) ->
        $(".beta_module_logs").html(data)

  $("body").on 'click', '.beta-paas-deploy', ->
    url = $(this).data("link")
    $(this).disable()
    $siblings = $(this).siblings(".btn")

    beta_paas_rollout_id = $(this).closest("tr").attr("beta_paas_rollout_id")
    project = $('#assign-machine').data('project')
    interval = null
    parent_span = $(this).closest("tr").find("span.deploystatus")

    getStatusFromPaas = (project, beta_paas_rollout_id) ->
      $.ajax
        type: 'GET'
        url: '/' + project + '/beta_paas_rollouts/get_status_from_paas'
        data: {
          beta_paas_rollout_id: beta_paas_rollout_id
        }
        success: (res, status, xhr) ->
          if res.indexOf("success") > 0
            clearInterval(interval)
            parent_span.attr('class','badge badge-success deploystatus')
            parent_span.text(res)
        error: (res, status, xhr) ->

    $.ajax(
      type: "PUT"
      url: url
      dataType: 'json'
      data: {}
      context: this
      beforeSend: ->
        $siblings.disable()
      success: (data) ->        
        $(this).closest('tr').find("span.deploystatus").attr('class','badge deploystatus')
        $(this).closest('tr').find("span.deploystatus").text('deploying')
        flash_notice(data['status'], data['notice'])
        $(this).closest('tr').find("span.deploystatus").append("&nbsp&nbsp <i class='icon-spinner icon-spin icon-large' style='color:grey'>")

        interval = setInterval(
          () ->
            getStatusFromPaas(project, beta_paas_rollout_id)
          5000)
      )

  $('body .content').popover
    selector:'.beta_paas_version'
    trigger:'hover'
    "background-color":'black'

  $("#auto_deploy").click ->
    $.blockUI(baseZ: 9999)
    $.ajax "/#{$("#assign-machine").data('project')}/virtual_machines/apply",
      type: 'GET'
      data:
        "machine[action_type]": 'apply'
        "machine[build_branch_id]": $("#build_branch_id").attr('value')
        "machine[module_name]": $("#module_name").attr('value')
        "machine[package_type]": $("#package_type").attr('value')
        "machine[project_id]": $("#project_id").val()
        "machine[deployer]": $(".machine_apply_type").val()
      success : (res, status, xhr) ->
        show_info_about_apply_or_bind_machine(res, status, xhr)
      error: (jqXHR, textStatus, errorThrown) ->
        $(".module-machine-list tr.editing").removeClass('editing')
        $.unblockUI()

  $("#bind_deploy").click ->
    $.blockUI(baseZ: 9999)
    data = $("#assign-machine").serializeArray()
    data.push({name: "machine[action_type]", value: 'bind'})
    data.push({name: "machine[package_type]", value: $("#package_type").attr('value')})
    data.push({name: "machine[project_id]", value: $("#project_id").val()})
    $.ajax "/#{$("#assign-machine").data('project')}/virtual_machines",
      type: 'POST'
      data: data
      context: this
      success : (res, status, xhr) ->
        show_info_about_apply_or_bind_machine(res, status, xhr)
      error: (jqXHR, textStatus, errorThrown) ->
        $.unblockUI()

  # $("#ci-build").click ->
  #   $.ajax "/api/v3/ci/#{$("#jenkins_job_name").text()}/build",
  #     type: 'GET'
  #     data:
  #       private_token: gon.api_token
  #     success : (res, status, xhr) ->
  #       $("#ci-build").attr("disabled", "disabled")
  #       $("#build_status").text("pending...")
  #       $("#build_status").removeClass("badge-important badge-warning badge-success")

  $("#ci-build").click ->
    $.ajax "/api/v3/ci/#{$("#jenkins_job_name").text()}/build",
      type: 'GET'
      data:
        private_token: gon.api_token
      success : (res, status, xhr) ->
        $("#ci-build").attr("disabled", "disabled")
        $("#build_status").text("running...")
        $("#build_status").removeClass("badge-important badge-warning badge-success")
        arr = ["success","failure","unstable"]
        if arr.toString().indexOf($("#build_status")[0].innerText) < 0
          $.ajax "/api/v3/ci/#{$("#jenkins_job_name").text()}/status",
            type: 'GET'
            data:
              private_token: gon.api_token
            success : (res, status, xhr) ->
               $(".bar").attr("style","width:" + "0%")
               $("#build_status")[0].innerText = "running"
               $("#build_status")[0].className = "badge"
              refresh_deploy_status($("#jenkins_job_name").text())


  $("#unbind-machine").click ->
    $.ajax "/#{$("#assign-machine").data('project')}/virtual_machines/unbind",
      type: 'PUT'
      data: {
        deployer: $("#deployer").val(),
        build_branch_id: $("#build_branch_id").val(),
        machine_ip: $.trim($("tr.editing").find('td.ip span.ip-addr').text())
      }
      success : (res, status, xhr) ->
        if res["status"]
          $("tr.editing .dn-button").attr('disabled',true)
          $("tr.editing .createModal").attr('disabled',true)
          $("tr.editing .editModal").attr('disabled',true)
        flash_notice(res['status'], res['flash'])
        $("#editModal").modal('hide')

  $("#add-slb").click ->
    $.ajax "/#{$("#assign-machine").data('project')}/virtual_machines/addslb",
      type: 'PUT'
      data: {
        build_branch_id: $("#build_branch_id").val(),
        machine_ip: $.trim($("tr.editing").find('td.ip span.ip-addr').text())
      }
      success : (res, status, xhr) ->
        flash_notice(res['status'], res['flash'])
        $("#slbModal").modal('hide')   

  $("#save-swimlane").click ->
    $.ajax "/#{$("#assign-machine").data('project')}/virtual_machines/saveswimlane",
      type: 'PUT'
      data: {
        swimlane: $("#swimlane").val(),
        machine_ip: $.trim($("tr.editing").find('td.ip span.ip-addr').text())
      }
      success : (res, status, xhr) ->
        flash_notice(res['status'], res['flash'])
        $("#swimlaneModal").modal('hide') 
        if res['status'] == true
          $("#swimlane-btn" + $.trim($("tr.editing").find('td.ip span.ip-addr').text()).replace(/\./g,'')).attr('data-original-title',"当前泳道：" + $("#swimlane").val())    

   $("#swimlane_query").click ->
    q = $("#swimlane").val()
    if q != ""
      window.open('http://code.dianpingoa.com/dashboard/swimlane?swimlane='+q,'swimlane')                   
  $(".content").on "change", "#paas_deploy_log", (event) ->
    beta_paas_id = $(this).closest("tr").attr("beta_paas_rollout_id")
    $.ajax "/#{$("#assign-machine").data('project')}/beta_paas_rollouts/log",
      type: 'get'
      data: {id:this.value,beta_paas_id:beta_paas_id}
      success : (res, status, xhr) ->
        $("#log-viewer .log-content").empty()
        $("#log-viewer .log-content").html(res)
        $("#log-viewer").modal("show")
    $(this).val("")
    $(this).trigger("chosen:updated")  


  $(".content").on "change", "#deploy_log", (event) ->
    return if this.value == ""
    beta_log = this.value
    this.setAttribute("beta_log",beta_log)
    if this.value.match(/^https?:\/\//)
      window.open(this.value)
    else if this.value.indexOf('jboss_log')>0
      url = this.value
      $.ajax(
        type: "GET"
        url: url
        data: {}
        success: (data) ->
          $("#jbossLogModal").html(data)
          $("#jbossLogModal").modal('show')
      )
    else
      $.get "/#{$("#assign-machine").data('project')}/ci_branch/#{$("#repository_ref").val()}/log",
        location: this.value
        module_name: this.value
        , (data) ->
          $("#log-viewer .log-content").empty()
          $("#log-viewer .log-content").html(data)
          $("#log-viewer").modal("show")
    $(this).val("")
    $(this).trigger("chosen:updated")

  $(".createBetaRollbackModal").click ->
    url = $(this).data("link")
    beta_paas_rollout_id = $(this).closest("tr").attr("beta_paas_rollout_id")
    $("body .content").attr("beta_paas_rollout_id",beta_paas_rollout_id)
    $("#to_select_the_group_to_rollback_chosen")[0].style.width = "80%"
    module_name = $(this).closest("tr").find("td .module-name").text()
    $(".controls.module_name").text(module_name)
    $("#createBetaRollbackModal").modal("show")
    $.ajax(
      type: "get"
      url: url
      data: {module_name: module_name}
      dataType: 'json'
      success: (data) ->
        $(".beta_tags_select_modal").empty()
        $(".beta_tags_select_modal").trigger("chosen:updated")
        i = 0
        while i < data.length
          $(".beta_tags_select_modal").append("<option value=#{data[i]['id']}>#{data[i]['value']}</option>")
          i = i+1
        $(".beta_tags_select_modal").trigger("chosen:updated")
      complete: ->
      )

  $(".rollback_beta_paas_package").click ->
    url = $(this).data("link")
    version = $(this).value
    beta_paas_rollout_id = $("body .content").attr("beta_paas_rollout_id")
    object = this
    paas_version = $("#to_select_the_group_to_rollback").val()
    $("body .content").removeAttr("beta_paas_rollout_id")
    $.ajax(
      type: "put"
      url: url
      data: {beta_paas_rollout_id:beta_paas_rollout_id,paas_version:paas_version}
      dataType: 'json'
      success: (data) ->
        $("#createBetaRollbackModal").modal("hide")
        flash_notice(true,'War has been send to rollback,please wait!')

        $(object).parent().parent().parent().find("tr.beta_paas_id_#{beta_paas_rollout_id}").find('span.deploystatus').attr('class','badge deploystatus')

        $(object).parent().parent().parent().find("tr.beta_paas_id_#{beta_paas_rollout_id}").find('span.deploystatus').text('deploying')

        $(object).parent().parent().parent().find("tr.beta_paas_id_#{beta_paas_rollout_id}").find(".beta-paas-deploy").attr("disabled",true)

        $(object).parent().parent().parent().find("tr.beta_paas_id_#{beta_paas_rollout_id}").find(".createBetaRollbackModal").attr("disabled",true)

      )

  $(".shadow-log").click ->
    $.get "/#{$("#assign-machine").data('project')}/ci_branch/#{$("#repository_ref").val()}/shadowlog",
      module_name: $(this).attr('module-name')
      , (data) ->
        $("#log-viewer .log-content").empty()
        $("#log-viewer .log-content").html(data)
        $("#log-viewer").modal("show")


  $(".dn-button").click ->
    $.ajax "/#{$("#assign-machine").data('project')}/virtual_machines/set_dn_for_ip",
      type: 'PUT'
      data: {
        build_branch_id: $("#build_branch_id").val()
        machine_ip: $.trim($(this).closest('tr').find('td.ip span.ip-addr').text())
        module_name: $.trim($(this).closest('tr').find('span.module-name').text())
      }
      success : (res, status, xhr) ->
        if res["status"]
          $(".module-machine-list tr.editing .domain-name .dn-button").attr('style','display:none')
          $(".module-machine-list tr.editing .domain-name").text(res["domain_name"])
          $(".module-machine-list tr.editing .domain-name").wrapInner('<a href="", style="_blank"/>')
          $(".module-machine-list tr.editing .domain-name a").attr('href', res["href"])
          $(".module-machine-list tr.editing .domain-ref a").text(res["domain_name"])
          $(".route-btn").addClass('btn')
        flash_notice(res['status'], res['flash'])

  $("#edit_domain_name").click ->
    $.ajax "/#{$("#assign-machine").data('project')}/virtual_machines/edit_dn_for_ip",
      type: 'PUT'
      data: {
        machine_ip: $("#ip_edit").attr('value')
        domain_name: $.trim($("#domain_name_edit").attr('value'))
      }
      success : (res, status, xhr) ->
        if res["status"]
          $(".module-machine-list tr.editing .editDomainName").attr('href','#deleteDomainName')
          $(".module-machine-list tr.editing .editDomainName").removeClass('editDomainName').addClass('deleteDomainName')
          $(".module-machine-list tr.editing .manage-domain-name").attr('domain_name',res["domain_name"])
          $(".module-machine-list tr.editing .domain-name").text(res["domain_name"])
          $(".module-machine-list tr.editing .domain-name").wrapInner('<a href="", style="_blank"/>')
          $(".module-machine-list tr.editing a").attr('href', res["href"])
          $(".module-machine-list tr.editing .domain-ref").text(res["domain_name"])
        flash_notice(res['status'], res['flash'])
        $("#editDomainName").modal('hide')

  $("#delete_domain_name").click ->
    $.ajax "/#{$("#assign-machine").data('project')}/virtual_machines/delete_dn_for_ip",
      type: 'PUT'
      data: {
        machine_ip: $("#ip_delete").attr('value')
        domain_name: $("#domain_name_delete").attr('value')
      }
      success : (res, status, xhr) ->
        if res["status"]
          $(".module-machine-list tr.editing .deleteDomainName").attr('href','#editDomainName')
          $(".module-machine-list tr.editing .manage-domain-name").attr('domain_name','')
          $(".module-machine-list tr.editing .deleteDomainName").removeClass('deleteDomainName').addClass('editDomainName')
          $(".module-machine-list tr.editing td.domain-name").text('尚未设置')
        flash_notice(res['status'], res['flash'])
        $("#deleteDomainName").modal('hide')

  $("#borrow_long_term").click ->
    $.ajax "/#{$("#assign-machine").data('project')}/virtual_machines/borrow_long_term",
      type: 'PUT'
      data: {
        machine_ip: $.trim($(".module-machine-list tr.editing").find('td.ip span.ip-addr').text())
      }
      context: this
      success : (res, status, xhr) ->
        if res["status"]
          $(".module-machine-list tr.editing .borrow-long-term").attr('style','display:none')
          $(".module-machine-list tr.editing .borrow-temporary").attr('style','display:yes')
          $(".module-machine-list tr.editing").find('td.machine-tenancy').text('长期')
        flash_notice(res['status'], res['flash'])
        $("#setLongTerm").modal('hide')

  $("#borrow_temporary").click ->
    $.ajax "/#{$("#assign-machine").data('project')}/virtual_machines/borrow_temporary",
      type: 'PUT'
      data: {
        machine_ip: $.trim($(".module-machine-list tr.editing").find('td.ip span.ip-addr').text())
      }
      context: this
      success : (res, status, xhr) ->
        if res["status"]
          $(".module-machine-list tr.editing .borrow-temporary").attr('style','display:none')
          $(".module-machine-list tr.editing .borrow-long-term").attr('style','display:yes')
          $(".module-machine-list tr.editing").find('td.machine-tenancy').text('临时')
        flash_notice(res['status'], res['flash'])
        $("#setTempTerm").modal('hide')

  $(".release-machine").click ->
    $('.module-machine-list tr').removeClass('editing')
    $(this).closest('tr').addClass('editing')
    $.ajax "/#{$("#assign-machine").data('project')}/virtual_machines/release",
      type: 'PUT'
      data: {
        machine_ip: $.trim($(".module-machine-list tr.editing").find('td.ip span.ip-addr').text())
      }
      context: this
      success : (res, status, xhr) ->
        flash_notice(res['status'], res['flash'])

  $("#deploy-switch").on 'switch-change', (e, data) ->
    $.ajax "/api/v3/ci/#{$("#jenkins_job_name").text()}/setdeploy",
      type: 'POST'
      data:
        private_token: gon.api_token
        auto_deploy: data.value

  $("#test-switch").on 'switch-change', (e, data) ->
    $.ajax "/api/v3/ci/#{$("#jenkins_job_name").text()}/settest",
      type: 'POST'
      data:
        private_token: gon.api_token
        run_test: data.value
      success: (res, status, xhr) ->
        flash_notice(res['status'], res['message'])  

  if ip_list?
    $("#ip").autocomplete
      source: ip_list
      minLength: 0
      select: (event, ui) ->
        ip = ui.item.value
        $.get "/#{$("#assign-machine").data('project')}/virtual_machines/machine_for_ip",
          ip: ip,
        , ((res) ->
          if res.status
            $("#port").val(res.machine.port)
            $("#username").val(res.machine.username)
            $("#password").val(res.machine.password)
          else
            flash_notice(res['status'], res['flash'])
        ), 'json'

  $("#app_default_domain").change ->
    url = $("#app_default_domain_td").data("link")
    $.ajax
      type: "POST"
      url: url
      data: {app_default_domain: document.getElementById("app_default_domain").value}
      success: (res, status, xhr) ->
        flash_notice(res['status'], res['flash'])

  $('.apply-machine-button').click ->
    td = $(this).parent()
    $(this).remove()
    loader_html = '<span class="loader"><i class="icon-refresh icon-spin"></i>&nbsp;<span>正在申请...</span></span>'
    id = td.closest('tr').attr('beta_paas_rollout_id')
    project = $('#assign-machine').data('project')
    deploy_button_html = '<button class="btn has_bottom_tooltip beta-paas-deploy"' +
                         'data-link= "/' +project + '/beta_paas_rollouts/' + id + '/deploy"' +
                         'data-original-title="" title="">部署</button>'
    td.append(loader_html)
    loader = td.find('span.loader').first()
    beta_paas_rollout_id = td.closest('tr').attr('beta_paas_rollout_id')
    module_name = $.trim(td.closest('tr').find('span.module-name').text())
    interval = null

    getBetaIpsFromPaas = (project, module_name) ->
      $.ajax
        type: 'GET'
        url: '/' + project + '/virtual_machines/get_ips_from_paas'
        data: {
          module_name: module_name
        }
        success: (res, status, xhr) ->
          if res.indexOf(".") > 0 
            clearInterval(interval)
            loader.html("<span>" + res + "(paas)" + "</span>")
            td.closest('tr').find('span.deploystatus').text('to deploy')
            td.next().append(deploy_button_html)
        error: (res, status, xhr) ->

    $.ajax
      type: 'GET'
      url: '/' + project + '/virtual_machines/apply_from_paas'
      data: {
        module_name: module_name,
        beta_paas_rollout_id: beta_paas_rollout_id
      }
      success: (res, status, xhr) ->
        interval = setInterval(
          () ->
            getBetaIpsFromPaas(project, module_name)
          5000)
      error: (res, status, xhr) ->
        $(this).text('申请失败, 联系开发')

    return(false)
