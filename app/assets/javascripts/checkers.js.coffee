# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/

$ ->
  $(".checker-switch").on 'switch-change', (e, data) ->    
    $.ajax "/api/v3/checkers/#{$(this).data('checkerid')}/setchecked",
      type: 'POST'
      data:
        private_token: gon.api_token
        checked: data.value
