$ ->
  $(".datepicker-start-from-today").datepicker
    minDate: 0
    dateFormat: "yy-mm-dd"
    showButtonPanel: true
    beforeShow: (input) ->
      setTimeout (->
        buttonPane = $(input).datepicker("widget").find(".ui-datepicker-buttonpane")
        $("<button>",
          text: "Clear"
          type: "button"
          class: "ui-state-default ui-priority-primary ui-corner-all"
          click: ->
            $.datepicker._clearDate input
        ).appendTo buttonPane
      ), 1

$ ->
  $(".datepicker").datepicker(dateFormat: "yy-mm-dd")

  $("#tasks-index-form #released_at, #tickets-index-form #released_at").change ->
    this.form.submit()

  $("#update-task-status-form .btn").click ->
    $("#update-task-status-form #to").val($(this).data('event'))
    $("#update-task-status-form .btn").disable()
    $("#update-task-status-form").submit()
  
  $(".btn_offline").click ->
    $(".offline").attr('disabled', false)
    return false

  $("#new_checker").click ->
    $('.create-style').attr('style','display:yes')
    $('#task-checker-form').attr('style','display:yes')
    $('.create-style select').val('1')
    $('hr.create-style').attr('style','display:yes;border-style:dotted')
    $("#checker_description").attr("value",'')
    $('#explanatoryText li').removeClass('descriptor')
    $('div.checker-assignee div.chosen-container').attr('style','width: 290px')
    $('div.checker-assignee select').val('').trigger("chosen:updated")
    $('#checker_save').removeClass('edit').addClass('new')

  $("div#checker-list-table").on 'click', '.editChecker', ->
    $('#task-checker-form').attr('style','display:yes')
    $('.create-style').attr('style','display:none')
    $('#explanatoryText li').removeClass('descriptor')
    $('#checker-list-table tr').removeClass('editing')
    $(this).closest('tr').addClass('editing')
    $('div.checker-assignee select').val($(this).parent().attr('assignee_id')).trigger("chosen:updated")
    $('div.checker-assignee div.chosen-container').attr('style','width: 290px')
    $('#checker_description').attr("value", $(this).parent().attr('description'))
    $('#checker_save').removeClass('new').addClass('edit')

  $(".create-style select").change ->
    if $('.create-style option').filter(':selected').val() != '1'
      $('#task-checker-form').attr('style','display:none')
      $('hr.create-style').attr('style','display:none')
      index = $(this).find('option:selected').index()
      $('#explanatoryText li.descriptor').removeClass('descriptor')
      $('#explanatoryText li').eq(index).addClass('descriptor')
    else
      $('#explanatoryText li').removeClass('descriptor')
      $('#task-checker-form').attr('style','display:yes')
      $('hr.create-style').attr('style','display:yes;border-style:dotted')

$ ->
  $("#save").click ->
    $form = $("#task-config-form")
    $.ajax
      url: $form.attr("action")
      type: 'POST'
      data: 
        $form.serializeArray()
      context: this
      success : (res, status, xhr) ->
        flash_notice(res['status'], res['flash'])
        $(".modal").modal('hide')
        $.unblockUI()
        $("#lion-config-table").load($("#task-config-form").attr("action") + " #lion-config-table")
      error: (jqXHR, textStatus, errorThrown) ->
        $.unblockUI()

$ ->
  $("div.modal-footer").on 'click', "#checker_save.new", ->
    $(".modal").modal('hide')
    $form = $("#task-checker-form")
    data = $form.serializeArray()
    data.push({name: "create_style", value: $('.create-style option').filter(':selected').val()})
    $.ajax
      url: $form.attr("action")
      type: 'POST'
      data: data
      context: this
      success : (res, status, xhr) ->
        flash_notice(res['status'], res['flash'])
        $("#checker-list-table").load($("#task_path").val() + " #checker-list-table")
      error: (jqXHR, textStatus, errorThrown) ->
      $.unblockUI()
      $('#task-checker-form').attr('style','display:yes')
      $('.create-style select').val('1')

$ ->
  $("div.modal-footer").on 'click', "#checker_save.edit", ->
    $(".modal").modal('hide')
    $.ajax
      url: $('#checker-list-table tr.editing').data('link')
      type: 'PUT'
      data:
        "checker[assignee_id]": $("#checker_assignee_id").attr("value")
        "checker[description]": $("#checker_description").attr("value")
        "task_id": $("#task_id").attr("value")
      success : (res, status, xhr) ->
        flash_notice(res['status'], res['flash'])
        $("#checker-list-table").load($("#task_path").val() + " #checker-list-table")
      error: (jqXHR, textStatus, errorThrown) ->
      $.unblockUI()
      $('.create-style').attr('style','display:yes')
      $('hr.create-style').attr('style','display:yes;border-style:dotted')

$ ->
  $(".container").on 'click', ".check-pass", ->
    $.ajax "/#{$("#checker-list-table").data('project')}/checkers/check_result",
      type: 'PUT'
      data: {
        id: $(this).closest('tr').data('checkerid')
        check_result: 1
      }
      context: this
      success : (res, status, xhr) ->
        if res["status"]
          $(this).closest('tr').find('.check-pass').attr('style','display:none')
          $(this).closest('tr').find('.check-fail').attr('style','display:yes')
          $(this).closest('tr').find('.check-result').removeClass("icon-remove").addClass("icon-ok")
        flash_notice(res['status'], res['flash'])

$ ->
  $(".container").on 'click', ".check-fail", ->
    $.ajax "/#{$("#checker-list-table").data('project')}/checkers/check_result",
      type: 'PUT'
      data: {
        id: $(this).closest('tr').data('checkerid')
        check_result: 0
      }
      context: this
      success : (res, status, xhr) ->
        if res["status"]
          $(this).closest('tr').find('.check-pass').attr('style','display:yes')
          $(this).closest('tr').find('.check-fail').attr('style','display:none')
          $(this).closest('tr').find('.check-result').removeClass("icon-ok").addClass("icon-remove")
        flash_notice(res['status'], res['flash'])
