show_info_about_apply_or_bind_machine = () ->
  $(".createRollbackModal").attr('href','#createRollbackModal')

$ ->
  $(".createRollbackModal").click ->
    $("#to_select_the_package_to_rollback_chosen")[0].style.width = "80%"
    $("#to_select_the_group_to_rollback_chosen")[0].style.width = "80%"
    $(".package_select").val("").trigger("chosen:updated")
    $(".group_select_modal").val("").trigger("chosen:updated")
    $(".swimlane_select_modal").val("").trigger("chosen:updated")
    $('.rollback-switch').prop('checked', true)
    $(".group-selection").attr('style','display:yes')
    $(".swimlane-selection").attr('style','display:none')
    $(".add_rollback_package").attr('style','display:none')

  $(".createImmediatelyRollbackModal").click ->
    $("#to_select_the_package_to_immediately_rollback_chosen_chosen")[0].style.width = "80%"
    $(".immediately_rollback_package_select").val("").trigger("chosen:updated")

  $(".add_whole_group_package").click ->
    url = $(this).data("link")
    module_name = $("#to_select_the_package_to_immediately_rollback_chosen").val()
    arr = $(".to-rollback-package-list-info").map ->
      this.getAttribute("data-group-id")
    arr_ing = $(".rollbacking-package-list-info").map ->
      this.getAttribute("data-group-id")
    arr_total = arr.get()+arr_ing.get()
    $.ajax(
      type: "PUT"
      url: url
      data: {module_name: module_name,arr_total:arr_total}
      dataType: 'html'
      success: (data) ->
        console.log(data["result"],data["message"])
        $(".to-be-rollback-packages-list").html(data)
        $(".package_tag_rollback").chosen()
        flash_notice(data["result"],data["message"])
        $("#createImmediatelyRollbackModal").modal("hide")
      complete: ->
      )

  update_selection = (selector, callback) ->
    url = $(selector)[0].getAttribute("url")
    arr_to_be_rollback = $(".to-rollback-package-list-info").map ->
      this.getAttribute("data-group-id")
    arr_rollbacking = $(".rollbacking-package-list-info").map ->
      this.getAttribute("data-group-id")
    arr = arr_to_be_rollback.get().concat(arr_rollbacking.get())
    $.get url, {action_id: this.value,exists_ids: arr}, ((data) ->
      $(selector).empty()
      $(selector).append("<option></option>")
      callback() if callback
      $.each data, (index, obj) ->
        $(selector).append("<option value='#{obj.id}'>#{obj.value}</option>")
      $(selector).trigger("chosen:updated")
    ), 'json'

  $("body .ticket_select").chosen().change ->
    update_selection.call this, ".package_select", ->
      $(".group_select_modal").empty()
      $(".package_select").append("<option></option>")
      $(".package_select").trigger("chosen:updated")

  $("body .package_select").chosen().change  ->
    update_selection.call(this, ".group_select_modal")
    update_selection.call(this, ".swimlane_select_modal")

  $(".content").on "switch-change", ".rollback-switch", (e, data) ->
    if $('.rollback-switch-box').is(':checked')
      $(".group-selection").attr('style','display:yes')
      $(".swimlane-selection").attr('style','display:none')
      $(".swimlane_select_modal").val("").trigger("chosen:updated")
    else
      $(".swimlane-selection").attr('style','display:yes')
      $(".group-selection").attr('style','display:none')
      $(".group_select_modal").val("").trigger("chosen:updated")

  $(".content").on "switch-change", ".rollback-lion-switch", (e, data) ->
    if $('.rollback-lion-switch-box').is(':checked')
      $('.lion_config').show()
    else
      $(".lion_config").hide()

  $(".content .group_select_modal").chosen().change ->
    $(".add_rollback_package").attr('style','display:yes')

  $(".content .swimlane_select_modal").chosen().change ->
    $(".add_rollback_package").attr('style','display:yes')
