$ ->
  vote = (logid, type, object) ->
    url = "beta_deploy_logs/#{logid}/vote"

    $.ajax
      url: url,
      type: 'PUT',
      data:
        type: type
      success: () ->
        if type == 'up'
          down = object.parent().find('.votedown').first()

          if down.attr('class').match(/unvote/)
            decrease(down)
        else
          up = object.parent().find('.voteup').first()

          if up.attr('class').match(/unvote/)
            decrease(up)

        increase(object)

  unvote = (logid, object) ->
    url = "beta_deploy_logs/#{logid}/unvote"

    $.ajax
      url: url,
      type: 'PUT',
      success: () ->
        decrease(object)

  increase = (object) ->
    object.find('i').first().animate({
      'font-size': '1.6em'
    }, 1000).animate({
      'font-size': '1.2em'
    })

    object.removeClass('vote')
    object.addClass('unvote')
    object.find('i').addClass('active')
    count = parseInt(object.find('.vote_count').text()) + 1
    object.find('.vote_count').text(count)

  decrease = (object) ->
    object.removeClass('unvote')
    object.addClass('vote')
    object.find('i').removeClass('active')
    count = parseInt(object.find('.vote_count').text()) - 1
    object.find('.vote_count').text(count)

  $('.vote').live 'click', ->
    classes = $(this).attr('class')
    logid = $(this).parent().parent().find('.id').text()

    if classes.match(/voteup/)
      vote(logid, 'up', $(this))
    else if classes.match(/votedown/)
      vote(logid, 'down', $(this))

  $('.unvote').live 'click', ->
    classes = $(this).attr('class')
    logid = $(this).parent().parent().find('.id').text()

    unvote(logid, $(this))

  $('#beta-deploy-log-search-input').on 'input propertychange', ->
    name = $(this).val()

    $('.logs-data').each ->
      project_name = $(this).find('.project-name').find('a').text()

      if project_name.match(new RegExp('.*' + name + '.*');)
        $(this).show()
      else
        $(this).hide()
