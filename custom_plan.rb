require 'zeus/rails'

class CustomPlan < Zeus::Rails

  # def my_custom_command
  #  # see https://github.com/burke/zeus/blob/master/docs/ruby/modifying.md
  # end
  def test
    require 'factory_girl'
    FactoryGirl.reload
    super
  end

  def spinach_environment
    require 'spinach'
  end

  def spinach
    spinach_main = Spinach::Cli.new(ARGV.dup)
    exit spinach_main.run
  end

end

Zeus.plan = CustomPlan.new
