Feature: Project Browse ci_branches
  Background:
    Given I sign in as a user
    And I own project "Shop"
    And Gitlab connect to jenkins "beta"
    And "Shop"-"master" branch link to jenkins job "beta-Shop"
    And jenkins job "beta-Shop" has modules "test-war" "test-jar"
    And I visit project "Shop" ci_branch "master" page

  Scenario: I should see modules list
    Then I should see module "test-war" with type "war"


