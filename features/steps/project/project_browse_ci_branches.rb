class ProjectBrowseCiBranches < Spinach::FeatureSteps
  include SharedAuthentication
  include SharedProject
  include SharedPaths

  And 'Gitlab connect to jenkins "beta"' do
    @ci_env = FactoryGirl.create(:ci_env, name: 'beta', url: 'beta.ci.dp')
  end

  And '"Shop"-"master" branch link to jenkins job "beta-Shop"' do
    @ci_branch = @project.ci_branchs.create(ci_env_id: @ci_env.id, 
      branch_name: "master", jenkins_job_name: "beta-Shop",
      gitlab_created: false, package_type: "war")
  end

  And 'jenkins job "beta-Shop" has modules "test-war" "test-jar"' do
    @mock_build_result = {"actions"=>[nil], 
      "description"=>"", "displayName"=>"beta-pengbo-test-001", "displayNameOrNull"=>nil, "name"=>"beta-pengbo-test-001", 
      "url"=>"http://beta.ci.dp/job/beta-pengbo-test-001/", "buildable"=>true, 
      "builds"=>[{"number"=>8, "url"=>"http://beta.ci.dp/job/beta-pengbo-test-001/8/", 
        "time"=>"2013-04-19_10-42-57", "result"=>"FAILURE", "timestamp"=>1366339377000}, 
        {"number"=>7, "url"=>"http://beta.ci.dp/job/beta-pengbo-test-001/7/", "time"=>"2013-04-19_10-42-52", "result"=>"FAILURE", "timestamp"=>1366339372000}, 
        {"number"=>4, "url"=>"http://beta.ci.dp/job/beta-pengbo-test-001/4/", "time"=>"2013-04-19_10-38-39", "result"=>"SUCCESS", "timestamp"=>1366339119000}], 
        "color"=>"red", 
        "firstBuild"=>{"number"=>4, "url"=>"http://beta.ci.dp/job/beta-pengbo-test-001/4/"}, 
        "healthReport"=>[{"description"=>"Build stability: 2 out of the last 4 builds failed.", "iconUrl"=>"health-40to59.png", "score"=>50}], 
        "inQueue"=>false, "keepDependencies"=>false, "lastBuild"=>{"number"=>8, "url"=>"http://beta.ci.dp/job/beta-pengbo-test-001/8/"}, 
        "lastCompletedBuild"=>{"number"=>8, "url"=>"http://beta.ci.dp/job/beta-pengbo-test-001/8/"}, 
        "lastFailedBuild"=>{"number"=>8, "url"=>"http://beta.ci.dp/job/beta-pengbo-test-001/8/"}, 
        "lastStableBuild"=>{"number"=>5, "url"=>"http://beta.ci.dp/job/beta-pengbo-test-001/5/"}, 
        "lastSuccessfulBuild"=>{"number"=>5, "url"=>"http://beta.ci.dp/job/beta-pengbo-test-001/5/"}, 
        "lastUnstableBuild"=>nil, "lastUnsuccessfulBuild"=>{"number"=>8, "url"=>"http://beta.ci.dp/job/beta-pengbo-test-001/8/"}, 
        "nextBuildNumber"=>9, "property"=>[], "queueItem"=>nil, "concurrentBuild"=>false, "downstreamProjects"=>[], "scm"=>{}, "upstreamProjects"=>[], 
        "modules"=>[{"name"=>"com.dianping:pay-cashier", "url"=>"http://beta.ci.dp/job/beta-pengbo-test-001/com.dianping$pay-cashier/", "color"=>"blue", "displayName"=>"pay-cashier", "status"=>"success"}, 
          {"name"=>"com.dianping:pay-cashier-biz", "url"=>"http://beta.ci.dp/job/beta-pengbo-test-001/com.dianping$pay-cashier-biz/", "color"=>"notbuilt", "displayName"=>"test-war", "status"=>"invalid"}, 
          {"name"=>"com.dianping:pay-unipay-web", "url"=>"http://beta.ci.dp/job/beta-pengbo-test-001/com.dianping$pay-unipay-web/", "color"=>"notbuilt", "displayName"=>"test-jar", "status"=>"invalid"}], 
        "status"=>"failure"}

    @mock_hash_pom  = {"pay-unipay"=>{:module_name=>"pay-unipay", :type=>"pom", :path=>"", :version=>"0.1.4-SNAPSHOT"}, 
      "test-war"=>{:module_name=>"test-war", :type=>"war", :path=>"/pay-unipay-web", :version=>"0.1.0"}, 
      "test-jar"=>{:module_name=>"test-jar", :type=>"", :path=>"/pay-unipay-dal", :version=>"0.1.0"}}


    @mock_remote_ci_job = RemoteCi::Job.new('beta', 'beta-Shop')
    @mock_remote_ci_job.stub(:build_result).and_return(@mock_build_result)
    CiBranchController.any_instance.stub(:ci_last_build).and_return(@mock_remote_ci_job)
    Tree.any_instance.stub(:hashed_pom).and_return(@mock_hash_pom)
    Project.any_instance.stub(:sonar).and_return(nil)
  end

  And 'I visit project "Shop" ci_branch "master" page' do
    visit project_ci_branch_path(project, "master") 
  end

  Given 'I should see module "test-war" with type "war"' do
    # debugger
    within '#module-machine-list' do
      page.should have_content 'test-war'
      page.should have_content 'war'
    end
  end

  Then 'I should see "Shop" protected branches list' do
    within "table" do
      page.should have_content "stable"
      page.should_not have_content "master"
    end
  end

  And 'project "Shop" has protected branches' do
    project = Project.find_by_name("Shop")
    project.protected_branches.create(:name => "stable")
  end
end
