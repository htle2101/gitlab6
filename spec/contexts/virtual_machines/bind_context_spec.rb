require 'spec_helper'

describe VirtualMachines::BindContext do
  describe '#execute' do
    let(:project_A) { create(:project) }
    let(:project_B) { create(:project) }
    let!(:machine_001) { create(:virtual_machine, project_id: project_A.id, ip: '10.0.0.1') }
    let!(:machine_002) { create(:virtual_machine, project_id: project_A.id, ip: '10.0.0.2', module_name: 'service') }
    let!(:machine_003) { create(:virtual_machine, project_id: project_A.id, ip: '10.0.0.3', status: 0) }
    let(:current_user) { create(:user, email: "current@email.com") }
    let(:alpha) { create(:alpha) }
    let(:ci_branch) { create(:ci_branch, project: project_A, ci_env: alpha) }
    let(:conn) { mock('connection').as_null_object}

    it "bind a new machine" do
      params = {
                :machine => {
                  :ip => '192.168.9.181',
                  :action_type => 'bind',
                  :package_type =>'war',
                  :build_branch_id => ci_branch.id,
                  :username =>'root',
                  :password =>'123',
                  :module_name=>'web',
                  :project_id => project_A.id.to_s,
                },         
               }
      Rye::Box.stub(new: conn)  
      VirtualMachines::BindContext.new(project_A.id, current_user, params).execute
      VirtualMachine.exists?( 
                              :ip => '192.168.9.181',
                              :build_branch_id => ci_branch.id,
                              :module_name => 'web',
                              :package_type => 'war',
                              :status => 1,
                              :action_type => "bind",
                             ).should be_true
    end

    it "can not bind the machine because it has been binded with other project" do
      params = {
                :machine => {
                  :ip => '10.0.0.1',
                  :action_type => 'bind',
                  :package_type =>'war',
                  :build_branch_id => ci_branch.id,
                  :username =>'root',
                  :password =>'123',
                  :module_name=>'web',
                  :project_id => project_B.id.to_s,
                },         
               }
      Rye::Box.stub(new: conn) 
      VirtualMachines::BindContext.new(project_B.id, current_user, params).execute

      VirtualMachine.exists?( 
                              :ip => '10.0.0.1',
                              :build_branch_id => ci_branch.id,
                              :module_name => 'web',
                              :package_type => 'war',
                              :status => 1,
                              :action_type => "bind",
                              :project_id => project_B.id,
                             ).should be_false
    end 

    it "can not bind the machine because it has been binded with other module" do
      params = {
                :machine => {
                  :ip => '10.0.0.2',
                  :action_type => 'bind',
                  :package_type =>'war',
                  :build_branch_id => ci_branch.id,
                  :username =>'root',
                  :password =>'123',
                  :module_name=>'web',
                  :project_id => project_A.id.to_s,
                },         
               }
      Rye::Box.stub(new: conn) 
      VirtualMachines::BindContext.new(project_B.id, current_user, params).execute

      VirtualMachine.exists?( 
                              :ip => '10.0.0.2',
                              :build_branch_id => ci_branch.id,
                              :module_name => 'web',
                              :package_type => 'war',
                              :status => 1,
                              :action_type => "bind",
                              :project_id => project_A.id,
                             ).should be_false
    end

    it "bind the machine that has been binded once" do
      params = {
                :machine => {
                  :ip => '10.0.0.3',
                  :action_type => 'bind',
                  :package_type =>'war',
                  :build_branch_id => ci_branch.id,
                  :username =>'root',
                  :password =>'123',
                  :module_name=>'web',
                  :project_id => project_A.id.to_s,
                },         
               }
      Rye::Box.stub(new: conn) 
      VirtualMachines::BindContext.new(project_A.id, current_user, params).execute

      VirtualMachine.exists?( 
                              :ip => '10.0.0.3',
                              :build_branch_id => ci_branch.id,
                              :module_name => 'web',
                              :package_type => 'war',
                              :status => 1,
                              :action_type => "apply",
                              :project_id => project_A.id,
                             ).should be_true
    end 

  end
end
