require 'spec_helper'

describe CiBranches::CreateContext do

  describe :create_ci_branch do

    let(:namespace){create(:namespace)}
    let(:project) { create(:project, namespace: namespace) }
    let(:user) { create(:user) }
    let(:ci_env_alpha) { create(:alpha) }
    let(:ci_env_beta) { create :ci_env, name: 'beta', is_single: true }
    let(:ci_env_product) { create :ci_env, name: 'product', is_single: true }
    let(:ci_template_alpha) { create :ci_template, template_name: 'alpha_template', ci_env: ci_env_alpha, package_type: 'war-maven3', ci_env_name: 'alpha' }
    let(:admin) {create(:admin, name: "Administrator")}
     
    context 'single create ' do
      before do
        @project = project
        @user = user
        @ci_branch_opt = {
          ci_env: ci_env_alpha.name,
          branch_name: "test_branch",
          root_type: "jar",
          package_type: "war-maven3"
        }
        @opts = {
          name: "GitLab",
          ci_branch: @ci_branch_opt
        }
        @ci_template_alpha = ci_template_alpha

        admin.stub(authentication_token: "abcxyz")
        User.stub(find_by_name: admin)
        CiBranch.any_instance.stub(:pom_exist?).and_return(true)
        CiEnv.any_instance.stub(:server).and_return(MockJenkinsApi::Client.new('alpha'))
        @ci_branch = create_ci_branch(project, user, @tree, @opts)
      end

      it { @ci_branch.should be_valid }
      it { @ci_branch.ci_env.name.should == 'alpha' }
      it { @ci_branch.branch_name.should == 'test_branch' }
      it { @ci_branch.package_type.should == 'war-maven3' }
      it { @ci_branch.gitlab_created.should be_true }
      it { @ci_branch.project.hooks.size.should == 2 }
    end

  end

  def create_ci_branch(project, user, tree, opts)
    CiBranches::CreateContext.new(project, user, opts).execute
  end
end
