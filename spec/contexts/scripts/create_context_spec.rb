#encoding: UTF-8

require 'spec_helper'

describe Scripts::CreateContext do

  describe :create do
    before do
      ScriptUploader.enable_processing = false
      Script.any_instance.stub(notify_dba_script_upload: true)
      @user = create :user
      @project = create :project
      @opts = {
        script: {
          name: "test.sql",
          todo: "上线前",
          db_name: "gitpub",
          desc: "foo"
        },
        file: Rack::Test::UploadedFile.new(File.open(File.expand_path("../../../data/files/test.sql", __FILE__)))
      }
    end

    context "totally new" do
      before do
        @script = build(:script, project: @project)
        @project.scripts.stub(build: @script)
      end

      it "should create a script" do
        @script.should_receive(:notify_dba_script_upload).once
        @script = Scripts::CreateContext.new(@project, @user, @opts).execute
        @script.should be_valid
        @script.author.should == @user
        @script.status.should == 0
      end
    end

  end
end
