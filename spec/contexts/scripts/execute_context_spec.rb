#encoding: UTF-8

require 'spec_helper'

describe Scripts::ExecuteContext do

  describe :create do
    before do
      ScriptUploader.enable_processing = false
      Script.any_instance.stub(notify_dba_script_execute: true)
      @user = create :user
      @project = create :project
    end

    context "execute" do
      it "should send ppe execute command successfully" do
        @script = create(:script, project: @project, status: 2)
        @project.scripts.stub(find: @script)
        @script.should_receive(:notify_dba_script_execute).once
        Scripts::ExecuteContext.new(@project, @user, id: @script.id, env: 0).execute.should be_true
        @script.ppe_status.should == 3
        @script.prd_status.should == 0
      end

      it "should send prd execute command successfully" do
        @script = create(:script, project: @project, status: 2, ppe_status: 5)
        @project.scripts.stub(find: @script)
        @script.should_receive(:notify_dba_script_execute).once
        Scripts::ExecuteContext.new(@project, @user, id: @script.id, env: 1).execute.should be_true
        @script.ppe_status.should == 5
        @script.prd_status.should == 3
      end

      it "should send execute command failed when no env parameter available" do
        @script = create(:script, project: @project, status: 2)
        @project.scripts.stub(find: @script)
        @script.should_not_receive(:notify_dba_script_execute)
        Scripts::ExecuteContext.new(@project, @user, id: @script.id).execute.should match(/mismatch/)
        @script.status.should == 2
        @script.ppe_status.should == 0
        @script.prd_status.should == 0
      end

      it "should send execute command failed when review status is incorrect" do
        @script = create(:script, project: @project, status: 1)
        @project.scripts.stub(find: @script)
        @script.should_not_receive(:notify_dba_script_execute)
        Scripts::ExecuteContext.new(@project, @user, id: @script.id, env: 0).execute.should match(/mismatch/)
        @script.status.should == 1
        @script.ppe_status.should == 0
        @script.prd_status.should == 0
      end

      it "should send execute command failed when prd execute before ppe execute" do
        @script = create(:script, project: @project, status: 2)
        @project.scripts.stub(find: @script)
        @script.should_not_receive(:notify_dba_script_execute)
        Scripts::ExecuteContext.new(@project, @user, id: @script.id, env: 1).execute.should match(/mismatch/)
        @script.status.should == 2
        @script.ppe_status.should == 0
        @script.prd_status.should == 0
      end

      it "should send prd execute command failed when ppe execute failed" do
        @script = create(:script, project: @project, status: 2, ppe_status: 4)
        @project.scripts.stub(find: @script)
        @script.should_not_receive(:notify_dba_script_execute)
        Scripts::ExecuteContext.new(@project, @user, id: @script.id, env: 1).execute.should match(/mismatch/)
        @script.ppe_status.should == 4
        @script.prd_status.should == 0
      end
    end

  end
end
