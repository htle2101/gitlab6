#encoding: UTF-8

require 'spec_helper'

describe Scripts::UpdateContext do

  describe :create do
    before do
      ScriptUploader.enable_processing = false
      Script.any_instance.stub(notify_dba_script_execute: true)
      @user = create :user
      @project = create :project
    end

    context "close" do
      it "should send close command successfully" do
        @script = create(:script, project: @project, status: 0)
        @project.scripts.stub(find: @script)
        @script.should_receive(:notify_dba_script_close).once
        Scripts::CloseContext.new(@project, @user, id: @script.id).execute.should be_true
        @script.status.should == 100
      end

      it "should send close command failed" do
        @script = create(:script, project: @project, status: 1)
        @project.scripts.stub(find: @script)
        @script.should_not_receive(:notify_dba_script_close)
        Scripts::CloseContext.new(@project, @user, id: @script.id).execute.should be_false
        @script.status.should == 1
      end
    end

  end
end
