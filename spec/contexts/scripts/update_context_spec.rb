#encoding: UTF-8

require 'spec_helper'

describe Scripts::UpdateContext do

  describe :create do
    before do
      ScriptUploader.enable_processing = false
      Script.any_instance.stub(notify_dba_script_reupload: true)
      @user = create :user
      @project = create :project
      @opts = {
        script: {
          name: "test1.sql",
          todo: "上线前",
          db_name: "gitpub",
          desc: "foo"
        },
        file: Rack::Test::UploadedFile.new(File.open(File.expand_path("../../../data/files/test.sql", __FILE__)))
      }
    end

    context "update" do
      it "should update a script successfully" do
        @script = create(:script, project: @project, status: 1)
        @project.scripts.stub(find: @script)
        @script.should_receive(:notify_dba_script_reupload).once
        @script = Scripts::UpdateContext.new(@project, @user, @opts.merge(id: @script.id)).execute
        @script.should be_valid
        @script.name.should == "test1.sql"
        @script.status.should == 0
      end

      it "should update a script failed" do
        @script = create(:script, project: @project, status: 2)
        @project.scripts.stub(find: @script)
        @script.should_not_receive(:notify_dba_script_reupload)
        @script = Scripts::UpdateContext.new(@project, @user, @opts.merge(id: @script.id)).execute
        @script.name.should == "test.sql"
        @script.status.should == 2
      end
    end

  end
end
