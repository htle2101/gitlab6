require 'spec_helper'

describe Tasks::CreateContext do

  describe :create do
    before do
      @user = create :user
      @assignee = create :user
      @project1 = create :project
      @project2 = create :project
      @opts = {
        task: {
          subject: "some subject",
          assignee_id: @assignee.id,
          released_at: "2013-10-19"
        },
        modules: ["#{@project1.id}|foo-war", "#{@project2.id}|bar-war"]
      }
    end

    context "totally new" do
      before do
        @task = Tasks::CreateContext.new(@project1, @user, @opts).execute
      end

      it { @task.should be_valid }
      it { @task.creator.should == @user }
      it { @task.assignee.should == @assignee }
      it { @task.packages.count.should == 2 }
      it { Ticket.where(released_at: @task.released_at).count.should == 2 }
    end

    context "tickets and packages should be merged when the ticket with same release date exist" do
      before do
        @exist_task = Tasks::CreateContext.new(@project1, @user, @opts).execute
        @opts[:modules] << "#{@project2.id}|baz-war"
        @new_task = Tasks::CreateContext.new(@project2, @user, @opts).execute
      end

      it { @new_task.should be_valid }
      it { @new_task.creator.should == @user }
      it { @new_task.assignee.should == @assignee }
      it { @new_task.packages.count.should == 3 }
      it { Package.count.should == 3 }
      it { Ticket.where(released_at: @new_task.released_at).count.should == 2 }
    end

    context "new tickets and packages should be created when the released date doesn't exist" do
      before do
        @exist_task = Tasks::CreateContext.new(@project1, @user, @opts).execute
        @opts[:task][:released_at] = "2013-10-18"
        @new_task = Tasks::CreateContext.new(@project2, @user, @opts).execute
      end

      it { @new_task.should be_valid }
      it { @new_task.creator.should == @user }
      it { @new_task.assignee.should == @assignee }
      it { @new_task.packages.count.should == 2 }
      it { Package.count.should == 4 }
      it { Ticket.where(released_at: @new_task.released_at).count.should == 2 }
      it { Ticket.count.should == 4 }
    end

    context "only packages should be created when no release date specified." do
      before do
        @opts[:task][:released_at] = nil
        @task = Tasks::CreateContext.new(@project1, @user, @opts).execute
      end

      it { @task.should be_valid }
      it { @task.creator.should == @user }
      it { @task.assignee.should == @assignee }
      it { @task.packages.count.should == 2 }
      it { Ticket.count.should == 0 }
    end

    context "package which without associated to ticket should not be merge when created without release date." do
      before do
        @opts[:task][:released_at] = nil
        @exist_task = Tasks::CreateContext.new(@project1, @user, @opts).execute
        @new_task = Tasks::CreateContext.new(@project2, @user, @opts).execute
      end

      it { @new_task.should be_valid }
      it { @new_task.creator.should == @user }
      it { @new_task.assignee.should == @assignee }
      it { @new_task.packages.count.should == 2 }
      it { Package.count.should == 4 }
      it { Ticket.count.should == 0 }
    end
  end
end
