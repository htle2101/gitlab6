require 'spec_helper'

describe Tasks::UpdateContext do

  describe :update do
    before do
      @user = create :user
      @assignee = create :user
      @project1 = create :project
      @project2 = create :project
      @opts1 = {
        task: {
          subject: "some subject",
          assignee_id: @assignee.id,
          released_at: "2013-10-19"
        },
        modules: ["#{@project1.id}|foo-war", "#{@project2.id}|bar-war"]
      }

      @opts2 = {
        task: {
          subject: "some subject",
          assignee_id: @assignee.id,
          released_at: "2013-11-19"
        },
        modules: ["#{@project1.id}|foo-war", "#{@project2.id}|bar-war"]
      }

      @update_opts = {
        task: {
          subject: "some subject",
          assignee_id: @assignee.id,
          released_at: "2013-10-19"
        },
        modules: ["#{@project1.id}|foo-war", "#{@project2.id}|bar-war"]
      }

      @task1 = Tasks::CreateContext.new(@project1, @user, @opts1).execute
      @task2 = Tasks::CreateContext.new(@project1, @user, @opts2).execute
    end

    context "just change released date which already exist with other ticket" do
      before do
        opts = @update_opts.deep_dup.merge(id: @task1.id)
        opts[:task][:released_at] = "2013-11-19"
        @task = Tasks::UpdateContext.new(@project1, @user, opts).execute
      end

      it { @task.should be_valid }
      it { @task.creator.should == @user }
      it { @task.assignee.should == @assignee }
      it { @task.packages.count.should == 2 }
      it { Ticket.where(released_at: @task.released_at).count.should == 2 }
      it { Ticket.count.should == 2 }
      it { Package.count.should == 2 }
    end

    context "add and remove a package" do
      before do
        opts = @update_opts.deep_dup.merge(id: @task1.id)
        opts[:modules] = ["#{@project1.id}|fom-war", "#{@project2.id}|bar-war"]
        @task = Tasks::UpdateContext.new(@project1, @user, opts).execute
      end

      it { @task.should be_valid }
      it { @task.creator.should == @user }
      it { @task.assignee.should == @assignee }
      it { @task.packages.count.should == 2 }
      it { Ticket.where(released_at: @task.released_at).count.should == 2 }
      it { Ticket.count.should == 4 }
      it { Package.count.should == 4 }
    end

    context "just clear released date which already exist with other ticket" do
      before do
        opts = @update_opts.deep_dup.merge(id: @task1.id)
        opts[:task][:released_at] = nil
        @task = Tasks::UpdateContext.new(@project1, @user, opts).execute
      end

      it { @task.should be_valid }
      it { @task.creator.should == @user }
      it { @task.assignee.should == @assignee }
      it { @task.packages.count.should == 2 }
      it { Ticket.count.should == 2 }
      it { Package.count.should == 4 }
    end
  end
end
