require 'spec_helper'

describe Tasks::DestroyContext do

  describe :destroy do
    before do
      @user = create :user
      @assignee = create :user
      @project1 = create :project
      @project2 = create :project
      @opts1 = {
        task: {
          subject: "some subject",
          assignee_id: @assignee.id,
          released_at: "2013-10-19"
        },
        modules: ["#{@project1.id}|foo-war", "#{@project2.id}|bar-war"]
      }

      @opts2 = {
        task: {
          subject: "some subject",
          assignee_id: @assignee.id,
          released_at: "2013-11-19"
        },
        modules: ["#{@project1.id}|foo-war", "#{@project2.id}|bar-war"]
      }

      @opts3 = {
        task: {
          subject: "some subject",
          assignee_id: @assignee.id,
          released_at: "2013-11-19"
        },
        modules: ["#{@project1.id}|foo-war", "#{@project2.id}|baz-war"]
      }

      @task1 = Tasks::CreateContext.new(@project1, @user, @opts1).execute
      @task2 = Tasks::CreateContext.new(@project1, @user, @opts2).execute
      @task3 = Tasks::CreateContext.new(@project1, @user, @opts3).execute
    end

    context "destroy a task which not connect any other task by ticket" do
      before do
        @task = Tasks::DestroyContext.new(@project1, @user, {id: @task1.id}).execute
      end

      it { @task.should be_destroyed }
      it { Ticket.where(released_at: @task.released_at).count.should == 0 }
      it { Ticket.count.should == 2 }
      it { Package.count.should == 3 }
    end

    context "destroy a task which connect to other task by ticket" do
      before do
        @task = Tasks::DestroyContext.new(@project1, @user, {id: @task2.id}).execute
      end

      it { @task.should be_destroyed }
      it { Ticket.where(released_at: @task.released_at).count.should == 2 }
      it { Ticket.count.should == 4 }
      it { Package.count.should == 4 }
    end
  end
end
