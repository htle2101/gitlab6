require 'spec_helper'

describe GroupingStrategies::BranchCheckContext do
  before do
    @project = create :project_with_code
    @rollout_branch = create :rollout_branch, project_id: @project.id

    @packing = create :packing, rollout_branch: @rollout_branch, tag: :v1
    @success_packing = create :packing, rollout_branch: @rollout_branch, tag: :v2

    @grouping_strategy = create :grouping_strategy, project_id: @project.id, tag: :v1, appname: 'dianping'
    @success_grouping_strategy = create :grouping_strategy, project: @project, status: 7, appname: 'dianping', tag: :v2

    @context = GroupingStrategies::BranchCheckContext.new(@project, @grouping_strategy)
  end

  it 'should execute return a error' do
    Repository.any_instance.stub(commits_between: true) 

    @context.execute.should be_instance_of(GroupingStrategies::BranchCheckContext::Error)
  end

  it 'should execute return nil' do
    Repository.any_instance.stub(commits_between: false) 

    @context.execute.should be_nil
  end

  it 'should get_current_branch_and_commit_and_tag' do
    @context.send(:get_current_branch_and_commit_and_tag).should ==
      [@packing.branch_name, @packing.commit, @packing.tag.to_s]
  end

  it 'should get_last_deployed_branch_and_commit_and_tag' do
    @context.send(:get_last_deployed_branch_and_commit_and_tag).should ==
      [@success_packing.branch_name, @success_packing.commit, @success_packing.tag.to_s]
  end

  it 'should find_packing_by_tag' do
    @context.send(:find_packing_by_tag, :v1).should == @packing
  end

  it 'should get_last_success_grouping_strategy' do
    @context.send(:get_last_success_grouping_strategy).should == @success_grouping_strategy
  end
end

