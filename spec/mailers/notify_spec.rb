require 'spec_helper'

describe Notify do
  include EmailSpec::Helpers
  include EmailSpec::Matchers

  let(:recipient) { create(:user, email: 'recipient@example.com') }
  let(:project) { create(:project_with_code) }

  shared_examples 'a multiple recipients email' do
    it 'is sent to the given recipient' do
      should deliver_to recipient.email
    end
  end

  describe 'for new users, the email' do
    let(:example_site_path) { root_path }
    let(:new_user) { create(:user, email: 'newguy@example.com', created_by_id: 1) }

    subject { Notify.new_user_email(new_user.id, new_user.password) }

    it 'is sent to the new user' do
      should deliver_to new_user.email
    end

    it 'has the correct subject' do
      should have_subject /^gitlab \| Account was created for you$/i
    end

    it 'contains the new user\'s login name' do
      should have_body_text /#{new_user.email}/
    end

    it 'contains the new user\'s password' do
      should have_body_text /password/
    end

    it 'includes a link to the site' do
      should have_body_text /#{example_site_path}/
    end
  end


  describe 'for users that signed up, the email' do
    let(:example_site_path) { root_path }
    let(:new_user) { create(:user, email: 'newguy@example.com', password: "securePassword") }

    subject { Notify.new_user_email(new_user.id, new_user.password) }

    it 'is sent to the new user' do
      should deliver_to new_user.email
    end

    it 'has the correct subject' do
      should have_subject /^gitlab \| Account was created for you$/i
    end

    it 'contains the new user\'s login name' do
      should have_body_text /#{new_user.email}/
    end

    it 'should not contain the new user\'s password' do
      should_not have_body_text /password/
    end

    it 'includes a link to the site' do
      should have_body_text /#{example_site_path}/
    end
  end

  describe 'user added ssh key' do
    let(:key) { create(:personal_key) }

    subject { Notify.new_ssh_key_email(key.id) }

    it 'is sent to the new user' do
      should deliver_to key.user.email
    end

    it 'has the correct subject' do
      should have_subject /^gitlab \| SSH key was added to your account$/i
    end

    it 'contains the new ssh key title' do
      should have_body_text /#{key.title}/
    end

    it 'includes a link to ssh keys page' do
      should have_body_text /#{profile_keys_path}/
    end
  end

  context 'for a project' do
    describe 'items that are assignable, the email' do
      let(:assignee) { create(:user, email: 'assignee@example.com') }
      let(:previous_assignee) { create(:user, name: 'Previous Assignee') }

      shared_examples 'an assignee email' do
        it 'is sent to the assignee' do
          should deliver_to assignee.email
        end
      end

      context 'for issues' do
        let(:issue) { create(:issue, assignee: assignee, project: project ) }

        describe 'that are new' do
          subject { Notify.new_issue_email(issue.assignee_id, issue.id) }

          it_behaves_like 'an assignee email'

          it 'has the correct subject' do
            should have_subject /#{project.name} \| new issue ##{issue.id} \| #{issue.title}/
          end

          it 'contains a link to the new issue' do
            should have_body_text /#{project_issue_path project, issue}/
          end
        end

        describe 'that have been reassigned' do
          before(:each) { issue.stub(:assignee_id_was).and_return(previous_assignee.id) }

          subject { Notify.reassigned_issue_email(recipient.id, issue.id, previous_assignee.id) }

          it_behaves_like 'a multiple recipients email'

          it 'has the correct subject' do
            should have_subject /changed issue ##{issue.id} \| #{issue.title}/
          end

          it 'contains the name of the previous assignee' do
            should have_body_text /#{previous_assignee.name}/
          end

          it 'contains the name of the new assignee' do
            should have_body_text /#{assignee.name}/
          end

          it 'contains a link to the issue' do
            should have_body_text /#{project_issue_path project, issue}/
          end
        end

        describe 'status changed' do
          let(:current_user) { create(:user, email: "current@email.com") }
          let(:status) { 'closed' }
          subject { Notify.issue_status_changed_email(recipient.id, issue.id, status, current_user) }

          it 'has the correct subject' do
            should have_subject /changed issue ##{issue.id} \| #{issue.title}/i
          end

          it 'contains the new status' do
            should have_body_text /#{status}/i
          end

          it 'contains the user name' do
            should have_body_text /#{current_user.name}/i
          end

          it 'contains a link to the issue' do
            should have_body_text /#{project_issue_path project, issue}/
          end
        end

      end

      context 'for merge requests' do
        let(:merge_request) { create(:merge_request, assignee: assignee, source_project: project, target_project: project) }

        describe 'that are new' do
          subject { Notify.new_merge_request_email(merge_request.assignee_id, merge_request.id) }

          it_behaves_like 'an assignee email'

          it 'has the correct subject' do
            should have_subject /new merge request !#{merge_request.id}/
          end

          it 'contains a link to the new merge request' do
            should have_body_text /#{project_merge_request_path(project, merge_request)}/
          end

          it 'contains the source branch for the merge request' do
            should have_body_text /#{merge_request.source_branch}/
          end

          it 'contains the target branch for the merge request' do
            should have_body_text /#{merge_request.target_branch}/
          end
        end

        describe 'that are reassigned' do
          before(:each) { merge_request.stub(:assignee_id_was).and_return(previous_assignee.id) }

          subject { Notify.reassigned_merge_request_email(recipient.id, merge_request.id, previous_assignee.id) }

          it_behaves_like 'a multiple recipients email'

          it 'has the correct subject' do
            should have_subject /changed merge request !#{merge_request.id}/
          end

          it 'contains the name of the previous assignee' do
            should have_body_text /#{previous_assignee.name}/
          end

          it 'contains the name of the new assignee' do
            should have_body_text /#{assignee.name}/
          end

          it 'contains a link to the merge request' do
            should have_body_text /#{project_merge_request_path project, merge_request}/
          end

        end
      end
    end

    describe 'project was moved' do
      let(:project) { create(:project) }
      let(:user) { create(:user) }
      subject { Notify.project_was_moved_email(project.id, user.id) }

      it 'has the correct subject' do
        should have_subject /project was moved/
      end

      it 'contains name of project' do
        should have_body_text /#{project.name_with_namespace}/
      end

      it 'contains new user role' do
        should have_body_text /#{project.ssh_url_to_repo}/
      end
    end

    describe 'project access changed' do
      let(:project) { create(:project) }
      let(:user) { create(:user) }
      let(:users_project) { create(:users_project,
                                   project: project,
                                   user: user) }
      subject { Notify.project_access_granted_email(users_project.id) }
      it 'has the correct subject' do
        should have_subject /access to project was granted/
      end
      it 'contains name of project' do
        should have_body_text /#{project.name}/
      end
      it 'contains new user role' do
        should have_body_text /#{users_project.project_access_human}/
      end
    end

    context 'items that are noteable, the email for a note' do
      let(:note_author) { create(:user, name: 'author_name') }
      let(:note) { create(:note, project: project, author: note_author) }

      before :each do
        Note.stub(:find).with(note.id).and_return(note)
      end

      shared_examples 'a note email' do
        it 'is sent to the given recipient' do
          should deliver_to recipient.email
        end

        it 'contains the name of the note\'s author' do
          should have_body_text /#{note_author.name}/
        end

        it 'contains the message from the note' do
          should have_body_text /#{note.note}/
        end
      end

      describe 'on a project wall' do
        let(:note_on_the_wall_path) { project_wall_path(project, anchor: "note_#{note.id}") }

        subject { Notify.note_wall_email(recipient.id, note.id) }

        it_behaves_like 'a note email'

        it 'has the correct subject' do
          should have_subject /#{project.name}/
        end

        it 'contains a link to the wall note' do
          should have_body_text /#{note_on_the_wall_path}/
        end
      end

      describe 'on a commit' do
        let(:commit) { project.repository.commit }

        before(:each) { note.stub(:noteable).and_return(commit) }

        subject { Notify.note_commit_email(recipient.id, note.id) }

        it_behaves_like 'a note email'

        it 'has the correct subject' do
          should have_subject /note for commit #{commit.short_id}/
        end

        it 'contains a link to the commit' do
          should have_body_text commit.short_id
        end
      end

      describe 'on a merge request' do
        let(:merge_request) { create(:merge_request, source_project: project, target_project: project) }
        let(:note_on_merge_request_path) { project_merge_request_path(project, merge_request, anchor: "note_#{note.id}") }
        before(:each) { note.stub(:noteable).and_return(merge_request) }

        subject { Notify.note_merge_request_email(recipient.id, note.id) }

        it_behaves_like 'a note email'

        it 'has the correct subject' do
          should have_subject /note for merge request !#{merge_request.id}/
        end

        it 'contains a link to the merge request note' do
          should have_body_text /#{note_on_merge_request_path}/
        end
      end

      describe 'on an issue' do
        let(:issue) { create(:issue, project: project) }
        let(:note_on_issue_path) { project_issue_path(project, issue, anchor: "note_#{note.id}") }
        before(:each) { note.stub(:noteable).and_return(issue) }

        subject { Notify.note_issue_email(recipient.id, note.id) }

        it_behaves_like 'a note email'

        it 'has the correct subject' do
          should have_subject /note for issue ##{issue.id}/
        end

        it 'contains a link to the issue note' do
          should have_body_text /#{note_on_issue_path}/
        end
      end
    end

    describe 'machine assign' do
      let(:project) { create(:project) }
      let(:user) { create(:user) }
      let(:users_project) { create(:users_project, project: project, user: user) }
      let(:alpha){ create(:alpha) }
      let(:ci_branch) { create(:ci_branch, project: project, jenkins_job_name: "foo", branch_name: "bar", ci_env: alpha) }
      let(:virtual_machine) { create(:virtual_machine, ip: "192.168.2.10", ci_branch: ci_branch, module_name: "mod") }
      subject { Notify.machine_assign(virtual_machine.id, "fake_account", "fake_password") }
      it 'has the correct subject' do
        should have_subject /Machine assigned/
      end
      it 'contains ip of machine' do
        should have_body_text /#{virtual_machine.ip}/
      end
      it 'contains account of sudo user' do
        should have_body_text /fake_account/
      end
      it 'contains password of sudo user' do
        should have_body_text /fake_password/
      end
    end

    describe 'war deployed' do
      let(:project) { create(:project) }
      let(:user) { create(:user) }
      let(:users_project) { create(:users_project, project: project, user: user) }
      let(:alpha) { create(:alpha) }
      let(:ci_branch) { create(:ci_branch, project: project, ci_env: alpha, jenkins_job_name: "alpha-foo-master") }
      let!(:machine) {create(:virtual_machine, status: 1, module_name: 'foo-web', ci_branch: ci_branch, package_type: 'war', project_id: project.id)}
      let(:war) { Deployers::War.new(ci_branch: ci_branch, module_name: 'foo-web', path: '/foo/foo-web', type: 'war', version: '0.1.0') }
      let(:workspace_path) { "/foo/workspace" }
      let(:ts) { "20130101101010" }
      let(:deployed_wars) { [{war: war, status: true}] }
      before do
        Deployers::War.any_instance.stub(ts: ts)
        ci_branch.stub(workspace_path: workspace_path)
      end

      subject { Notify.war_deployed(ci_branch.id, deployed_wars) }

      it 'has the correct subject' do
        should have_subject /War deployed for ci `alpha-foo-master`/
      end
      it 'contains deploy name of the war' do
        should have_body_text /#{war.deploy_name}/
      end
      it 'contains ip of the machine which the war deployed' do
        should have_body_text /#{machine.ip}/
      end
      it 'contains release path of the war' do
        should have_body_text /#{war.release_path}/
      end
      it 'contains deploy status of the war' do
        should have_body_text /success/
      end

    describe 'jar deployed' do
      let(:project) { create(:project) }      
      let(:ci_branch) { create(:ci_branch, project: project, branch_name: "branch_test", ci_env: alpha, jenkins_job_name: "alpha-foo-master") }      
      before do
        @email = "changkang.cai@dianping.com"
        @module_name = "module_test"
        @status = "success"        
      end

      subject { Notify.jar_deployed(ci_branch.id, @email, ci_branch.jenkins_job_name,@module_name, @status, 'test err')}

      it 'has the correct subject' do
        should have_subject /GitLab | jar deployed for ci_branch.jenkins_job_name | module_name is @module_name/
      end
      it 'contains project name of the jar' do
        should have_body_text /#{project.name}/
      end
      it 'contains jenkins_job_name of the jar' do
        should have_body_text /#{ci_branch.jenkins_job_name}/
      end
      it 'contains branch_name of the jar' do
        should have_body_text /#{ci_branch.branch_name}/
      end  
      it 'contains deploy status of the jar' do
        should have_body_text /success/
      end
    end
    end
  end
end
