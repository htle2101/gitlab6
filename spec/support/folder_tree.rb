class FolderTree
  def initialize(folder)
    @folder = folder
  end

  def name
    @folder[0]
  end

  def data
    @folder[1]
  end

  def / (path)
    @folder[1].each do |f|
      if f[0] == path
        return FolderTree.new(f)
      end
    end
    nil
  end
end