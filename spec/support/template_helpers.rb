require 'erb'
require 'ostruct'

module TemplateHelpers

  def template(from, params = {})
    erb = File.read(File.expand_path("../../data/#{from}", __FILE__))
    namespace = OpenStruct.new(params)
    ERB.new(erb).result(namespace.instance_eval { binding })
  end
end
