module JenkinsApi
  class Client
    @@retry = nil  

    def initialize(*args)
      @args = *args
    end 

    def retry
      if @@retry
        @@retry = nil
        return 'success' 
      else
        @@retry = true
        raise 'retry'
      end    
    end 
  end
  class Client
    class Job
      # instance_methods.each do |m|
      #   undef_method m unless m.to_s = ~/method_missing|respond_to?|^/
      # end

      def get_config(name='')
        'xml'
      end
      
      def create(jobname, xml)
        true
      end 

      def exist?(jobname)
        false
      end 
    end 
  end
end 


module MockJenkinsApi
  class Client
    @@retry = nil  

    def initialize(*args)
      @args = *args
    end 

    def retry
      if @@retry
        @@retry = nil
        return 'success' 
      else
        @@retry = true
        raise 'retry'
      end    
    end 

    def job
      @job ||= MockJenkinsApi::Client::Job.new(self)
    end 

    class Job
      # instance_methods.each do |m|
      #   undef_method m unless m.to_s = ~/method_missing|respond_to?|^/
      # end

      def initialize(client)
        @client = client
      end 

      def get_config(jobname)
        %{<?xml version='1.0' encoding='UTF-8'?>
          <maven2-moduleset>
            <actions/>
            <description></description>
            <logRotator>
              <daysToKeep>-1</daysToKeep>
              <numToKeep>5</numToKeep>
              <artifactDaysToKeep>-1</artifactDaysToKeep>
              <artifactNumToKeep>-1</artifactNumToKeep>
            </logRotator>
            <keepDependencies>false</keepDependencies>
            <properties/>
            <scm class="hudson.plugins.git.GitSCM">
              <configVersion>2</configVersion>
              <userRemoteConfigs>
                <hudson.plugins.git.UserRemoteConfig>
                  <name></name>
                  <refspec></refspec>
                  <url>git@code.dianpingoa.com:paycenter/pay-unipay.git</url>
                </hudson.plugins.git.UserRemoteConfig>
              </userRemoteConfigs>
              <branches>
                <hudson.plugins.git.BranchSpec>
                  <name>master</name>
                </hudson.plugins.git.BranchSpec>
              </branches>
              <disableSubmodules>false</disableSubmodules>
              <recursiveSubmodules>false</recursiveSubmodules>
              <doGenerateSubmoduleConfigurations>false</doGenerateSubmoduleConfigurations>
              <authorOrCommitter>false</authorOrCommitter>
              <clean>false</clean>
              <wipeOutWorkspace>false</wipeOutWorkspace>
              <pruneBranches>false</pruneBranches>
              <remotePoll>false</remotePoll>
              <ignoreNotifyCommit>false</ignoreNotifyCommit>
              <useShallowClone>false</useShallowClone>
              <buildChooser class="hudson.plugins.git.util.DefaultBuildChooser"/>
              <gitTool>Default</gitTool>
              <submoduleCfg class="list"/>
              <relativeTargetDir></relativeTargetDir>
              <reference></reference>
              <excludedRegions></excludedRegions>
              <excludedUsers></excludedUsers>
              <gitConfigName></gitConfigName>
              <gitConfigEmail></gitConfigEmail>
              <skipTag>false</skipTag>
              <includedRegions></includedRegions>
              <scmName></scmName>
            </scm>
            <canRoam>true</canRoam>
            <disabled>false</disabled>
            <blockBuildWhenDownstreamBuilding>false</blockBuildWhenDownstreamBuilding>
            <blockBuildWhenUpstreamBuilding>false</blockBuildWhenUpstreamBuilding>
            <triggers class="vector"/>
            <concurrentBuild>false</concurrentBuild>
            <rootModule>
              <groupId>com.dianping</groupId>
              <artifactId>pay-unipay</artifactId>
            </rootModule>
            <goals>clean install -Dmaven.test.skip=true</goals>
            <mavenName>MAVEN2</mavenName>
            <mavenOpts>-Xmx1024m</mavenOpts>
            <aggregatorStyleBuild>true</aggregatorStyleBuild>
            <incrementalBuild>false</incrementalBuild>
            <perModuleEmail>true</perModuleEmail>
            <ignoreUpstremChanges>true</ignoreUpstremChanges>
            <archivingDisabled>false</archivingDisabled>
            <resolveDependencies>false</resolveDependencies>
            <processPlugins>false</processPlugins>
            <mavenValidationLevel>-1</mavenValidationLevel>
            <runHeadless>false</runHeadless>
            <settingConfigId></settingConfigId>
            <globalSettingConfigId></globalSettingConfigId>
            <reporters/>
            <publishers/>
            <buildWrappers/>
            <prebuilders>
              <hudson.tasks.Shell>
                <command/>
              </hudson.tasks.Shell>
            </prebuilders>
            <postbuilders>
              <hudson.tasks.Shell>
                <command>echo 'hello jenkins'</command>
              </hudson.tasks.Shell>
            </postbuilders>
            <runPostStepsIfResult>
              <name>FAILURE</name>
              <ordinal>2</ordinal>
              <color>RED</color>
            </runPostStepsIfResult>
          </maven2-moduleset>
        }
      end

      def post_config(job_name, content)
        true
      end

      def create(jobname, xml)
        true
      end 

      def build(job_name)
        true
      end

      def exists?(jobname)
        if @be_checked
          return true
        else
          @be_checked = true
          return false
        end   
      end 
    end 
  end


end 
