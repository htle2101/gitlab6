require 'spec_helper'

describe RolloutPackingModule do
  describe "Associations" do
    it { should belong_to(:packing) }
  end

  describe "Validation" do
    it { should validate_presence_of(:name) }
  end
end
