require 'spec_helper'

describe RolloutPacking do
  describe "Associations" do
    it { should belong_to(:rollout_branch) }
    it { should belong_to(:author) }
  end

  describe "Validation" do

    it { should validate_presence_of(:build_branch_id) }
    it { should validate_presence_of(:branch_name) }
    it { should validate_presence_of(:commit) }
    it { should validate_presence_of(:tag) }
    it { should validate_presence_of(:module_names) }
    it { should validate_presence_of(:author_id) }
  end

  context "instance method" do
    let(:product) { create(:product) }
    let(:project) { create(:project_with_code) }
    let(:rollout_branch) { create(:rollout_branch, project: project, ci_env: product, jenkins_job_name: "product-foo-master") }
    let(:packing) { create(:packing, rollout_branch: rollout_branch, status: 5, module_names: "foo-web") }
    let!(:packing_module) { create(:packing_module, packing: packing, name: "foo-web") }

    it "should return message of status" do
      packing.status_message.should == :uploading
    end

    it "should return array of module names" do
      packing.module_name_list.should == ["foo-web"]
    end

    context "#update_status" do
      it "should update status" do
        packing.update_status(:upload_success).should be_true
      end

      it "should not update status when status is incorrect" do
        packing.should_not_receive(:save)
        packing.update_status(:foo)
      end
    end

    context "#wars" do
      before do
        @modules = [
          RemoteCi::PomModule.new({ module_name: "foo-web", type: 'war', path: '/foo/foo-web', :version => "0.1.0" }),
          RemoteCi::PomModule.new({ module_name: "foo-service", type: 'jar', path: '/foo/foo-service', :version => "0.2.0" })
        ]
        rollout_branch.stub(modules: @modules)
      end
      it "should return wars only in module name list" do
        packing.packages.should have(1).item
        packing.packages.first.should be_an_instance_of Deployer
      end
    end

    it "should return command" do
      packing.command.should == "upload/#{packing.id}/$BUILD_NUMBER"
    end

    context "#can_rebuild?" do
      it "should return true when jenkins job is not running and status is failed" do
        rollout_branch.stub(job_running?: false)
        packing.stub(status: RolloutPacking::STATUS_LIST[:build_failed])
        packing.can_rebuild?.should be_true
      end

      it "should return false when jenkins job is running" do
        rollout_branch.stub(job_running?: true)
        packing.can_rebuild?.should be_false
      end

      it "should return false when status is deploying or deploy_success" do
        rollout_branch.stub(job_running?: false)
        packing.stub(status: RolloutPacking::STATUS_LIST[:deploying])
        packing.can_rebuild?.should be_false
      end
    end

    context "#can_deploy?" do
      it "should return true when jenkins job is not running and status is upload success" do
        rollout_branch.stub(job_running?: false)
        packing.stub(status: RolloutPacking::STATUS_LIST[:upload_success])
        packing.can_deploy?.should be_true
      end

      it "should return false when jenkins job is running" do
        rollout_branch.stub(job_running?: true)
        packing.can_deploy?.should be_false
      end

      it "should return false when status is not upload success" do
        rollout_branch.stub(job_running?: false)
        packing.stub(status: RolloutPacking::STATUS_LIST[:deploying])
        packing.can_rebuild?.should be_false
      end
    end

    context "#processing?" do
      it "should return true when jenkins job is running" do
        rollout_branch.stub(job_running?: true)
        packing.processing?.should be_true
      end

      it "should return true when status is uoloading" do
        rollout_branch.stub(job_running?: false)
        packing.stub(status: RolloutPacking::STATUS_LIST[:uploading])
        packing.processing?.should be_true
      end
    end

    it "#logger_file" do
      packing.logger_file.should == "ftp/#{rollout_branch.jenkins_job_name}/#{packing.id}.log"
    end
  end
end
