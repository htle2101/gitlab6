require 'spec_helper'

describe Artifact do
  it { should validate_presence_of :artifact_id }
  it { should validate_presence_of :group_id }
  it { should validate_presence_of :version }
  it { should validate_uniqueness_of(:version).scoped_to([:group_id, :artifact_id]) }
end
