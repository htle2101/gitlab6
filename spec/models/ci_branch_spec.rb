require 'spec_helper'

describe CiBranch do
  describe "Associations" do
    it { should belong_to(:project) }
    it { should belong_to(:ci_env) }
  end

  describe "Validation" do
    it { should validate_presence_of(:branch_name) }
    it { should validate_presence_of(:jenkins_job_name) }
  end

  context "instance method" do
    let(:alpha) { create(:alpha) }
    let(:project) { create(:project) }
    let(:ci_branch) { create(:ci_branch, project: project, ci_env: alpha, jenkins_job_name: "alpha-foo-master") }
    let!(:machine) {create(:virtual_machine, status: 7, module_name: 'foo-web', build_branch_id: ci_branch.id, project_id: project.id, package_type: 'war')}

    it "should return jenkins_url" do
      ci_branch.jenkins_url.should == "alpha.dp/job/alpha-foo-master"
    end

    it "should return workspace_path" do
      ci_branch.workspace_path.should == "/foo/jobs/alpha-foo-master/workspace"
    end

    context "#wars" do
      before do
        @modules = [
          RemoteCi::PomModule.new({ module_name: "foo-web", type: 'war', path: '/foo/foo-web', :version => "0.1.0" }),
          RemoteCi::PomModule.new({ module_name: "foo-service", type: 'jar', path: '/foo/foo-service', :version => "0.2.0" })
        ]
        ci_branch.stub(modules: @modules)
      end

      it "should return war array of modules" do
        ci_branch.wars.should have(1).item
        ci_branch.wars.first.should be_an_instance_of Deployer
      end
    end

    context "#machine_for_module" do
      let!(:machine) {create(:virtual_machine, module_name: 'foo', ci_branch: ci_branch)}
      it "should return machine with module_name" do
        ci_branch.machine_for_module('foo').should == machine
      end
    end

    context "#job_running?" do
      it "should return true when current build status is running" do
        ci_branch.stub(current_build_status: "running")
        ci_branch.job_running?.should be_true
      end

      it "should return true when current build status is not running" do
        ci_branch.stub(current_build_status: "anything else")
        ci_branch.job_running?.should be_false
      end
    end
  end

  describe :expire_cache do
    let(:project) { create(:project) }
    let(:alpha) { create(:alpha) }

    it "should call expire cache after create" do
      Rails.cache.should_receive(:delete)
      create(:ci_branch, project: project, ci_env: alpha, jenkins_job_name: "alpha-foo-master")
    end

    it "should call expire cache after create" do
      Rails.cache.should_receive(:delete).twice
      ci_branch = create(:ci_branch, project: project, ci_env: alpha, jenkins_job_name: "alpha-foo-master")
      ci_branch.destroy
    end
  end

end
