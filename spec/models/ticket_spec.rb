require 'spec_helper'

describe Ticket do
  describe "Associations" do
    it { should belong_to(:project) }
    it { should have_many(:packages) }
  end

  describe "Validation" do
    it { should validate_uniqueness_of(:released_at).scoped_to(:project_id) }
  end

  context "instance method" do
    let(:project) { create(:project) }
    let(:ticket) { create(:ticket, project: project) }

    context "#released_at_is_a_valid_date?" do
      it "should add a error when released_at is not a valid date" do
        ticket.released_at = "i am not a date"
        ticket.valid?.should be_false
        ticket.errors[:base].should include "Release date isn't a valid date."
      end

      it "should do nothing when released_at is not a valid date" do
        ticket.released_at = "2013-10-12"
        ticket.valid?.should be_true
      end
    end

    context "#released_at_has_been_taken?" do
      it "should add a error when released_at is already been taken" do
        t = Ticket.create(released_at: ticket.released_at, project_id: project.id)
        t.errors[:base].should include "Release date has been taken."
      end

      it "should no nothing when released_at didn't be taken" do
        t = Ticket.create(released_at: "2013-02-03", project_id: project.id)
        t.errors.should be_blank
      end
    end
  end
end
