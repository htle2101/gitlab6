require 'spec_helper'

describe 'ci_template' do
  before do
    @ci_env = create(:alpha)
    @ci_template = create(:ci_template, ci_env: @ci_env, ci_env_name: 'alpha', package_type: 'war')
    @build_branch = create(:build_branch, ci_env: @ci_env)
  end

  it 'should return ci_template' do
    @build_branch.ci_template.should == @ci_template
  end
end
