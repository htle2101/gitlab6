require 'spec_helper'

describe VirtualMachine do

  describe "Associations" do
    it { should belong_to(:ci_branch) }    
  end  

  describe "#available_machine" do
    it "should return a machine" do
      machine = create(:virtual_machine, project_id: nil)
      VirtualMachine.available_machine.should == machine
    end

    it "should return nil when no machine available" do
      machine = create(:virtual_machine, status: 0)
      machine.update_column(:project_id, 10)
      VirtualMachine.available_machine.should be_nil
    end
  end

  describe "#applicable_machine" do
    let(:conn) { mock('connection').as_null_object}
    let(:machine) {create(:virtual_machine, project_id: nil)}
    
    it "should return a machine" do
      Rye::Box.stub(new: conn)
      Rye::Box.should_receive(:new).once
      machine.stub(applicable?: conn)
      VirtualMachine.applicable_machine.should == machine
    end

    it "should return nil when no machine applicable" do
      machine.stub(applicable?: nil)
      VirtualMachine.applicable_machine.should be_nil
    end
  end
  
  describe "#update_status" do
    let(:machine) {create(:virtual_machine)}
    
    it "should be 5 when status is deploying" do
      machine.update_status('deploying')
      machine.status.should eq 5
    end
  end

  describe "#status_message" do
    let(:machine) {create(:virtual_machine, status: 7, module_name: 'test', 
                          build_branch_id: 2, project_id: 10, package_type: 'war')}
    
    it "should be 'deploy success' when 7" do
      machine.status_message.should eq "deploy success"
    end
  end
 
  describe 'validations' do
    let(:ci_branch) { create(:ci_branch) }
    let(:alpha) { create(:alpha) }
    before do
      ci_branch.stub(ci_env: alpha)
      subject.stub(ci_branch: ci_branch)
    end
    #it { should allow_value('192.168.111.100').for(:ip) }
    #it { should_not allow_value('256.0.0.1').for(:ip) }
  end  

  describe "#unbind" do
    let(:machine) { create(:virtual_machine) }
    it "should return true if machine didn't use" do
      machine.stub(used?: false)
      machine.unbind.should be_true
    end

    it "should return true if machine unbind successful" do
      machine.stub(used?: true)
      machine.unbind.should be_true
    end

    it "should return false if machine unbind failed" do
      machine.stub(used?: true)
      machine.stub(update_attributes: false)
      machine.unbind.should be_false
    end
  end

  describe "#release" do
    let(:machine) {create(:virtual_machine)}
    
    it "delete record after releasing the bind-type machine" do
      machine.update_column(:action_type, 'bind')
      machine.release
      VirtualMachine.find_by_id(machine.id).should eq nil
    end

    it "modify machine's attributes after releasing the apply-type machine" do
      machine.update_column(:action_type, 'apply')
      machine.release
      VirtualMachine.find_by_id(machine.id).should_not eq nil
    end
  end

  describe "#used?" do
    let(:machine) {create(:virtual_machine)}
    
    it "should be not used when status is nil" do
      machine.used?.should be_false
    end

    it "should be used when status > 0" do
      machine.update_column(:status, 5)
      machine.used?.should be_true
    end
  end

end
