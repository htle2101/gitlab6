require 'spec_helper'

describe CiEnv do

  describe "Respond to" do
    it { CiEnv.should respond_to(:connect_servers) }
    it { CiEnv.should respond_to(:servers) }
  end

  describe 'find_by_project_and_name' do
    before do
      namespace = create(:namespace)
      @project = create(:project, namespace: namespace)
      @ci_env = create(:alpha)
      ci_envs_namespace = create(:ci_envs_namespace, namespace: @project.namespace, ci_env: @ci_env, ci_env_name: 'alpha')
    end

    it 'should return alpha ci_env' do
      CiEnv.find_by_project_and_name(@project, 'alpha').should ==  @ci_env
    end
  end

  describe 'no_filled_envs?' do
    let!(:project) { create(:project) }
    let!(:alpha) { create :alpha }
    let!(:beta) { create :beta }
    let!(:product) { create :product }

    let!(:ci_template_alpha) { create :ci_template, template_name: 'alpha_template', ci_env: alpha, package_type: 'static' }
    let!(:ci_template_beta) { create :ci_template, template_name: 'alpha_template', ci_env: beta, package_type: 'static' }
    let!(:ci_template_product) { create :ci_template, template_name: 'alpha_template', ci_env: product, package_type: 'static' }

    let!(:ci_branch_alpha) { create :ci_branch, project: project, ci_env: alpha, jenkins_job_name: 'alpha-test-b', branch_name: 'b', gitlab_created: true }
    let!(:ci_branch_beta) { create :ci_branch, project: project, ci_env: beta, jenkins_job_name: 'alpha-test', branch_name: 'master', gitlab_created: true }

    before do
      @envs = CiEnv.no_filled_envs(project).map{|e| e.name }
    end

    it "returns no fillded envs" do
      @envs.should include 'alpha'
      @envs.should_not include 'beta'
      @envs.should include 'product'
    end 
  end

  context "instance method" do
    let(:alpha) { create :alpha }
    let(:beta) { create :beta }

    it "should return base path" do
      alpha.base_path.should == "/foo/jobs"
    end

    it "should return label by env" do
      alpha.label.should == "alpha"
      beta.label.should == "qa"
    end
    
    it "should return jenkins hook url" do
      alpha.jenkins_hook_path.should == "http://alpha.dp/gitlab/build_now"
    end

    it "should return ci server url" do
      alpha.full_url.should == "http://alpha.dp"
    end

    it 'should return server' do
      alpha.server.should be_kind_of(RemoteCi::Jenkins)
    end
  end

end
