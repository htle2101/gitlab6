require 'spec_helper'

describe Dependency do
  describe "Associations" do
    it { should belong_to(:target_ci_branch) }
    it { should belong_to(:ci_branch) }
  end

  describe "Validation" do
    it { should validate_presence_of(:target_build_branch_id) }
    it { should validate_presence_of(:build_branch_id) }
    it { should validate_presence_of(:module_name) }
    it { should validate_uniqueness_of(:module_name).scoped_to([:build_branch_id]).with_message(/has been added/) }
  end
end
