require 'spec_helper'

describe CiTemplate do
  include TemplateHelpers

  describe "Associations" do
    it { should belong_to(:ci_env) }
    it { should belong_to(:namespace) }
  end

  describe "#template action" do
    let(:ci_template_alpha) { create :ci_template, template_name: 'alpha_template', ci_env: ci_env_alpha, package_type: 'static', ci_env_name: 'alpha', deployers: 'Static' }
    let(:ci_env_alpha) { create(:alpha) }
    let(:project_A) { create(:project) }
    let(:ci_branch) { create(:ci_branch, project: project_A, ci_env: ci_env_alpha) }
    let(:project_with_code) { create(:project_with_code) }
    let(:rollout_branch) { create(:rollout_branch, project: project_with_code, ci_env: ci_env_alpha) }
    let!(:admin) {create(:admin, name: "Administrator")}
    let!(:packing) {create(:packing, branch_name: "foo", rollout_branch: rollout_branch)}

    it "should return correct data" do
      source = template("model/ci_template/source.xml.erb", {url: project_A.full_ssh_url_to_repo})
      dest = template("model/ci_template/dest.xml.erb", {
        url: project_A.full_ssh_url_to_repo, token: admin.authentication_token, host_and_port: RemoteCi::CiPostBuild.host_with_port})
      ci_template_alpha.template(ci_branch, source).should == dest
    end

    it "should change ref and update callback url" do
      source = template("model/ci_template/switch_ref.source.xml.erb")
      dest = template("model/ci_template/switch_ref.dest.xml.erb", {
        ref: packing.branch_name, token: admin.authentication_token,
        host_and_port: RemoteCi::CiPostBuild.host_with_port, url: packing.command })
      ci_template_alpha.switch_ref(packing, source).should == dest
    end

  end

end
