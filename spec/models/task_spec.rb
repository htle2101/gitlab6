require 'spec_helper'

describe Task do
  describe "Associations" do
    it { should belong_to(:project) }
    it { should belong_to(:creator) }
    it { should belong_to(:assignee) }
    it { should have_and_belong_to_many(:packages) }
    it { should have_many(:configs) }
  end

  describe "Validation" do
    it { should validate_presence_of(:subject) }
    it { should validate_presence_of(:assignee_id) }
    it { should validate_presence_of(:creator_id) }
  end

  context "instance method" do
    let(:user1) { create(:user) }
    let(:user2) { create(:user) }
    let!(:foo_user) { create(:user) }
    let!(:admin) { create(:admin) }
    let!(:task1) { create(:task, creator: user1, assignee: user2) }
    let!(:task2) { create(:task, creator: user2, assignee: user1) }
    let(:package1) { create(:package, name: "foo-war") }
    let(:ticket) { create(:ticket, released_at: "2013-11-11") }
    let(:package2) { create(:package, name: "bar-war", ticket: ticket) }
    
    it "should return tasks with specify assignee" do
      Task.assigned_to(user1).should == [task2]
    end

    it "should return tasks with specify creator" do
      Task.created_by(user1).should == [task1]
    end

    context "#package_name_list" do
      before do
        task1.packages << package1
        task1.packages << package2
      end

      it "should return package name list" do
        task1.package_name_list.should == ["foo-war", "bar-war"]
      end
    end

    context "#can_edit?" do
      it "should return true when user is creator" do
        task1.can_edit?(user1).should be_true
      end

      it "should return true when user is assignee" do
        task1.can_edit?(user2).should be_true
      end

      it "should return true when user is admin" do
        task1.can_edit?(admin).should be_true
      end

      it "should return false when user is not valid" do
        task1.can_edit?(foo_user).should be_false
      end
    end

    context "#can_destroy?" do
      it "should return true when user is creator" do
        task1.can_destroy?(user1).should be_true
      end

      it "should return true when user is admin" do
        task1.can_destroy?(admin).should be_true
      end

      it "should return false when user is assignee" do
        task1.can_destroy?(user2).should be_false
      end

      it "should return false when user is not valid" do
        task1.can_destroy?(foo_user).should be_false
      end
    end
  end
end
