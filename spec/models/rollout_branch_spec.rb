require 'spec_helper'

describe RolloutBranch do
  describe "Associations" do
    it { should have_many(:packings) }
  end

  context "instance method" do
    let(:product) { create(:product) }
    let(:project) { create(:project_with_code) }
    let(:rollout_branch) { create(:rollout_branch, project: project, ci_env: product, jenkins_job_name: "product-foo-master") }
    let(:packing) { create(:packing, rollout_branch: rollout_branch) }
    let!(:ci_template) { create :ci_template, template_name: 'alpha_template', ci_env: product, package_type: 'war-maven2' }

    before do
      @mock_client = MockJenkinsApi::Client.new('product')
      @template_content = "xmlcontent"
      CiEnv.any_instance.stub(:server).and_return(@mock_client)
      @mock_client.job.stub(post_config: true)
      @mock_client.job.stub(build: true)
      @mock_client.job.stub(get_config: @template_content)
      ci_template.stub(switch_ref: @template_content)
      rollout_branch.stub(ci_template: ci_template)
    end

    it "should return upload command" do
      rollout_branch.command.should == "upload"
    end

    it "should call jenkins to build" do
      ci_template.should_receive(:switch_ref).with(packing, @template_content)
      @mock_client.job.should_receive(:post_config).with(rollout_branch.jenkins_job_name, @template_content)
      @mock_client.job.should_receive(:build).with(rollout_branch.jenkins_job_name)
      rollout_branch.change_jenkins_and_build(packing)
    end
  end
end
