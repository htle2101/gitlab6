require 'spec_helper'

describe Script do
  describe "Associations" do
    it { should belong_to(:project) }
    it { should belong_to(:author) }
  end

  describe "Validation" do
    it { should validate_presence_of(:db_name) }
    it { should validate_presence_of(:todo) }
    it { should validate_presence_of(:status) }
    it { should validate_presence_of(:project) }
  end

  context "instance method" do
    let(:project) { create(:project) }
    let(:script1) { create(:script, project: project) }
    let(:script2) { create(:script, project: project, status: 2) }
    let(:service) { Service::DBA::Request.new }

    before do
      service.stub(upload: true)
      service.stub(execute: true)
      Service::DBA::Request.stub(new: service)
    end

    it "should return true" do
      script1.can_edit?.should be_false
      script1.can_close?.should be_true
    end

    it "should return false" do
      script2.can_edit?.should be_false
      script2.can_close?.should be_false
    end

    it "should return can_execute?" do
      script1.can_execute?.should be_false
      script2.can_execute?.should be_true
    end

    it "should cal dba upload service" do
      service.should_receive(:upload)
      script2.notify_dba_script_upload
    end

    it "should cal dba upload service with status 1" do
      service.should_receive(:upload).with(hash_including(status: 1))
      script2.notify_dba_script_reupload
    end

    it "should cal dba execute service" do
      service.should_receive(:execute).with(hash_including(env: 1))
      script2.notify_dba_script_execute(1)
    end

    it "should cal dba upload service with status 6" do
      service.should_receive(:upload).with(hash_including(status: 6))
      script2.notify_dba_script_close
    end
  end
end
