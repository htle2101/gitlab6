require 'spec_helper'

describe LightMerge do
  

  describe "Validation" do
    let!(:light_merge) { create(:light_merge) }
    # project.should validate_presence_of(:ci_branch_id) 
    # project.should validate_uniqueness_of(:ci_branch_id)
    it 'be validate' do
      light_merge.should validate_presence_of(:base_branch)
    end  
  end

  describe "Respond to" do
    it { should respond_to(:check_if_can_be_merged) }
    it { should respond_to(:current_conflict_branches) }
    it { should respond_to(:merge_action) }
    it { should respond_to(:automerge!) }
  end
end
