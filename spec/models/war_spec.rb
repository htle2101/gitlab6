require "spec_helper"

describe Deployers::War do
  let(:conn) { mock('connection').as_null_object }
  let(:alpha_conn) { mock('connection').as_null_object }
  let(:project) { create(:project) }
  let(:alpha) { create(:alpha) }
  let(:ci_branch) { create(:ci_branch, project: project, ci_env: alpha, jenkins_job_name: "alpha-foo-master") }
  let!(:machine) {create(:virtual_machine, status: 1, module_name: 'foo-web', ci_branch: ci_branch, package_type: 'war', project_id: project.id)}
  let(:war) { Deployers::War.new(ci_branch: ci_branch, module_name: 'foo-web', path: '/foo/foo-web', type: 'war', version: '0.1.0', warName: 'foo-web')}
  let(:workspace_path) { "/foo/workspace" }
  let(:ts) { "20130101101010" }

  before do
    Deployers::War.any_instance.stub(ts: ts)
    war.stub(:logger).and_return(mock('logger').as_null_object)
    #war.stub(:logger).and_return(::Logger.new(STDOUT))
    ci_branch.stub(workspace_path: workspace_path)
    alpha.stub(conn: alpha_conn)
    machine.stub(conn: conn)
  end

  it "should return full path of war file" do
    war.full_path.should == "#{workspace_path}/foo/foo-web/target/foo-web-alpha-0.1.0.war"
  end

  it "should return release path" do
    war.release_path.should == "/data/webapps/releases/#{ts}"
  end

  it "should return current path" do
    war.current_path.should == "/data/webapps/current"
  end

  it "should return name of war file" do
    war.name.should == "foo-web-alpha-0.1.0.war"
  end

  it "should return deploy name which does not contains env and version" do
    war.deploy_name.should == "foo-web.war"
  end

  it "should return machine for module of the war" do
    war.machine.should == machine
  end

  it "should return http url of war" do
    war.url.should == "http://alpha.dp/job/alpha-foo-master/ws/foo/foo-web/target/foo-web-alpha-0.1.0.war"
  end

  context "#exists?" do
    it "should return true when target file exist" do
      alpha.conn.stub(test: true)
      war.exists?.should be_true
    end

    it "should return false when target file does not exist" do
      alpha.conn.stub(:test) { raise Rye::Err.new('bomb') }
      war.exists?.should be_false
    end
  end

  it "should create logger_file_name dynamically" do
    war.logger_file_name.should == "deploy/alpha-foo-master/foo-web-#{ts}.log"
  end

  context "#deploy" do
    it "should return false when no machine available" do
      war.stub(machine: nil)
      war.deploy.should be_false
    end

    it "should return false when no ip assigned" do
      war.machine.stub(ip: nil)
      war.deploy.should be_false
    end

    it "should return false when war does not exist" do
      war.stub(exists?: false)
      war.deploy.should be_false
    end

    it "should return true when war deploy successfully" do
      war.stub(machine: machine)
      war.stub(exists?: true)
      conn.should_receive(:sudo_exec).with("mkdir -p #{war.release_path} #{Deployers::War::BACKUP_PATH} #{Deployers::War::LOG_PATH} #{Deployers::War::DATA_PATH}", :timeout => 10)
      conn.should_receive(:sudo_exec).with("chown -R nobody:nobody #{Deployers::War::LOG_PATH} #{Deployers::War::DATA_PATH}", :timeout => 15)
      alpha_conn.should_receive(:execute).with("scp -P #{Settings.dp['ssh_port']} #{war.full_path} #{Settings.dp['ssh_user']}@#{machine.ip}:#{Deployers::War::BACKUP_PATH}", :timeout => 30)
      conn.should_receive(:sudo_exec).with("unzip -q #{Deployers::War::BACKUP_PATH}/#{war.name} -d #{war.release_path}/#{war.deploy_name}", :timeout => 30)
      conn.should_receive(:sudo_exec).with("rm -rf #{war.current_path}", :timeout => 10)
      conn.should_receive(:sudo_exec).with("ln -s #{war.release_path} #{war.current_path}", :timeout => 10)
      conn.should_receive(:sudo_exec).with("chown root:root /data/webapps ", :timeout => 15)
      conn.should_receive(:sudo_exec).with("[ -f /data/webapps/appenv ] || echo 'deployenv=alpha \nzkserver=alpha.lion.dp:2182' >> /data/webapps/appenv", :timeout => 10)
      conn.should_receive(:sudo_exec).with("/sbin/service jboss restart", :timeout => 200)
      war.deploy.should be_true
    end

    it "should return false when error raised" do
      war.stub(exists?: true)
      war.stub(machine: machine)
      alpha_conn.stub(:execute) { raise RuntimeError.new('bomb') }
      war.deploy.should be_false
    end
  end
end
