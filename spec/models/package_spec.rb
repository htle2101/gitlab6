require 'spec_helper'

describe Package do

  describe "Associations" do
    it { should belong_to(:packing) }
    it { should belong_to(:packing_module) }
    it { should belong_to(:project) }
    it { should belong_to(:ticket) }
    it { should have_and_belong_to_many(:tasks) }
  end

  describe "Validation" do
    it { should validate_presence_of(:project) }
    it { should validate_presence_of(:name) }
  end

  context "#can_change_tag?" do
    let(:package) { create(:package) }

    it "should return true when no grouping strategies available" do
      package.can_change_tag?.should be_true
    end

    it "should return true when not deploy grouping strategy available" do
      create(:grouping_strategy, package: package, status: 1024)
      create(:grouping_strategy, package: package, status: 1)
      package.can_change_tag?.should be_true
    end

    it "should return false when no not grouping strategy available" do
      create(:grouping_strategy, package: package, status: 1)
      package.can_change_tag?.should be_false
    end

    it "should return false when in progress grouping strategy available" do
      create(:grouping_strategy, package: package, status: 1024)
      create(:grouping_strategy, package: package, status: 5)
      package.can_change_tag?.should be_false
    end
  end
end
