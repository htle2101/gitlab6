require 'spec_helper'

describe Sonar do
  let(:sonar) { Sonar.new('com.dianping:gitlab-test')}

  context "#dashboard_path" do
    it "should be the right path" do
      sonar.dashboard_path.should == "http://#{Settings.sonar['sonar_url']}/dashboard/index/com.dianping:gitlab-test"
    end  
  end

  context "dynamic method generator" do
    before do
      Service::Sonar::Api::Coverage.any_instance.stub(:execute).and_return({foo: "bar"})
      Service::Sonar::Api::Coverage.any_instance.stub(:result).and_return({bar: "foo"})
    end

    it "should return {foo: 'bar'}" do
      sonar.coverage.should == {foo: "bar"}
    end

    it "should return {foo: 'bar'}" do
      Sonar.instance_methods.should include(:coverage)
      sonar.coverage
    end
  end

end      
