require 'spec_helper'

describe DatabaseUpdateService do
  let (:service) { DatabaseUpdateService.new }


  context "Database Update" do

    it "should add 2 new databases" do
      service.execute("foo, bar").should == ["foo", "bar"]
    end

    it "should add 1 new database if a database(case insensitive) already exist" do
      create(:database, name: "Foo")
      service.execute("foo, bar").should == ["bar"]
    end
  end

end

