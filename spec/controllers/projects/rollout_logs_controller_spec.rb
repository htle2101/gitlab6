require 'spec_helper'

describe Projects::RolloutLogsController do
  before do
    @project = create(:project_with_code)
    @user = create(:user)

    sign_in(@user)

    @project.team << [@user, :master]
  end

  it 'show GET index as HTML' do
    get :index, project_id: @project.path

    response.should be_success
  end

  it 'show GET index as JS' do
    get :index, format: :js, project_id: @project.path

    response.should be_success
  end
end
