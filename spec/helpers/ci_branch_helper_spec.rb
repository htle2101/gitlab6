require 'spec_helper'

describe CiBranchHelper do

  describe 'ci_env_options?' do
    let!(:project) { create(:project) }
    let!(:ci_env_alpha) { create :ci_env, name: 'alpha' }
    let!(:ci_env_beta) { create :ci_env, name: 'beta', is_single: true }
    let!(:ci_env_product) { create :ci_env, name: 'product', is_single: true }

    let!(:ci_branch_alpha) { create :ci_branch, project: project, ci_env: ci_env_alpha, jenkins_job_name: 'alpha-test-b', branch_name: 'b', gitlab_created: true }
    let!(:ci_branch_beta) { create :ci_branch, project: project, ci_env: ci_env_beta, jenkins_job_name: 'alpha-test', branch_name: 'master', gitlab_created: true }
    let!(:ci_template_alpha) { create :ci_template, template_name: 'alpha_template', ci_env: ci_env_alpha, package_type: 'war-maven3' }
    let!(:ci_template_beta) { create :ci_template, template_name: 'beta_template', ci_env: ci_env_beta, package_type: 'war-maven3' }

    it "returns grouped_ci_envs_for_select from all project" do 
      ci_env_options.should include 'alpha'
      ci_env_options.should include 'beta'
      #ci_env_options.should include 'product'
    end  

    it "returns grouped_ci_envs_for_select from no fillded envs" do 
      ci_env_options(project).should include 'alpha'
      ci_env_options(project).should_not include 'beta'
      #ci_env_options(project).should include 'product'
    end 

  end
end
