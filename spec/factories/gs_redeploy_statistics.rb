# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :gs_redeploy_statistic do
    appname "MyString"
    gs_id 1
    tag "MyString"
    group "MyString"
    group_id 1
    ticket_id 1
  end
end
