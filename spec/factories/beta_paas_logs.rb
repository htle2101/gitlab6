# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :beta_paas_log do
    module_name "MyString"
    log "MyText"
    status 1
    tag "MyString"
    build_branch_id 1
    beta_paas_rollout_id 1
  end
end
