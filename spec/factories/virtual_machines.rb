# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :virtual_machine do
    ip "192.168.9.10"
    status 0
    module_name nil
    build_branch_id nil
    action_type "apply"
    package_type nil
    username nil
    password nil
  end
end
