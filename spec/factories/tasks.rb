FactoryGirl.define do
  factory :task do
    subject "foo"
    project nil
    status 1
    association :assignee, factory: :user
    association :creator, factory: :user
    released_at Date.parse("2013-10-10")
  end
end
