# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :performance_test_module do
    appname "MyString"
    tag "MyString"
    app_type "MyString"
    cpu 1
    memory 1
    domain_name "MyString"
    package_url "MyString"
    performance_test_id 1
  end
end
