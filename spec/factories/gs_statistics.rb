# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :gs_statistic do
    gs_id 1
    status 1
    started_at ""
    finished_at ""
    time_interval 1.5
    ticket_id 1
    project_id 1
    tag "MyString"
  end
end
