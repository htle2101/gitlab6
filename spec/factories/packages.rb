FactoryGirl.define do
  factory :package do
    name "some-war"
    tag nil
    url nil
    packing_module nil
    packing nil
    association :ticket, factory: :ticket
    association :project, factory: :project
  end
end
