# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :ci_env do
    name "myenv"
    url "foo.bar.dp"
    user "user"
    password "password"
    ssh_user "user"
    ssh_password "password"
    path "/foo"
    is_single false
    is_cmdb false
    default true

    factory :alpha do
      name "alpha"
      url "alpha.dp"
      user "alpha.user"
      password "alpha.password"
      is_single false
      is_cmdb false
    end

    factory :beta do
      name "beta"
      url "beta.dp"
      user "beta.user"
      password "beta.password"
      is_single true
      is_cmdb true
    end

    factory :product do
      name "product"
      url "product.dp"
      user "product.user"
      password "product.password"
      is_single true
      is_cmdb false
    end
  end

end
