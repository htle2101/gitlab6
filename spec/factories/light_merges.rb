# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :light_merge do
    project nil
    association :build_branch
    base_branch "MyString1"
    source_branches ['','b2','b3']
    conflict_branches "MyText"
    status 1
  end
end
