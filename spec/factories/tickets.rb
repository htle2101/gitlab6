FactoryGirl.define do
  factory :ticket do
    project nil
    released_at Date.parse("2013-10-10")
  end
end
