# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :beta_paas_rollout do
    ci_branch_id 1
    status 1
    released_on ""
    tag "MyString"
    group_id 1
    hostname "MyString"
    operation_id 1
    machine "MyString"
  end
end
