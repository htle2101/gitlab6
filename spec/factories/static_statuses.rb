# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :static_status do
    build_status "MyString"
    cache_status "MyString"
    build_number 1
    build_branch_id 1
  end
end
