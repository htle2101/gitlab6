# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :online_job do
    jar_name "MyString"
    machine "MyString"
    env "beta"
    tag "Tag1"
    online_script "MyText"
    status "MyString"
    project_id nil
    build_branch_id nil
    success_at "2014-04-14 11:14:18"
  end
end
