# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :paas_rollout do
    project nil
    package nil
    status 0
    released_on "2014-03-05 11:21:51"
    tag "MyString"
  end
end
