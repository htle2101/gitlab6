# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :performance_test do
    title "MyString"
    target "MyString"
    uid "MyString"
    test_log_url "MyString"
    concurrent 1
    duration 1
    upload_file "MyString"
    project_id 1
  end
end
