FactoryGirl.define do
  factory :packing, class: "RolloutPacking" do
    rollout_branch nil
    branch_name "foo"
    sequence(:tag) { |n| "tag#{n}" }
    commit "123456"
    module_names "foo,bar"
    war_urls nil
    association :author, factory: :user
    status nil
  end
end
