# encoding: UTF-8

FactoryGirl.define do
  factory :script do
    name "test.sql"
    todo "上线前"
    db_name "gitpub"
    path "foo/bar/test.sql"
    project
    desc "test"
    status 0
    author nil
    req_url nil
  end
end

