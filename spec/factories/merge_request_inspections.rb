# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :merge_request_inspection do
    project_id 1
    merge_request_id 1
    source_branch "MyString"
    target_branch "MyString"
    jenkins_job_url "MyString"
    message "MyText"
    result 1
  end
end
