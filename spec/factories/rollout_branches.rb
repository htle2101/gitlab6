FactoryGirl.define do
  factory :rollout_branch, class: "RolloutBranch" do
    project factory: :project_with_code
    ci_env nil
    branch_name "master"
    jenkins_job_name "jenkins_job_name"
    gitlab_created false
    package_type "war"
  end
end
