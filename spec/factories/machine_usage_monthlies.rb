# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :machine_usage_monthly do
    namespace nil
    project nil
    virtual_machine nil
    year 1
    month 1
    duration 1
  end
end
