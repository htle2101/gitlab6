# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :packing_module, class: "RolloutPackingModule" do
    packing_id 1
    name "MyString"
    url nil
  end
end
