# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :ci_template do
    namespace nil
    ci_env nil
    template_name "MyString"
    package_type "MyString"
    comment "MyText"
    visable true
  end
end
