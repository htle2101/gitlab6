# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :gs_group_statistic do
    namespace_id 1
    namespace_name "MyString"
    weekth "MyString"
    date_interval "MyString"
    order 1
    appname "MyString"
    project_id 1
  end
end
