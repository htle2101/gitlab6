# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :artifact do
    group_id "MyString"
    artifact_id "MyString"
    version "MyString"
  end
end
