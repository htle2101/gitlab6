# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :grouping_strategy do
    name "group1"
    hostname "['host1', 'host2']"
    swimlane 1
    appname "someapp"
    group_id 1
    package nil
    ticket nil
    status 1024
  end
end
