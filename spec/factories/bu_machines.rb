# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :bu_machine do
    bu_name "MyString"
    department "MyString"
    alpha 1
    beta 1
  end
end
