# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :build_branch do
    project nil
    ci_env nil
    branch_name "master"
    jenkins_job_name "jenkins_job_name"
    gitlab_created false
    package_type "war"
  end
  factory :ci_branch do
    project nil
    ci_env nil
    branch_name "master"
    jenkins_job_name "jenkins_job_name"
    gitlab_created false
    package_type "war"
  end
end
