require 'spec_helper'
require 'fileutils'

describe Gitlab::InitBranch  do 

  let(:init_branch) {Gitlab::InitBranch.new('foo')}
  before do
    name = "foo"
    local_path = "#{Rails.root}/tmp/local/#{name}_#{Time.now.to_i}"
    @remote_repo_path = "#{Rails.root}/tmp/remote/#{name}_#{Time.now.to_i}#{name}.git"

    Grit::Repo.init_bare @remote_repo_path    
    init_branch.stub(:path).and_return(local_path)
    init_branch.stub(:full_repos_path).and_return(@remote_repo_path)
  end

  context 'branch init after project created' do    
    it 'should contains master and rc branch' do
      init_branch.execute
      %x[cd #{@remote_repo_path}  && git branch ].include?("master").should == true
      #{}%x[cd #{@remote_repo_path}  && git branch ].include?("rc").should == true
    end

    it 'should contains scm.dp' do
      init_branch.execute
      %x[cd #{@remote_repo_path} && git shortlog -s ].include?("scm.dp").should == true
    end
  end

  after(:all) do
    FileUtils.rm_r("#{Rails.root}/tmp/local")
    FileUtils.rm_r("#{Rails.root}/tmp/remote")
  end
end
