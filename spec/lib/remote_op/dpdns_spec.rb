require 'spec_helper'
require 'net/http'

describe RemoteOp::DpDns do
  let(:dns) { RemoteOp::DpDns.new }

  it "should set the domain name with ip." do
    uri = URI("http://#{Settings.op['dpdns_ip']}:#{Settings.op['dpdns_port']}/addname?name=test.alpha.dp&rdata=192.168.0.1")
    Net::HTTP.should_receive(:get_response).with(uri)
    dns.set_domain_name('test.alpha.dp', '192.168.0.1')
  end

  it "should get the domain name of ip." do
    uri = URI("http://#{Settings.op['dpdns_ip']}:#{Settings.op['dpdns_port']}/queryip?ip=192.168.0.1")
    Net::HTTP.should_receive(:get_response).with(uri)
    dns.get_domain_name('192.168.0.1')
  end

  it "should delete the domain name with ip." do
    uri = URI("http://#{Settings.op['dpdns_ip']}:#{Settings.op['dpdns_port']}/delname?name=test.alpha.dp&rdata=192.168.0.1")
    Net::HTTP.should_receive(:get_response).with(uri)
    dns.delete_domain_name('test.alpha.dp', '192.168.0.1')
  end 
    
end