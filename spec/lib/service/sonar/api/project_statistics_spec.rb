require 'spec_helper'

describe Service::Sonar::Api::ProjectStatistics do
  before(:each) do
    @project = create :project
    @project_statis = Service::Sonar::Api::ProjectStatistics.new(@project)
    @ci_branch = create :ci_branch
    @project.stub(:branch_for_sonar).and_return(@ci_branch)
    @module_1 = RemoteCi::PomModule.new({:module_name=>"account",
                                          :type=>"pom",
                                          :path=>"",
                                          :version=>"0.0.2-SNAPSHOT",
                                          :groupId=>"com.dianping",:full_name=>"com.dianping:account",
                                          :warName=>"account",
                                          :ut=>nil,
                                          :leafNode=>false})
    @module_2 = RemoteCi::PomModule.new({:module_name=>"account-web",
                                         :type=>"war",
                                         :path=>"/account-web",
                                         :version=>"1.0.1",
                                         :groupId=>"com.dianping",
                                         :full_name=>"com.dianping:account-web",
                                         :warName=>"account-web",
                                         :ut=>nil,
                                         :leafNode=>true})
    @module_3 = RemoteCi::PomModule.new({:module_name=>"account-beans",
                                         :type=>"pom",
                                         :path=>"/account-beans",
                                         :version=>"1.0.2",
                                         :groupId=>"com.dianping",
                                         :full_name=>"com.dianping:account-beans",
                                         :warName=>"account-beans",
                                         :ut=>nil,
                                         :leafNode=>false})
    @module_4 = RemoteCi::PomModule.new({:module_name=>"account-biz",
                                         :type=>"",
                                         :path=>"/account-beans/account-biz",
                                         :version=>"1.4.7",
                                         :groupId=>"com.dianping",
                                         :full_name=>"com.dianping:account-biz",
                                         :warName=>"account-biz",
                                         :ut=>nil,
                                         :leafNode=>true})
    @module_5 = RemoteCi::PomModule.new({:module_name=>"account-client",
                                         :type=>"",
                                         :path=>"/account-beans/account-client",
                                         :version=>"1.0.1",
                                         :groupId=>"com.dianping",
                                         :full_name=>"com.dianping:account-client",
                                         :warName=>"account-client",
                                         :ut=>'false',
                                         :leafNode=>true})
    @modules = [ @module_1, @module_2, @module_3, @module_4, @module_5 ]
    @ci_branch.stub(modules: @modules)

  end

  describe "modules_for_statistics" do
    it "should return modules which are leafNode and under UT" do
      @project_statis.modules_for_statistics.should == [@module_2, @module_4]
    end
  end

  describe "query sonar " do
    before do
      @sonar = Sonar.new('com.dianping:account', params = {includetrends: true,resource: 'com.dianping:account'})
      @sonar_query = Service::Sonar::Api::Query.new('com.dianping:account')
      @project.stub(:resource_string_for_sonar).and_return('com.dianping:account')
      @project.sonar.stub(:query).and_return(@sonar_query)
      stub_request(:get, "http://192.168.5.142/api/resources?includetrends=true&resource=com.dianping:account").
                    with(:headers => {'Accept'=>'*/*', 'User-Agent'=>'Ruby'}).
                    to_return(:status => 200,
                    :body => "[{\"id\":35075,\"key\":\"com.dianping:account\",\"name\":\"account\",\"scope\":\"PRJ\",\"qualifier\":\"TRK\",\"date\":\"2013-08-15T15:25:39+0800\",\"lname\":\"account\",\"lang\":\"java\",\"version\":\"0.0.1-SNAPSHOT\",\"description\":\"\",\"p1\":\"previous_analysis\",\"p1p\":\"2013-08-15\",\"p1d\":\"2013-08-15T14:53:16+0800\",\"p2\":\"days\",\"p2p\":\"1\",\"p2d\":\"2013-08-14T15:25:39+0800\",\"p3\":\"days\",\"p3p\":\"7\",\"p3d\":\"2013-07-16T15:25:39+0800\",\"p4\":\"days\",\"p4p\":\"30\",\"p4d\":\"2013-07-11T15:25:39+0800\"}]",
                    :headers => {})
    end

    it "project_base_query" do
      @project_statis.project_base_query.should_not == false
    end

    it "period_number_for_statistics" do
      time = Time.parse("2013-8-20 11:00:00")
      Time.stub(:now) { time }
      @project_statis.period_number_for_statistics.should == 3
    end

    it "namespace_statistical?" do
      time = Time.parse("2013-8-20 11:00:00")
      Time.stub(:now) { time }
      @project_statis.namespace_statistical?.should == true
    end

  end

  describe "statistics" do
    before do
      @project_statis.stub(:query_general_metric).with( @module_2.full_name,'lines_to_cover,uncovered_lines,new_lines_to_cover,new_uncovered_lines').
                      and_return([{"key"=>"lines_to_cover", "val"=>1000.0, "var1"=>100.0, "var2"=>150.0, "var3"=>200.0, "var4"=>200},
                                  {"key"=>"uncovered_lines", "val"=>400.0, "var1"=>50.0, "var2"=>100.0, "var3"=>150.0, "var4"=>150.0},
                                  {"key"=>"new_lines_to_cover", "var1"=>100.0, "var2"=>50.0, "var3"=>0.0, "var4"=>0.0},
                                  {"key"=>"new_uncovered_lines", "var1"=>40.0, "var2"=>20.0, "var3"=>0.0, "var4"=>0.0}])
      @project_statis.stub(:query_general_metric).with( @module_4.full_name,'lines_to_cover,uncovered_lines,new_lines_to_cover,new_uncovered_lines').
                      and_return([{"key"=>"lines_to_cover", "val"=>2000.0, "var1"=>200.0, "var2"=>400.0, "var3"=>500.0, "var4"=>500.0},
                                  {"key"=>"uncovered_lines", "val"=>1000.0, "var1"=>100.0, "var2"=>200.0, "var3"=>400.0, "var4"=>500.0},
                                  {"key"=>"new_lines_to_cover", "var1"=>200.0, "var2"=>100.0, "var3"=>0.0, "var4"=>0.0},
                                  {"key"=>"new_uncovered_lines", "var1"=>40.0, "var2"=>20.0, "var3"=>0.0, "var4"=>0.0}])
      @project_statis.stub(:module_uncovered_lines_pairs).and_return({ @module_2.module_name => 400, @module_4.module_name => 1000 })
      @project_statis.stub(:module_lines_to_cover_pairs).and_return({ @module_2.module_name => 1000, @module_4.module_name => 2000 })
    end

    it "modules_average_coverage" do
      @project_statis.modules_average_coverage.should ==  (((600.0 + 1000.0) / (1000.0 + 2000.0)) * 100.0).try(:round, 2)
    end

  end

end
