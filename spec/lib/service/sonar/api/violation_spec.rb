require 'spec_helper'

describe Service::Sonar::Api::Violation do

  let(:full_name) { "com.dianping.foo" }
  let(:api) {  Service::Sonar::Api::Violation.new(full_name) }
  let(:result) { File.read(File.expand_path("../../data/violation.json", __FILE__)) }

  before do
    WebMock.stub_request(:get, "http://#{Settings.sonar['sonar_url']}/api/resources?#{Rack::Utils.build_query(api.params)}")
    .to_return(:status => 200, :body => result)
  end

  it "should format the result" do

    api.execute.result.should == {
      "violations" => { "frmt_val" => "1,656", "trend" => -1 },
      "violations_density" => { "frmt_val" => "92.0%", "trend" => 0 }
    }
  end  

end
