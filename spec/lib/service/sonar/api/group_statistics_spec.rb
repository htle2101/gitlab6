require 'spec_helper'

describe Service::Sonar::Api::GroupStatistics do
  before(:each) do
    @group = create(:group, name: 'sonargroup')
    @group_statis = Service::Sonar::Api::GroupStatistics.new(@group)
    @project_A = create(:project, name: 'A')
    @project_B = create(:project, name: 'B')
    @project_C = create(:project, name: 'C')
    @project_A_statis = Service::Sonar::Api::ProjectStatistics.new(@project_A)
    @project_B_statis = Service::Sonar::Api::ProjectStatistics.new(@project_B)
    @project_C_statis = Service::Sonar::Api::ProjectStatistics.new(@project_C)
    @project_A_statis.expire_cache
    @project_B_statis.expire_cache
    @project_C_statis.expire_cache
    @group_statis.expire_cache
    @group_statis.stub(:sample_projects).and_return([@project_A, @project_C])
    Service::Sonar::Api::ProjectStatistics.stub(:new).with(@project_A).and_return( @project_A_statis)
    Service::Sonar::Api::ProjectStatistics.stub(:new).with(@project_B).and_return( @project_B_statis)
    Service::Sonar::Api::ProjectStatistics.stub(:new).with(@project_C).and_return( @project_C_statis)
  end

  describe "new_line_code_average_coverage" do
    before do
      @project_A_statis.stub(:period_module_lines_to_cover_pairs).and_return({'A_m1' => 50.0, 'A_m2' => 20.0, 'A_m3' => 30.0})
      @project_B_statis.stub(:period_module_lines_to_cover_pairs).and_return({'B_m1' => 5.0, 'B_m2' => 45.0})
      @project_C_statis.stub(:period_module_lines_to_cover_pairs).and_return({'C_m1' => 30.0, 'C_m2' => 50.0}) 
      @project_A_statis.stub(:period_module_covered_lines_pairs).and_return({'A_m1' => 20.0, 'A_m2' => 15.0, 'A_m3' => 25.0})
      @project_B_statis.stub(:period_module_covered_lines_pairs).and_return({'B_m1' => 0.0, 'B_m2' => 5.0})
      @project_C_statis.stub(:period_module_covered_lines_pairs).and_return({'C_m1' => 20.0, 'C_m2' => 0.0})
    end

    it "should return new line code average coverage" do
      @group_statis.new_line_code_average_coverage[:new_line_code_average_coverage].should == (((60.0 + 20.0) / (100.0 + 80.0)) * 100.0).try(:round, 2)
    end

    it "should return project and new lines to cover pairs" do
      @group_statis.new_line_code_average_coverage[:project_lines_to_cover_pairs].should == {@project_A.name => 100 , @project_C.name => 80}
    end

    it "should return project and new uncovered lines pairs" do
      @group_statis.new_line_code_average_coverage[:project_covered_lines_pairs].should == {@project_A.name => 60 , @project_C.name => 20}
    end
  end

  describe "average_coverage" do
    before do
      @project_A_statis.stub(:module_lines_to_cover_pairs).and_return({'A_m1' => 50.0, 'A_m2' => 20.0, 'A_m3' => 30.0})
      @project_B_statis.stub(:module_lines_to_cover_pairs).and_return({'B_m1' => 5.0, 'B_m2' => 45.0})
      @project_C_statis.stub(:module_lines_to_cover_pairs).and_return({'C_m1' => 30.0, 'C_m2' => 50.0}) 
      @project_A_statis.stub(:module_covered_lines_pairs).and_return({'A_m1' => 20.0, 'A_m2' => 15.0, 'A_m3' => 25.0})
      @project_B_statis.stub(:module_covered_lines_pairs).and_return({'B_m1' => 0.0, 'B_m2' => 5.0})
      @project_C_statis.stub(:module_covered_lines_pairs).and_return({'C_m1' => 20.0, 'C_m2' => 0.0})
      @project_A_statis.stub(:modules_average_coverage).and_return(60.0)
      @project_B_statis.stub(:modules_average_coverage).and_return(10.0)
      @project_C_statis.stub(:modules_average_coverage).and_return(25.0)
    end

    it "should return average coverage" do
      @group_statis.average_coverage[:average_coverage].should == (((60.0 + 20.0) / (100.0 + 80.0)) * 100.0).try(:round, 2)
    end

    it "should return project and coverage pairs" do
      @group_statis.average_coverage[:project_coverage_pairs].should == {@project_A.name => 60.0, @project_C.name => 25.0}
    end
  end


end
