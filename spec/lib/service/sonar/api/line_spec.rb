require 'spec_helper'

describe Service::Sonar::Api::Line do

  let(:full_name) { "com.dianping.foo" }
  let(:api) {  Service::Sonar::Api::Line.new(full_name) }
  let(:result) { File.read(File.expand_path("../../data/line.json", __FILE__)) }

  before do
    WebMock.stub_request(:get, "http://#{Settings.sonar['sonar_url']}/api/resources?#{Rack::Utils.build_query(api.params)}")
    .to_return(:status => 200, :body => result)
  end

  it "should format the result" do

    api.execute.result.should == {
      "lines" => { "val" => 54620, "frmt_val" => "54,620", "trends" => { "2013-07-03" => 103, "2013-06-27" => 126, "2013-06-04" => 9029 } },
      "ncloc" => { "val" => 35175, "frmt_val" => "35,175", "trends" => { "2013-07-03" => 59, "2013-06-27" => 75, "2013-06-04" => 5983 } },
      "classes" => { "val" => 450, "frmt_val" => "450", "trends" => { "2013-07-03" => 1, "2013-06-27" => 1, "2013-06-04" => 77 } },
      "files" => { "val" => 414, "frmt_val" => "414", "trends" => { "2013-07-03" => 1, "2013-06-27" => 1, "2013-06-04" => 66 } },
      "packages" => { "val" => 61, "frmt_val" => "61", "trends" => { "2013-07-03"=>0, "2013-06-27"=>0, "2013-06-04" => 15 } },
      "functions" => { "val" => 1606, "frmt_val" => "1,606", "trends" => { "2013-07-03" => 4, "2013-06-27" => 5, "2013-06-04" => 340 } },
      "accessors" => { "val" => 2007, "frmt_val" => "2,007", "trends" => { "2013-07-03"=>0, "2013-06-27"=>0, "2013-06-04" => 338 } },
      "statements" => { "val" => 13265, "frmt_val" => "13,265", "trends" => { "2013-07-03" => 22, "2013-06-27" => 22, "2013-06-04" => 2169 } }
    }
  end  

end
