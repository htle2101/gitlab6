require 'spec_helper'

describe Service::Sonar::Api::Base do

  let(:full_name) { "com.dianping.foo" }
  let(:api) {  Service::Sonar::Api::Base.new(full_name) }

  context "#execute" do
    before do
      WebMock.stub_request(:get, "http://#{Settings.sonar['sonar_url']}/api/resources?")
      .to_return(:status => 200, :body => "")
    end

    it "should return self for chainable call" do
      api.execute.should == api
    end
  end

  context "#format_date" do
    it "should do nothing when param is blank" do
      api.format_date(nil).should be_nil
    end

    it "should return yyyy-mm-dd" do
      api.format_date("2013-07-04T20:14:57+0800").should == "2013-07-04"
    end
  end
  
end
