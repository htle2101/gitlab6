require 'spec_helper'

describe Service::Sonar::Api::Coverage do

  let(:full_name) { "com.dianping.foo" }
  let(:api) {  Service::Sonar::Api::Coverage.new(full_name) }
  let(:result) { File.read(File.expand_path("../../data/coverage.json", __FILE__)) }

  before do
    WebMock.stub_request(:get, "http://#{Settings.sonar['sonar_url']}/api/resources?#{Rack::Utils.build_query(api.params)}")
    .to_return(:status => 200, :body => result)
  end

  it "should format the result" do

    api.execute.result.should == {
      :coverage=>57.5, :new_coverage=>{"2013-07-03"=>74.48, "2013-06-27"=>68.06, "2013-06-04"=>45.6}, :trend=>0.6
    }
  end  

end
