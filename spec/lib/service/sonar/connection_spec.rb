require 'spec_helper'

describe Service::Sonar::Connection do

  context "host" do
    it "should request deafult sonar by default" do
      Service::Sonar::Connection.new.host.should == Settings.sonar['sonar_url']
    end  

    it "should request other sonar when change host" do
      Service::Sonar::Connection.new('sonar.test').host.should == "sonar.test"
    end
  end

  context "#get" do
    before do
      WebMock.stub_request(:get,"http://#{Settings.sonar['sonar_url']}/test?foo=bar").to_return(:status => 200, :body => "ok")
    end

    it "should send request" do
      Service::Sonar::Connection.new.get("/test", {foo: 'bar'}).should == "ok"
    end
  end

end
