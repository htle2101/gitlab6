require 'spec_helper'

describe Service::Sonar::Request do
  let(:request) { Service::Sonar::Request.new }

  before do
    WebMock.stub_request(:get,"http://#{Settings.sonar['sonar_url']}/api/resources").to_return(:status => 200, :body => "ok")
  end

  context "#resources" do

    it "should call /api/resources" do
      request.conn.should_receive(:get).with('/api/resources', {}, {})
      request.resources({})
    end
  end
end  
