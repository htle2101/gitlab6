require 'spec_helper'

describe 'RemoteCi::TypeParser', no_db: true do

  before do 
    @error_xml = '<id>daq2121'
    @root_xml = %{
        <project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
        <parent>
          <artifactId>dianping-parent</artifactId>
          <groupId>com.dianping</groupId>
          <version>2.0.0</version>
        </parent>
        <modelVersion>4.0.0</modelVersion>
        <groupId>com.dianping</groupId>
        <artifactId>pay-unipay</artifactId>
        <name>pay-unipay</name>
        <version>0.1.4-SNAPSHOT</version>
        <packaging>pom</packaging>
        <modules>
          <module>pay-unipay-web</module>
          <module>pay-unipay-dal</module>
          <module>pay-unipay-service</module>
        </modules>
        <properties>
          <project.version>0.1.0</project.version>
          <project.service.version>0.1.7</project.service.version>
          <project.cashier.service.version>0.1.8</project.cashier.service.version>
          <env>dev</env>
          <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
          <project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>
        </properties>
        <build>
        </build>
      <repositories>  
      </repositories>  
      </project>
      }
    @pay_unipay_web_pom = %{
      <?xml version="1.0"?>
      <project
        xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd"
        xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
        <modelVersion>4.0.0</modelVersion>
        <parent>
          <artifactId>pay-unipay</artifactId>
          <groupId>com.dianping</groupId>
          <version>0.1.4-SNAPSHOT</version>
        </parent>
        <groupId>com.dianping</groupId>
        <artifactId>pay-unipay-web</artifactId>
        <name>pay-unipay-web</name>
        <version>0.1.0</version>
        <packaging>war</packaging>
        <properties>
          <env>dev</env>
          <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
          <project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>
        </properties>
        <dependencies>
        </dependencies>
        <build>
          <resources>
            <resource>
              <directory>src/main/resources</directory>
              <filtering>true</filtering>
            </resource>
          </resources>

          <plugins>
            <plugin>
              <artifactId>maven-war-plugin</artifactId>
              <version>2.1-alpha-1</version>
              <configuration>
                <warName>ooxxooxx-${version}</warName>
                <encoding>UTF-8</encoding>
              </configuration>
            </plugin>
          </plugins>  
        </build>
        <profiles>
        </profiles>
      </project>
    } 

    @pay_unipay_dal_pom = %{
      <?xml version="1.0"?>
      <project
        xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd"
        xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
        <modelVersion>4.0.0</modelVersion>
        <parent>
          <artifactId>pay-unipay</artifactId>
          <groupId>com.dianping</groupId>
          <version>0.1.4-SNAPSHOT</version>
        </parent>
        <groupId>com.dianping</groupId>
        <artifactId>pay-unipay-dal</artifactId>
        <name>pay-unipay-dal</name>
        <version>0.1.0</version>
        <properties>
          <env>dev</env>
          <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
          <project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>
        </properties>
        <dependencies>
        </dependencies>
        <build>
          <resources>
            <resource>
              <directory>src/main/resources</directory>
              <filtering>true</filtering>
            </resource>
          </resources>

          <plugins>
            <plugin>
              <artifactId>maven-war-plugin</artifactId>
              <version>2.1-alpha-1</version>
              <configuration>
                <warName>pay-unipay-${env}-${version}</warName>
                <encoding>UTF-8</encoding>
              </configuration>
            </plugin>
          </plugins>  
        </build>
        <profiles>
        </profiles>
      </project>
    } 

    @mock_tree = ['root',[
      ['pom.xml',@root_xml],
      ['pay-unipay-web',[['pom.xml',@pay_unipay_web_pom],['file2','456']]],
      ['xx2',[]],
      ['pay-unipay-dal',[['xx3','xx3data'],['pom.xml',@pay_unipay_dal_pom]]]
      ]
    ]

    @mock_tree_instance = RemoteCi::TypeParser.new(FolderTree.new(@mock_tree),'',{ "${test.holder}" => 'holder_reset' })
  end

  context 'parse pom' do
    it { @mock_tree_instance.send(:parse_pom,@error_xml).should == {"type" => "" , "modules" => [], "version"=>"", "artifactId"=>"", "warName"=>"", "groupId"=>"", "name" => "", "leafNode" => true }}
    it { @mock_tree_instance.send(:parse_pom,@root_xml).should == {"type" => "pom" , "modules" => ["pay-unipay-web","pay-unipay-dal","pay-unipay-service"], "version"=>"0.1.4-SNAPSHOT", "artifactId"=>"pay-unipay", "warName"=>"pay-unipay", "groupId" => "com.dianping", "name" => "pay-unipay", "leafNode" => false }}
    it { @mock_tree_instance.send(:parse_pom,@pay_unipay_web_pom).should == {"type" => "war" , "modules" => [], "version"=>"0.1.0", "artifactId"=>"pay-unipay-web", "warName"=>"ooxxooxx-0.1.0", "groupId" => "com.dianping", "name" => "pay-unipay-web", "leafNode" => true } }
  end

  context 'reset placeholder ' do
    it { @mock_tree_instance.send(:reset_placeholder,"${test.holder}").should == 'holder_reset'}
  end  

  context 'branch parse' do
    it { @mock_tree_instance.pom_parser('root').map{|m| m.raw.except(:paas) }.should == [{:module_name=>"pay-unipay",
                                                            :type=>"pom",
                                                            :path=>"",
                                                            :version=>"0.1.4-SNAPSHOT",
                                                            :groupId=>"com.dianping",
                                                            :full_name=>"com.dianping:pay-unipay",
                                                            :warName=>"pay-unipay",
                                                            :ut => nil,
                                                            :leafNode => false },
                                                           {:module_name=>"pay-unipay-web",
                                                            :type=>"war",
                                                            :path=>"/pay-unipay-web",
                                                            :version=>"0.1.0",
                                                            :groupId=>"com.dianping",
                                                            :full_name=>"com.dianping:pay-unipay-web",
                                                            :warName=>"ooxxooxx",
                                                            :ut => nil,
                                                            :leafNode => true },
                                                           {:module_name=>"pay-unipay-dal",
                                                            :type=>"",
                                                            :path=>"/pay-unipay-dal",
                                                            :version=>"0.1.0",
                                                            :groupId=>"com.dianping",
                                                            :full_name=>"com.dianping:pay-unipay-dal",
                                                            :warName=>"pay-unipay",
                                                            :ut => nil,
                                                            :leafNode => true }]
                                                          }
  end

  context 'error_message' do
    it { @mock_tree_instance.error_message.should == "[ERROR] warName:ooxxooxx-${version} may error.\n[WARN] warName is not equal to ${artifactId}-${env}-${version}[WARN] artifactId:pay-unipay-dal doesn't have project.packaging.\n" }
  end  

end

