require 'spec_helper'

describe Utils::Logger do

  let(:cls) do
    klass = Struct.new("LoggerTest")
    klass.send(:include, Utils::Logger)
    klass
  end

  after do
    Struct.send(:remove_const, :LoggerTest)
  end

  it "should define an instance variable @_logger_file_name with class name" do
    cls.logger_file.should == "struct-logger-test.log"
  end

  it "should override logger file name with set_logger_file_name" do
    cls.logger_file = "my-log-file.log"
    cls.logger_file.should == "my-log-file.log"
  end

  it "should override class logger file name" do
    inst = cls.new
    inst.logger_file = "foo/bar.log"
    inst.logger_file.should == "foo/bar.log"
    cls.logger_file.should == "struct-logger-test.log"
  end

end
