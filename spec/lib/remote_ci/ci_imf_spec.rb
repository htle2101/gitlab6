require 'spec_helper'

describe RemoteCi::CiImf do
  subject { RemoteCi::CiImf.new(create(:ci_branch)) }

  describe "Respond to" do
    it { should respond_to(:check) }
    it { should respond_to(:pom_datasource) }
    it { should respond_to(:jenkins_datasource) }
    it { should respond_to(:machines) }
    it { should respond_to(:job_status) }
    it { should respond_to(:jenkins_modules) }
    it { should respond_to(:build_time) }
    it { should respond_to(:build_link) }
    it { should respond_to(:build_log_link) }
    it { should respond_to(:modules) }
  end

  context "#modules" do
    before do 
      @ci_imf = RemoteCi::CiImf.new(create(:ci_branch))
      @ci_imf.stub(:pom_datasource).and_return({ web: RemoteCi::PomModule.new(module_name: 'web'), service: RemoteCi::PomModule.new(module_name: 'service')})
      @ci_imf.stub(:jenkins_modules).and_return({ web: 'jenkins_web_imf', service: 'jenkins_service_imf' })
      @ci_imf.stub(:machines).and_return({ web: 'machine_web_imf' })
    end   

    it "include jenkins and machines imformation" do
      @ci_imf.modules.values.map{|imf| imf.raw }.should == [{ module_name: "web", machine: "machine_web_imf", jenkins: "jenkins_web_imf"}, { module_name: "service", jenkins: "jenkins_service_imf"}]
    end
  end
end
