require 'spec_helper'

describe RemoteCi::WarDeployer do
  let(:build_branch) { create(:ci_branch, jenkins_job_name: "beta-project-master") }
  let(:group) { create(:group, name: 'wargroup')  }
  let(:project) { create(:project, namespace: group) }
  let(:deployer) { RemoteCi::WarDeployer.new(build_branch,{"build_number" => 11}) }
  
  describe "deal with project-related cache" do
    before do
      deployer.stub(:deploy_all?).and_return(true)
      deployer.stub(:collect_jenkins_status).and_return(true)
      deployer.stub(:build_success?).and_return(false)
      build_branch.stub(:project).and_return(project)
      deployer.stub(:deployers).and_return([])
      Rails.cache.fetch("project_base_query:#{project.id}") do
        "test data,no meaning"
      end
    end

    it "should reload project-statistics cache " do
      deployer.deploy
      Rails.cache.read("project_base_query:#{project.id}").should == "test data,no meaning"
    end
  end
end