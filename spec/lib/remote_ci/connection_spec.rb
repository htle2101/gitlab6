require 'spec_helper'

describe RemoteCi::Connection do
  let(:box) { mock('connection').as_null_object }

  it "should create ssh connect once when first user/password is corrent" do
    Rye::Box.stub(new: box)
    Rye::Box.should_receive(:new).once
    RemoteCi::Connection.new('fackhost')
  end

  it "should try create ssh connect twice when first user/password is incorrent" do
    Rye::Box.stub(:new) { raise Timeout::Error.new 'timeout' }
    Rye::Box.should_receive(:new).twice
    expect { RemoteCi::Connection.new('fackhost') }.to raise_error(RemoteCi::ConnectionError)
  end

  it "should try linux command with rye box" do
    Rye::Box.any_instance.stub(connect: true)
    Rye::Box.any_instance.should_receive(:pwd).once
    RemoteCi::Connection.new('fackhost').pwd
  end
end
