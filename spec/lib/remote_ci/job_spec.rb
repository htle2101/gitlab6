require 'spec_helper'

describe RemoteCi::Job do
  let(:client) { mock('client') }
  let(:job_name) { "foo" }
  let(:job) { RemoteCi::Job.new(client, job_name) }
  before do
    @details = {
      "displayName" => job_name,
      "name" => job_name,
      "url" => "http://host/job/foo",
      "builds" => [
        {"number" => 1, "url" => "http://host/job/foo/1/", "time" => "2013-04-17_18-48-46", "result" => "SUCCESS", "timestamp" => 1366195726754},
        {"number" => 2, "url" => "http://host/job/foo/2/", "time" => "2013-04-17_18-48-46", "result" => "SUCCESS", "timestamp" => 1366195726754}
      ],
      "color" => "blue",
      "status" => "success",
      "modules" => [
          {"name" => "com.foo:mod-one", "url" => "http://host/job/foo/com.foo$mod-one/", "color" => "blue", "displayName" => "mod-one"},
          {"name" => "com.foo:mod-two", "url" => "http://host/job/foo/com.foo$mod-two/", "color" => "blue", "displayName" => "mod-two"}
      ]
    }
    @time_resp = {
      "builds"=>[
        {"id" => "2013-04-17_18-48-46", "number" => 1, "result" => "SUCCESS", "timestamp" => 1366195726754},
        {"id" => "2013-04-17_18-38-07", "number" => 2, "result" => "SUCCESS", "timestamp" => 1366195087892}
      ]
    }
    client.stub_chain(:job, :list_details).and_return(@details)
    client.stub_chain(:job, :color_to_status).and_return("success")
    client.stub_chain(:api_get_request).and_return(@time_resp)
  end

  it "should return build result with status and timestamps" do
    job.build_result["status"].should == "success"
    job.build_result["modules"].first["status"].should == "success"
    job.build_result["builds"].first["time"].should == @time_resp["builds"].first["id"]
    job.build_result["builds"].first["result"].should == @time_resp["builds"].first["result"]
    job.build_result["builds"].first["timestamp"].should == @time_resp["builds"].first["timestamp"]
  end

  it "should return a hash which key is displayName" do
    job.hashable_modules.keys.should include("mod-one", "mod-two")
  end


  it "should return a hash which key is number" do
    job.hashable_builds.keys.should include(1, 2)
  end
end
