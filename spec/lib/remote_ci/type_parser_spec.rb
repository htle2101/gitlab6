require 'spec_helper'

describe RemoteCi::TypeParser do
  before do
    @project = create :project_with_code
    @parser = RemoteCi::TypeParser.new(@project.tree.raw_tree)
  end

  context 'pom_parser' do
    it 'should parse with pom.xml' do
      @parser.tree.stub('/').with('pom.xml').and_return(true)
      @parser.should_receive(:parse_pom)
      @parser.should_receive(:create_java_modules)

      @parser.pom_parser
    end

    it 'should parse with package.json' do
      @parser.tree.stub('/').with('pom.xml').and_return(nil)
      @parser.tree.stub('/').with('package.json').and_return(true)
      @parser.should_receive(:parse_package)
      @parser.should_receive(:create_nodejs_modules)

      @parser.pom_parser
    end
  end

  it 'should parse_pom' do
    pom = double()
    pom.stub(:data).and_return(File.read("#{Rails.root}/spec/fixtures/pom.xml"))

    result = @parser.parse_pom(pom)

    result['type'].should_not be_blank
    result['modules'].should_not be_blank
  end

  it 'should create_java_modules' do
    pom = double()
    pom.stub(:data).and_return(File.read("#{Rails.root}/spec/fixtures/pom.xml"))
    result = @parser.parse_pom(pom)

    modules = @parser.create_java_modules(result)

    modules.should_not be_blank
    modules.first.should be_instance_of(RemoteCi::PomModule)
  end

  it 'should parse_package' do
    pom = double()
    pom.stub(:data).and_return(File.read("#{Rails.root}/spec/fixtures/package.json"))

    result = @parser.parse_package(pom)

    result['version'].should_not be_blank
    result['name'].should_not be_blank
  end

  it 'should create_nodejs_modules' do
    pom = double()
    pom.stub(:data).and_return(File.read("#{Rails.root}/spec/fixtures/package.json"))
    result = @parser.parse_package(pom)

    modules = @parser.create_nodejs_modules(result)

    modules.should_not be_blank
    modules.first.should be_instance_of(RemoteCi::PomModule)
  end
end
