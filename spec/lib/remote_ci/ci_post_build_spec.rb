require 'spec_helper'

describe RemoteCi::CiPostBuild do
  let(:admin) {create(:admin, name: "Administrator")}
  let(:build_branch) {create(:build_branch, jenkins_job_name: "myjob")}
  before do
    admin.stub(authentication_token: "abcxyz")
    User.stub(find_by_name: admin)
  end

  it "should return deploy api call command" do
    RemoteCi::CiPostBuild.command(build_branch).should == %Q[curl -XPOST --header "PRIVATE-TOKEN: abcxyz" "http://#{Settings.gitlab['host']}/api/v3/ci/myjob/deploy"]
  end
end
