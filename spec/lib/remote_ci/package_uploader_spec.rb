require 'spec_helper'

describe RemoteCi::PackageUploader do

  let(:product) { create(:product) }
  let(:project) { create(:project_with_code) }
  let(:rollout_branch) { create(:rollout_branch, project: project, ci_env: product, jenkins_job_name: "product-foo-master") }
  let(:packing) { create(:packing, rollout_branch: rollout_branch, status: 5, module_names: "foo-web") }
  let!(:packing_module) { create(:packing_module, packing: packing, name: "foo-web") }
  let(:build_number) { 10 }
  let(:uploader) { RemoteCi::PackageUploader.new(packing, build_number) }

  context "#check_build_status" do
    before do
      @mock_client = MockJenkinsApi::Client.new('product')
      CiEnv.stub(:servers).and_return({ 'product' => @mock_client })
    end

    it "should return false when build status is not SUCCESS" do
      @mock_client.stub(api_get_request: {"result" => "FAILED"})
      uploader.check_build_status == false
    end

    it "should return true when build status is SUCCESS" do
      @mock_client.stub(api_get_request: {"result" => "SUCCESS"})
      uploader.check_build_status == true
    end
  end

  context "#upload_packages" do
    before do
      @modules = [
        RemoteCi::PomModule.new({ module_name: "foo-web", type: 'war', path: '/foo/foo-web', :version => "0.1.0" }),
        RemoteCi::PomModule.new({ module_name: "foo-service", type: 'jar', path: '/foo/foo-service', :version => "0.2.0" })
      ]
      rollout_branch.stub(modules: @modules)
    end

    it "should return two failed package" do
      RemoteCi::Ftp.any_instance.stub(upload: false)
      success, failed = uploader.upload_packages
      success.size.should == 0
      failed.size.should == 1
    end

    it "should return two success package" do
      RemoteCi::Ftp.any_instance.stub(upload: true)
      success, failed = uploader.upload_packages
      success.size.should == 1
      failed.size.should == 0
    end
  end

  context "#upload_packing" do
    it "should update status to upload_failed when failed war available" do
      uploader.update_packing({}, {"foo-web" => false})
      packing.status.should == RolloutPacking::STATUS_LIST[:upload_failed]
    end

    it "should update status to upload_success and update war urls when all packages upload successfully" do
      uploader.update_packing({"foo-web" => "/foo/web/bar.war"}, {})
      packing.war_urls.should == {"foo-web" => "/foo/web/bar.war"}
      packing.status.should == RolloutPacking::STATUS_LIST[:upload_success]
    end
  end

  context "#upload" do
    it "should update status to build failed when #check_build_status return false" do
      uploader.stub(check_build_status: false)
      uploader.upload
      packing.status.should == RolloutPacking::STATUS_LIST[:build_failed]
    end

    it "should upload war can update packing when #check_build_status return true" do
      uploader.stub(check_build_status: true)
      uploader.stub(upload_packages: [{"foo-web" => "/foo/web/bar.war"}, {}])
      uploader.should_receive(:upload_packages)
      uploader.should_receive(:update_packing)
      uploader.upload
    end

  end

end
