require 'spec_helper'

describe RemoteCi::Command::SudoAccount do
  let(:conn) { mock('connection').as_null_object }
  let(:sudo_account) {'foo'}
  let(:sudo_password) {'bar'}
  let(:sa) { RemoteCi::Command::SudoAccount.new('fackhost', {sudo_account: sudo_account, sudo_password: sudo_password}) }
  before do
    RemoteCi::Connection.stub(new: conn)
    sa.stub(conn: conn)
    Rye::Box.stub(new: conn)
  end

  it "should add a user" do
    sa.stub(encrypt_password: 'foo')
    conn.should_receive(:sudo_exec).with("useradd -p 'foo' -m -s /bin/bash #{sudo_account}")
    sa.add_user
  end

  it "should return a encrypted password" do
    conn.should_receive(:execute).with("echo '#{sudo_password}' | openssl passwd -1 -stdin")
    sa.encrypt_password
  end

  it "should reset password for a user" do
    sa.stub(encrypt_password: 'foo')
    conn.should_receive(:sudo_exec).with("usermod -p 'foo' #{sudo_account}")
    sa.reset_password
  end

  it "should revoke sudo privilege" do
    conn.should_receive(:sudo_exec).with("sed -i /^#{sudo_account}.*ALL$/d /etc/sudoers")
    sa.revoke_sudo_privilege
  end

  it "should grant sudo privilege to user" do
    sa.should_receive(:revoke_sudo_privilege)
    conn.should_receive(:sudo_exec).with("echo '#{sudo_account} ALL=(ALL) ALL' >> /etc/sudoers")
    sa.grant_sudo_privilege
  end

  it "should delete user" do
    conn.should_receive(:sudo_exec).with("userdel -r #{sudo_account}")
    sa.delete_user
  end

  context "#user_created?" do
    it "should return true when user create successfully" do
      conn.stub(execute: nil)
      sa.send(:user_created?).should be_true
    end

    it "should return false when create user failed" do
      conn.stub(:execute) { raise "something error" }
      sa.send(:user_created?).should be_false
    end
  end

  context "#up" do
    it "should invoke create once and return account and password pair user has been created" do
      sa.stub(user_created?: true)
      sa.should_receive(:reset_password).once
      sa.should_receive(:grant_sudo_privilege).once
      sa.up.should == {account: sudo_account, password: sudo_password}
    end

    it "should reset user password when user does not exist" do
      sa.stub(user_created?: false)
      sa.should_receive(:add_user).once
      sa.should_receive(:grant_sudo_privilege).once
      sa.up.should == {account: sudo_account, password: sudo_password}
    end

    it "should raise error when command error has been raised" do
      sa.stub(:user_created?) { raise "something error" }
      expect { sa.up }.to raise_error
    end
  end
end
