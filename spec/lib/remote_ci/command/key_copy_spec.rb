require 'spec_helper'

describe RemoteCi::Command::KeyCopy do
  let(:conn) { mock('connection').as_null_object }
  let(:sudo_account) {'foo'}
  let(:sudo_password) {'bar'}
  let(:env) { create(:ci_env) }
  let(:kc) { RemoteCi::Command::KeyCopy.new('fackhost', envs: [env.name]) }
  let(:key) { "i am a key" }
  before do
    kc.stub(conn: conn)
    Rye::Box.stub(new: conn)
    @env_conn = mock('connection')
    @env_conn.stub_chain(:cat, :[] => key)
    CiEnv.any_instance.stub(:conn).and_return(@env_conn)
    CiEnv.stub(with_out_product: [env])
    conn.stub(user: sudo_account)
    kc.stub(key_exists?: false)
  end

  context "execute" do
    it "should add ssh pub key of alpha/beta ci to target host" do
      @env_conn.should_receive(:cat).with("/root/.ssh/id_rsa.pub")
      @env_conn.should_receive(:disconnect)

      conn.should_receive(:execute).with("mkdir -p /home/foo/.ssh")
      conn.should_receive(:execute).with("echo '#{key}' >> /home/foo/.ssh/authorized_keys")
      conn.should_receive(:disconnect)
      kc.execute
    end
  end
end
