require 'spec_helper'


describe 'RemoteCi::Jenkins', no_db: true do

  before do 
    @mock_remoteci_jenkins = RemoteCi::Jenkins.new(:server_ip => 'test', :username => 'test', :password => 'test')
  end

  context 'retry connection' do
  	it { @mock_remoteci_jenkins.retry.should == 'success' }
  end

end

