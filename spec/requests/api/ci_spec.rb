require 'spec_helper'

describe API::API do
  include ApiHelpers

  let(:user1)  { create(:user) }
  let(:admin) { create(:admin) }

  describe "POST /ci/:job_name/deploy" do
    let(:job_name) { "foo" }
    let(:resp) { "The deploy task has been added to the queue." }
    let(:ci_branch) { create(:ci_branch, job_name) }

    context "when authenticated as user" do
      it "should not deploy the package to server" do
        post api("/ci/#{job_name}/deploy", user1)
        response.status.should == 403
      end
    end

    context "when authenticated as admin" do
      it "should return 404 when non existing ci_branch found" do
        post api("/ci/bar/deploy", admin)
        response.status.should == 404
      end

      it "should return deploy status" do
        post api("/ci/#{job_name}/deploy", admin)
        json_response['status'] == resp
      end
    end
  end


end
