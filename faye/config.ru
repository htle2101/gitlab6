require 'faye'
require 'yaml'
require 'multi_json'
require 'logger'
require 'faye/redis'

Faye::WebSocket.load_adapter('thin')


config_file = File.expand_path("resque.yml", "../config")

resque_url = if File.exists?(config_file)
               YAML.load_file(config_file)[ENV["RAILS_ENV"] || "development"]
             else
               "redis://localhost:6379"
             end

app = Faye::RackAdapter.new(
  mount: '/faye',
  timeout: 25,
  ping: 2
  #:engine  => {
    #:type  => Faye::Redis,
    #:uri   => resque_url
  #}
)

run app
