#coding:utf-8
module API
  # ci API
  class Machines < Grape::API
    #before { authenticate! }

    namespace 'machines' do

      # deploy all war build from ci to server
      #
      # Parameters:
      #   job_name (required) - The name of the ci job
      # Example Request:
      #  POST /ci/:job_name/deploy
      #
      get "info/:module_name" , requirements: { module_name: /(\w|\.|_|-)+/ } do
        return "no found!" if params[:module_name].blank?
        info = VirtualMachine.list_machines(params[:module_name])
        return "no #{params[:module_name]} machines on gitpub" if info.blank?
        info
      end

    end
  end
end

