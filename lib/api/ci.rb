#coding:utf-8
module API
  # ci API
  class Ci < Grape::API
    #before { authenticate! }

    namespace 'ci' do

      # deploy all war build from ci to server
      #
      # Parameters:
      #   job_name (required) - The name of the ci job
      # Example Request:
      #  POST /ci/:job_name/deploy
      #
      post ":job_name/deploy" , requirements: { job_name: /(\w|\.|_|-)+/ } do
        authenticated_as_admin!
        build_branch = BuildBranch.find_by_jenkins_job_name(params[:job_name])
        if build_branch.blank?
          not_found!
        elsif build_branch.not_to_deploy?
          build_branch.wars rescue nil
          { status: "not to deploy" }
        else
          build_branch.wars rescue nil
          build_number = build_branch.ci_env.server.job.get_current_build_number(build_branch.jenkins_job_name)
          WarDeployWorker.perform_async(build_branch.id, 'build_number' => build_number)
          
          BetaPaasRolloutWorker.perform_async(build_branch.id)
          BuildLogWorker.perform_async(build_branch.id, build_number)
          { status: "The deploy task has been added to the queue." }
        end
      end

      get ":job_name/build" , requirements: { job_name: /(\w|\.|_|-)+/ } do
        ci_branch = CiBranch.find_by_jenkins_job_name(params[:job_name])
        if ci_branch
          ci_branch.build
          ci_branch.shadow_branch.build unless ci_branch.shadow_branch.blank?
          ci_branch.sonar_branch.build unless ci_branch.sonar_branch.blank?
          { status: true }
        else
          not_found!
        end
      end

      # you may add a webhook like http://localhost:3000/api/v3/ci/destroy?private_token=pBCPQ6BPEkq4NekS8txk
      post "/destroy" do
        puts 'destroy start'
        RemoteCi::WebHook.new(params).try_delete_ci_branch
        puts 'destroy complate'
        { status: true }
      end

      get ":job_name/set_percent" , requirements: { job_name: /(\w|\.|_|-)+/ } do
        machine_percent = params[:percent]
        $redis.hset("progress_bar_value","percent",machine_percent)
      end

      get ":job_name/get_percent" , requirements: { job_name: /(\w|\.|_|-)+/ } do
        $redis.hget("progress_bar_value","percent")
      end

      get ":job_name/status" , requirements: { job_name: /(\w|\.|_|-)+/ } do
        ci_branch = CiBranch.find_by_jenkins_job_name(params[:job_name])
        status_message = Hash.new
        ci_branch.machines.each do |machine|
          message = {:"#{machine.module_name}" =>"#{VirtualMachine::STATUS_LIST.invert[machine.status]}"}
          status_message = status_message.size > 0 ? status_message.merge(message) : message
        end
        if ci_branch.machines.length !=0
          progress_bar_value = ci_branch.progress_bar_value(params[:deploy_type])
        end

        # status_message.merge!(machine_percent: percent)
        status_message.merge!(machine_percent: progress_bar_value)
        status_message.to_json
        if ci_branch
          ci_last_build = RemoteCi::Job.new(ci_branch.ci_env.server, ci_branch.jenkins_job_name)
          a = {}
          begin
            Timeout::timeout(7) do
              globel = ci_last_build.build_result['status']
              unless globel.blank?
                a = ci_last_build.hashable_modules_status.merge({ globel: globel})
              end
            end
          rescue
          end
          a.merge({ status_message: status_message})
        else
          not_found!
        end
      end

      post ":job_name/phoenix_result" , requirements: { job_name: /(\w|\.|_|-)+/ } do
        ci_branch = CiBranch.find_by_jenkins_job_name(params[:job_name])
        build_number = ci_branch.ci_env.server.job.get_current_build_number(ci_branch.jenkins_job_name)
        phoenix_response = JSON.parse(params[:response])
        Service::Phoenix::Callback.new({ci_branch: ci_branch,
                                        module_name: CiBranch::PHOENIX_MODULE,
                                        build_number: build_number,
                                        phoenix_response: phoenix_response}).execute
      end

      get ":job_name/button_deploy" , requirements: { job_name: /(\w|\.|_|-)+/ } do
        build_branch = BuildBranch.find_by_jenkins_job_name(params[:job_name])
        build_branch.ci_env.server.job.build(build_branch.jenkins_job_name)
        $redis.lpush("button_deploy_params/#{params[:job_name]}",{package_name:params[:package_name], private_token: params[:private_token], package_id: params[:package_id]}.to_json)
      end

      get ":job_name/static_build" , requirements: { job_name: /(\w|\.|_|-)+/ } do
        return { success: false, message: 'cannot find the user ' } if current_user.blank?
        @rollout = BuildBranch.find(params[:id])
        module_key = @rollout.id.to_s + "---"+ @rollout.type+"---"+@rollout.jenkins_job_name
        $redis.set(module_key,params[:module_names])
        if ["201","200","302"].include?(@rollout.change_static_jenkins_and_build(params[:branch_name]))
          Logbin.logger('rolloutstatic').info('start static build', {
            project: @rollout.project.id,
            rollout: @rollout.id,
            commit: @rollout.project.repository.commit(@rollout.branch_name).id[0..9],
            user_id: current_user.id,
            type: @rollout.ci_env.name })
          return {success:true, message: "The task has been added to the queue."}
        else
          return {success:false, message: "Some error occurs when  add task to the queue."}
        end
      end

      get ":job_name/can_build", requirements: { job_name: /(\w|\.|_|-)+/ } do
        @rollout =  BuildBranch.find(params[:id])
        return  {success:false, message:"Sorry,同commit的ppe没有发布,不能发线上!"} if @rollout.ci_env.id == 3 && !@rollout.commit_equal_and_ppe_result_success?(params[:branch_name])
        return  {success:true, message:"您可以添加需要清缓存的module,然后进行发布!"}
      end

      post ":job_name/clear_cache",  requirements: { job_name: /(\w|\.|_|-)+/ } do
          CacheClearWorker.perform_async(params[:job_name])
      end

      post "update_package"  do
        rpm = RolloutPackingModule.find_by_url(params[:url])
        if rpm.blank?
          not_found!
        else
          tag = rpm.packing.tag
          if !params[:package_id].blank?
            package = Package.find(params[:package_id])
          else
            package = GroupingStrategy.find(params[:gs_id]).package if !params[:gs_id].blank?
          end
          package.update_attributes(:tag=>tag,:url=>params[:url],:packing_id=>rpm.packing_id,:packing_module_id=>rpm.id) if package.url.nil?
        end
      end

      post "collect_groups", requirements: { job_name: /(\w|\.|_|-)+/ } do
        id = ""
        project = Project.find(params[:project_id])
        project.tickets.each do |t|
          id = id + t.id.to_s + ","
        end
        id = id.chop
        result = GroupingStrategy.where("ticket_id in (#{id}) and appname = '#{params[:package_name]}'").map{|gs|gs.name}
      end

      post ":job_name/upload/:packing_number/:build_number" , requirements: { job_name: /(\w|\.|_|-)+/, packing_number: /\d+/, build_number: /\d+/ } do
        rollout_branch = RolloutBranch.find_by_jenkins_job_name(params[:job_name])
        packing = rollout_branch.packings.find(params[:packing_number])
        if packing
          if packing.status == RolloutPacking::STATUS_LIST[:upload_success]
            { status: "The war_upload task has been added to the queue." }
          else
            PackageUploadWorker.perform_async(packing.id, params[:build_number])
            { status: "The war_upload task has been added to the queue." }
          end
        else
          not_found!
        end
      end

      post ":job_name/setdeploy" , requirements: { job_name: /(\w|\.|_|-)+/ } do
        ci_branch = CiBranch.find_by_jenkins_job_name(params[:job_name])
        if ci_branch
          ci_branch.update_attributes(auto_deploy: params[:auto_deploy])
          { status: true }
        else
          not_found!
        end
      end

      post ":job_name/settest" , requirements: { job_name: /(\w|\.|_|-)+/ } do
        ci_branch = CiBranch.find_by_jenkins_job_name(params[:job_name])
        if ci_branch
          begin
            ci_branch.change_run_test(params[:run_test])
            { status: true, message: "finished." }
          rescue => e
            { status: false, message: "Some error occured." }
          end  
        else
          not_found!
        end
      end

      get "sync_jobs" do
        if params[:from].blank? || params[:to].blank? || params[:job_name].blank?
          not_found!
        else
          CiEnv.sync_jobs(params[:from], params[:to], params[:job_name])
        end 
      end
    end
  end
end

