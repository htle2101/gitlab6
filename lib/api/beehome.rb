module API

  class Beehome < Grape::API

    namespace 'beehome' do

      get "/" do
        { beehome: '123' }
      end

      get "/update_machine" do
        vm = VirtualMachine.find_by_ip(params[:ip])
        not_found! if vm.blank?
        vm.release
        { status: 0, message: 'success' } 
      end
    end 
  end
end

