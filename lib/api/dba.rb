module API

  class DBA < Grape::API

    namespace 'dba' do

      helpers do
        def env_status_valid?(script, env, params)
          env_status = Script.const_get("#{env.upcase}_STATUS")
          env_status &&
            env_status.slice(:execute_failed, :execute_successed).values.include?(params[:status].to_i) &&
            (script.send("#{env}_not_execute?") || script.send("#{env}_executing?"))
        end
      end

      post "/scripts" do
        Logbin.logger("scripts").info("Tools dba callback with params.", params.merge(issue_id: params[:fileid]))
        script = Script.find(params[:fileid])
        vaild_status = Script::STATUS.slice(:dba_in_charge, :dba_review_passed)
        if script
          updated = if vaild_status.values.include?(params[:status].to_i)
            script.update_attribute(:status, params[:status])
          elsif params[:env].present? && Script::ENV_TYPE.values.include?(params[:env].to_i)
            env = Script::ENV_TYPE.invert[params[:env].to_i]
            if env_status_valid?(script, env, params)
              script.update_attributes({"#{env}_status" => params[:status], "#{env}_executed_at" => Time.now})
            end
          end
          script.update_attributes(req_url: params[:requrl]) if updated && params[:requrl].present?
          { status: (updated ||= false).to_s }
        else
          not_found!
        end
      end

    end 
  end
end

