module API

  class Checkers < Grape::API

    namespace 'checkers' do

      post ":checker_id/setchecked" do
        checker = Checker.find(params[:checker_id])
        if checker
          checked = (params[:checked] == 'true' ? true : false)
          checker.update_attributes(checked: checked)
          { status: true }
        else
          not_found!
        end
      end

    end 
  end
end

