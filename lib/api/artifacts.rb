module API

  class Artifacts < Grape::API

    namespace 'artifacts' do

      get "/" do
				Artifact.all.map(&:to_s).join(",")
      end

    end 
  end
end

