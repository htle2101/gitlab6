module API

  class Taurus < Grape::API

    namespace 'taurus' do
      post ":online_job_id/update_status" do
        online_job = OnlineJob.find(params[:online_job_id])
        { status: "The online job doesn't exist in gitpub system ." } && return unless online_job
        { status: "Status should be [0-9]." } && return unless params[:status] =~ /^\d$/
        { status: "Url is null.Should be provided." } && return if (params[:status] == "0") && (params[:createurl].blank? || params[:updateurl].blank?)
        Service::Taurus::Callback.new(params).execute
        { status: "ok" }

      end
    end

  end
end