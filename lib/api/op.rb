#coding:utf-8
module API
  # ci API
  class Op < Grape::API
    # before { authenticate! }

    namespace 'op' do
      post ":job_name/deploy/:module_name", requirements: { job_name: /(\w|\.|_|-)+/ } do
        ci_branch = CiBranch.find_by_jenkins_job_name(params[:job_name])

        if ci_branch
          forbidden! if ci_branch.beta? && !current_user.can_deploy_to_beta?(params[:module_name])
          build_number = ci_branch.ci_env.server.job.get_current_build_number(ci_branch.jenkins_job_name)
          WarDeployWorker.perform_async(ci_branch.id, 'module_name' => params[:module_name], 'build_number' => build_number)
          { status: 200 }
        else
          not_found!
        end
      end

      post 'alpha_restart_tomcat' do
        virtual_machine = VirtualMachine.find_by_ip(params[:machine_ip])

        if virtual_machine
          virtual_machine.update_attributes(status: 5)

          TomcatRestartWorker.perform_async(params[:machine_ip])

          status 204
        else
          status 404
        end
      end

      post 'lzm_restart_tomcat' do
        machine_ips = params[:machine_ips]
        virtual_machine = VirtualMachine.find_by_module_name_and_build_branch_id(params[:module_name], params[:build_branch_id])

        uuids = machine_ips.map do |machine_ip|
          response = Service::Workflow::Workflow.new.restart_tomcat(machine_ip)

          response['uuid'] if response
        end.compact

        if uuids.count == machine_ips.count
          if virtual_machine
            virtual_machine.set_workflow_uuids(uuids)

            virtual_machine.update_attributes(status: 5)
          end

          { uuids: uuids }
        else
          status 400
        end
      end

      get 'lzm_restart_tomcat_status' do
        uuids = params[:uuids]

        statuses = uuids.map do |uuid|
          response = Service::Workflow::Workflow.new.restart_tomcat_result(uuid)

          response['code'] if response
        end.compact

        # 2 for success
        # 1 for restarting
        # 0 for failed
        if statuses.count == uuids.count
          if statuses.uniq == [200]
            virtual_machine = VirtualMachine.find_by_module_name_and_build_branch_id(params[:module_name], params[:build_branch_id])

            if virtual_machine
              virtual_machine.delete_workflow_uuids

              virtual_machine.update_attributes(status: 7)
            end

            { status: 2 }
          elsif statuses.uniq.include?(400)
            virtual_machine = VirtualMachine.find_by_module_name_and_build_branch_id(params[:module_name], params[:build_branch_id])

            if virtual_machine
              virtual_machine.delete_workflow_uuids

              virtual_machine.update_attributes(status: 6)
            end

            { status: 0 }
          elsif statuses.uniq.include?(206)
            { status: 1 }
          end
        else
          status 400
        end
      end

      post 'beta_paas_restart_tomcat' do
        beta_paas_rollout = BetaPaasRollout.find(params[:beta_paas_rollout_id])

        if beta_paas_rollout
          app_id = params[:module_name]

          beta_paas_rollout.update_attributes(status: 100)

          instance_id = Service::BetaPaas::BetaPaas.new.get_instance_id(app_id)

          response = Service::BetaPaas::BetaPaas.new.restart_tomcat(app_id, instance_id)

          if response
            status 200

            { operation_id: response['data']['operationId'] }
          else
            status 400
          end

        end
      end

      get 'beta_paas_restart_tomcat_status' do
        opertion_id = params[:operation_id]

        response = Service::BetaPaas::BetaPaas.new.restart_tomcat_result(opertion_id)

        if response
          status = response['data']['operationStatus']

          if status/100 == 2
            beta_paas_rollout = BetaPaasRollout.find(params[:beta_paas_rollout_id])

            if beta_paas_rollout
              beta_paas_rollout.update_attributes(status: 200)
            end

            { status: 2 }
          elsif status/100 == 1

            { status: 1 }
          else
            beta_paas_rollout = BetaPaasRollout.find(params[:beta_paas_rollout_id])

            if beta_paas_rollout
              beta_paas_rollout.update_attributes(status: 102)
            end

            { status: 0 }
          end
        end
      end

      #$step = log_url
      get ":token/update_status/:status_id" do
        # authenticated_as_admin!
        #steps may realize

        build_number = RemoteCi::DigitLink.separate(params[:token])[:first]
        machine_id = RemoteCi::DigitLink.separate(params[:token])[:second]
        machine = VirtualMachine.find(machine_id)
        if machine
          machine.status = params[:status_id]
          machine.save
          RemoteCi::LoggerBinder.new(machine.build_branch, build_number: build_number, module_name: machine.module_name).bind_lzm_log(params[:step]) if params[:step]
          begin
            Service::AutoTest::Request.new.call(machine.build_branch.project_name_for_auto_test)
            # Service::AutoTest::Request.new.call(machine.build_branch.project_name_for_auto_test) if machine.build_branch.all_deploy_finished?
          rescue
          end  
          { status: 200 }
        else
          not_found!
        end
      end

      get "/grouping/edit" do
        gs = GroupingStrategy.find(params[:package_id])
        # Logbin.logger("rollout").info("Lzm/cmdb callback with params '#{params.to_h}'.", {package_id: params[:package_id]})
        Logbin.logger("rollout").info("Lzm callback with params '#{params.to_h.first(2)}'.",gs.log_params.merge(:action_type =>'Lzm_Callback'))
        if gs
          unless params[:status].blank?
            state = GroupingStrategy.state_machines[:status].states.state_at(params[:status].to_i)
            gs.update_status_when_callback?(state.name,params[:status]) if state
          end
          gs.log = params[:step] unless params[:step].blank?
          gs.save
          { status: 200 }
        else
          not_found!
        end
      end

      get "/packing/edit" do
        begin
          project = Project.where(:private_token => params["private_token"]).first
          war_urls = JSON.parse(params[:war_urls])
          rp = RolloutPacking.new(:build_branch_id => project.rollout_branch.id,
            :tag => params[:tag],:module_names => params[:module_names], :branch_name => params[:branch_name],
            :war_urls => war_urls,:commit => project.repository.find_branch(params[:branch_name]).commit.id,:author_id => project.owner.id,:status => 7)
          Logbin.logger("rollout").info("add packing_modules => #{params[:module_names]}  to project-#{project.name}-#{project.id}")
          if rp.save
            war_urls = rp.war_urls
            war_urls.keys.each do |key|
              rpm = RolloutPackingModule.new(:packing_id => rp.id,:name =>key,:url => war_urls["#{key}"])
              rpm.save
              unless project.blank?
                project_module = project.project_modules.find_or_initialize_by_module_name(key)
                project_module.save if project_module.new_record?
              end
            end          
          end
        rescue => e
          Logbin.logger("rollout").info(e)
          not_found!
        end
      end

      post "performance_test/callback" do
        begin
          performance_test = PerformanceTest.find_by_uid(params[:uid])
          performance_test.update_attributes(:status => params[:status],:test_log_url => params[:test_log_url])
        rescue => e
          Logbin.logger("performance_test").info(e)
        end
      end

      post "performance_test_machine/callback" do
        begin
          response_result = [true,[{"001"=>"192.168.78.187"},{"002"=>"192.168.78.189"}]]
          result = response_result[0]
          if result
            machines_callbacks = response_result[1]
            machines_callbacks.each do |machine|
              key = machine.keys.first
              performance_test_module = PerformanceTestModule.find(key)
              performance_test_module.update_attributes(machine:machine.values.first)
              #扔包和重启tomcat的code
              

            end

          end        
          
        rescue => e
          Logbin.logger("performance_test").info(e)
        end
      end

      post "merge_request_event/callback" do
        
        mr = MergeRequest.find(params[:merge_request_id])
        mri = mr.merge_request_inspection
        status = params[:status] == "0" ? 6 : 7
        begin
          mri.update_attributes(:status => status, :stdout => params[:stdout],:stderr => params[:stderr])

          status 204
        rescue => e
          Logbin.logger("merge_request_event").info(e)
          status 500
        end
      end

    end
  end
end
