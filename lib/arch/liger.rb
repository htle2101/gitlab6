module Arch
  class LigerFileParseError < StandardError; end

  class Liger
    attr_accessor :data
    def initialize(data)
      @data = data
    end

    def keys
      @keys ||= get_keys_from_data
    end

    def get_keys_from_data
      return nil if @data.blank?
      Hash.from_xml(@data)['ligers']['key'].map{|k| [k['name'], LigerKey.new(k)] }.to_h rescue nil
    end

    def self.diff(keys1, keys2, env)
      diffs = []
      (keys1.keys + keys2.keys).uniq.each do |name|
        if keys1[name].values[env] != keys2[name].values[env]
          diffs << {name: name, from: keys1[name].values[env], to: keys2[name].values[env] }
        end     
      end
      diffs
    end 
  end
end