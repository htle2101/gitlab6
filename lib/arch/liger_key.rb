module Arch
  class LigerKey
    attr_accessor :name, :values
    def initialize(hash, env = nil)
      if env.blank?
        init_with_xml(hash)
      else 
        init_with_http(hash, env)
      end  
    end

    def init_with_xml(hash)
      @name = hash["name"]
      @envs = hash.keys.grep(/_value$/)
      @values = {}
      @envs.each do |env|
        @values[env.gsub('_value', '')] = hash[env]
      end 
    end
    
    def init_with_http(hash, env)

    end  
  end
end    