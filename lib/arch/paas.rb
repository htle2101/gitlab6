module Arch
  class Paas
    attr_accessor :version, :name, :data
    def initialize(data)
      @data = data
      parse
    end

    def parse
      return if @data.blank?
      begin
        doc = Nokogiri::XML(@data)
        @version = doc.xpath('/application').attr('version').to_s
        @name = doc.xpath('/application').attr('name').to_s
      rescue
      
      end 
    end  
  end
end    