module Utils
  def wrap_common_params(hash)
    hash ||= {}
    Thread.current[:current_user] ? hash.merge(author: Thread.current[:current_user].username) : hash
  end

  extend self
end
