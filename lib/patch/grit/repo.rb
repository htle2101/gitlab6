module Grit

  class Repo
    def grep_with_one(searchtext, contextlines = 3, branch = 'master', path = '')
      context_arg = '-C ' + contextlines.to_s
      result = git.native(:grep, {}, '-n', '-E', '-i', '-z', '--heading', '--break', context_arg, searchtext, branch, '--', path)
      begin
        result = result.force_encoding('UTF-8')
      rescue
        result = result.encode('UTF-8', invalid: :replace, undef: :replace, replace: '')
      end
      greps = []
      filematches = result.split("\n\n")
      filematches.each do |filematch|
        binary = false
        file = ''
        matches = filematch.split("--\n")
        matches.each_with_index do |match, i|
          content = []
          startline = 0
          lines = match.split("\n")
          if i == 0
            text = lines.first
            lines.slice!(0)
            file = text[/^Binary file (.+) matches$/]
            if file
              binary = true
            else
              text.slice! /^#{branch}:/
              file = text
            end
          end
          lines.each_with_index do |line, j|
            line.chomp!
            number, text = line.split("\0", 2)
            if j == 0
              startline = number.to_i
            end
            content << text
          end
          greps << Grit::Grep.new(self, file, startline, content, binary)
        end
      end
      greps
    end    

    def diff_tree(oldrev, newrev, options = {})
      opts = {:timeout => false, :r => true}.merge(options)
      DiffTree.list_from_string(self, self.git.diff_tree(opts, oldrev, newrev).strip)
    end
  end

end
