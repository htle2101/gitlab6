require 'pathname'

module Grit

  class DiffFile
    attr_reader :repo, :id, :status, :path, :name
    def initialize(args)
      args.each_pair { |k,v| instance_variable_set(:"@#{k}", v) }
      pn = Pathname(@path)
      @name = pn.basename.to_s
      @path = pn.dirname.to_s == "." ? "" : "#{pn.dirname.to_s}/"
    end

    def blob
      Blob.create(repo, :id => id, name: name)
    end
  end

  class DiffTree
    attr_reader :repo, :diff_files

    def initialize(repo)
      @repo = repo
      @diff_files = []
    end

    def self.list_from_string(repo, text)
      diff_tree = DiffTree.new(repo)
      text.split("\n").each do |line|
        frags = line.split
        diff_tree.diff_files << DiffFile.new(repo: repo, id: frags[3], status: frags[4], path: frags[5])
      end
      diff_tree
    end

    def deleted_files
      diff_files.select { |file| file.status == "D" }
    end

    def added_files
      diff_files.select { |file| file.status == "A" }
    end

    def changed_files
      diff_files.select { |file| file.status == "M" }
    end

    def added_and_changed_files
      diff_files.select { |file| file.status == "M" || file.status == "A" }
    end
  end
end
