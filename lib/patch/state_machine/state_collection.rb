class StateMachine::StateCollection

  def state_at(value)
    detect { |state| state.value == value }
  end
end
