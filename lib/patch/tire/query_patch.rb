module Tire
  module Search
    class Query
      def wildcard(field, value)
        @value = { :wildcard => { field => value } }
      end
    end
    class Facet
      def terms_with_script(script, options={})
        @value[:terms] = {script: script}.update(options)
        self
      end
    end
  end
end
