module Gitlab
  module Git
    class Repository
      def search_files_with_one(query, path, ref = nil)
        if ref.nil? || ref == ""
          ref = root_ref
        end

        greps = repo.grep_with_one(query, 3, ref, path)

        return nil if greps.blank?

        Gitlab::Git::BlobSnippet.new(ref, greps.last.content, greps.last.startline, greps.last.filename)

      end
    end
  end
end
