module DianpingPaas
  class Client
    def aaa
      '11'
    end  

    def bbb
      '222'
    end

    def rollout(appId, version,groupId)
      get('/console/api/app', 'op' => 'upgrade', 'appId' => appId, 'version' => version,'groupId' =>groupId)
    end
    
    def rollout_status(appId, operationId)
      get('/console/api/app', 'op' => 'operationStatus', 'appId' => appId, 'operationId' => operationId)
    end

    def create1(appId, owner)
      get('/console/api/app', 'op' => 'add', 'app.appId' => appId, 'app.owner' => owner)
    end

    # def get
    #   '111'
    # end  
  
  end
end    