module RemoteCi
  class CiPostBuild
    class << self
      def command(build_branch)
        user = User.find_by_name('Administrator')
        %Q[curl -XPOST --header "PRIVATE-TOKEN: #{user.authentication_token}" "http://#{host_with_port}/api/v3/ci/#{build_branch.jenkins_job_name}/#{build_branch.command}"]
      end

      def host_with_port
        host = Settings.gitlab['host'].clone
        host << ":3000" if Rails.env.development?
        host
      end
    end
  end
end
