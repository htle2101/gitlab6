require 'nokogiri'
module RemoteCi
  class TypeParser

    attr_reader :tree, :path, :properties, :modules
    attr_accessor :errors

    def initialize(tree, path='', properties={})
      @tree = tree
      @path = path
      @properties = properties
      @errors = ''
    end

    def pom_parser(module_name = nil)
      return [] if tree.class == Grit::Submodule
      @modules ||= begin
        if (node = tree / 'pom.xml') != nil
          result = parse_pom(node)

          create_java_modules(result)

        elsif ((node = tree / 'zipfile.json') != nil )
          result = parse_package(node)

          create_zipfile_modules(result)

        elsif (node = tree / 'package.json') != nil
          result = parse_package(node)

          create_nodejs_modules(result)
        elsif (node = tree / 'config.json') != nil
          result = parse_config(node)

          create_php_modules(result)

        elsif ((node = tree / 'package.json') != nil ) || ((node = tree / 'cortex.json') != nil )

          result = parse_package(node)

          create_nodejs_modules(result)
        else
          []
        end
      end
    end

    def create_java_modules(result)
      modules = []

      modules << PomModule.new({
        module_name: result['artifactId'],
        type: result['type'],
        path: @path,
        version: result['version'],
        groupId: result['groupId'],
        full_name: "#{result['groupId']}:#{result['artifactId']}",
        warName: result['warName'],
        ut: result['ut'],
        leafNode: result['leafNode'],
        paas: Arch::Paas.new((tree / 'src/main/resources/WEB-INF/dae-web.xml').try(:data))
      })

      result['modules'].each do |m|
        if (node = tree / m.to_s) != nil
          child = TypeParser.new(node, "#{@path}/#{m}", properties)

          modules = modules + child.pom_parser(m)

          @errors = @errors + child.errors
        end
      end

      modules
    end

    # parse the nodejs project package.json file
    def parse_package(package)
      JSON.parse(package.data) rescue { 'name' => 'packge.json or cortex.json parser error' }
    end

    def create_nodejs_modules(result)
      modules = []

      modules << PomModule.new({
        type: 'nodejs',
        module_name: result['name'],
        path: @path,
        version: result['version']
      })

      modules
    end

    def create_php_modules(result)
      modules = []

      modules << PomModule.new({
        type: 'php',
        module_name: result['name'],
        path: @path,
        version: result['version']
      })

      if result['modules'].present?
        result['modules'].each do |submodule|
          modules << PomModule.new({
            type: 'php',
            module_name: submodule['name'],
            path: "/#{submodule['name']}",
            version: submodule['version']
          })
        end
      end

      modules
    end

    def create_zipfile_modules(result)
      modules = []

      modules << PomModule.new({
        type: 'zipfile',
        module_name: result['name'],
        warName: result['name'],
        path: @path,
        version: result['version']
      })

      if result['modules'].present?
        result['modules'].each do |submodule|
          modules << PomModule.new({
            type: 'zipfile',
            module_name: submodule['name'],
            warName: result['name'],
            path: "/#{submodule['name']}",
            version: submodule['version']
          })
        end
      end

      modules
    end


    def parse_config(config)
      JSON.parse(config.data) rescue { 'name' => 'config.json parser error' }
    end

    # parse the nodejs project package.json file
    def parse_package(package)
      JSON.parse(package.data) rescue { 'name' => 'packge.json parser error' }
    end

    def parse_pom(pom)
      result = {}

      pom = pom.data rescue pom

      doc = Nokogiri::XML(pom)
      doc.remove_namespaces!
      doc.xpath("/project/properties/*").each{|p| properties["${#{p.name}}"] = reset_placeholder(p.text) }
      doc.xpath("/project/*").map do |p| 
        properties["${#{p.name}}"] = reset_placeholder(p.text) if p.children.length == 1
      end

      result['type'] = reset_placeholder(doc.xpath("/project/packaging").text)
      result['type'] = 'jar' if result['type'].blank?
      result['type'] = doc.xpath("/project/properties/job-packaging").text if doc.xpath("/project/properties/job-packaging").try(:text).present?
      result['modules'] = doc.xpath("/project/modules/module").map{|m| m.text }
      result['version'] = reset_placeholder(doc.xpath("/project/version").text)
      result['version'] = reset_placeholder(doc.xpath("/project/parent/version").text) if result['version'].blank?
      result['artifactId'] = reset_placeholder(doc.xpath("/project/artifactId").text)
      result['groupId'] = reset_placeholder(doc.xpath("/project/groupId").text)
      result['groupId'] = reset_placeholder(doc.xpath("/project/parent/groupId").text) if result['groupId'].blank?

      result['warName'] = doc.xpath("/project/build/plugins//warName").text
      result['name'] = reset_placeholder(doc.xpath("/project/name").text)
      result['ut'] = doc.xpath("/project/properties/ut").text if doc.xpath("/project/properties/ut").try(:text).present?
      result['leafNode'] = result['modules'].blank? ? true : false

      check(result)

      result['warName'] = warName_filter(result['warName'],result['artifactId']) 

      result
    end

    def reset_placeholder(key)
      key.gsub(/\$\{(\w|\.|-)+\}/) { |match| properties[match].nil? ? match : properties[match] }
    end

    # f**k the hard code
    def warName_filter(warName, artifactId)
      return artifactId if warName.blank?

      nocheckname = reset_placeholder(warName.strip.gsub('-${version}', '').gsub('-${project.version}', '').gsub('-${project.env}', '').gsub('-${env}', ''))
      return artifactId if nocheckname.index('$').present?
      return nocheckname
    end

    def error_message
      pom_parser('root')

      @errors
    end

    def check(result)
      if result['name'].blank?
        @errors += "[ERROR] artifactId:#{result['artifactId']} has no name.\n"
      elsif result['artifactId'] != result['name']
        @errors += "[ERROR] name:#{result['name']} is not equal to artifactId:#{result['artifactId']}\n"
      end

      if result['type'].blank?
        @errors += "[WARN] artifactId:#{result['artifactId']} doesn't have project.packaging.\n"
      end

      if result['type'] == 'war'
        @errors += "[ERROR] warName:#{result['warName']} may error.\n" unless (result['warName'].include?('-${env}-${version}') || result['warName'].include?('-${env}-${project.version}'))
        @errors += "[WARN] warName is not equal to ${artifactId}-${env}-${version}" unless (result['warName'].include?('${artifactId}-${env}-${version}') || result['warName'].include?('${project.artifactId}-${env}-${project.version}') || result['warName'].include?('${project.artifactId}-${env}-${version}'))
      end
    end
  end
end
