module RemoteCi
  class GroupDeployer
    def initialize(params)
      @package_id = params["package_id"]
      @grouping_strategy_id = params["grouping_strategy_id"]
      @gate = params["gate"]
      @action = params["action"]
    end

    def deploy
      package = Package.find(@package_id)
      @gs = GroupingStrategy.find(@grouping_strategy_id)
      if @gs.name != 'static'
        if package.owner_is?('task')
          lion_key = get_lion_configs( package )
        end
        params = {"package_id"=>@grouping_strategy_id, "app"=>package.name, "package"=>package.url}        
        params = {"package_id"=>@grouping_strategy_id, "app"=>package.name, "package"=>package.url, "gate"=>@gate }  if @action =="deploy"
        params = params.merge("lion_key"=>lion_key) unless lion_key.blank?   
        Logbin.logger("rollout").info("Start to call lzm to deploy war [#{@gs.appname}][#{@gs.name}].", @gs.log_params)
        res = Service::Op::Lzm.new.grouping_rollout(@action, params)        
        status = (@action == "prerelease")?2:5  if res.code == 200        
        @gs.update_attributes(status:status)
        Logbin.logger("rollout").info("Finish call lzm/cmdb to deploy war.", @gs.log_params)
      else
        Logbin.logger("rollout").info("Start to call lzm to deploy static [#{@gs.appname}].", @gs.log_params)
        res = Service::Op::Lzm.new.static_package_rollout(@gs.appname, @gs.id, package.url)
        status = 6 if res.code != 200
        status = 5 if res.code == 200
        @gs.update_attributes(status:status)
        Logbin.logger("rollout").info("Finish call lzm/cmdb to deploy static.", @gs.log_params)
      end

    end

    protected
    def get_lion_configs(package)
      lion_key = ''
      if !@gs.swimlane.nil?
        tcs = TaskConfig.where(name: package.name, task_id: package.ticket.package_tasks.pluck(:id), type: 'Lion')
        lion_key = tcs.where(:swimlane => ["", @gs.swimlane]).map(&:key).compact.uniq.join('|')
      end
      lion_key
    end

  end
end  
