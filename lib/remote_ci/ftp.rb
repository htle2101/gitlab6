require 'pathname'
require 'net/ftp'

module RemoteCi
  class Ftp

    attr_reader :package, :packing

    def initialize(packing, package)
      @packing = packing
      @package = package
    end

    def upload
      `mkdir -p #{Settings.ftp['tmp_path']}`
      logger.info("Download package #{package.url} to #{temp_path}")
      logger.error("Download package #{package.url} Failed.") and return if !download_file_from_jenkins
      logger.info("Download successfully.")
      begin
        upload_packages
        `rm -f #{temp_path}`
      rescue => e
        logger.error("Upload Failed. #{e}")
        return nil
      end
      upload_path
    end

    def download_file_from_jenkins
      system("wget --quiet -O #{temp_path} #{package.url}")
    end

    def temp_path
      extname = File.extname(name)
      basename = File.basename(name, extname)
      "#{Settings.ftp['tmp_path']}/#{basename}-#{dir_name}#{extname}"
    end

    def upload_path
      "/#{package.ci_branch.jenkins_job_name}/#{dir_name}/#{name}"
    end

    def ftp_url
      ProjectModule.find_by_module_name_and_project_id(simple_true_name, package.ci_branch.project.id).try(:ftp_url) || Settings.ftp['url']
    end

    def upload_packages
      ftp = Net::FTP.new(ftp_url)
      ftp.login(Settings.ftp['username'], Settings.ftp['password'])
      ftp.passive = true
      logger.info("Connect to ftp server.")
      ftp.mkdir(package.ci_branch.jenkins_job_name) if ftp.ls(package.ci_branch.jenkins_job_name).blank?
      ftp.chdir(package.ci_branch.jenkins_job_name)
      logger.info("Change directory to #{package.ci_branch.jenkins_job_name}.")
      ftp.mkdir(dir_name) if ftp.ls(dir_name).blank?
      ftp.chdir(dir_name)
      logger.info("Change directory to #{dir_name}.")
      logger.info("Start to upload #{name}.")
      ftp.putbinaryfile(temp_path, name)
      logger.info("Upload successfully.")
      ftp.close
      upload_path
    end

    def dir_name
      "#{ts}_#{packing.tag}"
    end

    def name
      "#{simple_true_name}-#{package.ci_env.label}-#{package.version}.#{package.ext}"
    end

    def simple_true_name
      if package.ci_branch.nodejs? || package.ci_branch.zipfile? || package.ci_branch.php? || package.ci_branch.cmdb_php?
        package.module_name
      else
        package.warName
      end
    end

    def logger
      packing.logger
    end

    def ts
      @ts ||= Time.now.strftime("%Y-%m-%d_%H-%M-%S")
    end

    def download_file_from_ftp
      begin
        logger.info('Connect to ftp server.')
        ftp = Net::FTP.new(Settings.ftp['url'])
        ftp.login(Settings.ftp['username'], Settings.ftp['password'])
        ftp.passive = true
        ftp.getbinaryfile("#{Settings.ftp['root_path']}#{package.url}", downloaded_file)
        logger.info('download war success.')
        return true
      rescue => e
        logger.error("download war error. #{e}")
        return false
      end
    end

    def downloaded_file
      "#{Settings.ftp['tmp_path']}/#{File.basename(package.url)}"
    end

    def upload_to_paas
      if download_file_from_ftp
        begin
          logger.info('begin upload war to paas.')

          module_name = package.warname || package.name

          url = ProjectModule.find_by_module_name_and_project_id(module_name, package.project.id).paas_url

          DianpingPaas::Client.new(url: url).upload_war(module_name, packing.paas_version, downloaded_file)

          logger.info('upload war to paas success.')

          `rm -r #{downloaded_file}`

          logger.info("deleted temp file #{downloaded_file} success.")

          return true
        rescue => e
          logger.error("upload war to paas error. #{e}")

          return false
        end
      end
      return false
    end
  end
end
