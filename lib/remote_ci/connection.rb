module RemoteCi
  class ConnectionError < StandardError; end
  class Connection
    include Utils::Logger

    attr_reader :host, :port, :user, :passwords, :timeout, :conn

    def initialize(host, options = {})
      @host = host
      @port = options[:port] || Settings.dp['ssh_port']
      @user = options[:user].present? ? options[:user] : Settings.dp['ssh_user']
      @passwords = Array.wrap(options[:password].present? ? options[:password] : Settings.dp['ssh_password'])
      @timeout = options[:timeout] || 4
      connect
    end

    def connect
      passwords.each do |password|
        begin
          Timeout::timeout(timeout) do
            @conn = Rye::Box.new(host, port: port, user: user, password: password, safe: false)
            @conn.connect
            return @conn
          end
        rescue SystemCallError, Timeout::Error, Errno::EHOSTUNREACH => e
          logger.warn("Can not login to `#{host}:#{port}` with user `#{user}` and `#{password}`, #{e}.")
        end
      end
      raise ConnectionError.new("Can not login to `#{host}:#{port}` with user `#{user}` and `#{passwords}`.")
    end

    def method_missing(name, *args, &block)
      options = args.extract_options!
      Timeout::timeout(options[:timeout] || timeout) do
        conn.send(name, *args, &block)
      end
    end

    def disconnect
      conn.disconnect
    end
  end

end
