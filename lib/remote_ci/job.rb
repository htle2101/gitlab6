module RemoteCi
  class Job
    attr_reader :client, :job_name
    attr_accessor :revisions
    def initialize(client, job_name)
      @client = client
      @job_name = job_name
      @revisions = {}
    end

    def build_result
      @build_result ||= begin
        details = client.job.list_details(job_name)
        color_to_status(details)
        merge_timestamp(details)
        # merge_revision(details)
        details
      rescue JenkinsApi::Exceptions::NotFoundException => e
        {}
      end
    end

    def hashable_modules
      @hashable_modules ||= hashable(build_result["modules"], "displayName")
    end

    def hashable_modules_status
      result = {}
      hashable_modules.each_key do |key|
        result[key] = hashable_modules[key]['status']
      end
      result
    end

    def hashable_builds
      @hashable_builds ||= hashable(build_result["builds"], "number")
    end

    def last_success_build_number
      hashable_builds.values.each do |build|
        return build['number'] if build['result'] == 'SUCCESS'
      end
      return []
    end

    def last_success_build_revision
      return '' if last_success_build_number.blank?
      @last_success_build_revision ||= client.api_get_request("/job/#{job_name}/#{last_success_build_number}/git/")['lastBuiltRevision']['SHA1']
    end

    def get_build_revision(build_number)
      return revisions[build_number] unless revisions[build_number].blank? 
      revision = (client.api_get_request("/job/#{job_name}/#{build_number}/git/")['lastBuiltRevision']['SHA1'][0..9] rescue "")
      revisions[build_number] = revision
      return revision
    end

    def get_build_revisions
      build_result["builds"].each_with_index do |build, index|
        res ||= begin
          get_build_revision(build['number'])
        rescue
          ""
        end
        build.merge!({"revision" => res[0..9]})
      end
      @build_result
    end

    private
    def color_to_status(details)
      details["modules"].each { |mod| mod["status"] = client.job.color_to_status(mod["color"]) } if !details["modules"].nil?
      details["status"] = client.job.color_to_status(details["color"])
    end

    def merge_timestamp(details)
      resp = client.api_get_request("/job/#{job_name}", "tree=builds[number,status,timestamp,id,result]")
      details["builds"].each_with_index do |build, index|
        build.merge!({
          "time" => resp["builds"][index]["id"], "result" => resp["builds"][index]["result"], "timestamp" => resp["builds"][index]["timestamp"]
        })
      end
    end

    def merge_revision(details)
      details["builds"].each_with_index do |build, index|
        res ||= begin
          client.api_get_request("/job/#{job_name}/#{build['number']}/git/")['lastBuiltRevision']['SHA1']
        rescue
          ""
        end
        build.merge!({"revision" => res[0..9]})
      end
    end

    def hashable(array, key)
      return {} if array.blank?
      array.inject({}) do |results, element|
        results[element[key]] = element
        results
      end
    end

  end
end
