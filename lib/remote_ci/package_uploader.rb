module RemoteCi
  class PackageUploader
    attr_reader :rollout_branch, :packing, :build_number
    def initialize(packing, build_number)
      @packing = packing
      @rollout_branch = @packing.rollout_branch
      @build_number = build_number
    end

    def check_build_status
      result = false
      begin
        client = rollout_branch.ci_env.server

        resp = client.api_get_request("/job/#{rollout_branch.jenkins_job_name}/#{build_number}")

        if resp["result"] != "SUCCESS"
          packing.logger.error("Jenkins build result error: #{resp["result"]}. log: #{@rollout_branch.imf.build_link}")
          packing.logger.error("Stop upload package to ftp.")
        else
          result = true
        end
      rescue => e
        packing.logger.error("Exception catch: #{e}")
        packing.logger.error("Stop upload package to ftp.")
      end
      result
    end

    def upload 
      if check_build_status
        success, failed = upload_packages
        update_packing(success, failed)
      else
        packing.update_status(:build_failed)
        packing.logger.error("Jenkins build failed.")
      end
    end

    def upload_packages
      success, failed = {}, {}
      packing.packages.each do |package|
        ftp = RemoteCi::Ftp.new(packing, package)
        if path = ftp.upload
          success[package.module_name] = path
        else
          failed[package.module_name] = path
        end
      end
      [success, failed]
    end

    def update_packing(success, failed)
      RolloutPacking.transaction do
        if failed.present?
          packing.update_status(:upload_failed)
          packing.logger.error("Some package upload failed:, #{failed.keys.join(",")}")
        else
          packing.war_urls = success
          packing.modules.each do |mod|
            mod.update_attributes(url: success[mod.name]) if success[mod.name].present?
          end
          packing.update_status(:upload_success)
          packing.save!
          packing.logger.info("All pacakges upload successfully.")
        end
      end
    end

    def create_tag
      packing.logger.info("Start to Create Tag: #{packing.tag}.")
      begin
        packing.create_tag
        packing.logger.info("Create Tag: #{packing.tag}.")
      rescue => e
        packing.logger.info("Create Tag Failed: #{e}.")
      end
    end
  end
end  
