module RemoteCi
  class WebHook
    attr_reader :params
    def initialize(params)
      @params = params.dup
    end

    def try_delete_ci_branch
      if is_delete_branch_commit?
        project_name = params[:repository][:homepage].split('/')[-1]
        namespace_id = Namespace.find_by_name(params[:repository][:homepage].split('/')[-2])
        return false if namespace_id.blank?
        branch_name = params[:ref].split('/').reject{|ref| ref =~ /\A(ref|head)s?\z/}.join("/")
        ci_branch = Project.find_by_name_and_namespace_id(project_name, namespace_id).ci_branchs.find_by_branch_name(branch_name)
        ::CiBranches::DestroyContext.new(ci_branch.id).execute if ci_branch 
      end  
    end

    def is_delete_branch_commit?
      return false unless params
      params["after"].squeeze == "0" if params && params["after"]
    end  
  end
end