module RemoteCi
  class BuildLogError < StandardError; end
  class BuildLog
    attr_reader :ci_branch, :build_number, :project
    def initialize(ci_branch, build_number)
      @ci_branch = ci_branch
      @build_number = build_number
      @project = ci_branch.project
    end

    def execute
      begin
        response = @ci_branch.job.get_build_details(@ci_branch.jenkins_job_name, @build_number)
        Logbin.logger("cibuild").info("Ci Build for #{response["fullDisplayName"]}", log_data(response))
      rescue JenkinsApi::Exceptions::NotFoundException => e
        # Logbin.logger("cibuild").info("Cannot found Ci Build for #{@ci_branch.jenkins_job_name}", result: e)
      end  
    end

    def log_data(response)
      begin
        commit = commit_from(response)
      rescue BuildLogError => e
        commit = nil
      end  
      # we read branch and jenkins_job_name from DB is not correct always
      # if user change the branch after build, we will get wrong info.
      # FIXME
      {
        namespace: project.namespace.name, project: project.name, branch: ci_branch.branch_name,
        jenkins_job_name: ci_branch.jenkins_job_name, env: ci_branch.ci_env.name,
        commit_id: commit.try(:id), author_name: commit.try(:author_name), author_email: commit.try(:author_email),
        result: response["result"], duration: response["duration"], build_at: time_from_timestamp(response["timestamp"])
      }
    end

    def commit_from(response)
      begin
        response["actions"].each do |entry|
          if entry.is_a?(Hash) && entry.has_key?("lastBuiltRevision")
            commit_id = entry["lastBuiltRevision"]["SHA1"]
            return project.repository.commit(commit_id)
          end
        end
        raise "commit id not found"
      rescue => e
        raise BuildLogError.new("Can not get commit_id from response, #{e}")
      end
    end

    def time_from_timestamp(ms)
      return if ms.blank?
      Time.at(ms / 1000, (ms % 1000) * 1000)
    end
  end
end
