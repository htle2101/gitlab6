 module RemoteCi
  class MachineManagement
    attr_accessor :options

    include Utils::Logger
    include Utils::Time

    def initialize(options = {})
      @options = options
      self.logger_file = logger_file_name
    end

    def release_each(machines, &block)
      return unless block_given? || machines.blank?
      machines.find_each do |machine|
        last_update_time = compare_time(machine)
        if last_update_time.blank?
          logger.error("can't find the machine #{machine.id} #{machine.ip} last_update_time")
        elsif time_expired?(last_update_time)
          logger.info("We are going to release #{machine.inspect}.") if machine.build_branch_id
          yield machine, last_update_time
          logger.info("#{machine.ip} was released.") if machine.status < 1
        end
      end
      nil
    end

    def compare_time(machine)
      if machine.build_branch_id.blank? || machine.project.blank?
        machine.updated_at
      else
        begin
          return machine.releaser.file_date
        rescue => e
          logger.error("#{machine.id} #{machine.ip} has some error. #{e}")
          return nil
        end
        # Gitlab::Git::Commit.last_for_path(machine.project.repository, machine.build_branch.branch_name).try(:created_at)
      end
    end

    def time_expired?(time)
      (Time.now - time >= options[:days].days.seconds)
    end

    def logger_file_name
      "machine_management/release_machines.log"
    end
  end
end
