require 'securerandom'

module RemoteCi
  module Command
    class SudoAccount < Base

      attr_reader :sudo_account, :sudo_password

      def initialize(host, options = {})
        @sudo_account = options[:sudo_account] || Settings.dp['sudo_account']
        @sudo_password = options[:sudo_password] || SecureRandom.hex(4)
        super(host, options)
      end

      def up
        begin
          user_created? ? reset_password : add_user
          grant_sudo_privilege
          { account: sudo_account, password: sudo_password }
        rescue => e
          raise CommandError.new("Can not create account `#{sudo_account}` for #{@host} with sudo privilege successfully, #{e}.")
        end
      end

      def down
        revoke_sudo_privilege
        delete_user
      end  

      def add_user
        conn.sudo_exec("useradd -p '#{encrypt_password}' -m -s /bin/bash #{sudo_account}")
      end

      def encrypt_password
        conn.execute("echo '#{sudo_password}' | openssl passwd -1 -stdin").first.chomp
      end

      def reset_password
        conn.sudo_exec("usermod -p '#{encrypt_password}' #{sudo_account}")
      end

      def grant_sudo_privilege
        revoke_sudo_privilege rescue nil
        conn.sudo_exec("echo '#{sudo_account} ALL=(ALL) ALL' >> /etc/sudoers")
      end

      def revoke_sudo_privilege
        conn.sudo_exec("sed -i /^#{sudo_account}.*ALL$/d /etc/sudoers")
      end

      def delete_user
        conn.sudo_exec("userdel -r #{sudo_account}")
      end

      def user_created?
        begin
          conn.execute "id #{sudo_account} >/dev/null 2>&1"
          true
        rescue
          false
        end
      end

    end
  end
end
