#coding:utf-8
module RemoteCi
  module Command
    class GetNginxConfError < StandardError; end
    class UpdateNginxConfError < StandardError; end
    class NginxConf < Base
      include Utils::Logger

      NGINX_PATH = "/usr/local/nginx/conf"

      def get_server_conf
        begin
          tmp_conf_file = "/data/tmp/nginx-#{host}.conf"
          File.open(tmp_conf_file, 'w') {|f| f.write conn.file_download("#{NGINX_PATH}/nginx.conf").string}
          file = File.open(tmp_conf_file)
          contents = ""
          file.each {|line|
            contents << line
          }
          contents
        rescue => e
          raise GetNginxConfError.new("#{host}:#{e}")
        end
      end

      def update_server_conf(content)
        begin
          tmp_conf_file = "/data/tmp/nginx-#{host}.conf"
          File.open(tmp_conf_file, 'w') {|f| f.write content}
          conn.cp("#{NGINX_PATH}/nginx.conf","#{NGINX_PATH}/nginx.conf.gitpub-bak")
          conn.file_upload(tmp_conf_file, NGINX_PATH)
          conn.mv("#{NGINX_PATH}/nginx-#{host}.conf","#{NGINX_PATH}/nginx.conf")
          result = conn.sudo_exec("service nginx restart")
          return true
        rescue => e
          raise UpdateGetNginxConfError.new("#{host}:#{e}")
        ensure
          if result.blank?
            #conn.mv("#{NGINX_PATH}/nginx.conf.gitpub-bak","#{NGINX_PATH}/nginx.conf")
            return false
          end
        end
      end

    end
  end
end