module RemoteCi
  module Command
    class JbossConf < Base
      include Utils::Logger

      JBOSS_CONF_FILE = '/usr/local/tomcat/conf/Catalina/localhost/ROOT.xml'

      def up
        begin
          configure
          true
        rescue => e
          logger.error("Can not configure ROOT.xml for #{host}, #{e}.")
          false
        end
      end

      private
      def configure
        war_name = options[:module_name] + '.war'
        docBase = "/data/webapps/current/" + war_name
        war_location_conf = " <Context path=\"/\" docBase=\"#{docBase}\" reloadable=\"false\">"
        conn.sudo_exec("sed -i \"/<Context/d\" #{JBOSS_CONF_FILE}")
        conn.sudo_exec("sed -i '/<\\?xml/a\\#{war_location_conf}' #{JBOSS_CONF_FILE}")
      end
    end
  end
end
