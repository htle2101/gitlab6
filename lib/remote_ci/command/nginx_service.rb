module RemoteCi
  module Command
    class NginxServiceError < StandardError; end
    class NginxService < Base
      include Utils::Logger

      NGINX_PATH = "/usr/local/nginx/"

      def down
        begin
          recover_nginx_conf
          conn.sudo_exec("service nginx stop", :timeout => 30)
        rescue => e
          raise NginxServiceError.new("#{@host}:#{e}")
        ensure
          conn.disconnect
        end
      end

      def recover_nginx_conf
        if options[:env].blank?
          conn.sudo_exec("'cp' -f /usr/local/nginx/conf/nginx.conf.default /usr/local/nginx/conf/nginx.conf")
        else
          ci_env = CiEnv.find_by_name(options[:env])
          ci_env.conn.execute("scp -P #{options[:port] || Settings.dp['ssh_port']} #{ci_env.nginx_conf_path} #{Settings.dp['ssh_user']}@#{host}:#{NGINX_PATH}conf", :timeout => 10)
        end    
      end  

    end
  end
end