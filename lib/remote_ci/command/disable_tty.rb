module RemoteCi
  module Command
    class DisableTty < Base

      def execute
        conn.execute("sed -i 's/^\s*Defaults\s*requiretty/# Defaults requiretty/g' /etc/sudoers")
      end

    end
  end
end
