module RemoteCi
  module Command
    class KeyCopy < Base

      def execute
        versions = CiEnv.uniq.pluck(:version)
        versions.each do |version|
          env = CiEnv.find_by_name_and_version(options[:envs], version)
          next if env.blank?
          pub_key = env.conn.cat("/root/.ssh/id_rsa.pub")[0]
          env.conn.disconnect
          unless key_exists?(pub_key)
            conn.execute("mkdir -p #{ssh_dir}")
            conn.execute("echo '#{pub_key}' >> #{ssh_dir}/authorized_keys")
            conn.disconnect
          end
        end  
        true
      end

      def ssh_dir
        conn.user == "root" ? "/#{conn.user}/.ssh" : "/home/#{conn.user}/.ssh"
      end

      def key_exists?(pub_key)
        begin
          conn.execute("cat #{ssh_dir}/authorized_keys | grep '#{pub_key}'")
          true
        rescue
          false
        end
      end
    end
  end
end
