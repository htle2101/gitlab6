module RemoteCi
  module Command
    class Base
      attr_reader :host, :options
      def initialize(host, options = {})
        @host = host
        @options = options
      end

      def conn
        @conn ||= Connection.new(host, options)
      end
    end
  end
end
