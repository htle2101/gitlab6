#coding:utf-8
module RemoteCi
  module Command
    class JbossLogError < StandardError; end
    class JbossLog < Base
      include Utils::Logger

      JBOSS_LOG_PATH = "/usr/local/tomcat/logs/catalina.out"

      def get_log
        begin
          conn.tail("-n 2000 #{JBOSS_LOG_PATH}")
        rescue => e
          raise JbossLogError.new("#{host}:#{e}")
        ensure
          conn.disconnect  
        end
      end
    end
  end
end