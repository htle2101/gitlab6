#coding:utf-8
module RemoteCi
  module Command
    class SetSwimlaneError < StandardError; end
    class DelSwimlaneError < StandardError; end
    class Swimlane < Base
      include Utils::Logger

  
      APPENV_PATH = "/data/webapps/appenv"

      def set_swimlane(swimlane)
        begin
          unless conn.execute("sed -n \'/swimlane/p\' /data/webapps/appenv", :timeout => 10).blank?
            conn.execute("sed -i -e  \'s/swimlane.*/swimlane=#{swimlane}/\' #{APPENV_PATH}", :timeout => 10)
          else 
            conn.execute("echo 'swimlane=#{swimlane}' >> #{APPENV_PATH}", :timeout => 10)  
          end  
        rescue => e
          raise SetSwimlaneError.new("#{host}:#{e}")
        ensure
          conn.disconnect  
        end
      end

      def del_swimlane
        begin
          unless conn.execute("sed -n \'/swimlane/p\' /data/webapps/appenv", :timeout => 10).blank?
            conn.execute("sed -i -e  \'s/swimlane.*/swimlane=/\' #{APPENV_PATH}", :timeout => 10)
          end
        rescue => e
          raise DelSwimlaneError.new("#{host}:#{e}")
        ensure
          conn.disconnect  
        end
      end

    end
  end
end