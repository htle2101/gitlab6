module RemoteCi
  module Command
    class JbossServiceError < StandardError; end
    class JbossService < Base
      include Utils::Logger

      def down
        begin
          conn.sudo_exec("service jboss stop", :timeout => 120)
        rescue => e
          raise JbossServiceError.new("#{@host}:#{e}")
        ensure
          conn.disconnect
        end
      end

    end
  end
end