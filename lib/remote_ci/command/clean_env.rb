module RemoteCi
  module Command
    class CleanEnv < Base
      APP_PATH = "/data/webapps"
      JBOSS_LOG_PATH = "/usr/local/jboss/server/default/log"
      APP_LOG_PATH = "/data/applogs"

      def clean_webapps
        clean { conn.sudo_exec("find #{APP_PATH}/*  -maxdepth 0   -not -name 'paas' -not -name 'phoenix-dev' -not -name 'phoenix-dev-agent'  -exec rm -rf {} \+") }
      end

      def clean_logs
        clean { conn.sudo_exec("find #{APP_LOG_PATH}/* #{JBOSS_LOG_PATH}/* -not -name 'tomcat' -exec rm -rf {} \+") }
      end

      
      protected

      def clean
        begin
          Timeout::timeout(5) {
            yield
          }          
        rescue => e
          return false
        ensure
          conn.disconnect
        end
        return true        
      end

    end
  end
end
