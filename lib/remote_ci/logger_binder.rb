module RemoteCi
  class LoggerBinder

    attr_reader :ci_branch, :build_number

    def initialize(ci_branch, options = {})
      @ci_branch = ci_branch
      @module_name = options[:module_name]
      @build_number = options[:build_number]
    end

    def bind_deploy_log(logger_file_name, log_module_name = @module_name)
      bind_log('deploy', log_module_name, logger_file_name, 'file')
    end

    def bind_lzm_log(logger_url, log_module_name = @module_name)
      bind_log('lzm', log_module_name, logger_url, 'url')
    end

    def bind_phoenix_log(logger_file_name, log_module_name)
      bind_log('phoenix', log_module_name, logger_file_name, 'file')
    end

    def bind_beta_paas_log(logger_file_name, log_module_name = @module_name)
      bind_log('beta_paas', log_module_name, logger_file_name, 'paas')
    end

    def last_twenty_logs
      lastbuild = $redis.hset 'jenkins_lastbuild', ci_branch.jenkins_job_name
      return [] if lastbuild.nil?
      (lastbuild..lastbuild-20).inject([]) do |results, number|
        log = get_build_log(number)
        results << log unless log.nil?
        results
      end  
    end

    def last_log
      lastbuild = $redis.hget 'jenkins_lastbuild', ci_branch.jenkins_job_name
      get_build_log(lastbuild)
    end  

    protected
    def bind_log(type, log_module_name, location, location_type)
      if build_number
        deploy_logs = $redis.hget('deploy_logs' ,"#{ci_branch.jenkins_job_name}/#{build_number}").to_s
        deploy_logs = deploy_logs.blank? ? { 'logs' => [] } : JSON.parse(deploy_logs)
        deploy_logs['logs'].push({ type: type, module_name: log_module_name, location: location, location_type: location_type, time: Time.now.strftime("%Y-%m-%d-%H-%M-%S")})
        $redis.hset 'jenkins_lastbuild', ci_branch.jenkins_job_name, build_number.to_s
        $redis.hset 'deploy_logs', "#{ci_branch.jenkins_job_name}/#{build_number.to_s}", deploy_logs.to_json
      end
    end  

    def get_build_log(number = build_number)
      return nil if number.nil?
      deploy_logs = $redis.hget('deploy_logs' ,"#{ci_branch.jenkins_job_name}/#{number}")
      deploy_logs.blank? ? {} : JSON.parse(deploy_logs)
    end

  end
end
