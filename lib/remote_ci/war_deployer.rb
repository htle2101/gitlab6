module RemoteCi
  class WarDeployer
    attr_reader :ci_branch, :module_name, :build_number, :logger_binder
    def initialize(ci_branch, options = {})
      @ci_branch = ci_branch
      @module_name = options['module_name']
      @module_name = CiBranch::PHOENIX_MODULE if options['module_name'].blank? && phoenix?
      @build_number = options['build_number']
      @logger_binder = LoggerBinder.new(ci_branch, { build_number: build_number, module_name: module_name })
    end

    def phoenix?
      ci_branch.deployers == "Phoenix"
    end

    def deploy_all?
      # deploy triggered via jenkins build didn't include any module_name, so we should deploy all wars
      module_name.blank?
    end

    def deploy
      if deploy_all?
        ci_branch.deployer_for_jar.each{|jar| jar.reset }
        return [] unless build_success?
      end

      deploy_result = deployers.inject([]) do |result, resource|
        # 如果是 beta 环境并且是 jenkins 回调, 则只创建机器不进行部署
        if ci_branch.beta? && deploy_all?
          return(VirtualMachines::LinkContext.new({ build_branch_id: ci_branch.id, module_name: resource.module_name, deployer: ci_branch.deployers }).execute)
        end
        if (deploy_all? || resource.module_name == module_name)
          logger_binder.bind_deploy_log(resource.logger_file_name, resource.module_name)
          resource.build_number = build_number
          collect_jenkins_status if resource.machine.present?
          result << { war: resource, status: resource.deploy } if resource.machine.present?
        end
        result
      end

      if ci_branch.jenkins_job_name =~ /shadow-/
        Service::Sonar::Api::ProjectStatistics.new(ci_branch.project).reload
        Service::Sonar::Api::GroupStatistics.new(ci_branch.project.namespace).reload
      end

      deploy_result
    end

    def deployers
      if module_name.blank?
        if ci_branch.deployers == 'Php' || ci_branch.deployers == 'CmdbPhp'
          ci_branch.deployer_for(:php)
        else
          ci_branch.deployer_for(:war) # war in alpha, cmdb in beta, never mind they are just alias for wars
        end
      else
        vm = ci_branch.machines.master.find_by_module_name(module_name)
        vm.present? ? [vm.deployer_instance] : []
      end
    end

    def build_status
      @build_status ||= ci_branch.ci_env.server.job.get_build_details(ci_branch.jenkins_job_name, build_number)['result']
    end

    def wait_for_jenkins_stop
      3.times do
        begin
          if  build_status.upcase == 'RUNNING'
            sleep 2
          else
            break
          end
        rescue => e
          sleep 2
          Gitlab::GitLogger.info e
        end
      end
    end

    def collect_jenkins_status
      begin
        wait_for_jenkins_stop
        Event.create(
          project: ci_branch.project,
          target_id: ci_branch.id,
          target_type: 'CiBranch',
          action: Event.const_get(build_status.upcase),
          title: ci_branch.ci_env.name
        ) unless build_status.blank?
      rescue
      end
    end

    def build_success?
      ['SUCCESS', 'UNSTABLE'].include?(build_status.upcase) ? true : false
    end
  end

end
