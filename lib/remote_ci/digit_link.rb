module RemoteCi
  class DigitLink
    class << self
      def link(first, second)
        "#{first.to_s.length.to_s}#{first.to_s}#{second.to_s}"
      end 
      
      def separate(digit)
        location = digit.to_s[0].to_i
        { first: digit.to_s[1..location], second: digit.to_s[location+1..-1]}
      end  
    end
  end
end
