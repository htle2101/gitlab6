module RemoteCi
  class BetaJobMonitor
    def execute
      vms = VirtualMachine.with_deployer("Cmdb").where(status: 5)
      vms.each do |vm|
        printf("vm %d ", vm.id)
        begin
          if vm.ci_branch.present?
            printf("ci %d ", vm.ci_branch.id)
            build_number = $redis.hget('jenkins_lastbuild', vm.ci_branch.jenkins_job_name)
            if build_number
              printf("build_number %s ", build_number)
              token = DigitLink.link(build_number, vm.id)
              printf("token %s ", token)
              status = Service::Op::Cmdb.new(host: Settings.op['lzm_url']).latest_rollout_status(token).parsed_response["status"]
              printf("status %s ", status)
              val = case status
              when "complete" then 7
              when "killed", "fail" then 6
              end
              if val
                vm.update_attributes(status: val)
                printf "update to #{val}."
              end

              update_status_by_workflow(vm)
            end
          end
        rescue => e
          printf("error %s", e)
        end
        puts "finish"
      end
    end

    private

    def update_status_by_workflow(vm)
      workflow_uuids = vm.get_workflow_uuids

      if workflow_uuids.present?
        codes = workflow_uuids.map do |uuid|
          response = Service::Workflow::Workflow.new.restart_tomcat_result(uuid)

          response['code'].to_i if response
        end.compact

        if codes.uniq == [200]
          vm.delete_workflow_uuids

          vm.update_attributes(status: 7)
        elsif codes.uniq.include?(400)
          vm.delete_workflow_uuids

          vm.update_attributes(status: 6)
        elsif codes.uniq.include?(206)
          vm.update_attributes(status: 5)
        end
      end
    end
  end
end
