module RemoteCi
  class CiImf
    attr_accessor :ci_branch

    def initialize(ci_branch)
      @ci_branch = ci_branch
    end

    def check
      pom_datasource
      jenkins_datasource
      job_status
      true
    end

    def pom_datasource
      @pom_datasource ||= @ci_branch.tree.hashed_pom
    end  

    def jenkins_datasource
      @jenkins_datasource ||= @ci_branch.ci_last_build
    end 

    def machines 
      @machines ||= @ci_branch.machines.inject({}) do |machine_list, m|
                      machine_list[m.module_name] = m
                      machine_list
                    end
    end  

    def job_status
      jenkins_datasource.build_result['status']
    end

    def jenkins_modules
      jenkins_datasource.hashable_modules
    end  

    def build_time
      (jenkins_datasource.build_result['builds'].blank? ? 'no built yet.' : jenkins_datasource.build_result['builds'][0]['time']) rescue 'no built yet.'
    end

    def build_link
      @build_link ||= jenkins_datasource.hashable_builds[jenkins_datasource.hashable_builds.keys.max]['url']
    end  

    def build_log_link
      "#{build_link}console"
    end  

    def modules
      @modules ||= pom_datasource.keys.inject({}) do |result, module_name|
          result[module_name] = pom_datasource[module_name]
          result[module_name].raw.merge!(machine: machines[module_name]) unless machines[module_name].blank?
          result[module_name].raw.merge!(jenkins: jenkins_modules[module_name]) unless jenkins_modules[module_name].blank?
          # result[module_name].try(:reset)
          result
        end          
    end
  end
end    