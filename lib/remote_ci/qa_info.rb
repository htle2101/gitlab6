module RemoteCi
  class QaInfo
    attr_accessor :params, :project
    def initialize(params, project = nil)
      @params = params
      @project = project
    end

    def no_alpha_ci
      Project.where(id: (rollouted_project_ids - has_alpha_project_ids))
    end

    def has_alpha_ci_but_no_alpha_machine
      Project.where(id: (has_alpha_project_ids & rollouted_project_ids - has_alpha_machine_project_ids))
    end

    def has_alpha_ci_but_not_build
      Project.where(id: (has_alpha_project_ids & rollouted_project_ids - builded_ci_project_ids_with_env('alpha', @params[:days] || 30 ))) 
    end

    def no_ppe_build_project_list
      Project.where(id: (rollouted_project_ids - ppe_bulid_project_ids))
    end

    def project_build_times
      Event.unscoped.ci_build.where(title: @params[:env] || 'beta', project_id: @project.id).where("updated_at >= ? ", (@params[:days] || 30).to_i.days.ago).group('date(updated_at)').count
    end  
      
    def java_project_ids
      @java_project_ids ||= ProjectModule.pluck(:project_id).uniq
    end

    def has_alpha_project_ids
      BuildBranch.where(ci_env_id: CiEnv.where(name: 'alpha')).uniq.pluck(:project_id)
    end

    def has_alpha_machine_project_ids
      VirtualMachine.where(deployer: ['War', 'Phoenix']).uniq.pluck(:project_id)
    end

    def builded_ci_project_ids_with_env(name, days)
      Event.unscoped.ci_build.where(title: name).where("updated_at >= ? ", days.to_i.days.ago).uniq.pluck(:project_id)
    end  

    def ppe_bulid_project_ids
      GroupingStrategy.prerelease.uniq.pluck(:project_id)
    end

    def rollouted_project_ids
      GroupingStrategy.uniq.pluck(:project_id)
    end  
  end
end    