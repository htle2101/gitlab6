 module RemoteCi
  class FtpManagement
    attr_accessor :options

    include Utils::Logger
    include Utils::Time

    def initialize(options = {})
      @options = options
      self.logger_file = logger_file_name
    end

    def clean_packages
      logger.info("Connect to ftp server.")
      ftp = Net::FTP.new(Settings.ftp['url'])
      ftp.login(Settings.ftp['username'], Settings.ftp['password'])
      ftp.passive = true
      RolloutPackingModule.where("updated_at > ?", Time.now - options[:clean_cycle].days.seconds).pluck(:name).uniq.try(:each) do |name|
        clean_packages_by_name( name, ftp )
      end
      ftp.close
      logger.info("Close ftp server.")
    end

    def clean_packages_by_name( name, ftp )
      packing_modules = RolloutPackingModule.where(name: name).where("status is null").where("url is not null").find(:all,:order => "updated_at desc")
      packing_modules[options[:retained_number]..(packing_modules .size - 1)].try(:each) do |pm|
        if ftp.nlst(pm.url).blank?
          pm.update_attributes( status: RolloutPackingModule::STATUS_LIST[:deleted] )
          logger.info("Once be deleted.#{pm.url}")
          next
        end
        
        begin
          ftp.delete( pm.url )
          pm.update_attributes( status: RolloutPackingModule::STATUS_LIST[:deleted] )
          logger.info("Delete successfully.#{pm.url}")
        rescue => e
          logger.error( "Delete Failed. #{e.message.encode('utf-8', {:invalid => :replace, :undef => :replace, :replace => '?'})}" )
          next
        end
      end      
    end

    def logger_file_name
      "ftp_management/clean_packages.log"
    end

  end
end
