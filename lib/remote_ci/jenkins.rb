require 'jenkins_api_client'

#  HTTParty.get("http://192.168.218.206/api/json", {:basic_auth => {:username => 'scm.dp', :password => 'SRnmyv22'}})
module RemoteCi
  class Jenkins
    attr_accessor :jenkins

    def initialize(*args)
      @args = *args
      @jenkins =  JenkinsApi::Client.new(*args)
    end

    #try reconnect to jenkins when got error.
    def method_missing(name, *args, &block)
      begin 
        @jenkins.send(name, *args, &block)
      rescue 
        @jenkins = JenkinsApi::Client.new(@args)  
        @jenkins.send(name, *args, &block)        
      end      
    end 
  end

end
