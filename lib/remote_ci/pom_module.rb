module RemoteCi
  class PomModule
    attr_accessor :raw, :module_name, :type, :path, :version, :groupId, :full_name, :warName, :ut, :paas

    def initialize(hash)
      hash.each { |k, v| send("#{k}=", v) if respond_to?("#{k}=") }
      @raw = hash
    end
    
    def [](index)
      @raw[index]
    end   

    def reset
      @raw.each { |k, v| send("#{k}=", v) if respond_to?("#{k}=") }
    end

    def dashboard_path
      "http://#{Settings.sonar['sonar_url']}/dashboard/index/#{@full_name}"
    end

    def name_with_version
      version.blank? ? module_name : "#{module_name} (#{version})"
    end  
  end
end