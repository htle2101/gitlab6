class Redcarpet::Render::GitlabHTML < Redcarpet::Render::HTML

  attr_reader :template
  alias_method :h, :template

  def initialize(template, options = {})
    @template = template
    @project = @template.instance_variable_get("@project")
    super options
  end

  def block_code(code, language)
    options = { options: {encoding: 'utf-8'} }
    lexer = Pygments::Lexer.find(language) # language can be an alias
    options.merge!(lexer: lexer.aliases[0].downcase) if lexer # downcase is required

    # New lines are placed to fix an rendering issue
    # with code wrapped inside <h1> tag for next case:
    #
    # # Title kinda h1
    #
    #     ruby code here
    #
    begin
      highlights = Pygments.highlight(code, options)
    rescue MentosError
      highlights = Pygments.highlight(code, { options: {encoding: 'utf-8'}, lexer: 'nginx' })
    end  
    <<-HTML

       <div class="#{h.user_color_scheme_class}">#{highlights}</div>

    HTML
  end

  def link(link, title, content)
    h.link_to_gfm(content, link, title: title)
  end

  def postprocess(full_document)
    h.gfm(full_document)
  end
end
