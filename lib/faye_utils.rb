module FayeUtils
  def publish(channel, data)
    begin
      message = {:channel => channel, :data => data}  
      uri = URI.parse(Settings.faye.url)  
      Net::HTTP.post_form(uri, :message => message.to_json)  
    rescue => e
      Gitlab::GitLogger.error(e)
    end
  end

  extend self
end
