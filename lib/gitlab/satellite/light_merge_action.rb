module Gitlab
  module Satellite
    # GitLab server-side merge
    class LightMergeAction < Action
      attr_accessor :light_merge, :conflict_branches, :temp_branch

      def initialize(user, light_merge)
        super user, light_merge.project
        @light_merge = light_merge
        @conflict_branches = { 'success' => [], 'conflict' => [], 'log' => {}}
        @temp_branch = Time.now.to_i.to_s
        @options[:git_timeout] = 90.seconds
      end

      # Checks if a merge request can be executed without user interaction
      def can_be_merged?
        in_locked_and_timed_satellite do |merge_repo|
          merge_in_satellite!(merge_repo)
        end
      end

      # Merges the source branch into the target branch in the satellite and
      # pushes it back to Gitolite.
      # It also removes the source branch if requested in the merge request.
      #
      # Returns false if the merge produced conflicts
      # Returns false if pushing from the satellite to Gitolite failed or was rejected
      # Returns true otherwise
      def merge!
        in_locked_and_timed_satellite do |merge_repo|
          if merge_in_satellite!(merge_repo)
            # push merge back to Gitolite
            # will raise CommandFailed when push fails
            # remove source branch
            merge_repo.git.push({raise: true, timeout: true, force: true}, :origin, "#{temp_branch}:#{light_merge.build_branch.branch_name}")

            # merge, push and branch removal successful
            true
          else
            false   
          end
        end
      rescue Grit::Git::CommandFailed => ex
        Gitlab::GitLogger.error(ex.message)
        false
      end

      private

      # Merges the source_branch into the target_branch in the satellite.
      #
      # Note: it will clear out the satellite before doing anything
      #
      # Returns false if the merge produced conflicts
      # Returns true otherwise
      def merge_in_satellite!(repo)
        prepare_satellite!(repo)

        # create target branch in satellite at the corresponding commit from Gitolite
        repo.git.checkout({raise: true, timeout: true, b: true}, temp_branch, "origin/#{light_merge.base_branch}")

        # merge the source branch from Gitolite into the satellite
        # will raise CommandFailed when merge fails
        light_merge.source_branches.each do |source_branch|
          begin 
            repo.git.pull({raise: true, timeout: true, no_ff: true}, :origin, source_branch)
            conflict_branches['success'] = (conflict_branches['success'] << source_branch)  
          rescue Grit::Git::CommandFailed => ex
            Gitlab::GitLogger.info(ex.message)

            conflict_branches['log'][source_branch] = repo.git.status
            conflict_branches['conflict'] = (conflict_branches['conflict'] << source_branch)  

            repo.git.reset(hard: true)
            repo.git.clean(f: true)
          end  
        end  
        @light_merge.conflict_branches = conflict_branches
        return false if (conflict_branches['success'].length == 0)
        return true
      rescue Grit::Git::CommandFailed => ex
        Gitlab::GitLogger.error(ex.message)
        return false
      end
    end
  end
end
