module Gitlab
  module Satellite

    class RemoveBranchAction < Action
      attr_accessor :branch

      def initialize(user, project, branch)
        super(user, project)
        @branch = branch
      end

      def remove!
        in_locked_and_timed_satellite do |repo|
          repo.git.push({raise: true, timeout: true}, :origin, ":#{branch}") if project.branch_can_be_remove?(branch)
          true
        end
      rescue Grit::Git::CommandFailed => ex
        Gitlab::GitLogger.error(ex.message)
        false
      end
    end
  end
end
