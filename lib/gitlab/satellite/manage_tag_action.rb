module Gitlab
  module Satellite

    class TagCanNotCreatedError < StandardError; end
    class ManageTagAction < Action

      def initialize(user, project, options = {})
        super(user, project, options)
      end

      def create_lightweight_tag
        in_locked_and_timed_satellite do |repo|
          prepare_satellite!(repo)
          repo.git.tag( { 'f' => true } , options[:tag_name], options[:commit_id] )
          repo.git.push({raise: true, timeout: true}, :origin, options[:tag_name])
          true
        end
      rescue Grit::Git::CommandFailed => ex
        Gitlab::GitLogger.error(ex.message)
        raise TagCanNotCreatedError.new(ex)
      end
    end
  end
end
