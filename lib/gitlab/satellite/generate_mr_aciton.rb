#coding:utf-8
module Gitlab
  module Satellite
    # RolloutBranch.where(deployers: ['War', 'Cmdb']).find_each(batch_size: 50){|r| Gitlab::Satellite::GenerateMrAction.new(r.project.owner, r.project, r.branch_name).generate_app_properties}
    class GenerateMrAction < Action
      attr_accessor :file_path, :ref

      def initialize(user, project, ref)
        super user, project
        @ref = ref
        @source_ref = 'add_app_properties'
      end

      def generate_app_properties
        @need_mr = false
        in_locked_and_timed_satellite do |repo|
          prepare_satellite!(repo)

          repo.git.checkout({raise: true, timeout: true, b: true}, @source_ref, "origin/#{ref}")

          @project.tree(ref).pom.each do |m|
            next if m.type != 'war'

            properties_dir_path = File.join(repo.working_dir, m.path, "src/main/resources/META-INF/")

            properties_file_path = File.join(properties_dir_path, 'app.properties')

            unless File.exist?(properties_file_path)
              @need_mr = true
              FileUtils.mkdir_p(properties_dir_path)

              File.open(properties_file_path, 'w') { |f| f.write("app.name=#{m.warName}") } 

              repo.add(properties_file_path)
            end  
          end  

          return true if @need_mr == false

          commit_message = "统一项目名
CMDB、机器名、gitpub、lion、cat，都对应用名有一些定义。 
在做这些应用之间的数据传输过程中，发现应用名的不一致导致很多问题。 
CAT的异常数据自动发送到Bug系统，帮助开发快速发现问题。 
Pigeon中简化Service的配置，让pigeon配置更加简单。 
发布系统调用监控系统的api，让发布过程能有效监控。 
解决Pigeon中，client和server端统计数据不一致的情况，能更加准确定位pigeon调用的问题。 
统一方便查询应用信息"

          repo.git.commit(raise: true, timeout: true, a: true, m: commit_message)
          repo.git.push({raise: true, timeout: true}, :origin, @source_ref)
        end

        create_mr
      # rescue Grit::Git::CommandFailed => ex
      #   Gitlab::GitLogger.error(ex.message)
      #   false    
      end  

      def create_mr
        current_user = User.find_by_email('scm@dianping.com')
        Thread.current[:current_user] = current_user
        @merge_request = @project.merge_requests.new(
            source_project_id: @project.id,
            source_branch: @source_ref,
            target_project_id: @project.id,
            target_branch: @ref,
            title: "统一项目名 ",
            assignee_id: @project.owner.id,
            author_id: current_user.id
          )

        @merge_request.save
        @merge_request.reload_code
      end  


      # def commit!(content, commit_message, last_commit)
      #   return false unless can_edit?(last_commit)

      #   in_locked_and_timed_satellite do |repo|
      #     prepare_satellite!(repo)

      #     # create target branch in satellite at the corresponding commit from Gitolite
      #     repo.git.checkout({raise: true, timeout: true, b: true}, ref, "origin/#{ref}")

      #     # update the file in the satellite's working dir
      #     file_path_in_satellite = File.join(repo.working_dir, file_path)
      #     File.open(file_path_in_satellite, 'w') { |f| f.write(content) }

      #     # commit the changes
      #     # will raise CommandFailed when commit fails
      #     repo.git.commit(raise: true, timeout: true, a: true, m: commit_message)


      #     # push commit back to Gitolite
      #     # will raise CommandFailed when push fails
      #     repo.git.push({raise: true, timeout: true}, :origin, ref)

      #     # everything worked
      #     true
      #   end
      # rescue Grit::Git::CommandFailed => ex
      #   Gitlab::GitLogger.error(ex.message)
      #   false
      # end

      # protected

      # def can_edit?(last_commit)
      #   current_last_commit = Gitlab::Git::Commit.last_for_path(@project.repository, ref, file_path).sha
      #   last_commit == current_last_commit
      # end
    end
  end
end
