module Gitlab
  class InitBranch 
    def initialize(name)
      @name = name
    end
    
    def execute
      repo = Grit::Repo.init path    
      
      repo.git.config({}, "user.name",  "scm.dp")
      repo.git.config({}, "user.email", "scm.dp@dianping.com")

      Dir.chdir(path) do
        readme_file = "README.md"
        File.open(readme_file,"w") {|f| f.write("## #{repo_name_without_namespace}")}
        repo.add(readme_file)
        repo.commit_index("Initial Commit")
        repo.remote_add("origin",full_repos_path)        
        repo.git.push({:process_info => true,:progress => true}, 'origin', 'master') 
      end        
    end

    def path
      @path ||= "#{Settings.gitlab_shell['path']}#{@name}_#{Time.now.to_i}"
    end

    def full_repos_path
      "#{Settings.gitlab_shell['repos_path']}#{@name}.git"
    end

    def repo_name_without_namespace
      return @name unless @name.include?("/")
      @name.split("/")[1]
    end

  end
end
