require 'net/http'

module RemoteOp
  module Http
    def get_request(request)     
      Net::HTTP.start(
        @server, 80
      ) do |http|
        response = http.request(request)
      end
    end
  end
end
