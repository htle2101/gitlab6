module RemoteOp
  class DpDnsError < StandardError; end
  class DpDns
    def initialize
      @conn = Connection.new(Settings.op['dpdns_ip'], port: Settings.op['dpdns_port'])
    end  
  
    def set_domain_name(domain_name, ip)
      @conn.get("/addname", name: domain_name, rdata: ip)
    end

    def get_domain_name(ip)
      @conn.get("/queryip", ip: ip)
    end

    def get_ip(domain_name)
      @conn.get("/queryname", name: domain_name)
    end

    def delete_domain_name(domain_name, ip)
      begin
        @conn.get("/delname", name: domain_name, rdata: ip)
      rescue => e
        raise DpDnsError.new("domain_name:ip")
      end
    end
  end
end
