require 'net/http'

module RemoteOp
  class Cmdb
    include Http

    def initialize(server)
      @server = server
    end 

    def ip(param)
      response = get_request(ip_request(param))
      response[:ip]
    end

  end
end
