module Service::Beehome
  class BeehomeServerConnectError < StandardError; end
  class BeehomeInterfaceAccessError < StandardError; end

  class Beehome
    include HTTParty

    def initialize(options = {})
      self.class.base_uri("http://#{  options[:host] || Settings.beehome['url']}")
      @timeout = options[:timeout] || 2
    end

    def post(path, params = {})
      begin
        response = Timeout::timeout(@timeout) do
          self.class.post(path, query: params)
        end
      rescue => ex
        raise BeehomeServerConnectError.new(ex)
      end

      if response.code != 201
        raise BeehomeInterfaceAccessError.new(path)
      end

      response
    end

    def apply_special_containers(purpose, machines)
      demand = {
        purpose: purpose,
        machines: machines
      }

      post('api/v1/apply_special_containers', demain)
    end

    def delete_containers(ips, return_url)
      post('api/v1/delete_containers', ips: ips, return_url: return_url)
    end

    def clear(ip)
      post('/api/v1/containers/rebuild', ip: ip, return_url: "#{server_path}/api/v3/beehome/update_machine")
    end

    def server_path
      "#{Settings['gitlab']['host']}:#{Settings['gitlab']['port']}"
    end
  end
end
