module Service::Lion
  class LionServerConnectError < StandardError; end
  class LionServerRequestError < StandardError; end
  class Connection
    attr_reader :host, :port, :timeout

    def initialize(host, options = {})
      @host = host
      @port = options[:port]
      @timeout = options[:timeout] || 1
    end

    def get(path, params = {})
      begin
        Timeout::timeout(@timeout) do
          uri = URI("http://#{@host}:#{@port}#{path}")
          uri.query = URI.encode_www_form(params)
          Net::HTTP.get_response(uri)
        end
      rescue => ex
        raise LionServerConnectError.new(ex)
      end
    end

    def json_get(path, params = {})
      res = get(path, params)
      if res.code != "200" || JSON(res.body)["status"] != 'success'
        messgae = JSON(res.body)['message'] rescue 'lion request error'
        raise LionServerRequestError.new(messgae)
      end
      res.body
    end
  end
end  