module Service::Lion
  class LionSetConfigError < StandardError; end
  class LionTakeEffectError < StandardError; end
  class LionGetConfigError < StandardError; end
  class LionGetDiffError < StandardError; end

  class Request
    def initialize
      @conn = Connection.new(Settings.op['lion_host'], port: Settings.op['lion_port'])
    end  
  
    def set_config(params = {})
      begin
        @conn.get("/setconfig", params).body
      rescue LionServerConnectError => ex
        raise LionSetConfigError.new(ex)
      end
    end

    def take_effect(params = {})
      begin
        @conn.get("/takeeffect", params).body
      rescue LionServerConnectError => ex
        raise LionTakeEffectError.new(ex)
      end
    end

    def get_config(params = {})
      begin
        @conn.get("/getconfig", params).body
      rescue LionServerConnectError => ex
        raise LionGetConfigError.new(ex)
      end      
    end

    def get_diff(params = {})
      begin
        @conn.get("/getconfigdiff", params).body
      rescue LionServerConnectError => ex
        raise LionGetConfigError.new(ex)
      end
    end

    def teams
      @conn.json_get("/team2/list")
    end

    def products(team)
      @conn.json_get("/product2/list", team: team)
    end

    def create(product, project, owner = 'enlight.chen')
      @conn.json_get("/project2/create",id: 2, product: product, project: project, owner: owner)
    end

    def get_service(service_url)
      @conn.get("/service2/get", env: 'alpha', service: service_url)
    end
    
    def set_service(service_url, ips_with_port)
      @conn.get("/service2/get", id: 2, env: 'alpha', service: service_url, address: ips_with_port)
    end  
  end
end
