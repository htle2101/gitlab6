class Service
  module ThorMonitor
    class ThorMonitor
      include HTTParty

      def run_test(options)
        url = "#{Settings.thor_monitor.url}/api/run_test"

        Timeout.timeout(30) do
          response = self.class.post(url, options)

          JSON.parse(response) if response.success?
        end
      end

      def test_state(uid)
        url = "#{Settings.thor_monitor.url}/api/test_state?uid=#{uid}"

        Timeout.timeout(30) do
          response = self.class.post(url)

          JSON.parse(response) if response.success?
        end
      end
    end
  end
end
