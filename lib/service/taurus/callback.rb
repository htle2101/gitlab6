module Service::Taurus

  class Callback
    attr_accessor :online_job_id, :status, :createurl, :updateurl
    def initialize(options ={})
      @options = options
      options.each { |k, v| send("#{k}=", v) if respond_to?("#{k}=") }
    end

    def execute
      @online_job = OnlineJob.find(online_job_id)
      @online_job.update_attributes(status: status, success_at: Time.now,
                                    create_url: createurl, update_url: updateurl)
    end

  end

end