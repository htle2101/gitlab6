module Service::Taurus
  class TaurusConnectionError < StandardError; end
  class TaurusDeployError < StandardError; end
  class TaurusGetDeployStatusError < StandardError; end

  class Taurus
    include HTTParty

    def initialize(options = {})
      if options[:taurus_env] == 'product'
        taurus_host = Settings.taurus['online_host']
      elsif options[:taurus_env] == 'ppe'
        taurus_host = Settings.taurus['ppe']
      else
        taurus_host = options[:taurus_env] == 'alpha' ? Settings.taurus['alpha'] : Settings.taurus['beta']
      end
      taurus_port = (options[:taurus_env] == "product") ? Settings.taurus['online_port'] : Settings.taurus['offline_port']
      self.class.base_uri("http://#{taurus_host}:#{taurus_port}")
      @timeout = options[:timeout] || 50
    end

    def post(path, params = {}, param_style = "form")
      begin
        puts Time.now
        response = Timeout::timeout(@timeout) do
          options = (param_style == "form") ? { :body => params } : { :query => params }
          self.class.post(path, options)
        end
      rescue => ex
        puts Time.now
        Logbin.logger("rollout").info("Start to deploy job.", {path: path, params: params, exception: ex})
        raise TaurusConnectionError.new(ex)
      end
      response
    end

    def deploy(params = {})
      deploy_params ={ :deployId => params[:deployId],
                       :ip => params[:ip],
                       :file => params[:file],
                       :name => params[:name],
                       :url => job_deploy_callback_url(params[:deployId]) }
      post("/api/deploy", deploy_params)
    end

    def job_deploy_callback_url(deployId)
      "http://#{Settings.dp['releases_ip']}/api/v3/taurus/#{deployId}/update_status"
    end

    def get(path, params = {})
      begin
        Timeout::timeout(@timeout) do
          self.class.get(path, query: params)
        end
      rescue => ex
        raise TaurusGetDeployStatusError.new(ex)
      end
    end

    def get_deploy_status(deployId)
      get("/deploy", {deployId: deployId})
    end


  end
end
