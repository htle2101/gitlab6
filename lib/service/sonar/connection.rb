module Service::Sonar
  class Connection

    attr_reader :host

    def initialize(host = Settings.sonar['sonar_url'])
      @host = host
    end

    def get(path, params = {}, options = { timeout: true })
      resp = if options[:timeout]
        timeout { request(path, params) }
      else
        request(path, params)
      end
      resp.body if resp && resp.code == '200'
    end  

    private
    def timeout(timeout = 2, &block)
      begin
        Timeout::timeout(2) do
          block.call
        end
      rescue Timeout::Error, NoMethodError
        nil  
      end
    end

    def request(path, params)
      uri = URI("http://#{@host}#{path}")
      uri.query = URI.encode_www_form(params)
      Net::HTTP.get_response(uri)
    end
  end  
end  
