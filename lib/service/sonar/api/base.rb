module Service::Sonar
  class Api::Base

    attr_reader :response, :request, :full_name

    def initialize(full_name, args = {})
      @full_name = full_name
      args.each {|k, v| instance_variable_set "@#{k}", v }
      @request = Service::Sonar::Request.new
    end

    def execute
      @response = @request.resources(params)
      self
    end

    def params
      {}
    end

    def format_date(date)
      return date if date.blank?
      date.split('T')[0]
    end  

  end
end  
