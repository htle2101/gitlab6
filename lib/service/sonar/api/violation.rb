module Service::Sonar
  module Api
    class Violation < Base

      def params
        { format: 'json', resource: @full_name, metrics: 'violations,violations_density', includetrends: true }
      end

      def result
        return nil if response.blank?
        json = JSON.parse(response)[0]
        return nil if json['msr'].blank?

        results = json['msr'].inject({}) do |result, msr|
          result[msr['key']] = msr.slice('frmt_val', 'trend')
          result
        end

        results
      end
    end
  end
end
