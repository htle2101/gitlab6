module Service::Sonar
  module Api
    class Coverage < Base

      def params
        { format: 'json', resource: @full_name, metrics: 'new_coverage,coverage', includetrends: true }
      end

      def result
        return nil if response.blank?
        j = JSON.parse(response)[0]

        coverage = j['msr'].select{|m| m['key'] == 'coverage'}[0]["val"].try(:round, 2)
        new_coverage = j['msr'].select{|m| m['key'] == 'new_coverage'}[0]

        new_coverage = {
          "#{format_date(j['p2d'])}" => new_coverage['var2'].try(:round, 2),
          "#{format_date(j['p3d'])}" => new_coverage['var3'].try(:round, 2), 
          "#{format_date(j['p4d'])}" => new_coverage['var4'].try(:round, 2)
        }

        trend = j['msr'].select{|m| m['key'] == 'coverage'}[0]['var3'].try(:round, 2)

        { coverage: coverage, new_coverage: new_coverage, trend: trend }          
      end
    end

  end
end
