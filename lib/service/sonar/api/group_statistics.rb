module Service::Sonar
  module Api
    class GroupStatistics
      def initialize(group)
        @group = group
      end

      def average_coverage
        Rails.cache.fetch(cache_key(:average_coverage)) do
          if sample_projects.blank?
            {}
          else
            project_coverage_pairs = sample_projects.inject({}) do |result, p|
              result[p.name] = Service::Sonar::Api::ProjectStatistics.new(p).modules_average_coverage
              result
            end

            project_lines_to_cover_pairs = sample_projects.inject({}) do |result, p|
              total_lines = 0
              Service::Sonar::Api::ProjectStatistics.new(p).module_lines_to_cover_pairs.each do |k, lines|
                total_lines = total_lines + lines unless lines.blank?
              end
              result[p.name] = total_lines
              result
            end

            project_covered_lines_pairs = sample_projects.inject({}) do |result, p|
              total_lines = 0
              Service::Sonar::Api::ProjectStatistics.new(p).module_covered_lines_pairs.each do |k, lines|
                total_lines = total_lines + lines unless lines.blank?
              end
              result[p.name] = total_lines
              result
            end

            average_coverage = (project_covered_lines_pairs.values.inject(:+).to_f / project_lines_to_cover_pairs.values.inject(:+).to_f * 100.0).try(:round, 2)
            {average_coverage: average_coverage, project_coverage_pairs: project_coverage_pairs}
          end
        end
      end

      def new_line_code_average_coverage
        Rails.cache.fetch(cache_key(:new_line_code_average_coverage)) do
          if sample_projects.blank?
            {}
          else
            project_lines_to_cover_pairs = sample_projects.inject({}) do |result, p|
              total_lines = 0
              Service::Sonar::Api::ProjectStatistics.new(p).period_module_lines_to_cover_pairs.each do |k, lines|
                total_lines = total_lines + lines unless lines.blank?
              end
              result[p.name] = total_lines
              result
            end
            project_covered_lines_pairs = sample_projects.inject({}) do |result, p|
              total_lines = 0
              Service::Sonar::Api::ProjectStatistics.new(p).period_module_covered_lines_pairs.each do |k, lines|
                total_lines = total_lines + lines unless lines.blank?
              end
              result[p.name] = total_lines
              result
            end
            new_line_count = project_lines_to_cover_pairs.values.inject(:+)
            covered_line_count = project_covered_lines_pairs.values.inject(:+)
            new_line_code_average_coverage = (covered_line_count.to_f / new_line_count * 100.0).try(:round, 2)

            {
              new_line_code_average_coverage: new_line_code_average_coverage,
              project_lines_to_cover_pairs: project_lines_to_cover_pairs,
              project_covered_lines_pairs: project_covered_lines_pairs,
              new_line_count:  new_line_count,
              covered_line_count: covered_line_count
            }
          end
        end
      end

      def sample_projects
        @sample_projects ||= begin
          @group.projects.select{ |p| Service::Sonar::Api::ProjectStatistics.new(p).namespace_statistical? }
        rescue => e
          []
        end
      end

      def expire_cache
        Rails.cache.delete(cache_key(:average_coverage))
        Rails.cache.delete(cache_key(:new_line_code_average_coverage))
      end

      def reload
        expire_cache
        average_coverage rescue nil
        new_line_code_average_coverage rescue nil
      end

      def cache_key(type)
        "#{type}:#{@group.id}"
      end

    end
  end
end
