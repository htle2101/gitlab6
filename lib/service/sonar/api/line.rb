module Service::Sonar
  module Api
    class Line < Base

      def params
        { format: 'json', resource: @full_name,
          metrics: 'ncloc,lines,statements,files,classes,packages,functions,accessors', includetrends: true }
      end

      def result
        return nil if response.blank?
        json = JSON.parse(response)[0]
        return nil if json['msr'].blank?

        results = json['msr'].inject({}) do |result, msr|
          result[msr['key']] = {
            'val' => msr['val'].to_i,
            'frmt_val' => msr['frmt_val'],
            'trends' => {
              "#{format_date(json['p2d'])}" => msr['var2'].to_i,
              "#{format_date(json['p3d'])}" => msr['var3'].to_i,
              "#{format_date(json['p4d'])}" => msr['var4'].to_i
            }
          } 
          result
        end

        results
      end
    end

  end
end
