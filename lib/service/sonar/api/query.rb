module Service::Sonar
  module Api
    class Query < Base

      def params
        @params.merge(resource: @full_name) if @params
      end

      def result
        return nil if response.blank?
        JSON.parse(response)[0]     
      end
    end

  end
end
