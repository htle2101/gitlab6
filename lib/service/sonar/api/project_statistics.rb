module Service::Sonar
  module Api
    class ProjectStatistics
      attr_reader :project

      def initialize(project)
        @project = project
      end

      def project_base_query
        return false unless project.sonar
        return false unless project.sonar.query
        Rails.cache.fetch(cache_key(:project_base_query)) do
          sonar = project.sonar
          return nil unless sonar
          sonar.query
        end
      end

      def namespace_statistical?
        return false unless project_base_query
        Rails.cache.fetch(cache_key(:namespace_statistical?)) do
          return false if project_base_query.blank?
          query_info =  project_base_query.result
          return false if query_info.blank?
          sonar_last_running_time = query_info['date'].to_time
          Time.now - sonar_last_running_time <= Settings.sonar['namespace_statistics_period'].seconds
        end
      end

      def query_period_metric(full_name,metric)
        if namespace_statistical?
          params = { format: 'json', includetrends: true, metrics: metric }
          return nil unless period_number_for_statistics
          varn = 'var' + period_number_for_statistics.to_s
          return nil unless project.sonar(full_name,params).query.result['msr']
          project.sonar(full_name,params).query.result['msr'].select{|m| m['key'] == metric}[0][varn]
        else
          nil
        end
      end

      def query_general_metric(full_name,metric)
        params = { format: 'json', includetrends: true, metrics: metric }
        query_result = project.sonar(full_name,params).query
        return nil if query_result.blank?

        query_result = query_result.result
        return nil if query_result.blank?

        query_result['msr']
      end

      def period_number_for_statistics
        Rails.cache.fetch(cache_key(:period_number_for_statistics)) do
          return nil unless namespace_statistical?
          periods = project_base_query.result.select{|k,v| k =~ /p\dd/}
          return nil unless periods
          period_dates = periods.values
          comparison_date = Time.now - Settings.sonar['namespace_statistics_period'].seconds

          arr = period_dates.map do |p|
            [(p.to_time - comparison_date).abs, p]
          end
          nearest_date = arr.sort_by {|sub_arr| sub_arr[0]}.first[1]
          nearest_pnd = periods.invert.select{|k,v| k == nearest_date}.values[0]
          nearest_pnd_number = nearest_pnd.slice(1,1).to_i
          return nearest_pnd_number if (nearest_date.to_time < comparison_date) || (period_dates.length == nearest_pnd_number)
          nearest_pnd_number + 1
        end
      end

      def modules_for_statistics
        @modules_for_statistics ||= begin
                                      return nil unless project.branch_for_sonar
                                      project.branch_for_sonar.modules.select{|m| m.raw[:leafNode] == true && m.raw[:ut] !~ /false/i}
        end
      end

      def ut_statistics_info
        Rails.cache.fetch(cache_key(:ut_statistics_info)) do
          begin
            modules_info = {}
            modules_for_statistics.each do |pomModule|
              modules_info[pomModule.module_name] = query_general_metric(pomModule.full_name,
                                                      'lines_to_cover,uncovered_lines,new_lines_to_cover,new_uncovered_lines'
                                                    )
            end
          rescue => e
            modules_info = {}
          end
          modules_info
        end
      end

      def count_average_coverage(uncovered_pairs,to_cover_pairs)
        uncovered_lines = uncovered_pairs.values
        lines_to_cover = to_cover_pairs.values
        return nil  if lines_to_cover.inject(:+).to_i == 0
        average_coverage = ((1 - (uncovered_lines.inject(:+).to_f / lines_to_cover.inject(:+))) * 100).try(:round, 2)
      end

      def module_uncovered_lines_pairs
        return nil unless namespace_statistical?
        Rails.cache.delete(cache_key(:module_uncovered_lines_pairs)) if Rails.cache.read(cache_key(:module_uncovered_lines_pairs)) == {}
        Rails.cache.fetch(cache_key(:module_uncovered_lines_pairs)) do
          begin
            pairs = {}
            ut_statistics_info.each do |module_name,info|
              pairs[module_name] = info.select{|m| m['key'] == 'uncovered_lines'}[0]['val'].to_i unless info.blank?
            end
          rescue => e
            pairs = {}
          end
          pairs
        end
      end

      def module_lines_to_cover_pairs
        return nil unless namespace_statistical?
        Rails.cache.delete(cache_key(:module_lines_to_cover_pairs)) if Rails.cache.read(cache_key(:module_lines_to_cover_pairs)) == {}
        Rails.cache.fetch(cache_key(:module_lines_to_cover_pairs)) do
          begin
            pairs = {}
            ut_statistics_info.each do |module_name,info|
              pairs[module_name] = info.select{|m| m['key'] == 'lines_to_cover'}[0]["val"].to_i unless info.blank?
            end
          rescue => e
            pairs = {}
          end
          pairs
        end
      end

      def module_covered_lines_pairs
        return nil unless namespace_statistical?
        Rails.cache.delete(cache_key(:module_covered_lines_pairs)) if Rails.cache.read(cache_key(:module_covered_lines_pairs)) == {}
        Rails.cache.fetch(cache_key(:module_covered_lines_pairs)) do
          pairs = {}
          module_uncovered_lines_pairs.each do |k,v|
            pairs[k] = module_lines_to_cover_pairs[k] - v
          end
          pairs
        end
      end

      def period_module_lines_to_cover_pairs
        module_new_code_lines_to_cover_pairs_var('var' + period_number_for_statistics.to_s)
      end

      def period_module_covered_lines_pairs
        module_new_code_covered_lines_pairs_var('var' + period_number_for_statistics.to_s)
      end

      def modules_average_coverage
        if module_uncovered_lines_pairs.blank?
          Rails.cache.delete(cache_key(:modules_average_coverage))
          return nil
        end
        Rails.cache.fetch(cache_key(:modules_average_coverage)) do
          count_average_coverage(module_uncovered_lines_pairs,module_lines_to_cover_pairs) unless module_uncovered_lines_pairs.blank?
        end
      end

      def modules_average_coverage_var(var_num='var2')
        count_average_coverage(module_uncovered_lines_pairs_var(var_num),module_lines_to_cover_pairs_var(var_num))
      end

      def module_lines_to_cover_pairs_var(var_num='var2')
        pairs = {}
        ut_statistics_info.each do |module_name,info|
          pairs[module_name] = ( module_lines_to_cover_pairs[module_name] - info.select{|m| m['key'] == 'lines_to_cover'}[0][var_num].to_i ) unless info.blank?
        end
        pairs
      end

      def module_uncovered_lines_pairs_var(var_num='var2')
        pairs = {}
        ut_statistics_info.each do |module_name,info|
          pairs[module_name] = ( module_uncovered_lines_pairs[module_name] - info.select{|m| m['key'] == 'uncovered_lines'}[0][var_num].to_i ) unless info.blank?
        end
        pairs
      end

      def module_new_code_lines_to_cover_pairs_var(var_num='var2')
        pairs = {}
        ut_statistics_info.each do |module_name,info|
          if !info.blank? && !info.select{|m| m['key'] == 'new_lines_to_cover'}.blank?
            pairs[module_name] = info.select{|m| m['key'] == 'new_lines_to_cover'}[0][var_num].to_i
          end
        end
        pairs
      end

      def module_new_code_uncovered_lines_pairs_var(var_num='var2')
        pairs = {}
        ut_statistics_info.each do |module_name,info|
          if !info.blank? && !info.select{|m| m['key'] == 'new_uncovered_lines'}.blank?
            pairs[module_name] = info.select{|m| m['key'] == 'new_uncovered_lines'}[0][var_num].to_i
          end
        end
        pairs
      end

      def module_new_code_covered_lines_pairs_var(var_num='var2')
        pairs = {}
        ut_statistics_info.each do |module_name,info|
          if !info.blank? && !info.select{|m| m['key'] == 'new_uncovered_lines'}.blank?
            uncovered_lines = info.select{|m| m['key'] == 'new_uncovered_lines'}[0][var_num].to_i
            pairs[module_name] = module_new_code_lines_to_cover_pairs_var(var_num)[module_name] - uncovered_lines unless uncovered_lines.blank?
          end
        end
        pairs
      end

      def modules_new_code_average_coverage_var(var_num='var2')
        count_average_coverage(module_new_code_uncovered_lines_pairs_var(var_num), module_new_code_lines_to_cover_pairs_var(var_num))
      end

      def modules_new_code_average_coverage
        return nil unless project_base_query
        return nil unless project_base_query.response
        Rails.cache.fetch(cache_key(:modules_new_code_average_coverage)) do
          parse_result = JSON.parse(project_base_query.response)[0]
          {
            "#{format_date(parse_result['p2d'])}" => modules_new_code_average_coverage_var('var2').try(:round, 2),
            "#{format_date(parse_result['p3d'])}" => modules_new_code_average_coverage_var('var3').try(:round, 2),
            "#{format_date(parse_result['p4d'])}" => modules_new_code_average_coverage_var('var4').try(:round, 2)
          }
        end
      end

      def format_date(date)
        return date if date.blank?
        date.split('T')[0]
      end

      def modules_average_coverage_trend
        return nil if modules_average_coverage.blank? || modules_average_coverage_var('var3').blank?
        if modules_average_coverage > modules_average_coverage_var('var3')
          'up'
        elsif modules_average_coverage < modules_average_coverage_var('var3')
          'down'
        else
          'keep'
        end
      end

      def expire_cache
        Rails.cache.delete(cache_key(:project_base_query))
        Rails.cache.delete(cache_key(:period_number_for_statistics))
        Rails.cache.delete(cache_key(:ut_statistics_info))
        Rails.cache.delete(cache_key(:namespace_statistical?))
        Rails.cache.delete(cache_key(:module_uncovered_lines_pairs))
        Rails.cache.delete(cache_key(:module_covered_lines_pairs))
        Rails.cache.delete(cache_key(:module_lines_to_cover_pairs))
        Rails.cache.delete(cache_key(:modules_average_coverage))
        Rails.cache.delete(cache_key(:modules_new_code_average_coverage))
      end

      def reload
        expire_cache
        project_base_query rescue nil
        period_number_for_statistics rescue nil
        ut_statistics_info rescue nil
        namespace_statistical? rescue nil
        module_uncovered_lines_pairs rescue nil
        module_covered_lines_pairs rescue nil
        module_lines_to_cover_pairs rescue nil
        modules_average_coverage rescue nil
        modules_new_code_average_coverage rescue nil
      end

      def cache_key(type)
        "#{type}:#{@project.id}"
      end
    end
  end
end
