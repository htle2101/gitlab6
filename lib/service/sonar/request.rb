module Service::Sonar
  class Request

    attr_reader :conn

    def initialize(conn = nil)
      @conn = conn || Connection.new
    end

    def resources(params, options = {})
      @conn.get('/api/resources', params, options)
    end

  end  
end  
