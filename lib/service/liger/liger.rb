module Service::Liger
  class LigerConnectError < StandardError; end
  class LigerAccessError < StandardError; end

  class Liger
    include HTTParty

    def initialize(options = {})
      self.class.base_uri("http://#{  options[:host] || Settings.liger['url']}")
      @timeout = options[:timeout] || 2
    end

    def post(path, params = {})
      begin
        response = Timeout::timeout(@timeout) do
          self.class.post(path, body: params)
        end
      rescue => ex
        raise LigerConnectError.new(ex)
      end
      if response.code != 200 || response['code'] != 0
        raise LigerAccessError.new(response["message"])
      end
      response
    end

    def get(path, params = {})
      begin
        response = Timeout::timeout(@timeout) do
          self.class.get(path, query: params)
        end
      rescue => ex
        raise LigerConnectError.new(ex)
      end
      if response.code != 200 || response['code'] != 0
        raise LigerAccessError.new(response)
      end
      response
    end  
    
    def view_keys(env, keys)

    end

    def update_keys(env, keys)

    end  
  end
end
