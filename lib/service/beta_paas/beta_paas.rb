class Service
  module BetaPaas
    class BetaPaas
      include HTTParty

      def get_instance_id(app_id)
        url = "#{Settings.op['beta_paas_url']}console/api/instance?op=instances&appId=#{app_id}"

        Timeout.timeout(30) do
          response = self.class.post(url)

          if response.success?
            response['data'].first['instanceId']
          end
        end
      end

      def restart_tomcat(app_id, instance_id)
        url = "#{Settings.op['beta_paas_url']}console/api/instance?op=restart&appId=#{app_id}&instanceId=#{instance_id}"

        Timeout.timeout(30) do
          response = self.class.post(url)

          if response.success?
            response
          end
        end
      end

      def restart_tomcat_result(operation_id)
        url = "#{Settings.op['beta_paas_url']}console/api/app?op=operationStatus&operationId=#{operation_id}"

        Timeout.timeout(30) do
          response = self.class.get(url)

          if response.success?
            response
          end
        end
      end
    end
  end
end

