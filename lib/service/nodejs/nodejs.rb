module Service::Nodejs
  class NodejsConnectError < StandardError; end
  class NodejsAccessError < StandardError; end

  class Nodejs
    include HTTParty

    def initialize(options = {})
      self.class.base_uri("http://#{  options[:host] || Settings.nodejs['url']}")
      @token = options[:token] || Settings.nodejs['token']
      @timeout = options[:timeout] || 5
    end

    def post(path, params = {})
      begin
        response = Timeout::timeout(@timeout) do
          self.class.post(path, body: params.to_json, headers: { 'Content-Type' => 'application/json', 'token' => @token })
        end
      rescue => ex
        raise NodejsConnectError.new(ex)
      end
      if response.code != 200
        errors = JSON(response.body)["error"] rescue 'rollback server return errors.'
        raise NodejsAccessError.new(errors)
      end
      response
    end

    def get(path, params = {})
      begin
        response = Timeout::timeout(@timeout) do
          self.class.get(path, query: params)
        end
      rescue => ex
        raise NodejsConnectError.new(ex)
      end
      raise NodejsAccessError.new(path) if response.code != 200
      response
    end

    def versions(appname)
      get("/api/package/#{appname}/versions")
    end

    def rollback(appname, from, to)
      post("/api/package/#{appname}/rollback", "fromVersion" => from, "toVersion" => to)
    end

    def histories(appname)
      get("/api/package/#{appname}/histories")
    end  
    
    # [{"ip":"10.1.1.2", "port": "8080", "name": "xxxx"}
    # def add_node(pool_name, members)
    #   params = { "name" => pool_name, "members" => members }.to_json
    #   post('/api/pool/add', params)
    # end

    # def delete_node(pool_name, node_names)
    #   post("/api/pool/#{pool_name}/delMember", node_names.to_json)
    # end
  end
end
