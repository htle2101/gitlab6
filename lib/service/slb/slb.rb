module Service::Slb
  class SlbConnectError < StandardError; end
  class SlbAccessError < StandardError; end

  class Slb
    include HTTParty

    def initialize(options = {})
      self.class.base_uri("http://#{  options[:host] || Settings.slb['url']}")
      @timeout = options[:timeout] || 4
    end

    def post(path, params = {})
      begin
        response = Timeout::timeout(@timeout) do
          self.class.post(path, body: params, headers: { 'Content-Type' => 'application/json' })
        end
      rescue => ex
        raise SlbConnectError.new(ex)
      end
      if response.code != 200 || !([0, -2].include? JSON(response.body)["errorCode"])
        raise SlbAccessError.new(JSON(response.body)["message"])
      end
      response
    end
    
    # [{"ip":"10.1.1.2", "port": "8080", "name": "xxxx"}
    def add_node(pool_name, members)
      params = { "name" => pool_name, "members" => members }.to_json
      post('/api/pool/add', params)
    end

    def delete_node(pool_name, node_names)
      post("/api/pool/#{pool_name}/delMember", node_names.to_json)
    end

    def deploy_node(pool_name)
      post("/api/pool/#{pool_name}/deploy")
    end 
  end
end