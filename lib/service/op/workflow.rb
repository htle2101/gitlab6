module Service::Op
  class WorkflowConnectError < StandardError; end
  class WorkflowInterfaceAccessError < StandardError; end

  class Workflow
    include HTTParty

    def initialize(options = {})
      self.class.base_uri("http://#{ options[:host] || Settings.op_prod['workflow_url'] }")
      @timeout = options[:timeout] || 3
    end

    def get(path, params = {})
      begin
        response = Timeout::timeout(@timeout) do
          self.class.get(path, query: params)
        end
      rescue => ex
        raise WorkflowConnectError.new(ex)
      end
      raise WorkflowInterfaceAccessError.new(path) if response.code == 404
      response
    end

    def transition_status(app,deploy_time)
    	params = { 'app' => app, 'deploy_time' => deploy_time}
    	get('/api/transition/status', params)
    end


  end
end