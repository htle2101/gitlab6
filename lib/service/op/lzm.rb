module Service::Op
  class LzmServerConnectError < StandardError; end
  class LzmInterfaceAccessError < StandardError; end

  class Lzm
    include HTTParty

    def initialize(options = {})
      self.class.base_uri("http://#{  options[:host] || Settings.op_prod['lzm_url'] }")
      @timeout = options[:timeout] || 2
    end

    def get(path, params = {})
      begin
        Timeout::timeout(@timeout) do
          self.class.get(path, query: params)
        end
      rescue => ex
        Logbin.logger("rollout").info("Start to call lzm.", {path: path, params: params, exception: ex})
        raise LzmServerConnectError.new(ex)
      end
    end

    def grouping_return_url
      "http://#{RemoteCi::CiPostBuild.host_with_port}/api/v3/ci/#{build_branch.jenkins_job_name}/#{build_branch.command}"
    end

    def post(path, params = {})
      begin
        response = Timeout::timeout(@timeout) do
          options = { :query => params }
          self.class.post(path, options)
        end
      rescue => ex
        Logbin.logger("rollout").info("Start to call lzm.", {path: path, params: params, exception: ex})
        raise LzmServerConnectError.new(ex)
      end
      raise LzmInterfaceAccessError.new(path) if [404,500].include?(response.code)
      response
    end

    def terminate_grouping_rollout(grouping_strategy_id, package_path)
      params = { 'package_id' => grouping_strategy_id, 'package_path' => package_path}
      post("/deploy/kill", params)
    end

    def static_package_rollout(app, grouping_strategy_id, path)
      params = { 'app' => app, 'package_id' => grouping_strategy_id , 'path' => path}
      post("/deploy/static_package_deploy", params)
    end

    def grouping_rollout(action, params = {})
      get("/deploy/#{action}", params)
    end

    def nodejs_grouping_rollout(action, params = {})
      get("/deploy/nodejs/#{action}", params)
    end

    def rollback(action,params = {})
      get("/deploy/#{action}", params)
    end

    def clear_cache(app, type)
      response = post("/deploy/cleancache", 'package' => app, 'type' => type) rescue false
      return { success: false, msg: 'clear cache fail.' } if response && !response.body.include?('ok')
      return { success: true, msg: 'clear cache success.' }
    end
  end
end
