module Service::Op
  class CmdbServerConnectError < StandardError; end
  class CmdbInterfaceAccessError < StandardError; end

  class Cmdb
    include HTTParty

    def initialize(options = {})
      self.class.base_uri("http://#{  options[:host] || Settings.op_prod['cmdb_url']}")
      @timeout = options[:timeout] || 3
    end

    def get(path, params = {})
      begin
        response = Timeout::timeout(@timeout) do
          self.class.get(path, query: params)
        end
      rescue => ex
        raise CmdbServerConnectError.new(ex)
      end
      raise CmdbInterfaceAccessError.new(path) if response.code != 200
      response
    end

    def grouping_strategy(name)
      get("/deploy/groups/#{name}")
    end

    def has_prelease(name)
       ( response = get("/deploy/hasprelease/#{name}") ) rescue return nil
      return nil if response.code != 200
      return response.body.include?("true")
    end

    def latest_rollout_status(id)
      get("/deploy/package/status/#{id}")
    end

    def app_grouped_machines(app, env)
      get("/deploy/vgroups/#{app}", type: env)
    end

    def app_ips(app, env)
      @app_grouped_machines = app_grouped_machines(app, env)
      @app_grouped_machines["groups"].map{|group| group_ips(group) }.flatten.uniq
    end

    def group_ips(group)
      return group[1] if group.class == Array # no group
      group["hostname"].map{|machine| machine[1]}
    end
  end
end