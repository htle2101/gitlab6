module Service::DBA

  class DBAServiceError < StandardError; end

  class Connection
    attr_reader :host

    def initialize(host)
      @host = host
    end

    def get(path, params, options = {})
      request(path, params, options) { |uri| Net::HTTP.get_response(uri) }
    end

    def post(path, params, options = {})
      request(path, params, options) { |uri| Net::HTTP.post_form(uri, params) }
    end

    def request(path, params, options = {})
      begin
        Timeout::timeout(options[:timeout] || 5) do
          uri = URI("http://#{host}#{path}")   
          res = yield uri
          if res.is_a?(Net::HTTPSuccess)
            res.body
          else
            raise "Request failed: #{res}."
          end
        end
      rescue => ex
        raise DBAServiceError.new(ex)
      end
    end
  end
end
