module Service::DBA
  class Request

    def initialize
      @conn = Connection.new(Settings.scripts['tools.dba'])
    end

    def upload(params)
      @conn.post('/redmine.php', params)
    end

    def execute(params)
      @conn.post('/button.php', params)
    end

    def getdb(params = nil)
      @conn.get('/getdb.php', params)
    end
  end

end
