require 'digest/sha1'

module Service::Search
  class RepoIndexer

    attr_accessor :project, :index_name, :branch

    LIMITED_SIZE = 2 * 1024 * 1024

    def initialize(project, branch = nil)
      @project = project
      @branch = branch
      @index_name = "git_#{project.path[0]}"
      @index = Tire::Index.new(@index_name)
    end

    def branch
      @branch.blank? ? project.repository.root_ref : @branch
    end

    def init!
      @index.delete
      self.mapping
    end

    def mapping
      unless @index.exists?
        @index.create(
          :settings => {
            :analysis => {
              :analyzer => {
                :code => {
                  :type => "pattern",
                  :filter => ["lowercase"],
                  :pattern => "[\.\(\),;\\[\\]\s+]"
                }
              }
            }
          },
          :mappings => {
            :file => {
              :properties => {
                path: {type: "string", :index => 'not_analyzed'},
                name: {type: "string", :index => 'not_analyzed'},
                language: {type: "string", :index => 'not_analyzed'},
                code: {type: "string", :analyzer => "code" }
              }
            }
          }
        )
      end
    end

    def refresh
      @index.refresh
    end

    def index!
      @project.tree.blobs.each{ |b| index_file(b, '') }
      @project.tree.trees.each{ |t| index_tree(t, "#{t.name}/") }
      return @index
    end

    def index_for_push!(oldrev, newrev)
      diff_tree = project.repo.diff_tree(oldrev, newrev)
      diff_tree.deleted_files.each { |file| delete_file(file) }
      diff_tree.added_and_changed_files.each { |file| index_file(file.blob, file.path) }
      return @index
    end

    def index_tree(tree, path)
      tree.blobs.each{|b| index_file(b, path) }
      tree.trees.each{|t| index_tree(t, "#{path}#{t.name}/") }
    end

    def index_file(blob, path)
      if blob.text? && blob.size < LIMITED_SIZE
        @index.store :_id => sha("#{path}#{blob.name}@#{branch}"), :type => 'file', :path => path, :name => blob.name,
          :language => blob.language.try(:name).to_s.downcase, :project_id => project.id, :public => project.public?, :code => blob.data
      end
    end

    def delete_file(file)
      @index.remove :_id => sha("#{file.path}#{file.name}@#{branch}"), :_type => "file"
    end

    def sha(string)
      Digest::SHA1.hexdigest string
    end

    def delete!
      begin
        project_id = project.id
        Tire::DeleteByQuery.new(@index_name, :type => 'file') do
          term :project_id, project_id
        end.perform
      rescue
      end
    end

  end
end
