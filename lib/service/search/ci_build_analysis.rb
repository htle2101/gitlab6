module Service::Search
  class CiBuildAnalysis
    MAX_SIZE = 1_000_000

    attr_reader :index

    def initialize(index)
      @index = index
    end

    def execute(params = {})
      results = _search(params).results
      terms = results.facets['result']["terms"]
      data = {}
      terms.each do |facet|
        job_name, build_result = facet['term'].split('|')
        data[job_name] ||= {}
        data[job_name][build_result] = facet['count']
        data[job_name]['total'] = (data[job_name]['total'] || 0) + facet['count']
      end
      data
    end

    private
    def _search(params = {})
      Tire.search index do
        query do
          boolean do
            must_not {wildcard :env, "*shadow" }
            must { term :env, params[:env] } if params[:env].present?
            must { range :log_at, :gte => params[:gt] } if params[:gt].present?
            must { range :log_at, :lte => params[:lt] } if params[:lt].present?
          end
        end
        facet "result" do
          terms_with_script("doc['jenkins_job_name'].value + \"|\" + doc['result'].value", {size: 10000})
        end
      end
    end
  end
end
