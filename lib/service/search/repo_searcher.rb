module Service::Search
  class RepoSearcher

    attr_reader :keywords, :options

    def initialize(keywords, options = {})
      @keywords = keywords
      @options = options
    end

    def index
     @index_name ||= if options[:project_id] && (project = Project.find(options[:project_id]))
        "git_#{project.path[0]}"
      else
        "git_*"
      end
    end

    def search

      return [] unless (options[:project_id].blank? || Tire::Index.new(index).exists?)

      r = request("*#{trans_keyword}*") rescue []
      return r.results if !r.blank? && !r.results.blank?
      r = request("#{trans_keyword}") rescue []
      return r.results if !r.blank? && !r.results.blank?
      r = request(keyword) rescue []
      return r.results if !r.blank? && !r.results.blank?
      return []
    end

    def language
      keywords[/(?<=language:)\w+/]
    end

    def keyword
      keywords.gsub(/language:\w+/, '').strip
      # Tire::Search::SearchRequestFailed
      # .delete("+").delete("-").delete("&&").delete("||").delete("!").delete("(").delete(")").delete("{").delete("}").delete("[").delete("]").delete("^").delete('"').delete("~").delete("*").delete("?").delete(":").delete("\\").delete("/")
    end

    def trans_keyword
      @trans_keyword ||= keyword.gsub("+", ' ').gsub("&&", "\\$\\$").gsub("||", "\\|\\|").gsub("!", "\\!").gsub(/(\(|\)|\{|\}|\[|\]|")/, ' ').gsub("^", "\\^").gsub("~", "\\~").gsub("*", "\\*").gsub("?", "\\?").gsub(":", "\\:").gsub("/", "\\/").gsub("\\", "\\\\")
    end

    def databaseinfo_search
      return [] if options[:project_ids].blank?
      databaseinfo_request.results
    end

    # private
    def request(words)
      _keyword = keyword
      Tire.search index do |tire|
        tire.query do
          boolean do
            must { string "code:#{words}" }
          end
        end

        tire.filter :term, :language => language.downcase if language.present?
        tire.filter :term, :project_id => options[:project_id] unless options[:project_id].blank?
        tire.filter :term, :public => true if options[:project_id].blank?
        tire.sort { by :_score }

        tire.from offset
        tire.size 20
        tire.fields :project_id, :path, :name, :language
      end
    end

    def databaseinfo_request
      Tire.search index do |tire|
        tire.query do
          boolean do
            must {
              string "code:*dp\\!*"
            }
            must {
              string "code:10.1"
            }
          end
        end

        tire.filter :or , options[:project_ids].map{|p| { term: { project_id: p }} } unless options[:project_ids].blank?

        tire.from offset
        tire.size 20
        tire.fields :project_id, :path, :name, :language, :code
      end
    end  

    def offset
      ((options[:page] || 1).to_i - 1) * 20
    end

  end
end
