module Service::Cache
  class CachePre < Cache
    def initialize(options = {})
      self.class.base_uri("http://#{  options[:host] || Settings.op_prod['cache_url_pre']}")
      @timeout = options[:timeout] || 2
    end
  end
end