module Service::Cache
  class CacheServerConnectError < StandardError; end
  class CacheInterfaceAccessError < StandardError; end

  class Cache
    include HTTParty

    def initialize(options = {})
      self.class.base_uri("http://#{  options[:host] || Settings.op_prod['cache_url']}")
      @timeout = options[:timeout] || 2
    end

    def get(path, params = {})
      begin
        response = Timeout::timeout(@timeout) do
          self.class.get(path, query: params)
        end
      rescue => ex
        raise CacheServerConnectError.new(ex)
        Logbin.logger("cache").info("call error.", { host: self.class.base_uri, path: path, params: params, exception: ex } )
      end
      if response.code != 200 || response.body.downcase.strip != 'ok'
        Logbin.logger("cache").info("return error.", { host: self.class.base_uri, path: path, params: params, code: ex } )
        raise CacheInterfaceAccessError.new(path)
      end
      response
    end

    def static_category
      ['DianPing.Common.StaticFile', 'oStaticFileMD5', 'CortexCombo', 'CortexDependency']
    end

    def clear_cache(category, ips = nil)
      params = {category: category}
      params.merge!(ips: ips) unless ips.blank?
      get('/cacheAdminRest/clearCacheByCategory', params)
    end

  end


end
