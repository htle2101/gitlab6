module Service::Phoenix
  
  class Callback
    attr_accessor :ci_branch, :logger_file_name, :phoenix_response, :module_name, :build_number
    include Utils::Logger
    include Utils::Time

    def initialize(attrs)
      attrs.each { |k, v| send("#{k}=", v) if respond_to?("#{k}=") }
      self.logger_file = logger_file_name
      @logger_binder = RemoteCi::LoggerBinder.new(ci_branch, { build_number: build_number, module_name: module_name })
    end

    def execute      
      machine = ci_branch.machines.with_deployer('Phoenix').first          
      return nil unless machine
      @logger_binder.bind_phoenix_log(self.logger_file_name, module_name)
      if phoenix_response['success'] == true
        logger.info("Phoenix deploy successfully and deploy status has been changed.")        
      	deploy_result = 'deploy success'        
        machine.update_status(deploy_result)
      elsif phoenix_response['success'] == false
        logger.info("Deploy Failed, error message: #{phoenix_response['errors']}.")
      	deploy_result = 'deploy error'
        machine.update_status(deploy_result)
      else
        logger.info("Deploy result unknown,check the phoenix system pls.")
      end
    end

    def logger_file_name
      @logger_file_name = "deploy/#{ci_branch.jenkins_job_name}/#{module_name}-#{ts}.log"
    end

  end

end  
