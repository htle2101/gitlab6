class Service
  module Workflow
    class Workflow
      include HTTParty

      def restart_tomcat(machine_ip)
        url = "http://workflow.dp/api/v0.1/tomcat/restart?private_ip=#{machine_ip}"

        Timeout.timeout(30) do
          response = self.class.post(url)

          response if response.success?
        end
      end

      def restart_tomcat_result(uuid)
        url = "http://workflow.dp/api/v0.1/tomcat/restart/result?uuid=#{uuid}"

        Timeout.timeout(30) do
          response = self.class.get(url)

          response if response.success?
        end
      end
    end
  end
end
