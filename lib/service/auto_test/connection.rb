module Service::AutoTest

  class AutoTestServiceError < StandardError; end

  class Connection
    attr_reader :host, :port, :timeout

    def initialize(host, options = {})
      @host = host
      @port = options[:port]
      @timeout = options[:timeout] || 5
    end

    def get(path, params = {})
      begin
        Timeout::timeout(@timeout) do
          uri = URI("http://#{@host}:#{@port}#{path}")
          uri.query = URI.encode_www_form(params)
          Net::HTTP.get_response(uri)
        end
      rescue => ex
        raise AutoTestServiceError.new(ex)
      end
    end

  end
end
