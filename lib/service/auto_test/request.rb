module Service::AutoTest
  class AutoTestCallError < StandardError; end

  class Request
    def initialize
      @conn = Connection.new("192.168.5.143", port: "9292")
    end  
  
    def call(test_project)
      begin
        @conn.get("/api/repo/" + test_project).body
      rescue AutoTestServiceError => ex
        raise AutoTestCallError.new(ex)
      end
    end

  end
end
