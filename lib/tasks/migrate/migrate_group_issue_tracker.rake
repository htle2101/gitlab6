desc "GITLAB | Migrate Group Issue Tracker"
task migrate_group_issue_tracker: :environment do

  Group.find_each(batch_size: 10) do |group|
    next unless group.issue_tracker.blank?
    puts group.name
    ::Projects::CreateContext.new(User.find_by_name(Settings.dp['superadmin']), name: 'issue-tracker', namespace_id: group.id, description: "Issue tracker for group #{group.name}.").execute
  end
end