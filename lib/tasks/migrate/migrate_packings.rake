desc "GITPAB | Migrate Packings"
task migrate_packing: :environment do
	RolloutPacking.find_each(batch_size: 20) do |packing|
		packing.module_names.split(",").each do |module_name|
			mod = packing.modules.find_or_initialize_by_name(module_name)
			mod.url = packing.war_urls[module_name] if packing.war_urls[module_name].present?
			mod.save
			print '.'
		end
	end
end
