desc "GITLAB | Migrate Script Status"
task migrate_script_status: :environment do

	Script.find_each do |script|
		# 3 executeing 4 executed_failed 5 executed_successed
		if script.status >= 3 && script.status <= 5
			# migrate the value of execute status to prd_status
			script.update_column(:prd_status, script.status)
			# rollback status to dba_review_pass
			script.update_column(:status, 2)
		end
	end
end
