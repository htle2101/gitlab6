require 'fileutils'

@timestamp = Time.now.to_i

desc "GITLAB | Migrate CiEnv"
task migrate_ci_envs: :environment do
  puts "1) Striting migrate ci_envs".green
  puts "Delete old ci_envs and create new ci_envs!".red

  ask_to_continue

  backup_database([:ci_envs])

  print "Deleting old ci_envs...".green

  # 删除 alpha 和 beta 的 ci_env
  CiEnv.where(name: ['alpha', 'beta']).delete_all
  # 更新其他环境为默认环境
  CiEnv.update_all(default: true, port: 58422)

  CiEnv.update_all(version: '1.563')
  
  CiEnv.all.each do |ci_env|
    ci_env.property = { "maven2" => 'mvn', "maven3" => 'mvn3', "workspace" => 'jobs/#{jenkins_job_name}/workspace' }
    ci_env.save
  end 

  puts "Done.".green

  puts "Creating new ci_envs...".green

  # 增加新的 ci_env, 5 个 alpha 环境, 5 个 beta 环境
  yaml = File.read("#{Rails.root}/db/migrate_jenkins_jobs/ci_envs.yml")

  ci_envs = YAML.load(yaml)['envs']

  ci_envs.each do |ci_env|
    _ci_env = CiEnv.new(ci_env)

    _ci_env.save

    _ci_env.reload_host_url

    puts "  > name: #{_ci_env.name}, url: #{_ci_env.url}, default: #{_ci_env.default}"
  end

  puts "Done.".green
end

desc "GITLAB | Create CiEnvsNamespace"
task create_ci_envs_namespaces: :environment do
  # 创建 alpha 和 beta 环境的映射关系
  ENVS = ['alpha', 'beta']

  puts "\n2) Staring migrate ci_envs_namespaces".green

  CiEnvsNamespace.delete_all

  Namespace.all.each do |namespace|
    puts "  > #{namespace.name}"

    ENVS.each do |env|
      ci_envs_namespace = CiEnvsNamespace.new(ci_env_name: env, namespace: namespace, ci_env: get_ci_env(env, namespace.id))

      ci_envs_namespace.save

      puts "    >> env: #{ci_envs_namespace.ci_env_name}, server: #{ci_envs_namespace.ci_env.url}"
    end
  end

  puts "Done.".green
end

desc "GITLAB | Migrate Branches"
task migrate_branches: :environment do
  puts "\n3) Starting migrate branches...".green

  backup_database([:build_branches])

  puts "Migrating...".green

  Project.all.each do |project|
    migrate_branches(project)
  end

  puts "Done".green
end

desc "GITLAB | Migrate Jenkins Job"
task migrate_jenkins_jobs: :environment do
  Rake::Task["migrate_ci_envs"].invoke
  Rake::Task["create_ci_envs_namespaces"].invoke
  Rake::Task["migrate_branches"].invoke
end

private

def migrate_branches(project)
  return(puts "  > #{project.name} namespace not found, skip".red) if project.namespace.blank?

  puts "  > #{project.namespace.path}/#{project.name}"

  project.build_branches.each do |branch|
    env_name = get_ci_env_name_by_jenkins_job_name(branch.jenkins_job_name) 

    next unless branch.java?
    # 迁移 alpha 和 beta 环境的 ci
    if env_name == 'alpha' || env_name == 'beta'
      ci_env = CiEnv.find_by_project_and_name(project, env_name)

      branch.ci_env.server.job.delete(branch.jenkins_job_name) rescue nil

      branch.update_attributes(ci_env_id: ci_env.id)

      puts "     >> Updating #{branch.type}(#{branch.id})'s ci_env to <id: #{ci_env.id}, name: #{ci_env.name}, url: #{ci_env.url}>"

      create_job(branch)
    end
  end
end

def create_job(branch)
  print "        >>> Creating jenkins job on remote server(http://#{branch.jenkins_url})..."

  begin
    ci_template = branch.ci_template
    ci_template_xml = branch.ci_env.server.job.get_config(ci_template.template_name)
    ci_template_xml = ci_template.template(branch, ci_template_xml)
    branch.ci_env.server.job.create(branch.jenkins_job_name, ci_template_xml)

    puts "Done".green
    branch.project.hooks.create(url: branch.ci_env.jenkins_hook_path) unless branch.project.hooks.exists?(url: branch.ci_env.jenkins_hook_path)
    branch.project.hooks.create(url: branch.destory_hook_path) unless branch.project.hooks.exists?(url: branch.destory_hook_path)
   
  rescue JenkinsApi::Exceptions::JobAlreadyExistsWithName => error
    puts error.message.red
  rescue => error
    puts "Create failed, #{error.message}".red
  end
end

def backup_database(tables=[])
  backup_dir = "#{Rails.root}/db/backups/"

  FileUtils.mkdir_p backup_dir

  tables.each do |table|
    backup_file = "#{backup_dir}#{table}-#{@timestamp}.sql"

    db_config = ActiveRecord::Base.configurations[Rails.env]

    print "Executing ".green

    backup_command = "mysqldump -u #{db_config['username']} -p#{db_config['password']} #{db_config['database']} #{table} > #{backup_file}"

    print "`#{backup_command}'...".green

    system(backup_command)

    puts "Done.".green
  end
end

def get_ci_env(ci_env_name, number)
  # 使用 namespace % 5 区域离散使用 ci_env
  offset = number % 5

  CiEnv.where(name: ci_env_name).offset(offset).first
end

def get_ci_env_name_by_jenkins_job_name(jenkins_job_name)
  jenkins_job_name =~ /(alpha|beta)-/

  $1
end
