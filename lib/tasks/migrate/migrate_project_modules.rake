desc "GITLAB | Migrate Project Modules"
task migrate_project_modules: :environment do

  Project.in_group_namespace.find_each(batch_size: 50) do |project|
    p project.name
    ProjectModulesUpdateService.new.execute(project, project.default_branch)
  end
end
