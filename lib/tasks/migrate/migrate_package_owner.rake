desc "GITPAB | Migrate Package Owner"
task migrate_package_owner: :environment do
  Package.where(owner: nil).update_all(owner: Package::OWNER[:task])
end
