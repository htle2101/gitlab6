desc "GITLAB | Migrate Project Token"
task migrate_project_token: :environment do

  Project.in_group_namespace.find_each(batch_size: 50, readonly: false) do |project|
    ProjectTokenUpdateService.new.execute(project)
  end
end
