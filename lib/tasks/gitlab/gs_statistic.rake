##coding:utf-8
namespace :gitlab do
  namespace :machines do
	desc "GITLAB | Migrate Gs Statistic Job"
	task gs_statistic: :environment do
		gss = GroupingStrategy.where("created_at > '#{DateTime.now-1}' and created_at < '#{DateTime.now}'").order("appname")
		appnames = gss.map{|gs|gs.appname}.uniq

		appnames.each do |appname|
			gss_for_each_appname = gss.where(:appname => appname)
			tags = gss_for_each_appname.where(:appname => appname).map{|gs|gs.tag if !gs.tag.nil?}.delete_if{|gs|gs.blank?}.uniq
			only_one_package_or_not = tags.size == 1 ? true : false
			@tag_info = tags.first
			@package_id = gss_for_each_appname.first.package_id
			if only_one_package_or_not
				::GroupingStrategies::GsStatisticContext.new(@package_id,@tag_info).execute
			else
				tags_extra = tags - [tags.first]
				gss_first_package = GroupingStrategy.where(:package_id => @package_id,:tag => @tag_info)
				@group_percent = "#{gss_first_package.select{|gs|gs if !gs.log.nil?}.size}"+"/"+ "#{gss_first_package.size}"
				group_percent_float = gss_first_package.select{|gs|gs if !gs.log.nil?}.size.to_f / gss_first_package.size
				if group_percent_float == 1.0
					::GroupingStrategies::GsStatisticContext.new(@package_id,@tag_info).execute
				else
					tags_extra.each do |tag|
					gss_per_tag = GroupingStrategy.where(:package_id =>@package_id,:tag => tag)
					group_percent_each = "#{gss_per_tag.select{|gs|gs if !gs.log.nil?}.size}"+"/"+ "#{gss_per_tag.size}"
					group_percent_float = gss_per_tag.select{|gs|gs if !gs.log.nil?}.size.to_f / gss_per_tag.size
					@tag_info = tag if group_percent_each > @group_percent
					break if group_percent_float = 1.0
					end
				::GroupingStrategies::GsStatisticContext.new(@package_id,@tag_info).execute
				end
			end
		end
	end
end
end