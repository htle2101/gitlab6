##coding:utf-8
namespace :gitlab do
  namespace :machines do
  desc "GITLAB | Gs Statistic By Group"
  task gs_statistic_by_group: :environment do 
    gsts = GsStatistic.where("created_at > '#{DateTime.now-7}' and created_at < '#{DateTime.now}'")
    namespace_ids = gsts.map{|gst|gst.namespace_id}.uniq
    namespace_ids.each do |ns|
    	ids = []
    	gsts_for_ns = gsts.where(:namespace_id =>ns)
    	appnames_for_ns = gsts_for_ns.map{|gst|gst.appname}.uniq
    	appnames_for_ns.each do |name|
    		gs_for_name = gsts_for_ns.where(:appname => name).order("time_interval_int desc").first
    		ids << gs_for_name.id    		
    	end
    	gst_for_ns_in_order = GsStatistic.where("id in (#{ids.join(',')})").order("time_interval_int desc").first(10)
      order = 1
    	gst_for_ns_in_order.each do |gst|
        namespace = Namespace.find(gst.namespace_id)
        weekth = gst.created_at.strftime("%W")
        date_interval = gst.started_at.beginning_of_week.to_s + "---"+ gst.started_at.end_of_week.to_s
        gs_group_statistic = GsGroupStatistic.new(:namespace_id =>namespace.id,:namespace_name => namespace.name,:weekth => weekth, :order => order,:appname => gst.appname,:project_id => gst.project_id,:date_interval => date_interval,:deploy_time_interval => gst.time_interval)
        gs_group_statistic.save and order = order + 1
    	end
    end
  end
end
end


