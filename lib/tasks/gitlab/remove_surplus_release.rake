##coding:utf-8
namespace :gitlab do
  namespace :machines do
  desc "GITLAB | Remove Surplus Release "
  task remove_surplus_release: :environment do 
    vms = VirtualMachine.where("ip is not null and success_at is not null")
    vms.each do |vm|
      all_releases = vm.conn.sudo_exec("ls -x /data/webapps/releases").to_s.split(" ")
      all_releases.each do |release|
        dateTime = (DateTime.now-7).strftime("%Y%m%d%H%M%S")
        vm.conn.sudo_exec("rm -rf /data/webapps/releases/#{release}") if release < dateTime
      end

      all_logs = vm.conn.sudo_exec("find /data/applogs -iname '*.log*' -type f")
      all_logs.each do |log|
        begin
          vm.conn.sudo_exec("rm -rf #{log}") if (DateTime.now - 7) >  DateTime.iso8601(vm.conn.ls(log,"-l --time-style '+%Y-%m-%dT%H:%M:%S+08:00'").to_s.split(' ')[5])
        rescue
        end
      end

      begin
        all_logs = vm.conn.sudo_exec("find /usr/local/tomcat/logs -iname 'catalina*'")
        all_logs.each do |log|
          log_size = vm.conn.sudo_exec("du --block-size=k '#{log}'").first.split("\t").first.split("K").first.to_i
          vm.conn.sudo_exec("rm -rf #{log}") if log_size > 512000
        end
      rescue  
      end
    end
  end
  end
end 
