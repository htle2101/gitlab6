namespace :gitlab do
  namespace :ftp do
    desc "delete inactive files on the ftp server"
    task :clean_packages => :environment do
      RemoteCi::FtpManagement.new({:retained_number => 15, :clean_cycle => 1}).clean_packages
    end
  end
end