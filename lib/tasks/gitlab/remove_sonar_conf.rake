desc "GITLAB | Migrate Remove Sonar Conf"
task remove_sonar_conf: :environment do
  BuildBranch.where(:ci_env_id => 2).find_each(batch_size: 50) do |ci|
    begin
      xml = ci.ci_env.server.job.get_config(ci.jenkins_job_name)
      doc = Nokogiri::XML(xml)
      doc.search("goals").each do |goal|
        goal.content = "-Denv=qa clean install -Dmaven.test.failure.ignore=true"
      end

      ci.ci_env.server.job.post_config(ci.jenkins_job_name,doc.to_s)
    rescue
    end
  end
end
