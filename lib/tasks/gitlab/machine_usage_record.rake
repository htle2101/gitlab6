namespace :gitlab do
  namespace :machines do
    desc "record all applied machines usage info by group"
    task :applied_machines_usage => :environment do
      VirtualMachine.applicable.used.each do |machine|
    		if machine.project
    			usage = VirtualMachineUsage.where(machine_id: machine.id,
                                            group_id: machine.project.namespace_id,
                                            using_to: nil).first
			    if usage && usage.project_id != machine.project_id
			      usage.project_id = machine.project
			      usage.save!
			    elsif usage.blank?
			    	start_use_record(machine)
			    end
    		end
      end
    end

    def start_use_record(virtual_machine)
		  usage = VirtualMachineUsage.new()
		  usage.machine_id = virtual_machine.id
		  usage.using_from = Time.now
		  usage.group_id = virtual_machine.project.namespace_id
		  usage.project_id = virtual_machine.project_id
		  usage.save!
    end
  end
end