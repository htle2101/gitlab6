##coding:utf-8
desc "GITLAB | Migrate Create Sonar Jenkins Job"
task create_sonar_jenkins_job: :environment do

  BuildBranch.in_env('beta').find_each(batch_size: 50) do |ci|
    # begin
      params = {:ci_branch=>{:ci_env => 'beta',:package_type=>"#{ci.package_type}",:root_type =>"#{ci.tree(ci.branch_name).pom_root_type}",:branch_name =>"#{ci.branch_name}"},:controller =>"projects/ci_branch",:action=>"create",:project_id =>"#{ci.project.path_with_namespace}",:id =>"#{ci.branch_name}"}
      @project,current_user = ci.project,ci.project.creator
      if ci.need_sonar? && ci.sonarbranch.blank? 
        SonarBranches::CreateContext.new(@project, current_user, params).execute 
      end  
    # rescue
    # end
    end
end
