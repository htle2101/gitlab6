namespace :gitlab do
  namespace :stats do

    task :user, [:user] => :environment do |t, args| 
      if args[:user].blank? || (user = User.find_by_username(args[:user])).blank?
        puts "User not found."
      else
        commits, additions, deletions = 0, 0, 0
        user.projects.each do |project|
          args = ['-6000', '--format=%aN%x0a%aE%x0a%cd', '--date=short', '--shortstat', '--no-merges', "--author=#{user.username}"]
          log = project.repository.raw.git.run(nil, 'log', nil, {}, args)
          json = Gitlab::Git::LogParser.parse_log(log)
          json.each do |entry|
            commits = commits + 1
            additions = additions + entry[:additions]
            deletions = deletions + entry[:deletions]
          end
        end
        puts "#{user.username}: commits #{commits}, additions: #{additions}, deletions: #{deletions}"
      end
    end

  end

end

