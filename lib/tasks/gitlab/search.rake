namespace :gitlab do
  namespace :search do
    desc "generate elasticsearch index for all project" 
    task :index => :environment do
      indexs = {}
      Project.order(:path).find_each do |project|
        puts 'generate index for ' + project.path_with_namespace
        next if (project.empty_repo? || project.namespace.blank? || project.forked?)
        project_index = Service::Search::RepoIndexer.new(project)
        project_index.init! if indexs[project_index.index_name].blank?
        indexs[project_index.index_name] = 'inited'
        project_index.index!
        project_index.refresh
        puts 'finished generate index for ' + project.path_with_namespace
      end  
    end  
  end   
end
