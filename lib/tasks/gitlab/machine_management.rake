namespace :gitlab do
  namespace :machines do
    desc "notify team members to release inactive machines"
    task :notify_in_advance_releasing_machines => :environment do
      machines = VirtualMachine.recoverable.applicable.where(build_branch_id: nil).where("project_id is not null")
      RemoteCi::MachineManagement.new(days: 1).release_each(machines) do |machine, last_update_time|
        Notify.delay.machine_release_notify_in_advance(machine.id, last_update_time)
      end

      machines = VirtualMachine.recoverable.applicable.where("build_branch_id is not null")
      RemoteCi::MachineManagement.new(days: 3).release_each(machines) do |machine, last_update_time|
        Notify.delay.machine_release_notify_in_advance(machine.id, last_update_time)
      end


      machines = VirtualMachine.where(tenancy: [9]).applicable.where(build_branch_id: nil)
      RemoteCi::MachineManagement.new(days: 7).release_each(machines) do |machine, last_update_time|
        Notify.delay.machine_release_notify_in_advance(machine.id, last_update_time)
      end
    end

    desc "release inactive machines and notify project team members"
    task :release_inactive_machines => :environment do
      machines = VirtualMachine.recoverable.applicable.where(build_branch_id: nil).where("project_id is not null")
      RemoteCi::MachineManagement.new(days: 2).release_each(machines) do |machine, last_update_time|
        attrs = machine.attributes.slice("ci_branch_id", "project_id", "module_name")
        ::VirtualMachines::UnbindContext.new(machine.build_branch_id, machine.ip)
        machine.release
        Notify.delay.machine_release_notify(machine.id, last_update_time, attrs)
      end

      machines = VirtualMachine.recoverable.applicable.where("build_branch_id is not null")
      RemoteCi::MachineManagement.new(days: 4).release_each(machines) do |machine, last_update_time|
        attrs = machine.attributes.slice("ci_branch_id", "project_id", "module_name")
        ::VirtualMachines::UnbindContext.new(machine.build_branch_id, machine.ip)
        machine.release
        Notify.delay.machine_release_notify(machine.id, last_update_time, attrs)
      end

      machines = VirtualMachine.where(tenancy: [9]).applicable.where(build_branch_id: nil)
      RemoteCi::MachineManagement.new(days: 8).release_each(machines) do |machine, last_update_time|
        attrs = machine.attributes.slice("ci_branch_id", "project_id", "module_name")
        ::VirtualMachines::UnbindContext.new(machine.build_branch_id, machine.ip)
        machine.release
        Notify.delay.machine_release_notify(machine.id, last_update_time, attrs)
      end

    end
  end
end
