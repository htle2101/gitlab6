namespace :gitlab do
  namespace :databases do
    desc "Update database metadata from tools.dba API"
    task :update => :environment do
      begin
        db_string = Service::DBA::Request.new.getdb
        updated_databases = DatabaseUpdateService.new.execute(db_string)
        puts "Updated databases: #{updated_databases}"
      rescue => e
        puts e
      end
    end
  end
end
