namespace :sidekiq do
  desc "GITLAB | Stop sidekiq"
  task :stop do
    system "bundle exec sidekiqctl stop #{pidfile('gitlab')}"
    system "bundle exec sidekiqctl stop #{pidfile('rollout')}"
    system "bundle exec sidekiqctl stop #{pidfile('other')}"
  end

  desc "GITLAB | Start sidekiq"
  task :start do
    system "nohup bundle exec sidekiq -q post_receive,system_hook,project_web_hook,gitlab_shell,common,default -e #{Rails.env} -P #{pidfile('gitlab')} >> #{Rails.root.join("log", "sidekiq.log")} 2>&1 &"
    system "nohup bundle exec sidekiq -q package_upload,group_rollback,grouping_rollout_terminate,grouping_rollout_update,paas_rollout,cache_clear -e #{Rails.env} -P #{pidfile('rollout')} >> #{Rails.root.join("log", "sidekiq.log")} 2>&1 &"
    system "nohup bundle exec sidekiq -q tomcat_restart,war_deploy,faye_publish,ci_destroy,beta_paas,beta_paas_upload,beta_paas_rollout,machine_unbind,build_log,sonar_statistics -e #{Rails.env} -P #{pidfile('other')} >> #{Rails.root.join("log", "sidekiq.log")} 2>&1 &"
  end

  desc "GITLAB | Start sidekiq with launchd on Mac OS X"
  task :launchd do
    system "bundle exec sidekiq -q post_receive,system_hook,project_web_hook,beta_paas,beta_paas_upload,beta_paas_rollout,gitlab_shell,common,default,war_deploy,faye_publish,package_upload,group_rollback,ci_destroy,cache_clear,machine_unbind,grouping_rollout_terminate,grouping_rollout_update,build_log,paas_rollout -e #{Rails.env} -P #{pidfile} >> #{Rails.root.join("log", "sidekiq.log")} 2>&1"
  end

  desc "GITLAB | Delete non-retryable job"
  task delete_non_retryable_jobs: :environment do
    require 'set'
    tempset = Set.new
    set = Sidekiq::RetrySet.new
    set.each do |entry|
      tempset << entry if entry["retry_count"] >= 5 && entry["error_class"] =~ /ActiveRecord::RecordNotFound/
    end
    tempset.each { |e| e.delete }
  end

  def pidfile(type)
    Rails.root.join("tmp", "pids", "sidekiq_"+type+".pid")
  end
end
