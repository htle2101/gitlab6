namespace :faye do
  desc "Faye | Stop server"
  task :stop do
    system "cd faye && bundle exec thin -C thin.yml -R config.ru -f stop"
  end

  desc "Faye | Start server"
  task :start do
    system "cd faye && bundle exec thin -C thin.yml -R config.ru start"
  end

  desc "Faye | Restart server"
  task :restart do
    system "cd faye && bundle exec thin -C thin.yml -R config.ru -f restart"
  end
end
