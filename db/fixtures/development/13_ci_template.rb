CiTemplate.seed(:id, [  
  { id: 1, namespace_id: 1, ci_env_id: 1, template_name: "beta-template-jar", package_type: "jar", comment: "test" },     
  { id: 2, namespace_id: 1, ci_env_id: 1, template_name: "alpha-template-war-maven2", package_type: "war-maven2", comment: "test" },
  { id: 3, namespace_id: 1, ci_env_id: 1, template_name: "alpha-template-war-maven3", package_type: "war-maven3", comment: "test" },
  { id: 4, namespace_id: 1, ci_env_id: 1, template_name: "alpha-template-static", package_type: "static", comment: "test" },  
  { id: 5, namespace_id: 1, ci_env_id: 2, template_name: "beta-template-jar", package_type: "jar", comment: "test" },     
  { id: 6, namespace_id: 1, ci_env_id: 2, template_name: "beta-template-war-maven2", package_type: "war-maven2", comment: "test" },
  { id: 7, namespace_id: 1, ci_env_id: 2, template_name: "beta-template-war-maven3", package_type: "war-maven3", comment: "test" },
  { id: 8, namespace_id: 1, ci_env_id: 2, template_name: "beta-template-static", package_type: "static", comment: "test" },
  { id: 10, namespace_id: 1, ci_env_id: 3, template_name: "product-template-jar", package_type: "jar", comment: "test" },     
  { id: 11, namespace_id: 1, ci_env_id: 3, template_name: "product-template-war-maven2", package_type: "war-maven2", comment: "test" },
  { id: 12, namespace_id: 1, ci_env_id: 3, template_name: "product-template-war-maven3", package_type: "war-maven3", comment: "test" },
  { id: 13, namespace_id: 1, ci_env_id: 3, template_name: "product-template-static", package_type: "static", comment: "test" }  
 ])
