CiTemplate.seed(:id, [
  { id: 1, namespace_id: 1, ci_env_id: 1, template_name: "alpha-template-jar", package_type: "war", comment: "test" },
  { id: 2, namespace_id: 1, ci_env_id: 1, template_name: "alpha-template-war", package_type: "war", comment: "test" },
  { id: 3, namespace_id: 1, ci_env_id: 2, template_name: "beta-template-jar", package_type: "war", comment: "test" },
  { id: 4, namespace_id: 1, ci_env_id: 2, template_name: "beta-template-war", package_type: "war", comment: "test" },
  { id: 5, namespace_id: 1, ci_env_id: 1, template_name: "alpha-template-static", package_type: "static", comment: "test" }, 
  { id: 6, namespace_id: 1, ci_env_id: 2, template_name: "beta-template-static", package_type: "static", comment: "test" }
 ])
