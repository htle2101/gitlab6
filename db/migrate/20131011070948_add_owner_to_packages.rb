class AddOwnerToPackages < ActiveRecord::Migration
  def change
    add_column :packages, :owner, :integer, limit: 1, after: :project_id
    add_index :packages, :owner
  end
end
