class ChangeDefaultValueToProjectsPublic < ActiveRecord::Migration

  def up
    change_column :projects, :public, :boolean, :default => true
  end

  def down
    change_column :projects, :public, :boolean, :default => false
  end
 
end
