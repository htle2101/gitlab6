class AddGsFromModuleNameToPackages < ActiveRecord::Migration
  def change
  	add_column :packages, :gs_from_module_name, :integer, :default => 1
  end
end
