class AddLogToGroupingStategies < ActiveRecord::Migration
  def change
    add_column :grouping_strategies, :log, :string
    rename_column :grouping_strategies, :group_status, :status
  end
end
