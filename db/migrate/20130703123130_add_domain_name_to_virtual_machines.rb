class AddDomainNameToVirtualMachines < ActiveRecord::Migration
  def change
  	add_column :virtual_machines, :domain_name, :string
  end
end