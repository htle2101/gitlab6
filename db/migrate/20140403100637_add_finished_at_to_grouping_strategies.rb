class AddFinishedAtToGroupingStrategies < ActiveRecord::Migration
  def change
  	add_column :grouping_strategies, :finished_at, :datetime, after: :started_at
  end
end
