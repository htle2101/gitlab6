class RenameTableLigerIncrements < ActiveRecord::Migration
  def up
  	rename_table :ligerincrements, :liger_increments
  end

  def down
  end
end
