class CreateBuMachines < ActiveRecord::Migration
  def change
    create_table :bu_machines do |t|
      t.string :bu_name
      t.string :department
      t.integer :alpha
      t.integer :beta

      t.timestamps
    end
  end
end
