class AddDefaultValueForStatus < ActiveRecord::Migration
  def change
   	change_column :virtual_machines, :status, :integer, :default => 0   	
  end
end

