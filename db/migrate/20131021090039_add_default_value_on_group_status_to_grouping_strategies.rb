class AddDefaultValueOnGroupStatusToGroupingStrategies < ActiveRecord::Migration
  def change
    change_column :grouping_strategies, :status, :integer, default: 1024
  end
end
