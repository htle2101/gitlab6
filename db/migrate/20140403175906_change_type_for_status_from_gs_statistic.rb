class ChangeTypeForStatusFromGsStatistic < ActiveRecord::Migration
  def up
  	change_column :gs_statistics, :status, :string
  end

  def down
  end
end
