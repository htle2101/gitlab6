class AddStartedAtToGroupingStrategies < ActiveRecord::Migration
  def change
  	add_column :grouping_strategies, :started_at, :datetime, after: :artifact_id
  end
end
