class AddTicketIdToPaasRollouts < ActiveRecord::Migration
  def change
  	add_column :paas_rollouts, :ticket_id, :string, after: :warname
  end
end
