class ChangeCiBranchIdToBuildBranchIdInBetaPaasRollouts < ActiveRecord::Migration
  def up
  	rename_column :beta_paas_rollouts, :ci_branch_id, :build_branch_id
  end

  def down
  end
end
