class ChangeDataTypeForTicketsTag < ActiveRecord::Migration
  def self.up
  	change_column :tickets, :tag, :string
  end

  def self.down
  	change_column :tickets, :tag, :integer
  end
end
