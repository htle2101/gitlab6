class AddStatusToRolloutPackingModule < ActiveRecord::Migration
  def up
  	add_column :rollout_packing_modules, :status, :integer
  end

  def down
  	remove_column :rollout_packing_modules, :status, :integer
  end
end
