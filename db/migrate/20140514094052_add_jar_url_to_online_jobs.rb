class AddJarUrlToOnlineJobs < ActiveRecord::Migration
  def change
  	add_column :online_jobs, :jar_url, :string, after: :tag
  end
end
