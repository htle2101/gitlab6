class ChangePaasNewUrlTypeFromIntegerToString < ActiveRecord::Migration
  def up
  	change_column :project_modules, :paas_new_url, :string, :default => "http://10.101.0.11:8080/", :null => false
  end

  def down
  end
end
