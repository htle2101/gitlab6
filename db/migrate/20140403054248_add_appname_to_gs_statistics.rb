class AddAppnameToGsStatistics < ActiveRecord::Migration
  def change
  	add_column :gs_statistics, :appname, :string, after: :package_url
  end
end