class CreateMergeRequestInspections < ActiveRecord::Migration
  def change
    create_table :merge_request_inspections do |t|
      t.integer :project_id
      t.integer :merge_request_id
      t.string :source_branch
      t.string :target_branch
      t.string :jenkins_job_url
      t.text :stdout
      t.text :stderr
      t.integer :status, :default => 1024

      t.timestamps
    end
  end
end
