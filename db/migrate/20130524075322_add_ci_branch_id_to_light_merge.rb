class AddCiBranchIdToLightMerge < ActiveRecord::Migration
  def change
    add_column :light_merges, :ci_branch_id, :integer
    add_index :light_merges, :ci_branch_id
  end
end
