class CreateMachineUsageMonthlies < ActiveRecord::Migration
  def change
    create_table :machine_usage_monthlies do |t|
      t.references :namespace
      t.references :project
      t.references :virtual_machine
      t.integer :year
      t.integer :month
      t.integer :duration

      t.timestamps
    end
    add_index :machine_usage_monthlies, :namespace_id
    add_index :machine_usage_monthlies, :project_id
    add_index :machine_usage_monthlies, :virtual_machine_id
  end
end
