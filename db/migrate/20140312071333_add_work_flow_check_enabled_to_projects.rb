class AddWorkFlowCheckEnabledToProjects < ActiveRecord::Migration
  def change
  	add_column :projects, :workflow_check_enabled, :boolean, :default => false, :null => false, after: :ticket_add_wars_enabled
  end
end
