class AddRolloutEnabledToProjects < ActiveRecord::Migration
  def change
    add_column :projects, :rollout_enabled, :boolean, :default => true, :null => false
  end
end
