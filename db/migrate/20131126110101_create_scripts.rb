class CreateScripts < ActiveRecord::Migration
  def change
    create_table :scripts do |t|
      t.string :name
      t.string :todo
      t.string :db_name
      t.string :path
      t.integer :project_id
      t.string :desc
      t.integer :status, default: 0, limit: 1
      t.integer :author_id
      t.string :req_url
      t.text :data
      t.datetime :executed_at

      t.timestamps
    end

    add_index :scripts, :project_id
    add_index :scripts, :status
    add_index :scripts, :author_id
    execute "ALTER TABLE scripts AUTO_INCREMENT=1000000"
  end
end
