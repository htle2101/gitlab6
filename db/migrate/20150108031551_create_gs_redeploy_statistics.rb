class CreateGsRedeployStatistics < ActiveRecord::Migration
  def change
    create_table :gs_redeploy_statistics do |t|
      t.string :appname
      t.integer :gs_id
      t.string :tag
      t.string :group
      t.integer :group_id
      t.integer :ticket_id

      t.timestamps
    end
  end
end
