class AddPathToCiEnv < ActiveRecord::Migration
  def change
    add_column :ci_envs, :ssh_user, :string, :after => :is_single
    add_column :ci_envs, :ssh_password, :string, :after => :ssh_user
    add_column :ci_envs, :path, :string, :after => :ssh_password
    update_data
  end

  def update_data
    [
      {name: 'alpha', ssh_user: 'root', ssh_password: '12qwaszx', path: '/data/jenkins'},
      {name: 'beta', ssh_user: 'root', ssh_password: '12qwaszx', path: '/data/jenkinshome/jenkins/jenkins'}
    ].each do |data|
      if env = CiEnv.find_by_name(data[:name])
        env.update_attributes(data)
      end
    end
  end
end
