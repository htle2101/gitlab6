class AddHasPaasToProjectModule < ActiveRecord::Migration
  def change
    add_column :project_modules, :has_paas, :boolean, :default => false
  end
end
