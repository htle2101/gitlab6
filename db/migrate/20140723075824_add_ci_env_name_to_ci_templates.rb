class AddCiEnvNameToCiTemplates < ActiveRecord::Migration
  def up
    add_column :ci_templates, :ci_env_name, :string

    initialize_ci_env_name
  end

  def down
    remove_column :ci_templates, :ci_env_name
  end

  private

  def initialize_ci_env_name
    CiTemplate.all.each do |ci_template|
      ci_env_name = ci_template.ci_env.try(:name)

      ci_template.update_attributes(ci_env_name: ci_env_name)
    end
  end
end
