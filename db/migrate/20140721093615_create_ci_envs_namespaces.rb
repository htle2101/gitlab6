class CreateCiEnvsNamespaces < ActiveRecord::Migration
  def change
    create_table :ci_envs_namespaces do |t|
      t.integer :ci_env_id, null: false
      t.integer :namespace_id, null: false
      t.string :ci_env_name, null: false
    end

    add_index :ci_envs_namespaces, [:namespace_id, :ci_env_name], unique: true
  end
end
