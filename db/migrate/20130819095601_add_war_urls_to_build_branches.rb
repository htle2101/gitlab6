class AddWarUrlsToBuildBranches < ActiveRecord::Migration
  def change
    add_column :build_branches, :war_urls, :string
  end
end
