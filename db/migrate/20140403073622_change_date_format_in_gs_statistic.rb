class ChangeDateFormatInGsStatistic < ActiveRecord::Migration
  def up
  	change_column :gs_statistics, :started_at, :datetime
  	change_column :gs_statistics, :finished_at, :datetime
  end

  def down
  	change_column :gs_statistics, :started_at, :date
  	change_column :gs_statistics, :finished_at, :date
  end
end
