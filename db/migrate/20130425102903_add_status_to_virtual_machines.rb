class AddStatusToVirtualMachines < ActiveRecord::Migration
  def change
    add_column :virtual_machines, :status, :integer, :default => 0
  end  
end
