class CreatePerformanceTestModules < ActiveRecord::Migration
  def change
    create_table :performance_test_modules do |t|
      t.string :appname
      t.string :tag
      t.string :app_type
      t.integer :cpu
      t.integer :memory,:limit => 8
      t.string :domain_name
      t.string :package_url
      t.integer :performance_test_id

      t.timestamps
    end
  end
end
