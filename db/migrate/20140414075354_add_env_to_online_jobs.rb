class AddEnvToOnlineJobs < ActiveRecord::Migration
  def change
  	 add_column :online_jobs, :env, :string, after: :machine
  end
end
