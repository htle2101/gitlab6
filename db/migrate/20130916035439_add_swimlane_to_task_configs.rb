class AddSwimlaneToTaskConfigs < ActiveRecord::Migration
  def change
    add_column :task_configs, :swimlane, :integer
    add_column :task_configs, :state, :integer
  end
end
