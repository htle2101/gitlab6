class CreateTasks < ActiveRecord::Migration
  def change
    create_table :tasks do |t|
      t.string :subject
      t.datetime :date
      t.integer :project_id

      t.timestamps
    end
  end
end
