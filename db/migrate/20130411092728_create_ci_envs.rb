class CreateCiEnvs < ActiveRecord::Migration
  def change
    create_table :ci_envs do |t|
      t.string :name
      t.string :url
      t.string :user
      t.string :password
      t.boolean :is_single

      t.timestamps
    end
  end
end
