class AddGroupStatusToGroupingStrategies < ActiveRecord::Migration
  def change
    add_column :grouping_strategies, :group_status, :integer
  end
end
