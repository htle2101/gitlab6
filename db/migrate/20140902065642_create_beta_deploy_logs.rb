class CreateBetaDeployLogs < ActiveRecord::Migration
  def change
    create_table :beta_deploy_logs do |t|
      t.integer :project_id, null: false
      t.string :branch_name, null: false
      t.string :project_path_with_namespace, null: false
      t.string :module_name, null: false
      t.integer :voteup_count, default: 0
      t.integer :votedown_count, default: 0
      t.date :date, null: false

      t.timestamps
    end

    add_index :beta_deploy_logs, :date
  end
end
