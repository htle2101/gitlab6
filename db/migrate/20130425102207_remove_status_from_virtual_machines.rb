class RemoveStatusFromVirtualMachines < ActiveRecord::Migration
  def up
    remove_column :virtual_machines, :status
  end

  def down
    add_column :virtual_machines, :status, :integer
  end
end
