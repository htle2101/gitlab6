class AddMachineToPerformanceTestModules < ActiveRecord::Migration
  def change
  	add_column :performance_test_modules, :machine, :string, :after => :app_type
  end
end
