class AddRunTestToBuildBranches < ActiveRecord::Migration
  def change
    add_column :build_branches, :run_test, :boolean, default: true
  end
end
