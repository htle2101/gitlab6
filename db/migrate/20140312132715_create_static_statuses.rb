class CreateStaticStatuses < ActiveRecord::Migration
  def change
    create_table :static_statuses do |t|
      t.string :build_status
      t.string :cache_status
      t.integer :build_number
      t.integer :build_branch_id

      t.timestamps
    end
  end
end
