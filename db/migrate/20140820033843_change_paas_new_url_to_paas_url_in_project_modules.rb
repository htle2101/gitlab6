class ChangePaasNewUrlToPaasUrlInProjectModules < ActiveRecord::Migration
  def up
  	 rename_column :project_modules, :paas_new_url, :paas_url
  end

  def down
  end
end
