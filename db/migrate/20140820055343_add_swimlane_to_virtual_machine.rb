class AddSwimlaneToVirtualMachine < ActiveRecord::Migration
  def change
  	add_column :virtual_machines, :swimlane, :string, :default => ''
  end
end
