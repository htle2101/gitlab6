class RemoveFinishedAtFromGsStatistic < ActiveRecord::Migration
  def up
  	remove_column :gs_statistics, :finished_at
  end

  def down
  end
end
