class CreateLigerIncrements < ActiveRecord::Migration
  def up
  	create_table :ligerincrements do |t|
      t.string :appname, :null => false
      t.integer :packing_id, :null => false
      t.string :last_push
      t.string :last_commit
      t.string :tag
      t.string :key
      t.string :origin_value
      t.string :current_value
      t.string :target_value
      t.string :final_value
      t.string :liger_commit
    end
  end

  def down
  end
end
