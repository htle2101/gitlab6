class RemoveStartedAtFromGsStatistic < ActiveRecord::Migration
  def up
  	remove_column :gs_statistics, :started_at
  end

  def down
  end
end
