class AddFtpUrlToProjectModules < ActiveRecord::Migration
  def change
    add_column :project_modules, :ftp_url, :string
  end
end
