class AddPpeStatusToScripts < ActiveRecord::Migration
  def change
    add_column :scripts, :prd_status, :integer, default: 0, limit: 1, after: :status
    add_column :scripts, :ppe_status, :integer, default: 0, limit: 1, after: :status
    add_column :scripts, :ppe_executed_at, :datetime, after: :executed_at
    rename_column :scripts, :executed_at, :prd_executed_at
    add_index :scripts, :prd_status
    add_index :scripts, :ppe_status
  end
end
