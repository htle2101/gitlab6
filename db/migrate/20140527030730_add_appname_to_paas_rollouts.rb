class AddAppnameToPaasRollouts < ActiveRecord::Migration
  def change
  	add_column :paas_rollouts, :appname, :string, after: :tag
  end
end
