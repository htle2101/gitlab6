class CreateGroupingStrategies < ActiveRecord::Migration
  def change
    create_table :grouping_strategies do |t|
      t.string :name
      t.string :hostname
      t.integer :swimlane
      t.string :appname
      t.integer :package_id

      t.timestamps
    end
  end
end
