class AddGroupIdToPaasRollouts < ActiveRecord::Migration
  def change
  	add_column :paas_rollouts, :group_id, :integer, after: :warname
  end
end
