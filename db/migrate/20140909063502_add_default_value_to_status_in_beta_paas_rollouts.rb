class AddDefaultValueToStatusInBetaPaasRollouts < ActiveRecord::Migration
  def change
  	 change_column :beta_paas_rollouts, :status, :integer, :default => 0
  end

end
