class ClassificationsNamespaces < ActiveRecord::Migration
  def change
    create_table :classifications_namespaces do |t|
      t.references :classification
      t.references :namespace

      t.timestamps
    end
    add_index :classifications_namespaces, :classification_id
    add_index :classifications_namespaces, :namespace_id
  end
end
