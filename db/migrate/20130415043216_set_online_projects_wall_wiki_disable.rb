class SetOnlineProjectsWallWikiDisable < ActiveRecord::Migration
  
  Project.update_all({ :wall_enabled => false })
  Project.update_all({ :wiki_enabled => false }) 
 
end
