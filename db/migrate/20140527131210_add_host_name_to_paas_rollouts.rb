class AddHostNameToPaasRollouts < ActiveRecord::Migration
  def change
  	add_column :paas_rollouts, :hostname, :string, after: :warname
  end
end
