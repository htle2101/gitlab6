class AddTypeToPackages < ActiveRecord::Migration
  def up
    add_column :packages, :package_type, :string, :after => :name
  end

  def down
  	remove_column :packages, :package_type
  end

end
