class AddGroupPercentToGsStatistics < ActiveRecord::Migration
  def change
  	add_column :gs_statistics, :group_percent, :string, after: :gs_id
  end
end
