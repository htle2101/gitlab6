class ChangeTypeForPackageIdFromGsStatistic < ActiveRecord::Migration
  def up
  	change_column :gs_statistics, :package_id, :integer
  end

  def down
  end
end
