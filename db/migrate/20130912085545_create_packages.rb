class CreatePackages < ActiveRecord::Migration
  def change
    create_table :packages do |t|
      t.string :name
      t.string :tag
      t.string :url
      t.integer :packing_module_id
      t.integer :packing_id
      t.integer :ticket_id
      t.integer :project_id

      t.timestamps
    end

    add_index :packages, :name
    add_index :packages, :tag
    add_index :packages, :packing_module_id
    add_index :packages, :packing_id
    add_index :packages, :ticket_id
    add_index :packages, :project_id

    create_table :tasks_packages do |t|
      t.integer :task_id
      t.integer :package_id
    end

    add_index :tasks_packages, :task_id
    add_index :tasks_packages, :package_id
  end
end
