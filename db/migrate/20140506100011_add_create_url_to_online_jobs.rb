class AddCreateUrlToOnlineJobs < ActiveRecord::Migration
  def change
  	add_column :online_jobs, :create_url, :string
  end
end
