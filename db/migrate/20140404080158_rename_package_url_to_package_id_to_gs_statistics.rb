class RenamePackageUrlToPackageIdToGsStatistics < ActiveRecord::Migration
  def up
  	rename_column :gs_statistics, :package_url, :package_id
  end

  def down
  end
end
