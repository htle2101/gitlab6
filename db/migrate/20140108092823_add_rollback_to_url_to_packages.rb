class AddRollbackToUrlToPackages < ActiveRecord::Migration
  def change
    add_column :packages, :rollback_to_url, :string
  end
end
