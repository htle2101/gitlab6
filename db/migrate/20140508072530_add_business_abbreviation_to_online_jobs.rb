class AddBusinessAbbreviationToOnlineJobs < ActiveRecord::Migration
  def change
  	add_column :online_jobs, :business_abbreviation, :string
  end
end
