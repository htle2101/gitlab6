class ChangeWarUrlsTypeOfBuildBranches < ActiveRecord::Migration
  def up
    change_column :build_branches, :war_urls, :text
    add_column :build_branches, :status, :integer, :default => 0
  end

  def down
    change_column :build_branches, :war_urls, :string
    remove_column :build_branches, :status, :integer
  end
end
