class ChangeDataTypeForTaskConfig < ActiveRecord::Migration
  def up
  	change_column :task_configs, :type,                :text
  	change_column :task_configs, :online_value,        :text
  	change_column :task_configs, :offline_dev,         :text
  	change_column :task_configs, :offline_alpha,       :text
  	change_column :task_configs, :offline_beta,        :text
  	change_column :task_configs, :offline_pre_release, :text
  	change_column :task_configs, :state,               :integer,     :default => 1
  end

  def down
  	remove_column :task_configs, :type
  	change_column :task_configs, :online_value,        :string
  	change_column :task_configs, :offline_dev,         :string
  	change_column :task_configs, :offline_alpha,       :string
  	change_column :task_configs, :offline_beta,        :string
  	change_column :task_configs, :offline_pre_release, :string
  	change_column :task_configs, :state,               :integer
  end
end