class AddProjectIdToVirtualMachines < ActiveRecord::Migration
  def change
    add_column :virtual_machines, :project_id, :integer, after: :ci_branch_id
    add_index :virtual_machines, :project_id
    VirtualMachine.where("ci_branch_id is not null").find_each do |m|
      m.update_attributes(project_id: m.ci_branch.project.id)
    end
  end

end
