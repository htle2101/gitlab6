class AddArtifactIdToGroupingStrategies < ActiveRecord::Migration
  def change
  	add_column :grouping_strategies, :artifact_id, :string
  end
end
