class ChangeRollbackOnDateFormatInGroupingStrategies < ActiveRecord::Migration
  def up
    change_column :grouping_strategies, :rollback_on, :datetime
  end

  def down
    change_column :grouping_strategies, :rollback_on, :date
  end
end
