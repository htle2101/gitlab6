class AddProjectIndexToGs < ActiveRecord::Migration
  def change
    add_index :grouping_strategies, :project_id
  end
end
