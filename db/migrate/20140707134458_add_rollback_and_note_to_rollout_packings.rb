class AddRollbackAndNoteToRolloutPackings < ActiveRecord::Migration
  def change
    add_column :rollout_packings, :cant_rollback, :boolean, :default => false
    add_column :rollout_packings, :note, :text
  end
end
