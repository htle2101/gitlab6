class AddIsCmdbToCiEnvs < ActiveRecord::Migration
  def change
    add_column :ci_envs, :is_cmdb, :boolean
  end
end
