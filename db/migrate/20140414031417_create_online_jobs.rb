class CreateOnlineJobs < ActiveRecord::Migration
  def change
    create_table :online_jobs do |t|
      t.string :jar_name
      t.string :machine
      t.text :online_script
      t.string :status
      t.integer :project_id
      t.integer :build_branch_id
      t.datetime :success_at

      t.timestamps
    end
  end
end
