class AddNamespaceIdToGsStatistics < ActiveRecord::Migration
  def change
  	add_column :gs_statistics, :namespace_id, :integer, after: :project_id
  end
end
