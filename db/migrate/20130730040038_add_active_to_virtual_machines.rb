class AddActiveToVirtualMachines < ActiveRecord::Migration
  def change
    add_column :virtual_machines, :active, :boolean, :default => true
  end
end
