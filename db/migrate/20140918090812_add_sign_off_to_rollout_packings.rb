class AddSignOffToRolloutPackings < ActiveRecord::Migration
  def change
  	add_column :rollout_packings, :sign_off, :string, :default => nil
  end
end
