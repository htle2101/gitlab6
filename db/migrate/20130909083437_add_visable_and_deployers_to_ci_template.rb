class AddVisableAndDeployersToCiTemplate < ActiveRecord::Migration
  def change
    add_column :ci_templates, :visable, :boolean, :default => true, :after => :comment
    add_column :ci_templates, :deployers, :string, :default => 'War', :after => :package_type
  end
end
