class CreateBetaPaasRollouts < ActiveRecord::Migration
  def change
    create_table :beta_paas_rollouts do |t|
      t.integer :ci_branch_id
      t.integer :status
      t.datetime :released_on
      t.string :tag
      t.integer :group_id
      t.string :hostname
      t.integer :operation_id
      t.string :machine

      t.timestamps
    end
  end
end
