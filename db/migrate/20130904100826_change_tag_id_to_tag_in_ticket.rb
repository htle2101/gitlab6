class ChangeTagIdToTagInTicket < ActiveRecord::Migration
  def self.up
       rename_column :tickets, :tag_id, :tag
  end

  def self.down
  	rename_column :tickets, :tag, :tag_id
  end

end
