class AddDeployersToCiBranches < ActiveRecord::Migration
  def change
    add_column :ci_branches, :deployers, :string, :default => 'War'
  end
end
