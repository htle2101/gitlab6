class CreatePerformanceTests < ActiveRecord::Migration
  def change
    create_table :performance_tests do |t|
      t.string :title
      t.string :target
      t.string :uid
      t.string :test_log_url
      t.integer :concurrent
      t.integer :duration
      t.string :upload_file
      t.integer :project_id

      t.timestamps
    end
  end
end
