class AddActionTypeToVirtualMachines < ActiveRecord::Migration
  def change
    add_column :virtual_machines, :action_type, :string
  end
end
