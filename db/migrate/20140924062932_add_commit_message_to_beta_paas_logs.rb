class AddCommitMessageToBetaPaasLogs < ActiveRecord::Migration
  def change
  	add_column :beta_paas_logs, :commit_message, :string
  end
end
