class AddFinishedAtToGsStatistics < ActiveRecord::Migration
  def change
  	add_column :gs_statistics, :finished_at, :string, after: :started_at
  end
end
