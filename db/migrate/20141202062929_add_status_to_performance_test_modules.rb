class AddStatusToPerformanceTestModules < ActiveRecord::Migration
  def change
  	add_column :performance_test_modules, :status, :integer, :after => :performance_test_id
  end
end
