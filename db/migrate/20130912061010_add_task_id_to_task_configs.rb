class AddTaskIdToTaskConfigs < ActiveRecord::Migration
  def change
    add_column :task_configs, :task_id, :integer
  end
end
