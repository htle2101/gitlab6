class AddStartedAtToGsStatistics < ActiveRecord::Migration
  def change
  	add_column :gs_statistics, :started_at, :datetime, after: :gs_id
  end
end
