class AddColumnsToTasks < ActiveRecord::Migration
  def change
    add_column :tasks, :creator_id, :integer, :after => :project_id
    add_column :tasks, :assignee_id, :integer, :after => :project_id
    add_column :tasks, :status, :integer, limit: 1, :after => :project_id

    add_index :tasks, :creator_id
    add_index :tasks, :assignee_id
    add_index :tasks, :status
  end
end
