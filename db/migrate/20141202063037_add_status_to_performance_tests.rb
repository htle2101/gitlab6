class AddStatusToPerformanceTests < ActiveRecord::Migration
  def change
  	add_column :performance_tests, :status, :integer, :after => :project_id
  end
end
