class AddReviewedToMergeRequests < ActiveRecord::Migration
  def change
    add_column :merge_requests, :reviewed, :boolean, default: false
  end
end
