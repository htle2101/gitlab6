class TicketAddWarsEnabledToProjects < ActiveRecord::Migration
  def change
    add_column :projects, :ticket_add_wars_enabled, :boolean, :default => true, :null => false, after: :rollout_enabled
  end
end
