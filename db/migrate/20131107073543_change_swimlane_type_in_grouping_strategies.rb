class ChangeSwimlaneTypeInGroupingStrategies < ActiveRecord::Migration
  def up
    change_column :grouping_strategies, :swimlane, :string
  end

  def down
    change_column :grouping_strategies, :swimlane, :integer
  end
end
