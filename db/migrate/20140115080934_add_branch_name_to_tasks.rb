class AddBranchNameToTasks < ActiveRecord::Migration
  def change
  	add_column :tasks, :branch_name, :string, after: :project_id
  end
end
