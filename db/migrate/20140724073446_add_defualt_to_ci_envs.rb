class AddDefualtToCiEnvs < ActiveRecord::Migration
  def change
    add_column :ci_envs, :default, :boolean, default: false
  end
end
