class CreateClassifications < ActiveRecord::Migration
  def change
    create_table :classifications do |t|
      t.string :name
      t.text :description

      t.timestamps
    end
    add_index :classifications, :name
  end
end
