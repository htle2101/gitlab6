class CreateTickets < ActiveRecord::Migration
  def change
    create_table :tickets do |t|
      t.datetime :date
      t.integer :tag_id
      t.integer :project_id

      t.timestamps
    end
  end
end
