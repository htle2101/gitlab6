class AddDeployerToVirtualMachines < ActiveRecord::Migration
  def change
    add_column :virtual_machines, :deployer, :string, :default => 'War'
    add_index :virtual_machines, :deployer
    VirtualMachine.where("action_type is not null").find_each do |m|
      if m.action_type == 'link'
        m.update_attributes(deployer: 'Cmdb')
      else
        m.update_attributes(deployer: 'War')
      end  
    end
  end
end
