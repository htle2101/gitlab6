class CreateVirtualMachineUsages < ActiveRecord::Migration
  def change
    create_table :virtual_machine_usages do |t|
      t.integer :machine_id
      t.datetime :using_from
      t.datetime :using_to
      t.integer :duration
      t.integer :group_id
      t.integer :project_id

      t.timestamps
    end
    add_index :virtual_machine_usages, :group_id
  end
end
