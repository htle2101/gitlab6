class AddIsServiceToVirtualMachines < ActiveRecord::Migration
  def change
  	add_column :virtual_machines, :is_service, :boolean, :default => false
  end
end
