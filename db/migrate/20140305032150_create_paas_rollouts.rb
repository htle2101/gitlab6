class CreatePaasRollouts < ActiveRecord::Migration
  def change
    create_table :paas_rollouts do |t|
      t.references :project
      t.references :package
      t.integer :status, :default => 0
      t.datetime :released_on
      t.string :tag
      t.string :operation_id

      t.timestamps
    end
    add_index :paas_rollouts, :project_id
    add_index :paas_rollouts, :package_id
  end
end
