class AddUrlToGroupingStrategies < ActiveRecord::Migration
  def change
    add_column :grouping_strategies, :url, :string
  end
end
