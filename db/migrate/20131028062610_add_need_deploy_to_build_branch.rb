class AddNeedDeployToBuildBranch < ActiveRecord::Migration
  def change
    add_column :build_branches, :auto_deploy, :boolean, :default => true
  end
end
