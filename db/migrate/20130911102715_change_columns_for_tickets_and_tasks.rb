class ChangeColumnsForTicketsAndTasks < ActiveRecord::Migration
  def up
    rename_column :tasks, :date, :released_at
    change_column :tasks, :released_at, :date
    rename_column :tickets, :date, :released_at
    change_column :tickets, :released_at, :date
    remove_column :tickets, :tag
    add_index :tasks, :released_at
    add_index :tickets, :released_at
    add_index :tickets, :project_id
  end

  def down
    remove_index :tickets, :project_id
    remove_index :tickets, :released_at
    remove_index :tasks, :released_at
    add_column :tickets, :tag, :string
    change_column :tickets, :released_at, :datetime
    rename_column :tickets, :released_at, :date
    change_column :tasks, :released_at, :datetime
    rename_column :tasks, :released_at, :date
  end
end
