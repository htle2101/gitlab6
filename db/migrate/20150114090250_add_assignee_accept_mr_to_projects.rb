class AddAssigneeAcceptMrToProjects < ActiveRecord::Migration
  def change
    add_column :projects, :assignee_accept_mr, :boolean, :default => true
  end
end
