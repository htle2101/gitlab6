class AddHasBetaPaasToProjectModules < ActiveRecord::Migration
  def change
  	add_column :project_modules, :has_beta_paas, :integer,:default => 0
  end
end
