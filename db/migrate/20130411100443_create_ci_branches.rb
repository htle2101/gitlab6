class CreateCiBranches < ActiveRecord::Migration
  def change
    create_table :ci_branches do |t|
      t.references :project
      t.references :ci_env
      t.string :branch_name
      t.string :jenkins_job_name
      t.boolean :gitlab_created
      t.string :package_type
      t.string :deploy_ip

      t.timestamps
    end
    add_index :ci_branches, :project_id
    add_index :ci_branches, :ci_env_id
    add_index :ci_branches, :branch_name
    add_index :ci_branches, :jenkins_job_name
  end
end
