class CreateTaskConfigs < ActiveRecord::Migration
  def up
  	create_table :task_configs do |t|
  		t.string :type
  		t.string :key
  		t.string :name
  		t.string :online_op
  		t.string :online_value
  		t.string :offline_dev
  		t.string :offline_alpha
  		t.string :offline_beta
  		t.string :offline_pre_release

      t.timestamps
  	end
  end

  def down
  	drop_table :task_configs
  end
end
