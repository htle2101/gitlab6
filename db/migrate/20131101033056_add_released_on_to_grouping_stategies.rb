class AddReleasedOnToGroupingStategies < ActiveRecord::Migration
  def change
    add_column :grouping_strategies, :released_on, :datetime
  end
end
