class ChangeCiBranchesToBuildBranches < ActiveRecord::Migration
  def change
    rename_table :ci_branches, :build_branches
  end
end
