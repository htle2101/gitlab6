class CreateBetaPaasLogs < ActiveRecord::Migration
  def change
    create_table :beta_paas_logs do |t|
      t.string :module_name
      t.text :log
      t.integer :status
      t.string :tag
      t.integer :build_branch_id
      t.integer :beta_paas_rollout_id

      t.timestamps
    end
  end
end
