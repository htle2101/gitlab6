class CreateGsStatistics < ActiveRecord::Migration
  def change
    create_table :gs_statistics do |t|
      t.integer :gs_id
      t.integer :status
      t.date :started_at
      t.date :finished_at
      t.float :time_interval
      t.integer :ticket_id
      t.integer :project_id
      t.string :tag
      t.string :package_url

      t.timestamps
    end
  end
end
