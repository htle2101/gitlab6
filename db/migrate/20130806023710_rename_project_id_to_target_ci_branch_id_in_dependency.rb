class RenameProjectIdToTargetCiBranchIdInDependency < ActiveRecord::Migration
  def change
    rename_column :dependencies, :project_id, :target_ci_branch_id
  end
end
