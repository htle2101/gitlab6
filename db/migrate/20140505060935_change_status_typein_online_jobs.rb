class ChangeStatusTypeinOnlineJobs < ActiveRecord::Migration
  def up
  	change_column :online_jobs, :status, :integer
  end

  def down
  end
end
