class CreateGsGroupStatistics < ActiveRecord::Migration
  def change
    create_table :gs_group_statistics do |t|
      t.integer :namespace_id
      t.string :namespace_name
      t.string :weekth
      t.string :date_interval
      t.integer :order
      t.string :appname
      t.integer :project_id

      t.timestamps
    end
  end
end
