class RenameWarnameToPaasRollouts < ActiveRecord::Migration
  def up
  	rename_column :paas_rollouts, :warname, :name
  end

  def down
  end
end
