class AddTypeToCiBranches < ActiveRecord::Migration
  def change
    add_column :ci_branches, :type, :string, :default => 'CiBranch', :after => :id
  end
end
