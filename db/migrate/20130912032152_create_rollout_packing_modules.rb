class CreateRolloutPackingModules < ActiveRecord::Migration
  def change
    create_table :rollout_packing_modules do |t|
      t.integer :packing_id
      t.string :name
      t.string :url

      t.timestamps
    end

    add_index :rollout_packing_modules, :packing_id
    add_index :rollout_packing_modules, :name
  end
end
