class AddStatusToTickets < ActiveRecord::Migration
  def change
    add_column :tickets, :status, :integer, limit: 1, after: :project_id
    add_index :tickets, :status
  end
end
