class AddPaasVersionToRolloutPackingModule < ActiveRecord::Migration
  def change
    add_column :rollout_packing_modules, :paas_version, :string
  end
end
