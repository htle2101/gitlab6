class AddProjectIdToGroupingStrategy < ActiveRecord::Migration
  def change
    add_column :grouping_strategies, :project_id, :integer
  end
end
