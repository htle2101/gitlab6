class CreateBetaDeployVotes < ActiveRecord::Migration
  def change
    create_table :beta_deploy_votes do |t|
      t.integer :user_id, null: false
      t.integer :beta_deploy_log_id, null: false
      t.string :type, null: false

      t.timestamps
    end

    add_index :beta_deploy_votes, [:user_id, :beta_deploy_log_id], unique: true
  end
end
