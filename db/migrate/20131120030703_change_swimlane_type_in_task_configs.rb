class ChangeSwimlaneTypeInTaskConfigs < ActiveRecord::Migration
  def up
    change_column :task_configs, :swimlane, :string
  end

  def down
    change_column :task_configs, :swimlane, :integer
  end
end
