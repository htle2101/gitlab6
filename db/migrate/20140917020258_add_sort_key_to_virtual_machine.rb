class AddSortKeyToVirtualMachine < ActiveRecord::Migration
  def change
    add_column :virtual_machines, :sort_key, :integer, :default => 1
  end
end
