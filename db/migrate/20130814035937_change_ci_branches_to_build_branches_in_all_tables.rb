class ChangeCiBranchesToBuildBranchesInAllTables < ActiveRecord::Migration
  def change
    rename_column :virtual_machines, :ci_branch_id, :build_branch_id
    rename_column :light_merges, :ci_branch_id, :build_branch_id
    rename_column :dependencies, :ci_branch_id, :build_branch_id
    rename_column :dependencies, :target_ci_branch_id, :target_build_branch_id
    add_column :build_branches, :module_names, :string
  end
end
