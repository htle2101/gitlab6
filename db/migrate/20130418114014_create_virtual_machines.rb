class CreateVirtualMachines < ActiveRecord::Migration
  def change
    create_table :virtual_machines do |t|
      t.string :ip
      t.integer :status
      t.string :module_name
      t.integer :ci_branch_id
      t.string :package_type
      t.string :username
      t.string :password

      t.timestamps
    end

    add_index :virtual_machines, :status
    add_index :virtual_machines, :ci_branch_id
    add_index :virtual_machines, :package_type
  end
end
