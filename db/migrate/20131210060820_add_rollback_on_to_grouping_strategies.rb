class AddRollbackOnToGroupingStrategies < ActiveRecord::Migration
  def change
    add_column :grouping_strategies, :rollback_on, :Date
  end
end
