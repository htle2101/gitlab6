class RemoveUrlFromGroupingStrategies < ActiveRecord::Migration
  def up
    remove_column :grouping_strategies, :url
  end

  def down
    add_column :grouping_strategies, :url, :string
  end
end
