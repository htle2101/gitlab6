class AddTagToOnlineJobs < ActiveRecord::Migration
  def change
  	add_column :online_jobs, :tag, :string, after: :machine
  end
end
