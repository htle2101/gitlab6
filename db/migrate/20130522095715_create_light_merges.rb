class CreateLightMerges < ActiveRecord::Migration
  def change
    create_table :light_merges do |t|
      t.references :project
      t.string :branch_name
      t.string :base_branch
      t.text :source_branches
      t.text :conflict_branches
      t.integer :status

      t.timestamps
    end
    add_index :light_merges, :project_id
  end
end
