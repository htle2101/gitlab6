class CreateCiTemplates < ActiveRecord::Migration
  def change
    create_table :ci_templates do |t|
      t.references :namespace
      t.references :ci_env
      t.string :template_name
      t.string :package_type
      t.text :comment

      t.timestamps
    end
    add_index :ci_templates, :namespace_id
    add_index :ci_templates, :ci_env_id
    add_index :ci_templates, :template_name
    add_index :ci_templates, :package_type
  end
end
