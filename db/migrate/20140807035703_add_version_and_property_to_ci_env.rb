class AddVersionAndPropertyToCiEnv < ActiveRecord::Migration
  def up
    add_column :ci_envs, :version, :string, :default => '1.563'
    add_column :ci_envs, :property, :text

    add_default_property
  end

  def down
    remove_colum :ci_envs, :version
    remove_colum :ci_envs, :property
  end  

  def add_default_property
    CiEnv.all.each do |ci_env|
      ci_env.property = { "maven2" => 'mvn', "maven3" => 'mvn3', "workspace" => 'jobs/#{jenkins_job_name}/workspace' }
      ci_env.save
    end  
  end  
end
