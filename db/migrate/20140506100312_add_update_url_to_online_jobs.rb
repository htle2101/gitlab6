class AddUpdateUrlToOnlineJobs < ActiveRecord::Migration
  def change
  	add_column :online_jobs, :update_url, :string
  end
end
