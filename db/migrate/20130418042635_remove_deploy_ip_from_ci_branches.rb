class RemoveDeployIpFromCiBranches < ActiveRecord::Migration
  def up
    remove_column :ci_branches, :deploy_ip
  end

  def down
    add_column :ci_branches, :deploy_ip, :string
  end
end
