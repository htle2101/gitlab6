class RemoveBranchNameFromLightMerge < ActiveRecord::Migration
  def up
    remove_column :light_merges, :branch_name
  end

  def down
    add_column :light_merges, :branch_name, :string
  end
end
