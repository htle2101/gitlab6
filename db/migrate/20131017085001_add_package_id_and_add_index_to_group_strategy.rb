class AddPackageIdAndAddIndexToGroupStrategy < ActiveRecord::Migration
  def change
    add_column :grouping_strategies, :package_id, :integer, after: :group_id
    add_index :grouping_strategies, :package_id
    add_index :grouping_strategies, :group_id
    add_index :grouping_strategies, :ticket_id
    add_index :grouping_strategies, :status
  end
end
