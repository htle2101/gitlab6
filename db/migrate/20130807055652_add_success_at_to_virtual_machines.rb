class AddSuccessAtToVirtualMachines < ActiveRecord::Migration
  def change
    add_column :virtual_machines, :success_at, :datetime
  end
end
