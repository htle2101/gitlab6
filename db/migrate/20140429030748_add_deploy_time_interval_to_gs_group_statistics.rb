class AddDeployTimeIntervalToGsGroupStatistics < ActiveRecord::Migration
  def change
  	add_column :gs_group_statistics, :deploy_time_interval, :string, after: :date_interval
  end
end
