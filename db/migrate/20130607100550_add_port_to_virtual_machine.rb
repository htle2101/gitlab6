class AddPortToVirtualMachine < ActiveRecord::Migration
  def change
    add_column :virtual_machines, :port, :integer, :default => 58422
  end
end
