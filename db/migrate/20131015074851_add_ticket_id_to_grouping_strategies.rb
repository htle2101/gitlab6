class AddTicketIdToGroupingStrategies < ActiveRecord::Migration
  def change
    add_column :grouping_strategies, :ticket_id, :integer
  end
end
