class MoveRolloutColumnsToRolloutPacking < ActiveRecord::Migration
  def up
    create_table :rollout_packings do |t|
      t.integer :build_branch_id
      t.string :branch_name
      t.string :tag
      t.string :commit, limit: 40
      t.string :module_names
      t.text :war_urls
      t.integer :author_id
      t.integer :status, limit: 1

      t.timestamps
    end

    add_index :rollout_packings, :build_branch_id
    add_index :rollout_packings, :author_id
    add_index :rollout_packings, :tag
    add_index :rollout_packings, :status

    remove_column :build_branches, :module_names
    remove_column :build_branches, :war_urls
    remove_column :build_branches, :status
  end

  def down
    add_column :build_branches, :module_names, :string
    add_column :build_branches, :war_urls, :text
    add_column :build_branches, :status, :integer

    drop_table :rollout_packings
  end
end
