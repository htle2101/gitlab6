class CreateArtifacts < ActiveRecord::Migration
  def change
    create_table :artifacts do |t|
      t.string :group_id
      t.string :artifact_id
      t.string :version

      t.timestamps
    end
  end
end
