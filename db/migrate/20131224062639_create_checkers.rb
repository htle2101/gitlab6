class CreateCheckers < ActiveRecord::Migration
  def change
    create_table :checkers do |t|
      t.references :task
      t.references :author
      t.references :assignee
      t.text :description
      t.boolean :checked

      t.timestamps
    end
    add_index :checkers, :task_id
    add_index :checkers, :author_id
    add_index :checkers, :assignee_id
  end
end
