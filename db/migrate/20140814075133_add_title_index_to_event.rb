class AddTitleIndexToEvent < ActiveRecord::Migration
  def change
    add_index :events, :title
    Event.unscoped.ci_build.where(project_id: BuildBranch.where(ci_env_id: CiEnv.where(name: 'alpha')).uniq.pluck(:project_id)).update_all(title: 'alpha') 
    Event.unscoped.ci_build.where(project_id: BuildBranch.where(ci_env_id: CiEnv.where(name: 'beta')).uniq.pluck(:project_id)).update_all(title: 'beta') 
  end
end
