class AddTagToGroupingStrategy < ActiveRecord::Migration
  def change
    add_column :grouping_strategies, :tag, :string
  end
end
