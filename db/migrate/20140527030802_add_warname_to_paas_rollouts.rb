class AddWarnameToPaasRollouts < ActiveRecord::Migration
  def change
  	add_column :paas_rollouts, :warname, :string, after: :appname
  end
end
