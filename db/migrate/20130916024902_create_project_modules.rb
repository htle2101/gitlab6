class CreateProjectModules < ActiveRecord::Migration
  def change
    create_table :project_modules do |t|
      t.references :project
      t.string :module_name

      t.timestamps
    end
    add_index :project_modules, :project_id
    add_index :project_modules, :module_name
  end
end
