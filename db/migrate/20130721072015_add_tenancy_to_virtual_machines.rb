class AddTenancyToVirtualMachines < ActiveRecord::Migration
  def change
    add_column :virtual_machines, :tenancy, :int
  end
end
