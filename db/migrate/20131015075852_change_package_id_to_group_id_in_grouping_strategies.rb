class ChangePackageIdToGroupIdInGroupingStrategies < ActiveRecord::Migration
  def up
    rename_column :grouping_strategies, :package_id, :group_id
  end

  def down
  end
end
