class AddBlacklistToProject < ActiveRecord::Migration
  def change
    add_column :projects, :blacklist, :string, :default => ''
  end
end
