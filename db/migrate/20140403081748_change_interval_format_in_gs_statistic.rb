class ChangeIntervalFormatInGsStatistic < ActiveRecord::Migration
  def up
  	change_column :gs_statistics, :time_interval, :string
  end

  def down
  end
end
