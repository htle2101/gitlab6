class CreateDependencies < ActiveRecord::Migration
  def change
    create_table :dependencies do |t|
      t.references :project
      t.references :ci_branch
      t.string :module_name

      t.timestamps
    end
    add_index :dependencies, :project_id
    add_index :dependencies, :ci_branch_id
  end
end
