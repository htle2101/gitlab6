# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20150203074510) do

  create_table "artifacts", :force => true do |t|
    t.string   "group_id"
    t.string   "artifact_id"
    t.string   "version"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "beta_deploy_logs", :force => true do |t|
    t.integer  "project_id",                                 :null => false
    t.string   "branch_name",                                :null => false
    t.string   "project_path_with_namespace",                :null => false
    t.string   "module_name",                                :null => false
    t.integer  "voteup_count",                :default => 0
    t.integer  "votedown_count",              :default => 0
    t.date     "date",                                       :null => false
    t.datetime "created_at",                                 :null => false
    t.datetime "updated_at",                                 :null => false
  end

  add_index "beta_deploy_logs", ["date"], :name => "index_beta_deploy_logs_on_date"

  create_table "beta_deploy_votes", :force => true do |t|
    t.integer  "user_id",            :null => false
    t.integer  "beta_deploy_log_id", :null => false
    t.string   "type",               :null => false
    t.datetime "created_at",         :null => false
    t.datetime "updated_at",         :null => false
  end

  add_index "beta_deploy_votes", ["user_id", "beta_deploy_log_id"], :name => "index_beta_deploy_votes_on_user_id_and_beta_deploy_log_id", :unique => true

  create_table "beta_paas_logs", :force => true do |t|
    t.string   "module_name"
    t.text     "log",                  :limit => 16777215
    t.integer  "status"
    t.string   "tag"
    t.integer  "build_branch_id"
    t.integer  "beta_paas_rollout_id"
    t.datetime "created_at",                               :null => false
    t.datetime "updated_at",                               :null => false
    t.string   "commit_message"
  end

  create_table "beta_paas_rollouts", :force => true do |t|
    t.integer  "build_branch_id"
    t.integer  "status",          :default => 0
    t.datetime "released_on"
    t.string   "tag"
    t.integer  "group_id"
    t.string   "hostname"
    t.integer  "operation_id"
    t.string   "machine"
    t.datetime "created_at",                     :null => false
    t.datetime "updated_at",                     :null => false
    t.string   "appname"
  end

  create_table "bu_machines", :force => true do |t|
    t.string   "bu_name"
    t.string   "department"
    t.integer  "alpha"
    t.integer  "beta"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "build_branches", :force => true do |t|
    t.string   "type",                          :default => "CiBranch"
    t.integer  "project_id",       :limit => 8
    t.integer  "ci_env_id"
    t.string   "branch_name"
    t.string   "jenkins_job_name"
    t.boolean  "gitlab_created"
    t.string   "package_type"
    t.datetime "created_at",                                            :null => false
    t.datetime "updated_at",                                            :null => false
    t.string   "deployers",                     :default => "War"
    t.boolean  "auto_deploy",                   :default => true
    t.boolean  "run_test",                      :default => true
  end

  add_index "build_branches", ["branch_name"], :name => "index_ci_branches_on_branch_name"
  add_index "build_branches", ["ci_env_id"], :name => "index_ci_branches_on_ci_env_id"
  add_index "build_branches", ["jenkins_job_name"], :name => "index_ci_branches_on_jenkins_job_name"
  add_index "build_branches", ["project_id"], :name => "index_ci_branches_on_project_id"

  create_table "checkers", :force => true do |t|
    t.integer  "task_id"
    t.integer  "author_id"
    t.integer  "assignee_id"
    t.text     "description"
    t.boolean  "checked"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  add_index "checkers", ["assignee_id"], :name => "index_checkers_on_assignee_id"
  add_index "checkers", ["author_id"], :name => "index_checkers_on_author_id"
  add_index "checkers", ["task_id"], :name => "index_checkers_on_task_id"

  create_table "ci_envs", :force => true do |t|
    t.string   "name"
    t.string   "url"
    t.string   "user"
    t.string   "password"
    t.boolean  "is_single"
    t.string   "ssh_user"
    t.string   "ssh_password"
    t.string   "path"
    t.datetime "created_at",                        :null => false
    t.datetime "updated_at",                        :null => false
    t.boolean  "is_cmdb"
    t.boolean  "default",      :default => false
    t.integer  "port"
    t.string   "version",      :default => "1.563"
    t.text     "property"
  end

  create_table "ci_envs_namespaces", :force => true do |t|
    t.integer "ci_env_id",    :null => false
    t.integer "namespace_id", :null => false
    t.string  "ci_env_name",  :null => false
  end

  add_index "ci_envs_namespaces", ["namespace_id", "ci_env_name"], :name => "index_ci_envs_namespaces_on_namespace_id_and_ci_env_name", :unique => true

  create_table "ci_templates", :force => true do |t|
    t.integer  "namespace_id"
    t.integer  "ci_env_id"
    t.string   "template_name"
    t.string   "package_type"
    t.string   "deployers",     :default => "War"
    t.text     "comment"
    t.boolean  "visable",       :default => true
    t.datetime "created_at",                       :null => false
    t.datetime "updated_at",                       :null => false
    t.string   "ci_env_name"
  end

  add_index "ci_templates", ["ci_env_id"], :name => "index_ci_templates_on_ci_env_id"
  add_index "ci_templates", ["namespace_id"], :name => "index_ci_templates_on_namespace_id"
  add_index "ci_templates", ["package_type"], :name => "index_ci_templates_on_package_type"
  add_index "ci_templates", ["template_name"], :name => "index_ci_templates_on_template_name"

  create_table "classifications", :force => true do |t|
    t.string   "name"
    t.text     "description"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  add_index "classifications", ["name"], :name => "index_classifications_on_name"

  create_table "classifications_namespaces", :force => true do |t|
    t.integer  "classification_id"
    t.integer  "namespace_id"
    t.datetime "created_at",        :null => false
    t.datetime "updated_at",        :null => false
  end

  add_index "classifications_namespaces", ["classification_id"], :name => "index_classifications_namespaces_on_classification_id"
  add_index "classifications_namespaces", ["namespace_id"], :name => "index_classifications_namespaces_on_namespace_id"

  create_table "databases", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "databases", ["name"], :name => "index_databases_on_name"

  create_table "dependencies", :force => true do |t|
    t.integer  "target_build_branch_id"
    t.integer  "build_branch_id"
    t.string   "module_name"
    t.datetime "created_at",             :null => false
    t.datetime "updated_at",             :null => false
  end

  add_index "dependencies", ["build_branch_id"], :name => "index_dependencies_on_ci_branch_id"
  add_index "dependencies", ["target_build_branch_id"], :name => "index_dependencies_on_project_id"

  create_table "deploy_keys_projects", :force => true do |t|
    t.integer  "deploy_key_id", :null => false
    t.integer  "project_id",    :null => false
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  add_index "deploy_keys_projects", ["project_id"], :name => "index_deploy_keys_projects_on_project_id"

  create_table "events", :force => true do |t|
    t.string   "target_type"
    t.integer  "target_id"
    t.string   "title"
    t.text     "data"
    t.integer  "project_id"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
    t.integer  "action"
    t.integer  "author_id"
  end

  add_index "events", ["action"], :name => "index_events_on_action"
  add_index "events", ["author_id"], :name => "index_events_on_author_id"
  add_index "events", ["created_at"], :name => "index_events_on_created_at"
  add_index "events", ["project_id"], :name => "index_events_on_project_id"
  add_index "events", ["target_id"], :name => "index_events_on_target_id"
  add_index "events", ["target_type"], :name => "index_events_on_target_type"
  add_index "events", ["title"], :name => "index_events_on_title"

  create_table "forked_project_links", :force => true do |t|
    t.integer  "forked_to_project_id",   :null => false
    t.integer  "forked_from_project_id", :null => false
    t.datetime "created_at",             :null => false
    t.datetime "updated_at",             :null => false
  end

  add_index "forked_project_links", ["forked_to_project_id"], :name => "index_forked_project_links_on_forked_to_project_id", :unique => true

  create_table "grouping_strategies", :force => true do |t|
    t.string   "name"
    t.string   "hostname"
    t.string   "swimlane"
    t.string   "appname"
    t.integer  "group_id"
    t.integer  "package_id"
    t.datetime "created_at",                    :null => false
    t.datetime "updated_at",                    :null => false
    t.integer  "ticket_id"
    t.integer  "status",      :default => 1024
    t.string   "log"
    t.datetime "released_on"
    t.integer  "project_id"
    t.string   "tag"
    t.datetime "rollback_on"
    t.string   "artifact_id"
    t.datetime "started_at"
    t.datetime "finished_at"
  end

  add_index "grouping_strategies", ["group_id"], :name => "index_grouping_strategies_on_group_id"
  add_index "grouping_strategies", ["package_id"], :name => "index_grouping_strategies_on_package_id"
  add_index "grouping_strategies", ["project_id"], :name => "index_grouping_strategies_on_project_id"
  add_index "grouping_strategies", ["status"], :name => "index_grouping_strategies_on_status"
  add_index "grouping_strategies", ["ticket_id"], :name => "index_grouping_strategies_on_ticket_id"

  create_table "gs_group_statistics", :force => true do |t|
    t.integer  "namespace_id"
    t.string   "namespace_name"
    t.string   "weekth"
    t.string   "date_interval"
    t.string   "deploy_time_interval"
    t.integer  "order"
    t.string   "appname"
    t.integer  "project_id"
    t.datetime "created_at",           :null => false
    t.datetime "updated_at",           :null => false
  end

  create_table "gs_redeploy_statistics", :force => true do |t|
    t.string   "appname"
    t.integer  "gs_id"
    t.string   "tag"
    t.string   "group"
    t.integer  "group_id"
    t.integer  "ticket_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.integer  "project_id"
    t.string   "hostname"
  end

  create_table "gs_statistics", :force => true do |t|
    t.integer  "gs_id"
    t.datetime "started_at"
    t.datetime "finished_at"
    t.string   "group_percent"
    t.string   "status"
    t.string   "time_interval"
    t.integer  "ticket_id"
    t.integer  "project_id"
    t.integer  "namespace_id"
    t.string   "tag"
    t.integer  "package_id"
    t.string   "appname"
    t.datetime "created_at",        :null => false
    t.datetime "updated_at",        :null => false
    t.integer  "time_interval_int"
  end

  create_table "issues", :force => true do |t|
    t.string   "title"
    t.integer  "assignee_id"
    t.integer  "author_id"
    t.integer  "project_id"
    t.datetime "created_at",                  :null => false
    t.datetime "updated_at",                  :null => false
    t.integer  "position",     :default => 0
    t.string   "branch_name"
    t.text     "description"
    t.integer  "milestone_id"
    t.string   "state"
  end

  add_index "issues", ["assignee_id"], :name => "index_issues_on_assignee_id"
  add_index "issues", ["author_id"], :name => "index_issues_on_author_id"
  add_index "issues", ["created_at"], :name => "index_issues_on_created_at"
  add_index "issues", ["milestone_id"], :name => "index_issues_on_milestone_id"
  add_index "issues", ["project_id"], :name => "index_issues_on_project_id"
  add_index "issues", ["title"], :name => "index_issues_on_title"

  create_table "keys", :force => true do |t|
    t.integer  "user_id"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
    t.text     "key"
    t.string   "title"
    t.string   "type"
    t.string   "fingerprint"
  end

  add_index "keys", ["user_id"], :name => "index_keys_on_user_id"

  create_table "liger_increments", :force => true do |t|
    t.string  "appname",                       :null => false
    t.integer "ci_branch_id"
    t.integer "packing_id"
    t.string  "last_commit",   :limit => 1024
    t.string  "tag"
    t.string  "key"
    t.string  "origin_value"
    t.string  "current_value"
    t.string  "target_value"
    t.string  "final_value"
    t.string  "liger_commit"
  end

  create_table "light_merges", :force => true do |t|
    t.integer  "project_id"
    t.string   "base_branch"
    t.text     "source_branches"
    t.text     "conflict_branches"
    t.integer  "status"
    t.datetime "created_at",        :null => false
    t.datetime "updated_at",        :null => false
    t.integer  "build_branch_id"
  end

  add_index "light_merges", ["build_branch_id"], :name => "index_light_merges_on_ci_branch_id"
  add_index "light_merges", ["project_id"], :name => "index_light_merges_on_project_id"

  create_table "machine_usage_monthlies", :force => true do |t|
    t.integer  "namespace_id"
    t.integer  "project_id"
    t.integer  "virtual_machine_id"
    t.integer  "year"
    t.integer  "month"
    t.integer  "duration"
    t.datetime "created_at",         :null => false
    t.datetime "updated_at",         :null => false
  end

  add_index "machine_usage_monthlies", ["namespace_id"], :name => "index_machine_usage_monthlies_on_namespace_id"
  add_index "machine_usage_monthlies", ["project_id"], :name => "index_machine_usage_monthlies_on_project_id"
  add_index "machine_usage_monthlies", ["virtual_machine_id"], :name => "index_machine_usage_monthlies_on_virtual_machine_id"

  create_table "merge_request_inspections", :force => true do |t|
    t.integer  "project_id"
    t.integer  "merge_request_id"
    t.string   "source_branch"
    t.string   "target_branch"
    t.string   "jenkins_job_url"
    t.text     "stdout"
    t.integer  "status"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
    t.text     "stderr"
  end

  create_table "merge_requests", :force => true do |t|
    t.string   "target_branch",                                              :null => false
    t.string   "source_branch",                                              :null => false
    t.integer  "source_project_id",                                          :null => false
    t.integer  "author_id"
    t.integer  "assignee_id"
    t.string   "title"
    t.datetime "created_at",                                                 :null => false
    t.datetime "updated_at",                                                 :null => false
    t.text     "st_commits",        :limit => 2147483647
    t.text     "st_diffs",          :limit => 2147483647
    t.integer  "milestone_id"
    t.string   "state"
    t.string   "merge_status"
    t.integer  "target_project_id",                                          :null => false
    t.boolean  "reviewed",                                :default => false
  end

  add_index "merge_requests", ["assignee_id"], :name => "index_merge_requests_on_assignee_id"
  add_index "merge_requests", ["author_id"], :name => "index_merge_requests_on_author_id"
  add_index "merge_requests", ["created_at"], :name => "index_merge_requests_on_created_at"
  add_index "merge_requests", ["milestone_id"], :name => "index_merge_requests_on_milestone_id"
  add_index "merge_requests", ["source_branch"], :name => "index_merge_requests_on_source_branch"
  add_index "merge_requests", ["source_project_id"], :name => "index_merge_requests_on_project_id"
  add_index "merge_requests", ["target_branch"], :name => "index_merge_requests_on_target_branch"
  add_index "merge_requests", ["title"], :name => "index_merge_requests_on_title"

  create_table "milestones", :force => true do |t|
    t.string   "title",       :null => false
    t.integer  "project_id",  :null => false
    t.text     "description"
    t.date     "due_date"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
    t.string   "state"
  end

  add_index "milestones", ["due_date"], :name => "index_milestones_on_due_date"
  add_index "milestones", ["project_id"], :name => "index_milestones_on_project_id"

  create_table "namespaces", :force => true do |t|
    t.string   "name",                        :null => false
    t.string   "path",                        :null => false
    t.integer  "owner_id",                    :null => false
    t.datetime "created_at",                  :null => false
    t.datetime "updated_at",                  :null => false
    t.string   "type"
    t.string   "description", :default => "", :null => false
  end

  add_index "namespaces", ["name"], :name => "index_namespaces_on_name"
  add_index "namespaces", ["owner_id"], :name => "index_namespaces_on_owner_id"
  add_index "namespaces", ["path"], :name => "index_namespaces_on_path"
  add_index "namespaces", ["type"], :name => "index_namespaces_on_type"

  create_table "notes", :force => true do |t|
    t.text     "note"
    t.string   "noteable_type"
    t.integer  "author_id"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
    t.integer  "project_id"
    t.string   "attachment"
    t.string   "line_code"
    t.string   "commit_id"
    t.integer  "noteable_id"
    t.text     "st_diff"
  end

  add_index "notes", ["author_id"], :name => "index_notes_on_author_id"
  add_index "notes", ["commit_id"], :name => "index_notes_on_commit_id"
  add_index "notes", ["created_at"], :name => "index_notes_on_created_at"
  add_index "notes", ["noteable_id", "noteable_type"], :name => "index_notes_on_noteable_id_and_noteable_type"
  add_index "notes", ["noteable_type"], :name => "index_notes_on_noteable_type"
  add_index "notes", ["project_id", "noteable_type"], :name => "index_notes_on_project_id_and_noteable_type"
  add_index "notes", ["project_id"], :name => "index_notes_on_project_id"

  create_table "online_jobs", :force => true do |t|
    t.string   "jar_name"
    t.integer  "status"
    t.string   "tag"
    t.string   "jar_url"
    t.string   "machine"
    t.string   "env"
    t.string   "job_type"
    t.text     "online_script"
    t.datetime "success_at"
    t.integer  "project_id"
    t.integer  "build_branch_id"
    t.datetime "created_at",            :null => false
    t.datetime "updated_at",            :null => false
    t.string   "log"
    t.string   "create_url"
    t.string   "update_url"
    t.string   "business_belongs_to"
    t.string   "business_abbreviation"
  end

  create_table "paas_rollouts", :force => true do |t|
    t.integer  "project_id"
    t.integer  "package_id"
    t.integer  "status",       :default => 0
    t.datetime "released_on"
    t.string   "tag"
    t.string   "appname"
    t.string   "name"
    t.integer  "group_id"
    t.string   "hostname"
    t.string   "ticket_id"
    t.string   "operation_id"
    t.datetime "created_at",                  :null => false
    t.datetime "updated_at",                  :null => false
  end

  add_index "paas_rollouts", ["package_id"], :name => "index_paas_rollouts_on_package_id"
  add_index "paas_rollouts", ["project_id"], :name => "index_paas_rollouts_on_project_id"

  create_table "packages", :force => true do |t|
    t.string   "name"
    t.string   "package_type"
    t.string   "tag"
    t.string   "url"
    t.integer  "packing_module_id"
    t.integer  "packing_id"
    t.integer  "ticket_id"
    t.integer  "project_id"
    t.integer  "owner",               :limit => 1
    t.datetime "created_at",                                      :null => false
    t.datetime "updated_at",                                      :null => false
    t.integer  "gs_from_module_name",              :default => 1
    t.string   "rollback_to_url"
    t.string   "warname"
  end

  add_index "packages", ["name"], :name => "index_packages_on_name"
  add_index "packages", ["owner"], :name => "index_packages_on_owner"
  add_index "packages", ["packing_id"], :name => "index_packages_on_packing_id"
  add_index "packages", ["packing_module_id"], :name => "index_packages_on_packing_module_id"
  add_index "packages", ["project_id"], :name => "index_packages_on_project_id"
  add_index "packages", ["tag"], :name => "index_packages_on_tag"
  add_index "packages", ["ticket_id"], :name => "index_packages_on_ticket_id"

  create_table "performance_test_modules", :force => true do |t|
    t.string   "appname"
    t.string   "tag"
    t.string   "machine"
    t.boolean  "main"
    t.integer  "cpu"
    t.string   "cpu_mode"
    t.integer  "memory",              :limit => 8
    t.string   "url"
    t.integer  "performance_test_id"
    t.integer  "status"
    t.datetime "created_at",                       :null => false
    t.datetime "updated_at",                       :null => false
  end

  create_table "performance_tests", :force => true do |t|
    t.string   "title"
    t.string   "target"
    t.string   "uid"
    t.string   "test_log_url"
    t.text     "upload_file"
    t.integer  "project_id"
    t.integer  "status"
    t.string   "status_message"
    t.string   "domain"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
  end

  create_table "project_modules", :force => true do |t|
    t.integer  "project_id"
    t.string   "module_name"
    t.datetime "created_at",                                            :null => false
    t.datetime "updated_at",                                            :null => false
    t.boolean  "has_paas",      :default => false
    t.string   "paas_url",      :default => "http://10.101.0.11:8080/", :null => false
    t.integer  "has_beta_paas", :default => 0
    t.string   "ftp_url"
  end

  add_index "project_modules", ["module_name"], :name => "index_project_modules_on_module_name"
  add_index "project_modules", ["project_id"], :name => "index_project_modules_on_project_id"

  create_table "projects", :force => true do |t|
    t.string   "name"
    t.string   "path"
    t.text     "description"
    t.datetime "created_at",                                    :null => false
    t.datetime "updated_at",                                    :null => false
    t.integer  "creator_id"
    t.string   "default_branch"
    t.boolean  "issues_enabled",          :default => true,     :null => false
    t.boolean  "wall_enabled",            :default => true,     :null => false
    t.boolean  "merge_requests_enabled",  :default => true,     :null => false
    t.boolean  "wiki_enabled",            :default => true,     :null => false
    t.integer  "namespace_id"
    t.boolean  "public",                  :default => true,     :null => false
    t.boolean  "is_product",              :default => true
    t.string   "issues_tracker",          :default => "gitlab", :null => false
    t.string   "issues_tracker_id"
    t.boolean  "snippets_enabled",        :default => true,     :null => false
    t.datetime "last_activity_at"
    t.boolean  "imported",                :default => false,    :null => false
    t.string   "import_url"
    t.boolean  "rollout_enabled",         :default => true,     :null => false
    t.boolean  "ticket_add_wars_enabled", :default => true,     :null => false
    t.boolean  "workflow_check_enabled",  :default => false,    :null => false
    t.string   "private_token"
    t.string   "bu_name"
    t.string   "blacklist",               :default => ""
    t.boolean  "assignee_accept_mr",      :default => true
  end

  add_index "projects", ["creator_id"], :name => "index_projects_on_owner_id"
  add_index "projects", ["last_activity_at"], :name => "index_projects_on_last_activity_at"
  add_index "projects", ["namespace_id"], :name => "index_projects_on_namespace_id"

  create_table "protected_branches", :force => true do |t|
    t.integer  "project_id", :null => false
    t.string   "name",       :null => false
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "protected_branches", ["project_id"], :name => "index_protected_branches_on_project_id"

  create_table "rollout_packing_modules", :force => true do |t|
    t.integer  "packing_id"
    t.string   "name"
    t.string   "url"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
    t.integer  "status"
    t.string   "paas_version"
  end

  add_index "rollout_packing_modules", ["name"], :name => "index_rollout_packing_modules_on_name"
  add_index "rollout_packing_modules", ["packing_id"], :name => "index_rollout_packing_modules_on_packing_id"

  create_table "rollout_packings", :force => true do |t|
    t.integer  "build_branch_id"
    t.string   "branch_name"
    t.string   "tag"
    t.string   "commit",          :limit => 40
    t.string   "module_names"
    t.text     "war_urls"
    t.integer  "author_id"
    t.integer  "status",          :limit => 1
    t.datetime "created_at",                                       :null => false
    t.datetime "updated_at",                                       :null => false
    t.boolean  "cant_rollback",                 :default => false
    t.text     "note"
    t.string   "sign_off"
  end

  add_index "rollout_packings", ["author_id"], :name => "index_rollout_packings_on_author_id"
  add_index "rollout_packings", ["build_branch_id"], :name => "index_rollout_packings_on_build_branch_id"
  add_index "rollout_packings", ["status"], :name => "index_rollout_packings_on_status"
  add_index "rollout_packings", ["tag"], :name => "index_rollout_packings_on_tag"

  create_table "scripts", :force => true do |t|
    t.string   "name"
    t.string   "todo"
    t.string   "db_name"
    t.string   "path"
    t.integer  "project_id"
    t.string   "desc"
    t.integer  "status",          :limit => 1, :default => 0
    t.integer  "ppe_status",      :limit => 1, :default => 0
    t.integer  "prd_status",      :limit => 1, :default => 0
    t.integer  "author_id"
    t.string   "req_url"
    t.text     "data"
    t.datetime "prd_executed_at"
    t.datetime "ppe_executed_at"
    t.datetime "created_at",                                  :null => false
    t.datetime "updated_at",                                  :null => false
  end

  add_index "scripts", ["author_id"], :name => "index_scripts_on_author_id"
  add_index "scripts", ["ppe_status"], :name => "index_scripts_on_ppe_status"
  add_index "scripts", ["prd_status"], :name => "index_scripts_on_prd_status"
  add_index "scripts", ["project_id"], :name => "index_scripts_on_project_id"
  add_index "scripts", ["status"], :name => "index_scripts_on_status"

  create_table "services", :force => true do |t|
    t.string   "type"
    t.string   "title"
    t.string   "token"
    t.integer  "project_id",                     :null => false
    t.datetime "created_at",                     :null => false
    t.datetime "updated_at",                     :null => false
    t.boolean  "active",      :default => false, :null => false
    t.string   "project_url"
    t.string   "subdomain"
    t.string   "room"
  end

  add_index "services", ["project_id"], :name => "index_services_on_project_id"

  create_table "snippets", :force => true do |t|
    t.string   "title"
    t.text     "content",    :limit => 2147483647
    t.integer  "author_id",                                          :null => false
    t.integer  "project_id"
    t.datetime "created_at",                                         :null => false
    t.datetime "updated_at",                                         :null => false
    t.string   "file_name"
    t.datetime "expires_at"
    t.boolean  "private",                          :default => true, :null => false
    t.string   "type"
  end

  add_index "snippets", ["author_id"], :name => "index_snippets_on_author_id"
  add_index "snippets", ["created_at"], :name => "index_snippets_on_created_at"
  add_index "snippets", ["expires_at"], :name => "index_snippets_on_expires_at"
  add_index "snippets", ["project_id"], :name => "index_snippets_on_project_id"

  create_table "static_statuses", :force => true do |t|
    t.string   "build_status"
    t.string   "cache_status"
    t.integer  "build_number"
    t.integer  "build_branch_id"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
  end

  create_table "taggings", :force => true do |t|
    t.integer  "tag_id"
    t.integer  "taggable_id"
    t.string   "taggable_type"
    t.integer  "tagger_id"
    t.string   "tagger_type"
    t.string   "context"
    t.datetime "created_at"
  end

  add_index "taggings", ["tag_id"], :name => "index_taggings_on_tag_id"
  add_index "taggings", ["taggable_id", "taggable_type", "context"], :name => "index_taggings_on_taggable_id_and_taggable_type_and_context"

  create_table "tags", :force => true do |t|
    t.string "name"
  end

  create_table "task_configs", :force => true do |t|
    t.text     "type"
    t.string   "key"
    t.string   "name"
    t.string   "online_op"
    t.text     "online_value"
    t.text     "offline_dev"
    t.text     "offline_alpha"
    t.text     "offline_beta"
    t.text     "offline_pre_release"
    t.datetime "created_at",                         :null => false
    t.datetime "updated_at",                         :null => false
    t.integer  "task_id"
    t.string   "swimlane"
    t.integer  "state",               :default => 1
  end

  create_table "tasks", :force => true do |t|
    t.string   "subject"
    t.date     "released_at"
    t.integer  "project_id"
    t.string   "branch_name"
    t.integer  "status",      :limit => 1
    t.integer  "assignee_id"
    t.integer  "creator_id"
    t.datetime "created_at",               :null => false
    t.datetime "updated_at",               :null => false
    t.text     "description"
  end

  add_index "tasks", ["assignee_id"], :name => "index_tasks_on_assignee_id"
  add_index "tasks", ["creator_id"], :name => "index_tasks_on_creator_id"
  add_index "tasks", ["released_at"], :name => "index_tasks_on_released_at"
  add_index "tasks", ["status"], :name => "index_tasks_on_status"

  create_table "tasks_packages", :force => true do |t|
    t.integer "task_id"
    t.integer "package_id"
  end

  add_index "tasks_packages", ["package_id"], :name => "index_tasks_packages_on_package_id"
  add_index "tasks_packages", ["task_id"], :name => "index_tasks_packages_on_task_id"

  create_table "tickets", :force => true do |t|
    t.date     "released_at"
    t.integer  "project_id"
    t.integer  "status",      :limit => 1
    t.datetime "created_at",               :null => false
    t.datetime "updated_at",               :null => false
  end

  add_index "tickets", ["project_id"], :name => "index_tickets_on_project_id"
  add_index "tickets", ["released_at"], :name => "index_tickets_on_released_at"
  add_index "tickets", ["status"], :name => "index_tickets_on_status"

  create_table "user_team_project_relationships", :force => true do |t|
    t.integer  "project_id"
    t.integer  "user_team_id"
    t.integer  "greatest_access"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
  end

  create_table "user_team_user_relationships", :force => true do |t|
    t.integer  "user_id"
    t.integer  "user_team_id"
    t.boolean  "group_admin"
    t.integer  "permission"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "user_teams", :force => true do |t|
    t.string   "name"
    t.string   "path"
    t.integer  "owner_id"
    t.datetime "created_at",                  :null => false
    t.datetime "updated_at",                  :null => false
    t.string   "description", :default => "", :null => false
  end

  create_table "users", :force => true do |t|
    t.string   "email",                  :default => "",    :null => false
    t.string   "encrypted_password",     :default => "",    :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                                :null => false
    t.datetime "updated_at",                                :null => false
    t.string   "name"
    t.boolean  "admin",                  :default => false, :null => false
    t.integer  "projects_limit",         :default => 10
    t.string   "skype",                  :default => "",    :null => false
    t.string   "linkedin",               :default => "",    :null => false
    t.string   "twitter",                :default => "",    :null => false
    t.string   "authentication_token"
    t.integer  "theme_id",               :default => 1,     :null => false
    t.string   "bio"
    t.integer  "failed_attempts",        :default => 0
    t.datetime "locked_at"
    t.string   "extern_uid"
    t.string   "provider"
    t.string   "username"
    t.boolean  "can_create_group",       :default => true,  :null => false
    t.boolean  "can_create_team",        :default => true,  :null => false
    t.string   "state"
    t.integer  "color_scheme_id",        :default => 1,     :null => false
    t.integer  "notification_level",     :default => 1,     :null => false
    t.datetime "password_expires_at"
    t.integer  "created_by_id"
  end

  add_index "users", ["admin"], :name => "index_users_on_admin"
  add_index "users", ["authentication_token"], :name => "index_users_on_authentication_token", :unique => true
  add_index "users", ["email"], :name => "index_users_on_email", :unique => true
  add_index "users", ["extern_uid", "provider"], :name => "index_users_on_extern_uid_and_provider", :unique => true
  add_index "users", ["name"], :name => "index_users_on_name"
  add_index "users", ["reset_password_token"], :name => "index_users_on_reset_password_token", :unique => true
  add_index "users", ["username"], :name => "index_users_on_username"

  create_table "users_groups", :force => true do |t|
    t.integer  "group_access",                      :null => false
    t.integer  "group_id",                          :null => false
    t.integer  "user_id",                           :null => false
    t.datetime "created_at",                        :null => false
    t.datetime "updated_at",                        :null => false
    t.integer  "notification_level", :default => 3, :null => false
  end

  add_index "users_groups", ["user_id"], :name => "index_users_groups_on_user_id"

  create_table "users_projects", :force => true do |t|
    t.integer  "user_id",                           :null => false
    t.integer  "project_id",                        :null => false
    t.datetime "created_at",                        :null => false
    t.datetime "updated_at",                        :null => false
    t.integer  "project_access",     :default => 0, :null => false
    t.integer  "notification_level", :default => 3, :null => false
  end

  add_index "users_projects", ["project_access"], :name => "index_users_projects_on_project_access"
  add_index "users_projects", ["project_id"], :name => "index_users_projects_on_project_id"
  add_index "users_projects", ["user_id"], :name => "index_users_projects_on_user_id"

  create_table "virtual_machine_usages", :force => true do |t|
    t.integer  "machine_id"
    t.datetime "using_from"
    t.datetime "using_to"
    t.integer  "duration"
    t.integer  "group_id"
    t.integer  "project_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "virtual_machine_usages", ["group_id"], :name => "index_virtual_machine_usages_on_group_id"

  create_table "virtual_machines", :force => true do |t|
    t.string   "ip"
    t.string   "module_name"
    t.integer  "build_branch_id"
    t.integer  "project_id"
    t.string   "package_type"
    t.datetime "created_at",                         :null => false
    t.datetime "updated_at",                         :null => false
    t.string   "action_type"
    t.integer  "status",          :default => 0
    t.integer  "port",            :default => 58422
    t.string   "domain_name"
    t.integer  "tenancy"
    t.string   "deployer",        :default => "War"
    t.boolean  "active",          :default => true
    t.datetime "success_at"
    t.string   "password"
    t.string   "username"
    t.string   "swimlane",        :default => ""
    t.boolean  "is_service",      :default => false
    t.integer  "sort_key",        :default => 1
  end

  add_index "virtual_machines", ["build_branch_id"], :name => "index_virtual_machines_on_ci_branch_id"
  add_index "virtual_machines", ["deployer"], :name => "index_virtual_machines_on_deployer"
  add_index "virtual_machines", ["package_type"], :name => "index_virtual_machines_on_package_type"
  add_index "virtual_machines", ["project_id"], :name => "index_virtual_machines_on_project_id"

  create_table "web_hooks", :force => true do |t|
    t.string   "url"
    t.integer  "project_id"
    t.datetime "created_at",                                       :null => false
    t.datetime "updated_at",                                       :null => false
    t.string   "type",                  :default => "ProjectHook"
    t.integer  "service_id"
    t.boolean  "push_events",           :default => true,          :null => false
    t.boolean  "merge_requests_events", :default => false,         :null => false
  end

  add_index "web_hooks", ["project_id"], :name => "index_web_hooks_on_project_id"

end
