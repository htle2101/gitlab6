namespace :redis do
  desc "Setup redis"
  task :setup, roles: :app do
    run "mkdir -p #{shared_path}/config"
    template "resque.yml.erb", "#{shared_path}/config/resque.yml"
  end
  after "deploy:setup", "redis:setup"
end
