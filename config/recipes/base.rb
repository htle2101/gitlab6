def template(from, to)
  erb = File.read(File.expand_path("../templates/#{from}", __FILE__))
  put ERB.new(erb).result(binding), to
end

def chown_to_user(file_or_dir, user, group = nil, options = {})
  run "#{sudo} chown #{options[:recursive] == true ? '-R': ''} #{user}:#{group || user} #{file_or_dir}"
end

def set_default(name, *args, &block)
  set(name, *args, &block) unless exists?(name)
end

