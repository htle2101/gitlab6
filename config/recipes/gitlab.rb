namespace :gitlab do
  desc "Setup Unicorn initializer and app configuration"
  task :setup, roles: :app do
    run "mkdir -p #{shared_path}/config"
    template "gitlab.yml.erb", "#{shared_path}/config/gitlab.yml"
  end
  after "deploy:setup", "gitlab:setup"
end
