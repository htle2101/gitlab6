namespace :faye do

  desc "Stop faye"
  task :stop do
    rails_env = fetch(:rails_env, "production")
    run "cd #{current_path} ; sudo -u git -H bundle exec rake faye:stop RAILS_ENV=#{rails_env}"
  end

  desc "Start faye"
  task :start do
    rails_env = fetch(:rails_env, "production")
    run "cd #{current_path} ; sudo -u git -H bundle exec rake faye:start RAILS_ENV=#{rails_env}"
  end

  desc "Restart faye"
  task :restart do
    rails_env = fetch(:rails_env, "production")
    run "cd #{current_path} ; sudo -u git -H bundle exec rake faye:restart RAILS_ENV=#{rails_env}"
  end

end
