require 'securerandom'

namespace :token do
  desc "Create secure token"
  task :setup, roles: :app do
    run "mkdir -p #{shared_path}/config"
    token = SecureRandom.hex(64)
    run "echo #{token} > #{shared_path}/config/secret"
    chown_to_user "#{shared_path}/config/secret", 'git', 'deployer'
  end
  after "deploy:setup", "token:setup"
end
