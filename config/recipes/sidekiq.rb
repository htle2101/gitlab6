before "deploy:update_code", "sidekiq:quiet"
after "deploy:stop",    "sidekiq:stop"
after "deploy:start",   "sidekiq:start"
before "deploy:restart", "sidekiq:restart"

_cset(:sidekiq_cmd) { "#{fetch(:bundle_cmd, "bundle")} exec sidekiq" }
_cset(:sidekiqctl_cmd) { "#{fetch(:bundle_cmd, "bundle")} exec sidekiqctl" }
_cset(:sidekiq_pid)       { "#{current_path}/tmp/pids/sidekiq.pid" }
_cset(:sidekiq_timeout)   { 10 }

namespace :sidekiq do

  desc "Quiet sidekiq (stop accepting new work)"
  task :quiet do
    pid_file = fetch(:sidekiq_pid)
    run "if [ -d #{current_path} ] && [ -f #{pid_file} ] && kill -0 `cat #{pid_file}`> /dev/null 2>&1; then cd #{current_path} && #{fetch(:sidekiqctl_cmd)} quiet #{pid_file} ; else echo 'Sidekiq is not running'; fi"
  end

  desc "Stop sidekiq"
  task :stop do
    rails_env = fetch(:rails_env, "production")
    run "cd #{current_path} ; sudo -u git -H bundle exec rake sidekiq:stop RAILS_ENV=#{rails_env}", :pty => false
  end

  desc "Start sidekiq"
  task :start do
    rails_env = fetch(:rails_env, "production")
    run "cd #{current_path} ; sudo -u git -H bundle exec rake sidekiq:start RAILS_ENV=#{rails_env}", :pty => false
  end

  desc "Restart sidekiq"
  task :restart do
    stop
    start
  end

end
