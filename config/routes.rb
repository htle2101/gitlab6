require 'sidekiq/web'
require 'api/api'

Gitlab::Application.routes.draw do

  #
  # Search
  #
  get 'search' => "search#show"

  # API
  API::API.logger Rails.logger
  mount API::API => '/api'

  constraint = lambda { |request| request.env["warden"].authenticate? and request.env['warden'].user.admin? }
  constraints constraint do
    mount Sidekiq::Web, at: "/admin/sidekiq", as: :sidekiq
  end

  # Enable Grack support
  mount Grack::Bundle.new({
    git_path:     Gitlab.config.git.bin_path,
    project_root: Gitlab.config.gitlab_shell.repos_path,
    upload_pack:  Gitlab.config.gitlab_shell.upload_pack,
    receive_pack: Gitlab.config.gitlab_shell.receive_pack
  }), at: '/', constraints: lambda { |request| /[-\/\w\.]+\.git\//.match(request.path_info) }

  #
  # Help
  #
  get 'help'                => 'help#index'
  get 'help/api'            => 'help#api'
  get 'help/api/:category'  => 'help#api', as: 'help_api_file'
  get 'help/markdown'       => 'help#markdown'
  get 'help/permissions'    => 'help#permissions'
  get 'help/public_access'  => 'help#public_access'
  get 'help/raketasks'      => 'help#raketasks'
  get 'help/ssh'            => 'help#ssh'
  get 'help/system_hooks'   => 'help#system_hooks'
  get 'help/web_hooks'      => 'help#web_hooks'
  get 'help/workflow'       => 'help#workflow'
  get 'help/shortcuts'
  get 'help/online_job'     => 'help#online_job'
  get 'help/merge_request_check'     => 'help#merge_request_check'

  resource :build, controller: "build"

  resources :beta_deploy_logs, only: [:index] do
    put :vote, on: :member
    put :unvote, on: :member
  end

  #
  # Global snippets
  #
  resources :snippets do
    member do
      get "raw"
    end
  end
  get "/s/:username" => "snippets#user_index", as: :user_snippets, constraints: { username: /.*/ }

  #
  # Public namespace
  #
  namespace :public do
    resources :projects, only: [:index]
    resources :projects, constraints: { id: /[a-zA-Z.\/0-9_\-]+/ }, only: [:show]
    root to: "projects#index"
  end


  #
  # Attachments serving
  #
  get 'files/:type/:id/:filename' => 'files#download', constraints: { id: /\d+/, type: /[a-z]+/, filename:  /.+/ }

  #
  # Admin Area
  #
  namespace :admin do
    resources :users, constraints: { id: /[a-zA-Z.\/0-9_\-]+/ } do
      member do
        put :team_update
        put :block
        put :unblock
      end
    end

    resources :groups, constraints: { id: /[^\/]+/ } do
      member do
        put :project_teams_update
      end
    end

    resources :hooks, only: [:index, :create, :destroy] do
      get :test
    end

    resource :logs, only: [:show]
    resource :background_jobs, controller: 'background_jobs', only: [:show]
    resource :ci_builds, controller: 'ci_builds', only: [:show]

    resources :projects, constraints: { id: /[a-zA-Z.\/0-9_\-]+/ }, only: [:index, :show] do
      resources :members, only: [:destroy]
    end

    root to: "dashboard#index"
  end

  get "errors/githost"

  #
  # Profile Area
  #
  resource :profile, only: [:show, :update] do
    member do
      get :account
      get :history
      get :token
      get :design

      put :update_password
      put :reset_private_token
      put :update_username
    end

    scope module: :profiles do
      resource :notifications, only: [:show, :update]
      resource :password, only: [:new, :create]
      resources :keys
      resources :groups, only: [:index] do
        member do
          delete :leave
        end
      end
    end
  end

  match "/u/:username" => "users#show", as: :user, constraints: { username: /.*/ }
  match "/scripts/:id" => "scripts#show", constraints: { id: /\d+/ }
  #
  # Dashboard Area
  #
  resource :dashboard, controller: "dashboard", only: [:show] do
    member do
      get :projects
      get :issues
      get :merge_requests
      get :sonar
      get :grouping_strategies
      get :checkers
      get :artifacts
      get :swimlane
    end
  end

  resources :classifications, only: [:index] do

  end

  #
  # Groups Area
  #
  resources :groups, constraints: {id: /(?:[^.]|\.(?!atom$))+/, format: /atom/}  do
    member do
      get :databaseinfo
      get :issues
      get :merge_requests
      get :members
      get :tickets
      get :rollout_logs
      get :packages
      get :groups_for_package
      get :package_logs
      put :logs
      put :static_logs
    end
    resources :users_groups, only: [:create, :update, :destroy]
  end

  resources :projects, constraints: { id: /[^\/]+/ }, only: [:new, :create]

  match "groups/logs", :to => "groups#logs"
  match "deploy_to_maven/deploy", :to => "deploy_to_maven#deploy"
  match "deploy_to_maven/pre_deploy", :to => "deploy_to_maven#pre_deploy"
  match "deploy_to_maven/deploy_status", :to => "deploy_to_maven#deploy_status"


  devise_for :users, controllers: { omniauth_callbacks: :omniauth_callbacks, registrations: :registrations }

  #
  # Project Area
  #
  resources :projects, constraints: { id: /(?:[a-zA-Z.0-9_\-]+\/)?[a-zA-Z.0-9_\-]+/ }, except: [:new, :create, :index], path: "/" do
    member do
      put :transfer
      get :packages_for_ticket
      get :rollback
      get :rollback_logs
      post :fork
      get :autocomplete_sources
      get :assignees
    end

    scope module: :projects do
      resources :blob,    only: [:show], constraints: {id: /.+/}
      resources :raw,    only: [:show], constraints: {id: /.+/}
      resources :tree,    only: [:show], constraints: {id: /.+/, format: /(html|js)/ }
      resources :edit_tree,    only: [:show, :update], constraints: {id: /.+/}, path: 'edit'
      resources :commit,  only: [:show], constraints: {id: /[[:alnum:]]{6,40}/}
      resources :commits, only: [:show], constraints: {id: /(?:[^.]|\.(?!atom$))+/, format: /atom/}
      resources :compare, only: [:index, :create]
      resources :blame,   only: [:show], constraints: {id: /.+/}
      resources :network,   only: [:show], constraints: {id: /(?:[^.]|\.(?!json$))+/, format: /json/}
      resources :graphs, only: [:show], constraints: {id: /(?:[^.]|\.(?!json$))+/, format: /json/}
      match "/compare/:from...:to" => "compare#show", as: "compare", via: [:get, :post], constraints: {from: /.+/, to: /.+/}

        resources :snippets do
          member do
            get "raw"
          end
        end

      resources :wikis, only: [:show, :edit, :destroy, :create] do
        collection do
          get :pages
          put ':id' => 'wikis#update'
          get :git_access
        end
        member do
          get "history"
        end
      end

      resource :wall, only: [:show] do
        member do
          get 'notes'
        end
      end

      resource :repository, only: [:show] do
        member do
          get "stats"
          get "archive"
        end
      end

      resources :services, constraints: { id: /[^\/]+/ }, only: [:index, :edit, :update] do
        member do
          get :test
        end
      end

      resources :deploy_keys do
        member do
          put :enable
          put :disable
        end
      end

      resources :branches, constraints: { id: /[a-zA-Z.0-9\/_\-#%+]+/ }, only: [:index, :new, :create, :destroy] do
        collection do
          get :recent
        end
      end

      resources :tags, only: [:index, :new, :create, :destroy]
      resources :protected_branches, only: [:index, :create, :destroy]

      resources :refs, only: [] do
        collection do
          get "switch"
        end

        member do
          # tree viewer logs
          get "logs_tree", constraints: { id: /[a-zA-Z.\/0-9_\-#%+]+/ }
          get "logs_tree/:path" => "refs#logs_tree",
            as: :logs_file,
            constraints: {
              id:   /[a-zA-Z.0-9\/_\-#%+]+/,
              path: /.*/
            }
        end
      end


      resources :merge_requests, constraints: {id: /\d+/}, except: [:destroy] do
        member do
          get :diffs
          get :automerge
          get :automerge_check
          get :ci_status
        end

        collection do
          get :branch_from
          get :branch_to
          get :update_branches
          put :info_to_verify
          get :merge_inspect
        end
      end

      resources :hooks, only: [:index, :create, :destroy] do
        member do
          get :test
        end
      end

      resources :team, controller: 'team_members', only: [:index]
      resources :milestones, except: [:destroy]

      resources :labels, only: [:index] do
        collection do
          post :generate
        end
      end

      resources :issues, except: [:destroy] do
        collection do
          post  :bulk_update
        end
      end

      resources :tasks do
        member do
          put :update_status
          post :remove_relation
        end
        resources :task_configs, path: 'configs'
      end

      resources :paas_rollouts do
        member do
          put :deploy
          put :reset
          get :log
        end
      end

      resources :beta_paas_rollouts do
        member do
          put :deploy
          put :reset
          get :tags
          put :rollback
        end

        collection do
          get :get_status_from_paas
          get :log
        end

      end

      resources :tickets do
        collection do
          get :current
        end

        member do
          put :wars
          put :add_wars
          get :grouping_strategy
          get :logs
        end
        resources :grouping_strategies, path: "strategies", only: [] do
          member do
            put :reset
            put :deploy_by_group
            put :terminate
            get :pre_rollback
            get :info_to_verify
            get :group_operation_logs
          end
        end
      end

      resources :online_jobs do
        collection do
          get :jars
          get :tags_for_jar
          get :ips_for_business
          get :business_groups
        end
        member do
          get :item
        end
      end

      resources :rollback_packages do
        collection do
          get :rollback_confirm
          put :rollback
          post :rollback_package
          get :groups_for_package
          get :swimlanes_for_package
          put :add_rollback_package
          put :add_whole_group_package
          put :delete_rollback_package
        end
      end

      resources :static_tickets do
        member do
          put :packages
          put :add_packages
          get :grouping_strategy
          put :update_static_package
        end
      end

      resources :team_members, except: [:index, :edit] do
        collection do

          # Used for import team
          # from another project
          get :import
          post :apply_import
        end
      end

      resources :notes, only: [:index, :create, :destroy, :update] do
        member do
          delete :delete_attachment
        end

        collection do
          post :preview
        end
      end

      resources :ci_branch, constraints: { id: /[a-zA-Z.0-9\/_\-#%+]+/ }, only: [:show, :destroy, :update, :edit] do
        member do
          get :log, :shadowlog
          get :beta_logs
          get :beta_logs_for_module
          post :war, :change_branch_for_ci, :module_deploy_config_for_phoenix, :set_app_default_domain
        end

        resource :phoenix, :controller => "phoenix" do
          get :ci_branches, :modules, :all_deps
          resources :deps, :controller => "deps", only: [:index, :create, :destroy] do
            collection do
              post :bat_create
            end
          end
        end

        resource :lion, :controller => "lion" do
          get :products
          post :new_project
        end
      end

      post "/ci_branch/:id" => "ci_branch#create", as: "ci_branch_build"

      resources :virtual_machines, only: [:new, :create, :update, :index] do
        collection do
          get :apply_from_paas
          get :get_ips_from_paas
          get :apply, :machine_for_ip, :info
          put :unbind,:set_dn_for_ip,:edit_dn_for_ip,:delete_dn_for_ip,:borrow_long_term,:borrow_temporary,:release, :addslb, :saveswimlane
        end
        member do
          get :local_nginx, :jboss_log
          post :update_local_nginx
        end
      end

      resource :cache do
        collection do
          post :clear
        end
      end

      resources :rollout_branches, constraints: { id: /\d+/ } do
        member do
          get :item
          get :log
          get :note
          put :rebuild
          post :static_shrinkwrap_build
          post :clear_cache
          post :clear_ip_cache
          post :ticket
          post :add_package_to_clear_cache
          get :sign_off
        end
        collection do
          get :change_branch
          get :cache_ip
          post :static_rollback
          post :change_jdk_version
        end
      end

      #copy from rollout_branch
      resources :ppe_branches, constraints: { id: /\d+/ } do
        member do
          post :static_shrinkwrap_build
          post :clear_cache
          get :item, :log
          put :rebuild
          post :ticket
          post :add_package_to_clear_cache
        end
        get :change_branch, on: :collection
      end


      resources :performance_test_modules, constraints: { id: /\d+/ } do

        member do
          get :item
        end

        collection do
          get :tags_for_module_name
          put :add_test_sample
          put :edit_test_sample
          put :auto_create_domain
          get :detail_config
          put :add_dependency_performance_test
          put :create_total_performance_tests
        end

      end

      resources :performance_tests do
        member do
          get :item
        end

        collection do
          get :tags_for_module_name
          put :add_test_sample
          put :edit_test_sample
          put :auto_create_domain
          get :detail_config
          put :add_dependency_performance_test_module
          put :create_total_performance_tests
          get :organize_network
          put :run_test
        end

      end


      resources :light_merges, constraints: {id: /\d+/} do
        member do
          get :automerge
          get :automerge_check
        end
      end

      resources :scripts, except: [:destroy] do
        member do
          put :close, :execute
          get :download
        end
      end

      resources :rollout_logs, only: [:index]

      resources :checkers do
        collection do
          put :check_result
        end
      end
    end
  end

  root to: "dashboard#show"
end
