server '192.168.9.139', :app, :web, :db, :primary => true
set :branch, ENV["BRANCH"] || "dev"
set :rails_env, "production"
set_default(:gitlab_host) { "192.168.9.139" }
set_default(:gitlab_email_from) { "gitlab@localhost" }
set_default(:gitlab_support_email) { "gitlab@localhost" }

set_default(:unicorn_workers, 4)

set_default(:tire_host) { "http://localhost:9200/" }
