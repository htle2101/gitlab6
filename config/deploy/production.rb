server '192.168.7.108', :app, :web, :db, :primary => true
server '192.168.211.11', :app, :web, :db, :primary => true
set :branch, ENV["BRANCH"] || "rc"
set :rails_env, "production"
set_default(:gitlab_host) { "code.dianpingoa.com" }
set_default(:gitlab_email_from) { "gitlab@dianpingoa.com" }
set_default(:gitlab_support_email) { "gitlab@dianpingoa.com" }

set_default(:unicorn_workers, 8)
set :domain, "code.dianpingoa.com"

set_default(:tire_host) { "http://192.168.218.14:9200/" }
