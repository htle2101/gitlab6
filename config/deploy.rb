#require 'sidekiq/capistrano'
load "config/recipes/base"
load "config/recipes/gitlab"
load "config/recipes/database"
load "config/recipes/unicorn"
load "config/recipes/nginx"
load "config/recipes/sidekiq"
load "config/recipes/redis"
load "config/recipes/token"
load "config/recipes/faye"

set :stages, %w(production staging)
set :default_stage, "staging"
require 'capistrano/ext/multistage'

set :git_server,     'code.dianpingoa.com'
set :db_adapter,     'mysql' # or postgres
set :mount_point,    '/'
set :group,          'ezc'
set :application,    'gitlab'
set :user,           'deployer'
set :rails_env,      'production'
set :deploy_to,      "/var/apps/#{application}"
set :bundle_without, %w[development test] + (%w[mysql postgres] - [db_adapter])
set :asset_env,      "RAILS_GROUPS=assets RAILS_RELATIVE_URL_ROOT=#{mount_point.sub /\/+\Z/, ''}"
set :shared_children, fetch(:shared_children) - ["log"]
set :log_path,       "/data/log"

set :use_sudo, true
default_run_options[:pty] = true

# Or: `accurev`, `bzr`, `cvs`, `darcs`, `git`, `mercurial`, `perforce`, `subversion` or `none`
set :scm,        :git
set :repository, "git@#{git_server}:#{group}/#{application}.git"
set :deploy_via, :remote_cache

# Alternatively, you can deploy via copy, if you don't have gitlab in git
#set :scm, :none
#set :repository, '.'
#set :deploy_via, :copy

set(:whenever_command) { "RAILS_ENV=#{rails_env} bundle exec whenever" }
set :whenever_environment, defer { stage }
require "whenever/capistrano"

#server domain, :app, :web, :db, primary: true
#
namespace :foreman do
  desc 'Export the Procfile to Ubuntu upstart scripts'
  task :export, roles: :app do
    foreman_export = "foreman export upstart /etc/init -f Procfile -a #{application} -u #{user} -l #{shared_path}/log/foreman"
    run "cd #{release_path} && #{sudo} #{fetch :bundle_cmd, 'bundle'} exec #{foreman_export}"
  end

  desc 'Start the application services'
  task :start, roles: :app do
    run "#{sudo} service #{application} start"
  end

  desc 'Stop the application services'
  task :stop, roles: :app do
    run "#{sudo} service #{application} stop"
  end

  desc 'Restart the application services'
  task :restart, roles: :app do
    run "#{sudo} service #{application} restart"
  end
end

namespace :deploy do
  desc 'Start the application services'
  task :start, roles: :app do
    run "#{sudo} service #{application} start"
  end

  desc 'Stop the application services'
  task :stop, roles: :app do
    run "#{sudo} service #{application} stop"
  end

  desc 'Restart the application services'
  task :restart, roles: :app do
    run "#{sudo} service #{application} restart"
  end

  desc 'Reload the application services'
  task :reload, roles: :app do
    run "#{sudo} service #{application} reload"
  end

  task :setup, :except => { :no_release => true } do
    dirs = [deploy_to, releases_path, shared_path]
    dirs += shared_children.map { |d| File.join(shared_path, d.split('/').last) }
    run "mkdir -p #{dirs.join(' ')}"
    run "#{try_sudo} chmod g+w #{dirs.join(' ')}" if fetch(:group_writable, true)
  end

  desc "create symlink configuration file"
  task :symlink_shared, :on_no_matching_servers => :continue do
    run "ln -nfs #{shared_path}/bundle #{release_path}/vendor/bundle"
    run "ln -nfs #{shared_path}/config/database.yml #{release_path}/config/database.yml"
    run "ln -nfs #{shared_path}/config/gitlab.yml #{release_path}/config/gitlab.yml"
    run "ln -nfs #{shared_path}/config/resque.yml #{release_path}/config/resque.yml"
    run "ln -nfs #{shared_path}/config/unicorn.rb #{release_path}/config/unicorn.rb"
    run "ln -nfs #{shared_path}/config/secret #{release_path}/.secret"
    run "ln -nfs /data/scripts #{release_path}/public/scripts"
    run "ln -nfs #{log_path} #{release_path}/log"
  end

  task :chown_for_release do
    chown_to_user(release_path, "git", "deployer", :recursive => true)
  end

  task :chown_for_shared do
    chown_to_user("#{shared_path}/config", "git", "deployer", :recursive => true)
    # chown_to_user(log_path, "git", "deployer", :recursive => true)
    chown_to_user("#{shared_path}/pids", "git", "deployer", :recursive => true)
    chown_to_user("#{shared_path}/system", "git", "deployer", :recursive => true)
  end
end

after 'deploy:cold' do
  run "cd #{release_path} && #{rake} gitlab:setup force=yes RAILS_ENV=#{rails_env}"
  deploy.restart
end

after "deploy:setup", "deploy:chown_for_shared"
#after 'deploy:update', 'foreman:export'  # Export foreman scripts
#after 'deploy:update', 'deploy:restart'
after "deploy:update", "deploy:chown_for_shared"
after "deploy:update", "deploy:chown_for_release"
before "bundle:install", "deploy:symlink_shared"
after "bundle:install", "deploy:migrate"
#after 'deploy:update', 'foreman:restart' # Restart application scripts
