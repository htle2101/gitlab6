# Use this file to easily define all of your cron jobs.
#
# It's helpful, but not entirely necessary to understand cron before proceeding.
# http://en.wikipedia.org/wiki/Cron

# Example:
#
# set :output, "/path/to/my/cron_log.log"
#
# every 2.hours do
#   command "/usr/bin/some_great_command"
#   runner "MyModel.some_method"
#   rake "some:great:rake:task"
# end
#
# every 4.days do
#   runner "AnotherModel.prune_old_records"
# end

# Learn more: http://github.com/javan/whenever
#every 1.days, :at => '5:25 pm' do
set :environment, ENV['RAILS_ENV']

ENV.each do |k, v|
  env k.to_sym, v
end

every 1.day,  :at => '06:30 pm' do
  rake "gitlab:machines:gs_statistic"
end

every 7.day,  :at => '06:30 pm' do
  rake "gitlab:machines:gs_statistic_by_group"
end

every 7.day,  :at => '05:30 pm' do
  rake "gitlab:machines:remove_surplus_release"
end

every 1.day,  :at => '09:30 pm' do
  rake "gitlab:machines:notify_in_advance_releasing_machines"
end

every 1.day, :at => '10:00 pm' do
  rake "gitlab:machines:release_inactive_machines"
end

every 1.day, :at => '6:00 am' do
  rake "gitlab:databases:update"
end

every 1.day, :at => '00:30 pm' do
  rake "gitlab:ftp:clean_packages"
end

every 5.minutes do
  runner "RemoteCi::BetaJobMonitor.new.execute"
end
