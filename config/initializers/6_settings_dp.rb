#
# xml parser
#
ActiveSupport::XmlMini.backend = 'Nokogiri'


#
# ci
#
Settings['dp'] ||= Settingslogic.new({})
Settings.dp['ssh_user'] = 'root'
Settings.dp['ssh_password'] = ['12qwaszx', '!@qwaszx']
Settings.dp['ssh_port'] = 58422
Settings.dp['sudo_account'] = 'dianping'
Settings.dp['snapshots_url']="http://mvn.dianpingoa.com/dianping-snapshots"
Settings.dp['releases_url']="http://mvn.dianpingoa.com/dianping-releases"
Settings.dp['ssh_path_prefix'] ||= "ssh://#{Settings.gitlab_shell.ssh_user}@#{Settings.gitlab_shell.ssh_host}:#{Settings.gitlab_shell.ssh_port}/"
Settings.dp['superadmin'] ||= 'scm.dp'
Settings.dp['dev_web_service_port'] ='8080'
Settings.dp['releases_ip'] ='192.168.7.7'
Settings.dp['ci_production'] ||= 'product'
Settings.dp['need_shadow'] ||= false
Settings.dp['need_sonar'] ||= true

#ci for merge_request
# Settings.dp['merge_request_ci_url'] ||= "http://192.168.7.228:8080/job/"
# Settings.dp['merge_request_delete_job_url'] ||= "http://192.168.7.228:8080/merge_request"

#
# op
#
Settings['op'] ||= Settingslogic.new({})
Settings.op['cmdb_url'] ||= 'cmdb.dp'
Settings.op['lzm_ip'] ||= '192.168.211.113'
Settings.op['lzm_port'] ||= '80'
Settings.op['lzm_url'] ||= "#{Settings.op['lzm_ip']}:#{Settings.op['lzm_port']}"
Settings.op['ssh_user'] ||= 'uploaduser'
Settings.op['ssh_host'] ||= '192.168.211.14'
Settings.op['dpdns_ip'] ||= '192.168.211.116'
Settings.op['dpdns_port'] ||= '8080'

#
# beta paas url
#
Settings.op['beta_paas_ip'] ||= '192.168.222.96'
Settings.op['beta_paas_port'] ||= '8080'
Settings.op['beta_paas_url'] = "http://192.168.222.96:8080/"
# Settings.op['beta_paas_url'] ||= "http://#{Settings.op['beta_paas_ip']}:#{Settings.op['beta_paas_port']}/"
# Settings.op['beta_paas_url'] = "http://localhost:2667/"

#
# op_prod
#
Settings['op_prod'] ||= Settingslogic.new({})
# Settings.op_prod['cmdb_url'] ||= Settings.op['cmdb_url']
# Settings.op_prod['lzm_url'] ||= Settings.op['lzm_url']
Settings.op_prod['cmdb_url'] = '10.1.1.129'
Settings.op_prod['lzm_url']  = '10.1.1.129'

Settings.op_prod['cache_url'] ||= 'product.cache.dp:8080'
Settings.op_prod['cache_url_pre'] ||= 'ppe.cache.dp:8080'

Settings.op_prod['workflow_url'] ||= 'workflow.dp'
#
# sonar
#
Settings['sonar'] ||= Settingslogic.new({})
Settings.sonar['sonar_url'] ||= '192.168.5.142'
Settings.sonar['namespace_statistics_period'] ||= 30.days

#
# ftp
#
Settings['ftp'] ||= Settingslogic.new({})
# Settings.ftp['url'] ||= '192.168.7.87'
Settings.ftp['url'] ||= '10.1.1.81'
Settings.ftp['username'] ||= 'qaupload'
Settings.ftp['password'] ||= 'uploadwar'
Settings.ftp['tmp_path'] ||= '/data/ftp'
Settings.ftp['root_path'] ||= ''
# Settings.ftp['username'] ||= 'root'
# Settings.ftp['password'] ||= '12qwaszx'
# Settings.ftp['tmp_path'] ||= ''
# Settings.ftp['root_path'] ||= ''


#
# lion
#
Settings.op['lion_host'] ||= 'lionapi.dp'
Settings.op['lion_port'] ||= '8080'

#
# taurus
#
Settings['taurus'] ||= Settingslogic.new({})
Settings.taurus['product_url'] ||= 'http://taurus.dp:8192/api/deploy'
Settings.taurus['ppe_url'] ||= 'http://ppe.taurus.dp:8192/api/deploy'
Settings.taurus['beta_url'] ||= 'http://beta.taurus.dp:8192/api/deploy'
Settings.taurus['alpha_url'] ||= 'http://alpha.taurus.dp:8192/api/deploy'
# Settings.taurus['test_host'] ||= '192.168.218.148'
# Settings.taurus['test_port'] ||= '8192'
Settings.taurus['alpha'] ||= '192.168.218.35'
Settings.taurus['beta'] ||= '192.168.222.81'
Settings.taurus['ppe'] ||= 'ppe.taurus.dp'
Settings.taurus['offline_port'] ||= '8192'
Settings.taurus['online_host'] ||= 'taurus.dp'
Settings.taurus['online_port'] ||= '8192'

#
# db scripts
#
Settings['scripts'] ||= Settingslogic.new({})
Settings.scripts['download_relative_path'] ||= 'scripts/db'
Settings.scripts['db_path'] ||= "/data/#{Settings.scripts['download_relative_path']}"
Settings.scripts['tools.dba'] ||= 'tools.dba.dp'

#
# faye
#
Settings['faye'] ||= Settingslogic.new({})


#
# paas
#
Settings['paas'] ||= Settingslogic.new({})
Settings.paas['open']||= true
Settings.paas['url'] ||= "http://192.168.222.96:8080/"
# Settings.paas['url'] ||= "http://localhost:2667/"
DianpingPaas::Client.base_uri(Settings.paas['url']) unless Settings.paas['url'].nil?



#
# cmdb_machine
#
Settings['cmdb_machine'] ||= "http://api.cmdb.dp/api/v0.1/projects/s"

#
# slb
#
Settings["slb"] ||= Settingslogic.new({})
Settings.slb['url'] ||= "slb.alpha.dp"

#
# nodejs
#
Settings["nodejs"] ||= Settingslogic.new({})
Settings.nodejs['url'] ||= "10.1.112.175:8080"
Settings.nodejs['token'] ||= "0d01646bba32b74cd15e02e11f1ff5be"


#
# beehome
#
Settings["beehome"] ||= Settingslogic.new({})
Settings.beehome['url'] ||= "192.168.9.87:3000"
# #
# # ci_env
# #
# Settings.ci_env['production'] ||= 'product'

#
# tire
#
Tire.configure do
  url Gitlab.config.tire['url'] || 'http://192.168.9.139:9200/'
  logger 'log/elasticsearch.log'
end


Logbin.configure do
  storage :elasticsearch, url: Gitlab.config.tire['url'], prefix: Gitlab.config.logbin['prefix']
  backup "log/logbin-backup.log"
end
