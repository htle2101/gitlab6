module Rye
  class Box
    def sudo_exec(*args)
      execute("echo #{@rye_opts[:password]} | sudo -S", args)
    end
  end
end
